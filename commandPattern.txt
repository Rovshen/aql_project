//http://www.journaldev.com/1624/command-design-pattern-in-java-example-tutorial
command pattern estimator: Table, Graph, Unstructured (obj)

//RECEIVER class that does the work - Command pattern
interface Estimator{
// action
// perform estimation based on operand type
 public void estimate(Operand operand, CoundDownLatch doneSingnal);
}

class GraphEstimator implements Estimator{
	@override
	estimate(Operand operand, CoundDownLatch doneSingnal){
		// perform estimation for the graph
		// doneSingnal decrement
	}
}

class TableEstimator implements Estimator{
	@override
	estimate(Operand operand, CoundDownLatch doneSingnal){
		// perform estimation for the graph
		// doneSingnal decrement
	}
}

//COMMAND class that does the work - Command pattern
// if any default implementation use abstract class

interface Command{
	public void execute(Operand op, doneSingnal);
}

// need to create number of command implementations that depends on the number of actions in receiver
// each command implementation forwards the request to the appropriate method of receiver

class EstimateCommand implements Command, Runnable{

	Estimator estimator;
	
	public EstimateCommand(Estimator estimator){
		this.estimator = estimator;
	}
	
	@Override
	public void run(){				
	}
	
	//estimate command is forwarding request to estimate method
	@Override
	public void execute(Operand op, doneSingnal){
		this.estimator.estimate(op, doneSingnal);
	}
}

// INVOKER class encapsulate the command and pass the request to it - Command pattern
class EstimateInvoker{	
	private CoundDownLatch doneSignal;
	private execServ;
	
	public EstimateInvoker(){		
	}
	
	public void execute(EstimateCommand estCom, Operand op){
	
		// run this command
		execServ.submit(estCom);
		estCom.execute(op, doneSignal)
	}
	
	public boolean isDone(){
		doneSignal.await();
	}
}

// Factory pattern to return appropriate type
class EstimatorFactory{
	public static Estimator getEstimatorBasedOnOperand(Operand o){
		Estimator estimator;
		switch(o.getImplType){
			//based on case return appropriate estimator
			Graph: return GraphEstimator
			Table: return TableEstimator
			Unstructured: return TableEstimator
		}
		return estimator;
	}
}


//for every method in receiver there will be a command implementation. 
//It works as a bridge between receiver and action methods
public static void main(String[] args){
	Operand o = new GraphOperand();
	
	//Creating the receiver object
	Estimator est1 = EstimatorFactory.getEstimatorBasedOnOperand(o);
	Estimator est2 = EstimatorFactory.getEstimatorBasedOnOperand(o);
	Estimator est3 = EstimatorFactory.getEstimatorBasedOnOperand(o);
	
	//creating command and associating with receiver
	EstimateCommand estComm = new EstimateCommand(est);
	
	//Creating invoker and associating with Command
	// may be more than one invoker for the same command
	EstimateInvoker estInv = new EstimateInvoker(estComm);
	
	//perform action on invoker object
	estInv.execute(o, doneSignal);
	
}












