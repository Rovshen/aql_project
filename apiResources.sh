#!/bin/bash
#Resource Measurement API
#size units are in mega measurement not Gyga
getAvailMem(){
	echo 1000
}

getTotalMem(){
	echo 4000
}

getCPUProcessPower(){
	echo 2400
}
# number of CPUs should be parametarized based on the workload and system resources:
getNumbCPU(){
	echo 2
}

getAvailDiskSpace(){
	echo 1000
}

getGraphSize(){
	echo 1000
}
