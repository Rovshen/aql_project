/*
 * RPC server implementation.
 * good tutorials http://www.cs.rutgers.edu/~pxk/rutgers/notes/rpc/step2.html
 */
#include "querymanagerrpc_server.h"

queryid_res* getqueryid_1_svc(request *req, struct svc_req *rqstp)
{
	int time_interval_milisec = req->sleep_millisec; //defines interval for next time based id
        int queryIdSize = MAXLEN;
	int fd; /* named pipe file descriptor */
	char fmt[queryIdSize];
	char tmp[queryIdSize];
	static queryid_res res;
	struct timeval  tv;
	struct tm       *time_str;	

	printf("server: RPC request received from %d \n", req->pid);
	/*get current time*/
	gettimeofday(&tv, NULL);
	if((time_str = localtime(&tv.tv_sec)) == NULL)
	{
		res.errno = errno;
		syslog(LOG_ERR, "can not get system time");
                return (&res);
	}
	/* Generate time based ID with milliseconds. */
	strftime(fmt, sizeof fmt, "%y_%m_%d_%H_%M_%S_%%06u;", time_str);
	snprintf(tmp, sizeof tmp, fmt, tv.tv_usec);
	res.queryid_res_u.response.data = tmp;
	res.queryid_res_u.response.bytes = sizeof fmt;

	/* Open named pipe to Transaction Manager */
	open_pipe();
	fd = open(AQL_NAME_PIPE, O_WRONLY);
        if(fd < 0){
                syslog(LOG_ERR, "can not open named pipe, %s", strerror(errno));
		res.errno = errno; /*failed to write to the pipe*/
                return (&res);
        }
        if (write(fd, tmp, strlen(tmp)) < 0){ /*write to named pipe*/
                res.errno = errno; /*failed to write to the pipe*/
		close(fd);
                return (&res);
        }
        close(fd);
		
	if(sleepForTime(0, time_interval_milisec) < 0){ /* sleep for 0 sec and millisec */
		res.errno = errno; /*failed to wait*/
                return (&res);
	}

	/*Return result*/
	res.errno = 0;
	/* write result to the Transaction Manager via named pipe */
	
	return (&res);
}
