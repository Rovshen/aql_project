/*Remote Query ID generating protocol - RPC for Query Manager for getting Query ID
* to run RPC server make sure to have rpcinfo running.
*/

/*QueryID max size*/
const MAXLEN = 10240;

/*
 * Type for storing QueryID.
 */
typedef string buff<MAXLEN>;

/*
 * Response sent by the server to the client as a response
 * to remote procedure call, containing the QueryID for
 * the call and number of bytes in the data.
 * Define identifier
 */
struct server_response {
    buff data;
    int bytes;
};

/*
 * Type that represents the structure for QueryID
 * to be received from the server.
 * Define type alias in the global name space to
 * use just server_response as a type.
 */
typedef struct server_response server_response;

/*
 * union for returning from remote procedure call, returns
 * the proper QueryID server response if everything worked ok
 * or will return the error number if an error occured.
 */
union queryid_res switch (int errno) {
    case 0:
       server_response response;
    default:
        void;
};

/*
* Structure for sending request. Expects
* Request process id and time to sleep in milliseconds.
*/
struct request{
	int pid;
	int sleep_millisec;
};

/*Type that represents the structure for the request*/
typedef struct request request;

/*compile rpcgen querymanagerrpc.x*/
program QUERYMANAGERQIRP {
	version GETQUERYIDVERS{
		/*in RPC Lang string is null terminated array of chars*/
		queryid_res GETQUERYID(request *) = 1; /*procedure number*/
	} = 1; /*version 1*/
} = 0x20000001; /*program number*/
