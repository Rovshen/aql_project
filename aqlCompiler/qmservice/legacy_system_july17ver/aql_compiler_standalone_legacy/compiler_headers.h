#ifndef COMPILER_HEADERS_H_INCLUDED
#define COMPILER_HEADERS_H_INCLUDED

//include standard c libraries
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <syslog.h>
//include c libraries for pipe generation
//and processing
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
//for systemwide error reporting
#include <errno.h>

#include "helper_functions.h"
#include "cclientqm.h"

/*set numbering for commands and lines in parser*/
//start at 1 for the commands numbering
int execNumb=1;
//start at 1 for in query numbering
int numb=1;
/*creat file*/
FILE *file;
/*create named pipe*/
int namedPipe;
char * tempPipe;
/*create stream to write to pipe*/
FILE *stream;
/*create buffer for the stream*/
char *buf;
/*len will hold length of the buffer*/
size_t len;
/*get line number from flex based tokenizer*/
extern yylineno; //from flex file
#endif
