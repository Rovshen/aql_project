/*
*
*Flex lexical analyzer for AQL - Analytical Querty Language.
*Designed by Rovshen Nazarov. All rights reserved.
*
*The tokenizer reads an input stream or file and chunks it
*into a series of tokens, which are passed to the parser.
*/
/*noyywrap tells flex to read only one input file*/
%option noyywrap nodefault yylineno case-insensitive
%{
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include "yaccGramma.tab.h" /*generated by yacc Bison*/

/*this function called on error*/
void yyerror(char *s);
int oldstate;
int lineno;
%}

%Option yylineno
i%Option noyywrap 

%%
	/*reserved words*/
FROM		  {return L_FROM; }
UNION		  {return L_UNION; }
INTER	 	  {return L_INTER; }
DIFF 		  {return L_DIFF; }
LOAD		  {return L_LOAD; }
exit		  { return L_EXIT; }
	/*operators. return operators as is*/
[();]	{ return yytext[0]; }
	/*match identifier*/
[/A-Za-z][/A-Za-z0-9_\.]*	{yylval.strval = strdup(yytext); return L_GRAPH_ID ;}

	/*comments*/
#.*	;
	/*all other characters*/
\n	lineno++;
[ \t\r]		;	/*ignore whitespace*/
.	{yyerror("unrecognized character");}
%%

void yyerror(char *s)
{
        printf("%d: %s at %s\n", lineno, s, yytext);
}
