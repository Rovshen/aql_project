/*
* Server for handling client requests.
* The only request handled at the moment is Query ID request.
*/

//standard libraries
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
//system libraries
#include <sys/resource.h>
#include <sys/stat.h>
#include <sys/types.h>
#include<sys/syslog.h> //error logging systemwide in /var/log/syslog
//other libraries
#include <unistd.h>
#include <fcntl.h>//file control
#include <errno.h>
#include <pthread.h>
//networking libraries
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <time.h> //for time based id
#include <sys/time.h>

/*input 
* millisec- length of time to sleep, in miliseconds
* sec - time to sleep in sec
* Return status 0 ok, -1 error.
*/
int
sleepForTime(int sec, int milisec);

/*
* Create server and accept client connections.
*  
* Inputs: server port to bind on, file descriptor to close connection later,
* Query ID generation time interval in milliseconds. 
* Return 0 on success and -1 on failure.
*/
int  start_server(int server_port, int time_interv, int client_queue_size, char *tempPipe);

/*
* Handle new connection.
* Create new Named pipe with generated QueryID.
* Send generated QueryID to Transaction Manager.
* Uses Time for QueryID.
* Return 0 on success and -1 on failure.
*/
int handle_connection(int server_port, int listen_file_decr_in, char *tempPipe, int time_interval);
