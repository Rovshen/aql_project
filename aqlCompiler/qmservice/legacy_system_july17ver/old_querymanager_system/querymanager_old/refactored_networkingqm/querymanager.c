#include "querymanager_globals.h" /*include globals*/
#include "querymanager.h" 

/*
* Driver function for Query Manager daemon.
*/
int main(int argc, char *argv[])
{
	/*default named pipe for Query Manager*/
	char * tempPipe = "/tmp/aqlquerymanager_namedpipe";
	int		err;
	/*signal control thread, such as Kill, etc*/
	pthread_t	tid;
	/* struct to save parameters for signal control thread */
	struct data_handlers_parms tid_args;
	char		*cmd;
	struct sigaction sa;

	if ((cmd = strrchr(argv[0], '/')) == NULL){
		cmd = argv[0];
	} else {
		cmd++;
	}
	/*make the process daemon/service*/
	startquerymanager(cmd);
	/*Ensure that only one instance of the service is running.*/
	int stat = already_running();
	if (stat == 1){
		syslog(LOG_ERR,"AQL Query Manager service already running");
		exit(0);
	}
	/*Restore SIGHUP and block all signals*/
        sa.sa_handler = SIG_DFL;
        sigemptyset(&sa.sa_mask);
        sa.sa_flags = 0;
        if (sigaction (SIGHUP, &sa, NULL) < 0){
                syslog(LOG_ERR,"can not restore SIGHUP default.");
                error_quit();//global
        }
        sigfillset(&mask);
        if ((err = pthread_sigmask(SIG_BLOCK, &mask, NULL)) != 0){
                syslog(LOG_ERR, "SIG_BLOCK error. %d", err);
                error_quit();//global
        }
	/*create FIFO pipe with pathname for Query Manager*/
        mkfifo(tempPipe, 0664); //mode rw rw r
	/*
	* Create a thread to handle SIGHUP and SIGTERM signals 
	* from the user to this process
	*/
	tid_args.temp_pipe_in = tempPipe;
	err = pthread_create(&tid, NULL, thr_fn, &tid_args);
	if (err != 0){
		syslog(LOG_ERR, "can not create thread %d", err);
		error_quit(); //global function
	}
	//syslog(LOG_INFO, "I am back in main line 66");
	int server_port = 5005;
	int time_interval = 100; /*wait 100 milliseconds*/
	int client_queue_size = 10; /*number of clients in the connection queue*/

	if(start_server(server_port, time_interval, client_queue_size, tempPipe) < 0){
		error_quit(); //global function
	}
	/*exit query manager on success*/
	syslog(LOG_INFO, "Exiting Query Manager Daemon.");
	closelog();
	/* 
	* Wait for signal thread to finish as it access local memory in
	* this thread. Do not check join result.
	*/
	pthread_join(tid, NULL);
	exit(0);
}
