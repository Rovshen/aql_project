//standard libraries
#include <stdlib.h> //for exit
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
//system libraries
#include <syslog.h>
#include <sys/resource.h>
#include <sys/stat.h>
#include <sys/types.h>
//other libraries
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <pthread.h>
#include<signal.h>

#define LOCKFILE "/tmp/aqlquerymanager.pid" /*file to lock to ensure only one daemon is running*/
#define LOCKMODE (S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH)

/*
* List of what a daemonize does:
*   -Fork to create a child, and exit the parent process.
*   -Change the umask so that not to rely on the one set in the parent.
*   -Open logs to write to in case of an error.
*   -Create a new session id and detach from the current session.
*   -Change the working directory to root directory that won’t get unmounted.
*   -Close STDIN, STDOUT and STDERR.
*/

/*
* Struct for signal thread thr_fn to pass
* parameters to the function.
*/

struct data_handlers_parms
{	
	/*
	* Name of the named pipe file
	* used for passing data between c and java
	*/
	char *temp_pipe_in;
};

/*
* Created based on the daemon design described in the book by Stevens
* "Advanced Programming in the UNIX® Environment, Second Edition".
* Rovshen Nazarov. Baylor University. June 2014. All Rights Reserved.
*/

sigset_t mask;

/*
* Check if the daemon already running by using file lock and 
* writing pid into that file.
* Return 0 on success and 1 if daemon is already running.
* Exit program on failure.
*/
int already_running(void);

/*
 Reread service configuration on restart.
*/
void reread(void);

/*
 Get and process user signals to this service/daemon.
 supported signals are reload, stop.
*/
void *thr_fn(void *arg);

/*
* Daemonize the process. By forking it from parent.
* To detach it from any terminal.
*/
void startquerymanager(const char *cmd);

/*
* Create a Read Write lock for a given file descriptor.
* Input file descripter
* Return -1 on error and 0 on success. 
* Return 1 if file already locked.
*/
int lockfile(int fd);
