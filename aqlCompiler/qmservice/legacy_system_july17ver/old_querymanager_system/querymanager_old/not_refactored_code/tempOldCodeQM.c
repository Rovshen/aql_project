#include "apue.2e/include/apue.h"
//standard libraries
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
//system libraries
#include <syslog.h>
#include <sys/resource.h>
#include <sys/stat.h>
#include <sys/types.h>
//other libraries
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <pthread.h>
//networking libraries
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <time.h> 

#define LOCKFILE "/tmp/aqlquerymanager.pid" /*file to lock to ensure only one daemon is running*/
#define LOCKMODE (S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH)

/*
* Created based on the daemon design described in the book by Stevens
* "Advanced Programming in the UNIX® Environment, Second Edition".
* Rovshen Nazarov. Baylor University. June 2014. All Rights Reserved.
*/

sigset_t mask;
static int run_status = 1; // server connection status
int listen_file_decr; //server socket
 /*create named pipe*/
int namedPipe;
char * tempPipe = "/tmp/aqlquerymanager_namedpipe";
// mutex for thread safety
pthread_mutex_t count_mutex     = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t condition_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t  condition_cond  = PTHREAD_COND_INITIALIZER;

/*
* Exit service on error.
*/
void error_quit(void)
{
	if (remove(LOCKFILE) != 0)
		syslog(LOG_ERR, "could not remove the file %s", LOCKFILE);
	closelog();
        exit(1);
}

/*
* Create a Read Write lock for a given file descriptor.
*/
int lockfile(int fd)
{
	struct flock file_lock;
	memset(&file_lock, 0, sizeof(file_lock)); /*init flock structure*/
	file_lock.l_type = F_WRLCK; /*lock we want to place*/
	file_lock.l_whence = SEEK_SET; /*lock based on range below*/
	file_lock.l_start = 0; /*start position in the file*/
	file_lock.l_len = 0; /*lock the whole file regardless its size*/
	/*check if the file locked*/
	fcntl(fd, F_GETLK, &file_lock);
	/*check status if we can lock the file*/
	if(file_lock.l_type == F_UNLCK){
		file_lock.l_type = F_WRLCK; /*lock file for read write*/
		if (fcntl(fd, F_SETLK, &file_lock) < 0){
                	if (errno == EACCES || errno == EAGAIN){ /*if file locked already*/
                        	return(1);/*an instance of daemon already running*/
               		}
                	return(-1); /*could not lock the file*/
        	}
		return(0); /*lock created ok*/
	}	
}

/*
 Check if the daemon already running by using file lock and 
 writing pid into that file.
*/
int already_running(void)
{
	int file_descriptor, lock_stat;
	char buff[16];
	/* open or create the file if it does not exist*/
	file_descriptor = open(LOCKFILE, O_RDWR | O_CREAT, LOCKMODE);
	if (file_descriptor < 0){
		syslog(LOG_ERR,"can not create %s: %s", LOCKFILE, strerror(errno));
		error_quit();
	}
	/*lock file to ensure single service is running at a time*/
	lock_stat = lockfile(file_descriptor);
	if (lock_stat == 1){
		//close(file_descriptor);
                return(1);/*an instance of daemon already running*/
	} else if (lock_stat == -1){
		syslog(LOG_ERR,"can not lock file %s", LOCKFILE);
		error_quit();
	}
	//lock stat = 0 at this point
	ftruncate(file_descriptor, 0); /*remove any old data from the lockfile*/
	sprintf(buff, "%ld", (long)getpid());
	write(file_descriptor, buff, strlen(buff)+1);
	return(0);
}

/*
 Reread service configuration on restart.
*/
void reread(void)
{
	//read configuration updates
}

/*
 Get and process user signals to this service/daemon.
 supported signals are reload, stop.
*/
void *thr_fn(void *arg)
{
	int err, signo;

	for(;;){
		err = sigwait(&mask, &signo);
		if (err != 0){
			syslog(LOG_ERR,"sigwait failed");
			error_quit();
		}

		switch (signo){
		case SIGHUP:
			syslog(LOG_INFO, "Reloading configuration file");
			reread(); break;
		case SIGTERM:
			syslog(LOG_INFO, "Got exit signal. Stopping daemon.");
                        close(listen_file_decr);
			break;
		default:
			syslog(LOG_INFO, "Unexpected signal %d\n", signo);
			break;
		}
	}
	return(0);
}

void startquerymanager(const char *cmd)
{
	int			i, fd0, fd1, fd2;
	pid_t			pid, sid_new;
	struct rlimit		rl;
	struct sigaction	sa;

	/*Clear file creation mask.*/
	umask(0);
	/*Get max number of file descriptors.*/
	if (getrlimit(RLIMIT_NOFILE, &rl) < 0){
		syslog(LOG_ERR,"%s: can not get file limit.", cmd);
		error_quit();
	}
	/*Loose controlling TTY by cloning the process*/
	if ((pid = fork()) < 0){
		syslog(LOG_ERR,"%s: can not fork.", cmd);
		error_quit();
	}else if (pid != 0){
		exit(0); /*exit parent*/
	}
	sid_new = setsid(); /*create new session*/
	if (sid_new < 0){
		syslog(LOG_ERR,"cat not get new session.");
		error_quit();
	}
	/*Prevent future opens from allocating cotrolling TTY to this daemon*/
	sa.sa_handler = SIG_IGN;
	sigemptyset(&sa.sa_mask);
	sa.sa_flags = 0;
	if (sigaction(SIGHUP, &sa, NULL) < 0){
		syslog(LOG_ERR,"can not ignore SIGHUP.");
		error_quit();
	}
	if ((pid = fork()) < 0){
		syslog(LOG_ERR,"%s: can not fork.", cmd);
		error_quit();
	} else if (pid != 0){
		exit(0); /*exit parent*/
	}
	
	/*change working directory to root, to allow file systems to be unmounted*/
	if (chdir("/") < 0){
		syslog(LOG_ERR,"can not change directory to /");
		error_quit();
	}

	/*close all open file descriptors in the process*/
	if (rl.rlim_max == RLIM_INFINITY){
		rl.rlim_max = 1024;
	}
	for(i = 0; i < rl.rlim_max; i++){
		close(i);
	}

	/*Attach file descriptors to /dev/null*/
	fd0 = open("/dev/null", O_RDWR);
	fd1 = dup(0);
	fd2 = dup(0);
	/*init log file with identity, options and facility*/
	openlog(cmd, LOG_CONS, LOG_DAEMON);
	if (fd0 != 0 || fd1 != 1 || fd2 != 2){
		syslog(LOG_ERR,"unexpected file descriptors %d %d %d", fd0, fd1, fd2);
		error_quit();
	}
}

int main(int argc, char *argv[])
{
	int		err;
	pthread_t	tid;
	char		*cmd;
	struct sigaction sa;

	if ((cmd = strrchr(argv[0], '/')) == NULL){
		cmd = argv[0];
	} else {
		cmd++;
	}
	/*make the process daemon/service*/
	startquerymanager(cmd);

	/*Ensure that only one instance of the service is running.*/
	if (already_running()){
		syslog(LOG_ERR,"AQL Query Manager service already running");
		exit(0);
	}
	/*Restore SIGHUP and block all signals*/
	sa.sa_handler = SIG_DFL;
	sigemptyset(&sa.sa_mask);
	sa.sa_flags = 0;
	if (sigaction (SIGHUP, &sa, NULL) < 0){
		syslog(LOG_ERR,"can not restore SIGHUP default.");
		error_quit();
	}
	sigfillset(&mask);
	if ((err = pthread_sigmask(SIG_BLOCK, &mask, NULL)) != 0){
		syslog(LOG_ERR, "SIG_BLOCK error. %d", err);
                error_quit();
	}
	/*
	* Create a thread to handle SIGHUP and SIGTERM signals 
	* from the user to this process
	*/
	err = pthread_create(&tid, NULL, thr_fn, 0);
	if (err != 0){
		syslog(LOG_ERR, "can not create thread %d", err);
		error_quit();
	}

	/*BEGIN SERVER BLOCK*/
	//named pipe variables
        /*create FIFO pipe with pathname, flags*/
        mkfifo(tempPipe, 0666);

	//server variables
	listen_file_decr = 0;
	int client_fd = 0;
	int server_port = 5005;
	int time_interval = 1;
	int client_queue_size = 10; // number of clients in the connection queue
	struct sockaddr_in server_addr; 

	char sendBuff[1025];
	time_t current_time; 
	struct tm * time_str;	

	listen_file_decr = socket(AF_INET, SOCK_STREAM, 0);
	if (listen_file_decr < 0) syslog(LOG_ERR, "could not create server socket");
	memset(&server_addr, '0', sizeof(server_addr));
	memset(sendBuff, '0', sizeof(sendBuff)); 

	server_addr.sin_family = AF_INET;
	server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	server_addr.sin_port = htons(server_port); 

	bind(listen_file_decr, (struct sockaddr*)&server_addr, sizeof(server_addr));
	if(listen(listen_file_decr, client_queue_size) < 0){
		 syslog(LOG_ERR, "could not start listening on the port %d", server_port);
		 error_quit();
	} 
	
	//server loop
	while(run_status)
	{
		syslog(LOG_ERR, "listening on the port %d", server_port);
		/*accept new connection*/
	        if((client_fd = accept(listen_file_decr, (struct sockaddr*)NULL, NULL)) < 0 )
		{
			syslog(LOG_ERR, "could not get connection from the client");
			// Lock Mutex
			//pthread_mutex_lock( &count_mutex );
			run_status = 0;
			//pthread_mutex_unlock( &count_mutex );
			continue;
			// Unlock mutex
		}
		/*get current time*/
	        current_time = time(NULL);
		time_str = localtime (&current_time);
		if (time_str == NULL) 
			syslog(LOG_ERR, "can not get system time");
		/*send the time, unique QueryID - year_month_hour_minute_seconds*/
		/*http://linux.die.net/man/3/strptime*/
	        strftime(sendBuff, sizeof(sendBuff), "%y_%m_%d_%H_%M_%S;", time_str);
	        write(client_fd, sendBuff, strlen(sendBuff)); 
		/*write to name pipe to the Transaction Manager*/
		/*open the pipe*/
	        namedPipe = open(tempPipe, O_WRONLY);
		write(namedPipe, sendBuff, strlen(sendBuff));		
		close(namedPipe);		
		/*close connection and wait 1 second*/
	        close(client_fd);
        	sleep(time_interval);
	}
	//pthread_join( tid, NULL); // wait for signal handling thread to terminate
	close(namedPipe);
        unlink(tempPipe);
	/*END SERVER BLOCK*/
	syslog(LOG_INFO, "Exiting Query Manager Daemon.");
	closelog();
	exit(0);
}
