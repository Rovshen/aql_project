#include "apue.2e/include/apue.h"
//standard libraries
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
//system libraries
#include <syslog.h>
#include <sys/resource.h>
#include <sys/stat.h>
#include <sys/types.h>
//other libraries
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <pthread.h>
//networking libraries
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <time.h>
//daemonize custom library
#include "serverdaemonize.h"
//C server implementation
#include "serverqm.h"
