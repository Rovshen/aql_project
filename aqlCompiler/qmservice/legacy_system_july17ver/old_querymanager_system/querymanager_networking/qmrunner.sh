#! /bin/sh
### BEGIN INIT INFO
# Provides:          Query Manager control
# Short-Description: Execute the query manager service commands.
# Description:
### END INIT INFO

# Author Rovshen Nazarov. June 2014. Baylor University

PATH=/sbin:/usr/sbin:/bin:/usr/bin
NAME=querymanager
DAEMON=$NAME
#PID_FILE=/var/run/aqlquerymanager.pid
PID_FILE=/tmp/aqlquerymanager.pid
#SCRIPT_NAME=/etc/init.d/$PROG_NAME.sh

# Exit if the daemon package is not installed
[ -x "$DAEMON"  ] || exit 1

# Start Query Manager Service
do_start() {
	# Start the daemon
	echo -n "Starting $NAME"	
	./$DAEMON
	echo
}

# Stop Query Manager Service
do_stop() {
	local pid=`cat $PID_FILE`	
	kill $pid
	RETVAL=$?
	[ $RETVAL -ne 0 ] && echo "Could not stop the query manager service with pid $pid"	
}

# Invoke Daemon Reset
do_restart(){
	local pid=`cat $PID_FILE`
        kill -HUP $pid
        RETVAL=$?
        [ $RETVAL -ne 0 ] && echo "Could not reload configuarations of the daemon with pid $pid"
}

# perform action based on the call
case "$1" in
  start)
	do_start
	;;
  restart)
	do_restart
	;;
  stop)
	do_stop
	;;
  *)
	echo "Usage: $0 start|stop|restart" >&2
	exit 3
	;;
esac

:
