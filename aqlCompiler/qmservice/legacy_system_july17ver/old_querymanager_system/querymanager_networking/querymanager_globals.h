/*
* These variables will be available in all scripts
* Where this Header is included.
* Make sure they defined only once.
*/

/*server file descriptor*/
extern int listen_file_decr;

/*
* Exit Service on error and remove the lock file.
*/
extern void error_quit(void);
