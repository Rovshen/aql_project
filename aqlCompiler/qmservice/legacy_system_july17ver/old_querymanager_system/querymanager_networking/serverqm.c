#include "querymanager_globals.h" /*include globals*/
#include "serverqm.h"

/*server file descriptor*/
int listen_file_decr; /*define global var here*/

int
sleepForTime(int sec, int milisec)
{
        struct timespec req = {0};
        req.tv_sec = sec;
        req.tv_nsec = milisec * 1000000L; /* 1 milisec has 1 000 000 nanosec */
        if(nanosleep(&req, (struct timespec *)NULL) < 0)
        {
                syslog(LOG_ERR, "could not wait for %d seconds and %d milliseconds", sec, milisec);
                return -1;
        }
        return 0;
}

int  start_server(int server_port, int time_interv, int client_queue_size, char *tempPipe)
{
	struct sockaddr_in server_addr;
	
	listen_file_decr = socket(AF_INET, SOCK_STREAM, 0); /*define global var*/
        if (listen_file_decr < 0){
		syslog(LOG_ERR, "could not create server socket");
		return -1;
	}
        memset(&server_addr, '0', sizeof(server_addr));
        server_addr.sin_family = AF_INET;
        server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
        server_addr.sin_port = htons(server_port);

        bind(listen_file_decr, (struct sockaddr*)&server_addr, sizeof(server_addr));
        if(listen(listen_file_decr, client_queue_size) < 0){
                 syslog(LOG_ERR, "could not start listening on the port %d", server_port);
                 return -1;
        }
	if(handle_connection(server_port, listen_file_decr, tempPipe, time_interv) < 0){
		return -1; /*problem accepting new connections*/
	}
	return 0;
        //pthread_join( tid, NULL); // wait for signal handling thread to terminate
        //close(listen_file_decr);
}

int handle_connection(int server_port, int listen_file_decr_in, char *tempPipe, int time_interval)
{
	int client_fd = 0;
	int buff_size = 1025;
        char sendBuff[buff_size];
	char fmt[buff_size]; /*buffer to hold time format*/
	struct timeval  tv;
        struct tm       *time_str;
	int namedPipe;
	
        while(1) //server loop
        {
		memset(sendBuff, '0', sizeof(sendBuff)); /*clear buffer*/
                syslog(LOG_ERR, "listening on the port %d", server_port);
                /*accept new connection*/
                if((client_fd = accept(listen_file_decr_in, (struct sockaddr*)NULL, NULL)) < 0 )
                {
                        syslog(LOG_ERR, "could not get connection from the client");
                        continue;//try to accept another client
                }
		/*get current time*/
	        gettimeofday(&tv, NULL);
		if((time_str = localtime(&tv.tv_sec)) == NULL)
	        {
                	syslog(LOG_ERR, "can not get system time");
                	return -1;
        	}		
		/*
	        * Generate time based ID with up to milliseconds accuracy.
	        * To include timezone after millisec add _%z after 06u
	        */
	        strftime(fmt, sizeof fmt, "%y_%m_%d_%H_%M_%S_%%06u;", time_str);
	        snprintf(sendBuff, sizeof sendBuff, fmt, tv.tv_usec);
	        //syslog(LOG_INFO, "%s", sendBuff);
		
                /*send the time, unique QueryID - year_month_hour_minute_seconds*/
                /*http://linux.die.net/man/3/strptime*/		
                if (write(client_fd, sendBuff, strlen(sendBuff)) < 0) return -1;
                /*write to name pipe to the Transaction Manager*/
		namedPipe = open(tempPipe, O_WRONLY);
		if(namedPipe < 0){
                	syslog(LOG_ERR, "can not open named pipe, %s", strerror(errno));
                	return -1; //global function
        	}
                if (write(namedPipe, sendBuff, strlen(sendBuff)) < 0) return -1;
		close(namedPipe);
		//syslog(LOG_INFO, "%s %d", sendBuff, namedPipe);
                /*close connection and wait for time interval*/
                close(client_fd);
                if(sleepForTime(0, time_interval) < 0){ /* sleep for 0 sec and millisec */
                        return -1; /* sleep failed */
                }
        }
}
