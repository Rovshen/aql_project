#include "querymanager_globals.h" /*include globals*/
#include "serverdaemonize.h"

int already_running(void)
{
        int file_descriptor, lock_stat;
        char buff[16];
        /* open or create the file if it does not exist*/
        file_descriptor = open(LOCKFILE, O_RDWR | O_CREAT, LOCKMODE);
        if (file_descriptor < 0){
                syslog(LOG_ERR,"can not create %s: %s", LOCKFILE, strerror(errno));
                error_quit();
        }
        /*
	* Try lock file to check if a single instance of 
	* the service is already running.
	*/
        lock_stat = lockfile(file_descriptor);
        if (lock_stat == 1){
                return(1);/*an instance of daemon already running*/
        } else if (lock_stat == -1){
                syslog(LOG_ERR,"can not lock file %s", LOCKFILE);
                error_quit();
        }
        /*
	* Lock stat = 0 at this point, which means lock succeeded.
	*/
        ftruncate(file_descriptor, 0); /*remove any old data from the lockfile*/
        sprintf(buff, "%ld", (long)getpid()); 
	/*store current process id into the lock file*/
        write(file_descriptor, buff, strlen(buff)+1);
        return(0);
}

void reread(void)
{
        //read service configuration updates if any
}

void *thr_fn(void *arg)
{
        int err, signo;
	/* Cast the arg pointer to the right type. */ 
	struct data_handlers_parms*  p = (struct data_handlers_parms*) arg; 

        for(;;){
                err = sigwait(&mask, &signo);
                if (err != 0){
                        syslog(LOG_ERR,"sigwait failed");
                        error_quit();
                }

                switch (signo){
                case SIGHUP:
                        syslog(LOG_INFO, "Reloading configuration file");
                        reread(); break;
                case SIGTERM:
                        syslog(LOG_INFO, "Got exit signal. Stopping daemon.");
                        unlink(p->temp_pipe_in); /*input var*/
                        close(listen_file_decr); /*global var*/
			if (remove(LOCKFILE) != 0){
		        	syslog(LOG_ERR, "could not remove the file %s", LOCKFILE);
			}
		        closelog();
                        exit(0);
                        break;
                default:
                        syslog(LOG_INFO, "Unexpected signal %d\n", signo);
                        break;
                }
        }
        return(0);
}

void startquerymanager(const char *cmd)
{
        int                     i, fd0, fd1, fd2;
        pid_t                   pid, sid_new;
        struct rlimit           rl;
        struct sigaction        sa;

        /*Reset file creation mask.*/
        umask(0);
        /*Get max number of file descriptors.*/
        if (getrlimit(RLIMIT_NOFILE, &rl) < 0){
                syslog(LOG_ERR,"%s: can not get file limit.", cmd);
                error_quit();
        }
        /*Loose controlling TTY by cloning the process*/
        if ((pid = fork()) < 0){
                syslog(LOG_ERR,"%s: can not fork.", cmd);
                error_quit();
        }else if (pid != 0){
                exit(0); /*exit parent*/
        }
        sid_new = setsid(); /*create new session*/
        if (sid_new < 0){
                syslog(LOG_ERR,"cat not get new session.");
                error_quit();
        }
        /*Prevent future opens from allocating cotrolling TTY to this daemon*/
        sa.sa_handler = SIG_IGN;
        sigemptyset(&sa.sa_mask);
        sa.sa_flags = 0;
        if (sigaction(SIGHUP, &sa, NULL) < 0){
                syslog(LOG_ERR,"can not ignore SIGHUP.");
                error_quit();
        }
        if ((pid = fork()) < 0){ /*detach from parent*/
                syslog(LOG_ERR,"%s: can not fork.", cmd);
                error_quit();
        } else if (pid != 0){
                exit(0); /*exit parent*/
        }
        /*change working directory to root, to allow file systems to be unmounted*/
        if (chdir("/") < 0){ /*change directory*/
                syslog(LOG_ERR,"can not change directory to /");
                error_quit();
        }
        if (rl.rlim_max == RLIM_INFINITY){ 
                rl.rlim_max = 1024;
        }
	/*close all open file descriptors inherited from the parent process*/
        for(i = 0; i < rl.rlim_max; i++){
                close(i);
        }
	/*do not use syslog at this point to keep proper file descriptor numnering*/
        /*Attach file descriptors to /dev/null*/
        fd0 = open("/dev/null", O_RDWR);
        fd1 = dup(0);
        fd2 = dup(0);
        /*init log file with identity, options and facility*/
        openlog(cmd, LOG_CONS, LOG_DAEMON);
        if (fd0 != 0 || fd1 != 1 || fd2 != 2){
                syslog(LOG_ERR,"unexpected file descriptors %d %d %d", fd0, fd1, fd2);
                error_quit();
        }	
	/* at this point terminal descriptor closed need to write to syslog */
	return;
}

int lockfile(int fd)
{
        struct flock file_lock;
        memset(&file_lock, 0, sizeof(file_lock)); /*init flock structure*/
        file_lock.l_type = F_WRLCK; /*lock we want to place*/
        file_lock.l_whence = SEEK_SET; /*lock based on range below*/
        file_lock.l_start = 0; /*start position in the file*/
        file_lock.l_len = 0; /*lock the whole file regardless its size*/
        /*check if the file leased/locked, sets l_type with lease status*/
        fcntl(fd, F_GETLK, &file_lock);
        /*check status if we can lock the file*/
        if(file_lock.l_type == F_UNLCK){ /*F_UNLCK - indicates no lease on the file*/
                file_lock.l_type = F_WRLCK; /*lock file for read write*/
                if (fcntl(fd, F_SETLK, &file_lock) < 0){
                        if (errno == EACCES || errno == EAGAIN){ /*if file locked already*/
                                return(1);/*an instance of daemon already running*/
                        }
                        return(-1); /*could not lock the file*/
                }
                return(0); /*lock created ok*/
        }	
	/*else file is locked/leased*/
	return(1);/*an instance of daemon already running*/	
}
//global function
void error_quit(void)
{
	//syslog(LOG_INFO, "I am in error_quit");
        if (remove(LOCKFILE) != 0)
                syslog(LOG_ERR, "could not remove the file %s", LOCKFILE);
        closelog();
        exit(1);
}
