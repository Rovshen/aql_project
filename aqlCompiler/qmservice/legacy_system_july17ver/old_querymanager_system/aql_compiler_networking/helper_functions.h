#ifndef HELPER_FUNCTIONS_H_INCLUDED
#define HELPER_FUNCTIONS_H_INCLUDED
void write_to_stdout(char *s, va_list ap, int numb);
void write_to_stream(char *s, va_list ap, FILE * file, int numb);
#endif
