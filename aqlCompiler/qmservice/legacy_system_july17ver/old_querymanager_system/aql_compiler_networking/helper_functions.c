/*
* Helper Functions for Compiler Client tool - Query Manager tool
* Rovshen Nazarov. All Rights Reserved. Baylor University. June 2014.
*/
//standard libs
#include <stdio.h>  //for std out
#include <stdlib.h> //for FILE steam
#include <stdarg.h> //for va_list
#include <string.h> //for string operations
#include <fcntl.h>  //for opening named pipe
//user libs
#include "helper_functions.h"

void write_to_stdout(char *s, va_list ap, int numb)
{
        /*write to st out*/
        printf("%d: ", numb);
        vfprintf(stdout, s, ap);
        printf("\n");
}
void write_to_stream(char *s, va_list ap, FILE * file, int numb)
{
        /*write to a file stream*/
        fprintf(file,"%d: ", numb);
        vfprintf(file, s, ap);
        fprintf(file,"\n");
        fflush(file);
}
