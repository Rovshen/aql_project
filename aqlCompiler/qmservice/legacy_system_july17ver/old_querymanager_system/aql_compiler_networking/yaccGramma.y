/*
*
*Bison parser for AQL - Analytical Querty Language.
*The design of the parser was inspired by the SQL example
*from the book "flex & bison: Text Processing Tools by John Levine (Aug 24, 2009)"
*Designed by Rovshen Nazarov. All rights reserved.
*
*This parser reads series of tokens produced by tokanizer
*and tries to determine the grammatical struct based on
*grammer defined below.
* Rovshen Nazarov. All Rights Reserved. Created: May 30, 2014.
*/
%{
#include "compiler_headers.h"
//function headers
/*this function called on error*/
void yyerror(char *s);
/*this function called on by parser on gramma production*/
void emit(char *s, ...);
void quit_compiler(); //quit compiler on error
%}
/* Simple data structures for
 * defining available types for terminals
 * within parser.
*/
%union {
        char *strval;
        double floatval;
        int numVal;
}
%start simple_query /*parser will start processing tokens supplied by the tokenizer for the non-terminal simple_query*/
/*tokens recognized by the parser*/
%token L_FROM
%token <strval> L_GRAPH_ID
%token L_UNION
%token L_INTER
%token L_DIFF
%token L_EXIT
%token L_LOAD

/* operator-precedence using left command
 * to avoid ambigouty
 * top-0: 
 *     1: 
 *     2: 
*/
/* The explicit precedence is provided by the %prec declaration; 
 * otherwise the precedence of the last terminal in the production is used
 * In the event of a shift/reduce conflict, 
 * the precedence of the production which could be reduced is compared to 
 * the precedence of the lookahead terminal which could be shifted, and 
 * whichever has the greater precedence wins
*/
%left L_INTER /*Third*/
%left L_DIFF /*Second*/
%left L_UNION /*First highest precedence*/
%%
simple_query	: error ';' 
		{ 
			yyerrok; yyclearin;  //if error was produced
			quit_compiler();
			return(1);
		} /*stop parser on error*/
		| L_EXIT ';' 
		{ /*on exit production close all streams and files and exit parser*/
			quit_compiler();
			return(0);
		}
		| 
		{	/*mark beginning of the query*/
			fprintf(file,"BEGIN\n"); 
			fprintf(stream,"BEGIN\n"); 
			fflush(stream); 
		} 
		L_FROM expression ';' /*the from terminal followd by non-terminal expression*/
		{	/*mark end of query and write the results*/
			emit("QUERY"); 
			fprintf(file,"END\n"); 
			fprintf(stream,"END\n"); 
			fflush(file);
			fflush(stream);
			write(namedPipe, buf, len); // write data to name pipe
			fseeko(stream, 0, SEEK_SET); // reset stream
			numb=1;execNumb=1; /*reset numbering of commands and query lines*/
		} 
		simple_query /*non-terminal production, process more queries if any*/
		;

expression      : '(' expression ')' /*process expressiono within bracets*/
		| expression L_INTER expression {emit("%d INTERSECTION",execNumb); execNumb++; numb++;}
		| expression L_DIFF expression {emit("%d DIFFERENCE",execNumb); execNumb++; numb++;}
		| expression L_UNION expression {emit("%d UNION",execNumb); execNumb++; numb++;}
		| 
		/*process graph id and free variable for later resuse*/
		L_GRAPH_ID {emit("GRAPH_ID %s", $1); free($1); numb++;}
                ;

%%

/*Helper function called on exit*/
void quit_compiler()
{
	fclose(file);
        fclose(stream);
        free(buf);
        close(namedPipe);
        unlink(tempPipe); //remove named pipe
}

/*
* This function called on by parser on gramma production.
* Input: pointer to a string emitted by the parser
* and a list of any number of variables ...
* Output: void
*/
void emit(char *s, ...)
{
	va_list ap;
	va_start(ap, s); //populate the list with variables
	write_to_stdout(s, ap, numb);
	va_end(ap);
	/*write to a pipe block*/
	va_start(ap, s);
	write_to_stream(s, ap, stream, numb);
	va_end(ap);
	/*write data from buffer to pipe*/
	write(namedPipe, buf, len);
	/*rewind backing buffer to the beginning
	update pointer of the stream*/
	fseeko(stream, 0, SEEK_SET);	
	len = 0;
	/*write to a file*/
	va_start(ap, s);
	write_to_stream(s, ap, file, numb);
	va_end(ap); //clean the ap var argument list
}

int yywrap()
{
        /*end of reading stream*/
        return 1;
}

void open_stream(char *pipe_name)
{
        /*open dynamically expandable stream*/
        stream = open_memstream(&buf, &len);
        if(stream == NULL)
                exit(1);//exit on stream error
        /*create FIFO pipe with pathname, flags*/
	if( mkfifo(pipe_name, 0666) < 0){
                printf("Could not create named pipe %s %s \n", pipe_name, strerror(errno));
                exit(1);
        }
        /*open the pipe*/
        namedPipe = open(pipe_name, O_WRONLY);//write only
	if( namedPipe < 0){
		printf("Could not open named pipe %s %s \n", pipe_name, strerror(errno));
                exit(1);
	}
}
/*
* Open stream and open named pipe.
* This function is kept here because it is highly tight
* to local variables.
i*/
int main(int argc, char **argv)
{
	char *received_msg;
	char *tmp_fold = "/tmp/"; /*named pipe location folder*/
	/*networking block*/
	int port_number = 5005; // aql query manager server port
	int receive_buff_size = 1024;
	char * ip_addr = "127.0.0.1";
	received_msg = connect_to_qm(port_number, receive_buff_size, ip_addr);
	char pipe_path[receive_buff_size + strlen(tmp_fold)];
	sprintf(pipe_path, "%s%s", tmp_fold, received_msg);	
	//erase last character in the buffer array
	pipe_path[strlen(pipe_path)-1] = '\0';
	tempPipe = pipe_path;
	puts(tempPipe);
	open_stream(tempPipe);
	/*if files exists erases file data if any*/
	file = fopen("intermediateCode.AIL","w"); /*AIL stands for AQL intermediate language*/
	if(!yyparse())
		printf("SQL parse worked\n");
	else
		printf("SQL parse failed\n");
	free(received_msg);//free memory alloc by malloc
	return(0); //all ok
}
