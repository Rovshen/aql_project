/*
*
*Bison parser for AQL - Analytical Querty Language.
*The design of the parser was inspired by the SQL example
*from the book "flex & bison: Text Processing Tools by John Levine (Aug 24, 2009)"
*Designed by Rovshen Nazarov. All rights reserved.
*
*This parser reads series of tokens produced by tokanizer
*and tries to determine the grammatical struct based on
*grammer defined below.
* Rovshen Nazarov. All Rights Reserved. Created: May 30, 2014.
*/
%{

#include "compiler_headers.h"
//function headers
/*this function called on error*/
void yyerror(char *s);
/*this function called on by parser on gramma production*/
void emit(char *s, ...);
void quit_compiler(); //quit compiler on error
void *get_data_from_aql(void *arg);
%}
/* Simple data structures for
 * defining available types for terminals
 * within parser.
*/
%union {
        char *strval;
        double floatval;
        int numVal;
}
%start command /*parser will start processing tokens supplied by the tokenizer for the non-terminal simple_query*/
/*tokens recognized by the parser*/
%token L_FROM
	/*%token <strval> L_OPERAND_ID*/
%token <strval> L_ATTR_VALUE
%token <strval> L_ATTR_TYPE
%token L_UNION
%token L_INTER
%token L_DIFF
%token L_JOIN
%token L_EXIT
%token L_LOAD
%token L_USING
%token L_AS

/* operator-precedence using left command
 * to avoid ambigouty
 * top-0: 
 *     1: 
 *     2: 
*/
/* The explicit precedence is provided by the %prec declaration; 
 * otherwise the precedence of the last terminal in the production is used
 * In the event of a shift/reduce conflict, 
 * the precedence of the production which could be reduced is compared to 
 * the precedence of the lookahead terminal which could be shifted, and 
 * whichever has the greater precedence wins
 * NOTE: based on the set theory and database implementations, the precedence
 * of the set operations should be equal precedence, which is evaluation
 * left to right.
*/
/* 
* PROBLEM if we do not provide specific order, 
* the operations are reversed, e.g. a U b - d becomes b - d U a 
*/
%left L_JOIN /*Fourth*/
%left L_UNION /*Third*/
%left L_DIFF /*Second*/
%left L_INTER /*First highest precedence*/
%%
command		: error ';' 
		{ /*stop parser on error*/ 
			yyerrok; yyclearin;  //if error was produced
			quit_compiler();
			return(ERR_STAT);
		}
		| L_EXIT ';' 
		{ /*on exit production close all streams and files and exit parser*/
			fprintf(file,"EXIT\n");
                        fprintf(stream,"EXIT\n");
                        fflush(stream);
			quit_compiler();
			return(0);
		}
		| simple_query command /*right recursion*/
		| load command
		;

simple_query	: {	/*mark beginning of the query*/
			fprintf(file,"BEGIN_QUERY\n"); 
			fprintf(stream,"BEGIN_QUERY\n"); 
			fflush(stream); 
		} 
		L_FROM dataset custom_command ';'
		{	/*mark end of query and write the results*/
			emit("QUERY"); 
			fprintf(file,"END_QUERY\n"); 
			fprintf(stream,"END_QUERY\n"); 
			fflush(file);
			fflush(stream);
			write(namedPipe, buf, len); // write data to name pipe
			fseeko(stream, 0, SEEK_SET); // reset stream
			numb=1;execNumb=1;subQueryNumb=1; /*reset numbering of commands, sub queries, and query lines*/
		} 
		;

dataset      	:
		/*data set is optional*/  
		|
		dataset L_AS L_ATTR_VALUE {emit("QUERY_ALIAS_NAME %s", $3); free($3); numb++;}
		|
		/*begin a query piece*/
		dataset		
		',' 
		dataset
		| 
		{     /*mark beginning of the sub query with its sequence number*/
                        fprintf(file,"BEGIN_SUB_QUERY %d\n", subQueryNumb);
                        fprintf(stream,"BEGIN_SUB_QUERY %d\n", subQueryNumb);
			subQueryNumb++; /*increment sub query sequence number*/
                        fflush(stream);
                }
		'(' L_FROM dataset custom_command ')' /*process expression within bracets*/
		{       /*mark end of sub query*/
                        fprintf(file,"END_SUB_QUERY\n");
                        fprintf(stream,"END_SUB_QUERY\n");
                        fflush(stream);
                }
		| dataset L_INTER dataset {emit("%d INTERSECTION",execNumb); execNumb++; numb++;}
		| dataset L_DIFF dataset {emit("%d DIFFERENCE",execNumb); execNumb++; numb++;}
		| dataset L_UNION dataset {emit("%d UNION",execNumb); execNumb++; numb++;}
		| dataset L_JOIN dataset {emit("%d JOIN",execNumb); execNumb++; numb++;}
		| 
		/*process graph id and free variable for later resuse*/
		L_ATTR_VALUE {emit("OPERAND_ID %s", $1); free($1); numb++;}
                ;

custom_command	: {	/*We use operand id for derivation because command name and 
			* operand name follow similar expression.
			* mark beginning of the loading*/
	                fprintf(file,"BEGIN_USING\n");
                        fprintf(stream,"BEGIN_USING\n");
                        fflush(stream);
		}
		/*will recurse for all atributes in the brackets of the custom command*/
		L_USING cust_comm_term
		{     /*Mark the end of the custom command */
                        fprintf(file,"END_USING\n");
                        fprintf(stream,"END_USING\n");
                        fflush(stream);
                }
		|
		/*allow empty production*/
		;

cust_comm_term	:
		L_ATTR_VALUE {emit("CUST_COMMAND_NAME %s", $1); free($1); numb++;} '(' attribute_term ')'
		;

load		: {       /*mark beginning of the loading*/			
                        fprintf(file,"BEGIN_LOAD\n");
                        fprintf(stream,"BEGIN_LOAD\n");
                        fflush(stream);
                } 
                L_LOAD load_attribute ';'
                {       /*mark end of query and write the results*/
                        emit("LOAD");
                        fprintf(file,"END_LOAD\n");
                        fprintf(stream,"END_LOAD\n");
                        fflush(file);
                        fflush(stream);
                        write(namedPipe, buf, len); // write data to name pipe
                        fseeko(stream, 0, SEEK_SET); // reset stream
                        numb=1;execNumb=1; /*reset numbering of commands and query lines*/
                }
                ;

attribute_term	:
		attribute
		|
                /*allow empty deriv*/
		;

attribute	:
		attribute ',' attribute
		| 
		attr_pair
		|
                /*allow operands in derivation, free variable for later re-use*/
                L_ATTR_VALUE {emit("OPERAND_ID %s", $1); free($1); numb++;}
		;

load_attribute	:
                load_attribute load_attribute
                | 
                attr_pair
		;
attr_pair	:
		L_ATTR_TYPE L_ATTR_VALUE {emit("ATTR_TYPE=%s,ATTR_VALUE=%s", $1, $2); free($1); free($2); numb++;}
		;


%%

/*Helper function called on exit*/
void quit_compiler()
{
	fclose(file);
        fclose(stream);
        free(buf);
	if ( shutdown_inet_stream_socket(namedPipe, LIBSOCKET_READ | LIBSOCKET_WRITE) < 0 )
        {
                exit(ERR_STAT);
        }
	//close(pipe_java_c);
        //unlink(fifo_c_java); //remove named pipe
	if(pthread_cancel(tid) < 0){
		exit(ERR_STAT);
	}
	pthread_join( tid, NULL); // wait
}

/*
* This function called on by parser on gramma production.
* Input: pointer to a string emitted by the parser
* and a list of any number of variables ...
* Output: void
*/
void emit(char *s, ...)
{
	va_list ap;
	/*va_start(ap, s); //populate the list with variables
	write_to_stdout(s, ap, numb);
	va_end(ap);*/
	/*write to a pipe block*/
	va_start(ap, s);
	write_to_stream(s, ap, stream, numb);
	va_end(ap);
	/*write data from buffer to pipe*/
	write(namedPipe, buf, len);
	/*rewind backing buffer to the beginning
	update pointer of the stream*/
	fseeko(stream, 0, SEEK_SET);	
	len = 0;
	/*write to a file*/
	va_start(ap, s);
	write_to_stream(s, ap, file, numb);
	va_end(ap); //clean the ap var argument list
}

int yywrap()
{
        /*end of reading stream*/
        return ERR_STAT;
}

/*
* Open C to Java FIFO.
* Input:
*   Named pipe file name
*
*/
void open_stream(char *ip_addr, char *port)
{
        /*open dynamically expandable stream*/
        stream = open_memstream(&buf, &len);
        if(stream == NULL)
                exit(ERR_STAT);//exit on stream error
        /*create TCP/Stream Socket with hostname and port*/
	int addres_family = get_address_family(ip_addr); /* IPv4 or IPv6 */
        if(addres_family < 0){
                printf("Failed to connect to AQL server.\n");
                exit(ERR_STAT);
        }
	 /* save file descriptor for the socket stream */
        namedPipe = create_inet_stream_socket(ip_addr, port, addres_family, 0);
	if( namedPipe < 0){
		printf("Could not connedct to the AQL server at %s and port %s %s \n", ip_addr, port, strerror(errno));
                exit(ERR_STAT);
	}
}

/*
* Open Java to C FIFO.
* Input:
*   Named pipe file name
*
*/
void open_read_pipe(char *pipe_name)
{
        /*create FIFO pipe with pathname, flags
        if( mkfifo(pipe_name, 0666) < 0){
                printf("Could not create named pipe %s %s \n", pipe_name, strerror(errno));
                exit(1);
        }
	*/
        /*open the pipe*/
        pipe_java_c = open(pipe_name, O_RDONLY);//read only
        if( pipe_java_c < 0){
                printf("Could not open named pipe JAVA to C %s %s \n", pipe_name, strerror(errno));
                exit(ERR_STAT);
        }
}

/*
* Read from JAVA usiong FIFO
* Read as a separate thread, stop thread
* on client exit.
*
* this method will block until there is something
* to read from the pipe, which will require
* compiler to wait for command execution.
*/
void *get_data_from_aql(void *arg){
	char buf[MAX_BUF];
	/* read, and display the message from the FIFO */
	while(recv(namedPipe, buf, MAX_BUF, 0) > 0){ /*read until the peer shutdown*/
		printf("\naql>> %s", buf);
		memset(&buf[0], 0, sizeof(buf)); /*clear buff*/
	}

	return(0);
}

int main(int argc, char **argv)
{
	char * ip_addr = "localhost";
	char * port = "8010";
	int sleep_millis = 100; /* server sleep time */
	open_stream(ip_addr, port);
	/*if files exists erases file data if any*/
        file = fopen("intermediateCode.AIL","w"); /*AIL stands for AQL intermediate language*/
	/*
        * Create a thread to read from AQL server
        */
        int err = pthread_create(&tid, NULL, get_data_from_aql, 0);
        if (err != 0){
                syslog(LOG_ERR, "can not create thread %d", err);
                return(ERR_STAT);
        }
	if(!yyparse())
		printf("SQL parse worked\n");
	else
		printf("SQL parse failed\n");
	return(0); //all ok
}
