/*
*
*Flex lexical analyzer for AQL - Analytical Querty Language.
*Designed by Rovshen Nazarov. All rights reserved.
*
*The tokenizer reads an input stream or file and chunks it
*into a series of tokens, which are passed to the parser.
*/
/*noyywrap tells flex to read only one input file*/
%option noyywrap nodefault yylineno case-insensitive
%{
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include "yaccGramma.tab.h" /*generated by yacc Bison*/

/*this function called on error*/
void yyerror(char *s);
int oldstate;
int lineno;
%}

%Option yylineno
%Option noyywrap 

%%
	/*reserved words*/
FROM		  {return L_FROM; }
UNION		  {return L_UNION; }
INTER	 	  {return L_INTER; }
DIFF 		  {return L_DIFF; }
JOIN              {return L_JOIN; }
LOAD		  {return L_LOAD; }
EXIT		  { return L_EXIT; }
USING		   { return L_USING;}
AS		  { return L_AS; }
	/*operators. return operators as is*/
[();]	{ return yytext[0]; }
,	{ return yytext[0]; }
	/*match identifier*/
	/*[A-Za-z][A-Za-z0-9_\.]*       {yylval.strval = strdup(yytext); return L_OPERAND_ID ;}*/
	/*[':=$/A-Za-z0-9_\.\X@]*       {yylval.strval = strdup(yytext); return L_ATTR_VALUE ;}*/
[a-zA-Z0-9_\.]+	{yylval.strval = strdup(yytext); return L_ATTR_VALUE ;} /*match a value that is a string with no spaces*/
'[^']+'	{yylval.strval = strdup(yytext); return L_ATTR_VALUE ;} /*treat string between single quotes as literal*/
	/*the - should be only used when in single quotes*/
--[A-Za-z][A-Za-z0-9_\.]*       {yylval.strval = strdup(yytext); return L_ATTR_TYPE ;}
	/* \w+(\w|\d)* */ /* more here: http://marvin.cs.uidaho.edu/Handouts/regex.html*/
	/*comments*/
#.*	;
	/*all other characters*/
\n	lineno++;
[ \t\r]		;	/*ignore whitespace*/
.	{yyerror("unrecognized character");}
%%

void yyerror(char *s)
{
        printf("%d: %s at %s\n", lineno, s, yytext);
}
