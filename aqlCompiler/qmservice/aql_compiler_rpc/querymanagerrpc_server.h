/*
 * RPC server implementation.
 */
#include <rpc/rpc.h>
#include <stdio.h>
#include <time.h> //for time based id
#include<sys/syslog.h> //error logging systemwide in /var/log/syslog

#include <fcntl.h>
//#include <errno.h> /* for named pipe error status */

#include "querymanagerrpc.h"

extern __thread int errno;

/*input 
* millisec- length of time to sleep, in miliseconds
* sec - time to sleep in sec
* Return status 0 ok, -1 error.
*/
int
sleepForTime(int sec, int milisec)
{
        struct timespec req = {0};
        req.tv_sec = sec;
        req.tv_nsec = milisec * 1000000L; /* 1 milisec has 1 000 000 nanosec */
        if(nanosleep(&req, (struct timespec *)NULL) < 0)
        {
                syslog(LOG_ERR, "could not wait for %d seconds and %d milliseconds", sec, milisec);
		return -1;
        }
	return 0;
}

/*
* Create named pipe if it does not already exists.
* The pipe is created outside in bash by RPC controller script.
*/

void
open_pipe(){
	/*create FIFO pipe with pathname for Query Manager*/
	/*if(mkfifo(AQL_NAME_PIPE, 0664) < 0);*/ /* 0664 mode rw rw r */
	/* unlink(AQL_NAME_PIPE); input var */
}
