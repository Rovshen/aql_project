#ifndef CCLIENTQM_H_INCLUDED
#define CCLIENTQM_H_INCLUDED
/*
* Compiler Client - Query Manager tool
* Rovshen Nazarov. All Rights Reserved. Baylor University. June 2014.
*/
char *connect_to_qm(int port_number, int receive_buff, char * ip_addr);
#endif
