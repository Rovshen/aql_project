/*
*
*Bison parser for AQL - Analytical Querty Language.
*The design of the parser was inspired by the SQL example
*from the book "flex & bison: Text Processing Tools by John Levine (Aug 24, 2009)"
*Designed by Rovshen Nazarov. All rights reserved.
*
*This parser reads series of tokens produced by tokanizer
*and tries to determine the grammatical struct based on
*grammer defined below.
* Rovshen Nazarov. All Rights Reserved. Created: May 30, 2014.
*/
%{
#include "compiler_headers.h"
//function headers
/*this function called on error*/
void yyerror(char *s);
/*this function called on by parser on gramma production*/
void emit(char *s, ...);
void quit_compiler(); //quit compiler on error
%}
/* Simple data structures for
 * defining available types for terminals
 * within parser.
*/
%union {
        char *strval;
        double floatval;
        int numVal;
}
%start command /*parser will start processing tokens supplied by the tokenizer for the non-terminal simple_query*/
/*tokens recognized by the parser*/
%token L_FROM
%token <strval> L_GRAPH_ID
%token L_UNION
%token L_INTER
%token L_DIFF
%token L_EXIT
%token L_LOAD

/* operator-precedence using left command
 * to avoid ambigouty
 * top-0: 
 *     1: 
 *     2: 
*/
/* The explicit precedence is provided by the %prec declaration; 
 * otherwise the precedence of the last terminal in the production is used
 * In the event of a shift/reduce conflict, 
 * the precedence of the production which could be reduced is compared to 
 * the precedence of the lookahead terminal which could be shifted, and 
 * whichever has the greater precedence wins
*/
%left L_INTER /*Third*/
%left L_DIFF /*Second*/
%left L_UNION /*First highest precedence*/
%%
command		: error ';' 
		{ /*stop parser on error*/ 
			yyerrok; yyclearin;  //if error was produced
			quit_compiler();
			return(1);
		}
		| L_EXIT ';' 
		{ /*on exit production close all streams and files and exit parser*/
			fprintf(file,"EXIT\n");
                        fprintf(stream,"EXIT\n");
                        fflush(stream);
			quit_compiler();
			return(0);
		}
		| simple_query command /*right recursion*/
		| load_graph command
		;

simple_query	: {	/*mark beginning of the query*/
			fprintf(file,"BEGIN\n"); 
			fprintf(stream,"BEGIN\n"); 
			fflush(stream); 
		} 
		expression ';'
		{	/*mark end of query and write the results*/
			emit("QUERY"); 
			fprintf(file,"END\n"); 
			fprintf(stream,"END\n"); 
			fflush(file);
			fflush(stream);
			write(namedPipe, buf, len); // write data to name pipe
			fseeko(stream, 0, SEEK_SET); // reset stream
			numb=1;execNumb=1; /*reset numbering of commands and query lines*/
			get_data_from_java(); /* block until JAVA responds with result */
		} 
		;

expression      : L_FROM expression 
		| '(' L_FROM expression ')' /*process expressiono within bracets*/
		| expression L_INTER expression {emit("%d INTERSECTION",execNumb); execNumb++; numb++;}
		| expression L_DIFF expression {emit("%d DIFFERENCE",execNumb); execNumb++; numb++;}
		| expression L_UNION expression {emit("%d UNION",execNumb); execNumb++; numb++;}
		| 
		/*process graph id and free variable for later resuse*/
		L_GRAPH_ID {emit("GRAPH_ID %s", $1); free($1); numb++;}
                ;

load_graph	: {       /*mark beginning of the loading*/
                        fprintf(file,"BEGIN_LOAD\n");
                        fprintf(stream,"BEGIN_LOAD\n");
                        fflush(stream);
                } 
                L_LOAD load_graph ';'
                {       /*mark end of query and write the results*/
                        emit("LOAD");
                        fprintf(file,"END_LOAD\n");
                        fprintf(stream,"END_LOAD\n");
                        fflush(file);
                        fflush(stream);
                        write(namedPipe, buf, len); // write data to name pipe
                        fseeko(stream, 0, SEEK_SET); // reset stream
                        numb=1;execNumb=1; /*reset numbering of commands and query lines*/
			get_data_from_java(); /* block until JAVA responds with result */
                }
		| 
                /*process graph id and free variable for later resuse*/
                L_GRAPH_ID {emit("GRAPH_PATH %s", $1); free($1); numb++;} 
                ;

%%

/*Helper function called on exit*/
void quit_compiler()
{
	fclose(file);
        fclose(stream);
        free(buf);
        close(namedPipe);
	close(pipe_java_c);
        unlink(fifo_c_java); //remove named pipe
}

/*
* This function called on by parser on gramma production.
* Input: pointer to a string emitted by the parser
* and a list of any number of variables ...
* Output: void
*/
void emit(char *s, ...)
{
	va_list ap;
	va_start(ap, s); //populate the list with variables
	write_to_stdout(s, ap, numb);
	va_end(ap);
	/*write to a pipe block*/
	va_start(ap, s);
	write_to_stream(s, ap, stream, numb);
	va_end(ap);
	/*write data from buffer to pipe*/
	write(namedPipe, buf, len);
	/*rewind backing buffer to the beginning
	update pointer of the stream*/
	fseeko(stream, 0, SEEK_SET);	
	len = 0;
	/*write to a file*/
	va_start(ap, s);
	write_to_stream(s, ap, file, numb);
	va_end(ap); //clean the ap var argument list
}

int yywrap()
{
        /*end of reading stream*/
        return 1;
}

/*
* Open C to Java FIFO.
* Input:
*   Named pipe file name
*
*/
void open_stream(char *pipe_name)
{
        /*open dynamically expandable stream*/
        stream = open_memstream(&buf, &len);
        if(stream == NULL)
                exit(1);//exit on stream error
        /*create FIFO pipe with pathname, flags*/
	if( mkfifo(pipe_name, 0666) < 0){
                printf("Could not create named pipe %s %s \n", pipe_name, strerror(errno));
                exit(1);
        }
        /*open the pipe*/
        namedPipe = open(pipe_name, O_WRONLY);//write only
	if( namedPipe < 0){
		printf("Could not open named pipe C to JAVA %s %s \n", pipe_name, strerror(errno));
                exit(1);
	}
}

/*
* Open Java to C FIFO.
* Input:
*   Named pipe file name
*
*/
void open_read_pipe(char *pipe_name)
{
        /*create FIFO pipe with pathname, flags
        if( mkfifo(pipe_name, 0666) < 0){
                printf("Could not create named pipe %s %s \n", pipe_name, strerror(errno));
                exit(1);
        }
	*/
        /*open the pipe*/
        pipe_java_c = open(pipe_name, O_RDONLY);//read only
        if( pipe_java_c < 0){
                printf("Could not open named pipe JAVA to C %s %s \n", pipe_name, strerror(errno));
                exit(1);
        }
}

/*
* Read from JAVA usiong FIFO
* this method will block until there is something
* to read from the pipe, which will require
* compiler to wait for command execution.
*/
void get_data_from_java(){
	char buf[MAX_BUF];
	int millisec = 100;
	sleepForTime(0, millisec);
	/* read, and display the message from the FIFO */
	if(read(pipe_java_c, buf, MAX_BUF) < 0){
		printf("Could not read from JAVA %s \n", strerror(errno));
                exit(1);
	}
	printf("Received result from Java:\n %s\n", buf);
}

int main(int argc, char **argv)
{
	char *received_msg;
	char *tmp_fold = "/tmp/"; /*named pipe folder*/
	char * ip_addr = "127.0.0.1";
	int sleep_millis = 100; /* server sleep time */
	/* use RPC */
	received_msg = querymanager_get_id (ip_addr, sleep_millis);
	char pipe_path[MAX_BUF + strlen(tmp_fold)];
	sprintf(pipe_path, "%s%s", tmp_fold, received_msg);	
	/* erase last character */
        pipe_path[strlen(pipe_path)-1] = '\0';

	char * strToAdd = "java_c";
	char pipe_path_read[MAX_BUF + strlen(tmp_fold) + strlen(strToAdd)];
        sprintf(pipe_path_read, "%s%s", pipe_path, strToAdd);

	fifo_c_java = pipe_path;
	fifo_java_c = pipe_path_read;
	puts(fifo_c_java);
	puts(fifo_java_c);

	open_stream(fifo_c_java); 
	open_read_pipe(fifo_java_c);
	/*if files exists erases file data if any*/
	file = fopen("intermediateCode.AIL","w"); /*AIL stands for AQL intermediate language*/

	if(!yyparse())
		printf("SQL parse worked\n");
	else
		printf("SQL parse failed\n");
	free(received_msg);//free memory alloc by malloc
	return(0); //all ok
}
