/*
* querymanagerrpc_client.c: remote procedure, remote invocation
* of getqueryid_rpc.c
* the code based on sample RPC client server at
* www.cs.cf.ac.uk/Dave/C/node34.html#ch:rpcgen
* linked with libnsl library for basic RPC types.
*/
#include<rpc/rpc.h>
#include<stdio.h>
#include<string.h>
#include<sys/syslog.h>

/*
* Get Query ID from the RPC server.
* Input ip address of the host, sleep time in milliseconds.
* returns query id as a C string.
*/
char *
querymanager_get_id(char *host, int sleep_millis);
