/*
* Compiler Client - Query Manager tool
* Rovshen Nazarov. All Rights Reserved. Baylor University. June 2014.
*/
// libraries for networking
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>

//standart libraries
#include <stdio.h>
#include <stdlib.h> //for exit command
#include <string.h> //for memset command
#include <sys/types.h> //for getpid
#include <unistd.h> //for get pid
//user defined libs
#include "cclientqm.h"
/*Quit with apropriate message*/
void err_quit(char *msg, int quit_stat){
	printf("%s", msg);
	exit(quit_stat);
}

char *connect_to_qm(int server_port, int receive_buff_size, char * ip_addr)
{
	/*client side networking*/
        int socket_fd = 0, bytes_read = 0;
        struct sockaddr_in server_addr;
	char *receive_buff = malloc(receive_buff_size * sizeof(char));
	//char receive_buff[receive_buff_size];

        memset(receive_buff, '0',sizeof(receive_buff)); //clear the buffer by setting it to all zeros
        if((socket_fd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
                err_quit("\n Error : Could not create socket \n", 1);
        memset(&server_addr, '0', sizeof(server_addr)); //clear server_addr variable
        server_addr.sin_addr.s_addr = inet_addr(ip_addr); // server is on the localhost
        server_addr.sin_family = AF_INET;
        server_addr.sin_port = htons(server_port);

        if (connect(socket_fd , (struct sockaddr *)&server_addr , sizeof(server_addr)) < 0)
                err_quit("\n Could not connect to the server.\n", 1);
        //request Query ID from the server
        int request = (int) getpid(); // send pid as request
        if (send(socket_fd, &request, sizeof(int), 0) < 0)
        	err_quit("Could not get Query ID", 1);
        //get server response
	int curr_bytes_read = 0;
	bytes_read = bytes_read + curr_bytes_read;
        if ((bytes_read = read(socket_fd, receive_buff, receive_buff_size-1)) < 0)
                err_quit("\n Could not read data from the server. \n", 1);
        receive_buff[bytes_read] = 0; // zero terminate bytes read
        close(socket_fd); //close client socket
	return receive_buff; 
}
