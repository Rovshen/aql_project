#ifndef COMPILER_HEADERS_H_INCLUDED
#define COMPILER_HEADERS_H_INCLUDED

#define MAX_BUF 1024 /*define max buffer for data exchange between C and JAVA*/

//include standard c libraries
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <syslog.h>
//include c libraries for pipe generation
//and processing
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
//for systemwide error reporting
#include <errno.h>

#include "helper_functions.h"
#include "cclientqm.h"
#include "querymanagerrpc_client.h"

/*set numbering for commands and lines in parser*/
//start at 1 for the commands numbering
int execNumb=1;
//start at 1 for in query numbering
int numb=1;
/*creat file*/
FILE *file;
/*create named pipe*/
int namedPipe; /*pipe for C to JAVA*/
int pipe_java_c;
char * fifo_c_java; /*to get results from C*/
char * fifo_java_c; /*to get results from JAVA*/
/*create stream to write to pipe*/
FILE *stream;
/*create buffer for the stream*/
char *buf;
/*len will hold length of the buffer*/
size_t len;
/*get line number from flex based tokenizer*/
extern yylineno; //from flex file

/*input 
* millisec- length of time to sleep, in miliseconds
* sec - time to sleep in sec
* Return status 0 ok, -1 error.
*/
int
sleepForTime(int sec, int milisec)
{
        struct timespec req = {0};
        req.tv_sec = sec;
        req.tv_nsec = milisec * 1000000L; /* 1 milisec has 1 000 000 nanosec */
        if(nanosleep(&req, (struct timespec *)NULL) < 0)
        {
                syslog(LOG_ERR, "could not wait for %d seconds and %d milliseconds", sec, milisec);
                return -1;
        }
        return 0;
}
#endif
