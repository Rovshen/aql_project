#!/bin/bash

#declare associate array BASH 4 required
COMMAND_ARR=();
declare -r command="$1"
declare -r pathToFiles="$2"
declare -r priorityLevel="$3"

usage(){
	# if not value supplied for optimization level, default optimization, which is no optimization is used
	# and Hadoop MapReduce based command is returned
	echo "Uasage:$0 <string command> <string path to the command lists>  (<int optimization level> | "" )"
	exit 1
}
getCommandList(){
	fileToRead=$(echo $command | tr '[:upper:]' '[:lower:]' | sed 's/$/List.txt/' | cut -d '/' -f3)
	fileToRead="`echo $pathToFiles/$fileToRead`"
	IFS=';' read -a COMMAND_ARR <<< "`cat $fileToRead | tr -d '\n'`"
	
}
getCommand(){
	case "$priorityLevel" in
	"1")
	    	echo "${COMMAND_ARR[1]}"
	    ;;
	"2" | "3")
	    	echo "bb"
	    ;;
	*)
	    	echo "${COMMAND_ARR[0]}"
	    ;;
	esac	
}
[[ "$#" < 2 ]] && usage
getCommandList
getCommand
