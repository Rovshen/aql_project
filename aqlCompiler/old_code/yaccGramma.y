/*
*
*Bison parser for AQL - Analytical Querty Language.
*Designed by Rovshen Nazarov. All rights reserved.
*
*This parser reads series of tokens produced by tokanizer
*and tries to determine the grammatical struct based on
*grammer defined below.
* Rovshen Nazarov. All Rights Reserved. Created: May 30, 2014.
*/
%{
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
//#include <ctype.h>
/*called on error*/
void yyerror(char *s);
/*call on gramma production*/
void emit(char *s, ...);
/*set numbering for commands and lines in parser*/
int execNumb=1;
int numb=1;
/*creat file*/
FILE *file; 
/*create named pipe*/
int namedPipe;
/*create stream to write to pipe*/
FILE *stream;
char *buf;
size_t len;
/*get line number from tokenizer*/
extern yylineno;
%}
%union {
        char *strval;
        double floatval;
        int numVal;
}
%start simple_query

%token L_FROM
%token <strval> L_GRAPH_ID
%token L_UNION
%token L_INTER
%token L_DIFF
%token L_EXIT

/* operator-precedence to avoid ambigouty
 * top-0: 
 *     1: 
 *     2: 
*/
/*the explicit precedence is provided by the %prec declaration; otherwise the precedence of the last terminal in the production is used*/
/*In the event of a shift/reduce conflict, the precedence of the production which could be reduced is compared to the precedence of the lookahead terminal which could be shifted, and whichever has the greater precedence wins*/
%left L_INTER /*Third*/
%left L_DIFF /*Second*/
%left L_UNION /*First*/
%%
simple_query	: error ';' { yyerrok; yyclearin; exit(1);} | L_EXIT ';' { fclose(file); fclose(stream); free(buf); close(namedPipe); exit(0);}
		| {fprintf(file,"BEGIN\n");} L_FROM expression ';' {emit("QUERY"); fprintf(file,"END\n"); numb=1;execNumb=1;} simple_query
		;

expression      : '(' expression ')'
		| expression L_INTER expression {emit("%d INTERSECTION",execNumb); execNumb++; numb++;}
		| expression L_DIFF expression {emit("%d DIFFERENCE",execNumb); execNumb++; numb++;}
		| expression L_UNION expression {emit("%d UNION",execNumb); execNumb++; numb++;}
		| L_GRAPH_ID {emit("GRAPH_ID %s", $1); free($1); numb++;}
                ;

%%

void
emit(char *s, ...)
{
	va_list ap;
	/*write to st out*/
	va_start(ap, s);
	printf("%d: ", numb);
	vfprintf(stdout, s, ap);
	printf("\n");
	va_end(ap);
	/*write to a pipe*/
	va_start(ap, s);
	/*write to stream*/
        fprintf(stream, "%d: ", numb);
        vfprintf(stream, s, ap);
        fprintf(stream, "\n");
	/*flush data to buffer*/
	int stat = fflush(stream);
	//printf ("buf=%s", buf); //for debugging
	if(stat != 0)
		exit(1);//exit on flush error
	/*write data from buffer to pipe*/
	write(namedPipe, buf, len);
	/*rewind to the position of the stream*/
	fseeko(stream, 0, SEEK_SET);	
        va_end(ap);
	/*write to a file*/
	va_start(ap, s);
	fprintf(file,"%d: ", numb);
	vfprintf(file, s, ap);
	fprintf(file,"\n");
	va_end(ap);
}


int yywrap()
{
        /*end of reading stream*/
        return 1;
}

void main(int argc, char **argv)
{
	/*open dynamically expandable stream*/
        stream = open_memstream(&buf, &len);
        if(stream == NULL)
                exit(1);//exit on stream error
	/*if files exists erases file data if any*/
	file = fopen("intermediateCode.AIL","w"); /*AIL stands for AQL intermediate language*/
	/*pathname, flags*/
	namedPipe = open("tempPipeFIFO", O_WRONLY);
	if(!yyparse())
		printf("SQL parse worked\n");
	else
		printf("SQL parse failed\n");

}
