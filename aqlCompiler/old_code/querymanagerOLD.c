#include "apue.2e/include/apue.h"

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <syslog.h>
#include <fcntl.h>
#include <sys/resource.h>
#include <sys/stat.h>
#include <string.h>
#include <errno.h>
#include <pthread.h>

#define LOCKFILE "/var/run/aqlquerymanager.pid" /*file to lock to ensure only one daemon is running*/
#define LOCKMODE (S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH)

/*
* Created based on the daemon design described in the book by Stevens
* "Advanced Programming in the UNIX® Environment, Second Edition".
* Rovshen Nazarov. Baylor University. June 2014. All Rights Reserved.
*/

sigset_t mask;

/*
* Create a Read Write lock for a given file descriptor.
*/
int lockfile(int fd)
{
	struct flock file_lock;
	file_lock.l_type = F_WRLCK; /*lock file for read write*/
	file_lock.l_start = 0;
	file_lock.l_whence = SEEK_SET;
	file_lock.l_len = 0;
	return(fcntl(fd, F_SETLK, &file_lock));
}

/*
 Check if the daemon already running by using file lock and 
 writing pid into that file.
*/
int already_running(void)
{
	int file_descriptor;
	char buff[16];

	file_descriptor = open(LOCKFILE, O_RDWR|O_CREAT, LOCKMODE);
	if (file_descriptor < 0){
		syslog(LOG_ERR, "can not open %s: %s", LOCKFILE, strerror(errno));
		exit(1);
	}
	if (lockfile(file_descriptor) < 0){
		if (errno == EACCES || errno == EAGAIN){ /*if file locked already*/
			close(file_descriptor);
			return(1);/*an instance of daemon already running*/
		}
		syslog(LOG_ERR, "can not lock %s: %s", LOCKFILE, strerror(errno));
		exit(1);
	}
	ftruncate(file_descriptor, 0); /*remove any old data from the lockfile*/
	sprintf(buff, "%ld", (long)getpid());
	write(file_descriptor, buff, strlen(buff)+1);
	return(0);
}

/*
 Reread service configuration on restart.
*/
void reread(void)
{
	//read configuration updates
}

/*add program id -ident with the log messages*/
//void openlog(const char *ident, int option, int facility);/*called automatically on syslog*/
/*system wide logging*/
//void syslog(int priority, const char *format, ...);/*write to syslog daemon*/
//void closelog(void);/*closees descriptor that was being used to communicate with syslogd*/
//int setlogmask(int maskpri);/*set log priority for this process*/

/*
 Get and process user signals to this service/daemon.
 supported signals are reload, stop.
*/
void *thr_fn(void *arg)
{
	int err, signo;

	for(;;){
		err = sigwait(&mask, &signo);
		if (err != 0){
			syslog(LOG_ERR, "sigwait failed");
			exit(1);
		}

		switch (signo){
		case SIGHUP:
			syslog(LOG_INFO, "Reloading configuration file");
			reread(); break;
		case SIGTERM:
			syslog(LOG_INFO, "Got exit signal. Stopping daemon.");
			exit(0);
		default:
			syslog(LOG_INFO, "Unexpected signal %d\n", signo);
		}
	}
	return(0);
}

void startquerymanager(const char *cmd)
{
	int			i, fd0, fd1, fd2;
	pid_t			pid;
	struct rlimit		rl;
	struct sigaction	sa;
	/*Clear file creation mask.*/
	umask(0);
	/*Get max number of file descriptors.*/
	if (getrlimit(RLIMIT_NOFILE, &rl) < 0){
		err_quit("%s: can not get file limit.", cmd);
	}
	/*Loose controlling TTY by cloning the process*/
	if ((pid = fork()) < 0){
		err_quit("%s: can not fork.", cmd);
	}else if (pid != 0){
		exit(0); /*exit parent*/
	}
	setsid(); /*create new session*/
	/*Prevent future opens from allocating cotrolling TTY to this daemon*/
	sa.sa_handler = SIG_IGN;
	sigemptyset(&sa.sa_mask);
	sa.sa_flags = 0;
	if (sigaction(SIGHUP, &sa, NULL) < 0){
		err_quit("%s: can not ignore SIGHUP.");
	}
	if ((pid = fork()) < 0){
		err_quit("%s: can not fork.", cmd);
	} else if (pid != 0){
		exit(0); /*exit parent*/
	}
	
	/*change working directory to root, to allow file systems to be unmounted*/
	if (chdir("/") < 0){
		err_quit("%s: can not change directory to /");
	}

	/*close all open file descriptors in the process*/
	if (rl.rlim_max == RLIM_INFINITY){
		rl.rlim_max = 1024;
	}
	for(i = 0; i < rl.rlim_max; i++){
		close(i);
	}

	/*Attach file descriptors to /dev/null*/
	fd0 = open("/dev/null", O_RDWR);
	fd1 = dup(0);
	fd2 = dup(0);
	/*init log file*/
	openlog(cmd, LOG_CONS, LOG_DAEMON);
	if (fd0 != 0 || fd1 != 1 || fd2 != 2){
		syslog(LOG_ERR, "unexpected file descriptors %d %d %d", fd0, fd1, fd2);
		exit(1);
	}
}

int main(int argc, char *argv[])
{
	int		err;
	pthread_t	tid;
	char		*cmd;
	struct sigaction sa;
	printf("I am in main");
	if ((cmd = strrchr(argv[0], '/')) == NULL){
		cmd = argv[0];
	} else {
		cmd++;
	}
	/*make the process daemon/service*/
	startquerymanager(cmd);
	/*Ensure that only one instance of the service is running.*/
	if (already_running()){
		syslog(LOG_ERR, "AQL Query Manager service already running");
		exit(1);
	}
	/*Restore SIGHUP and block all signals*/
	sa.sa_handler = SIG_DFL;
	sigemptyset(&sa.sa_mask);
	sa.sa_flags = 0;
	if (sigaction (SIGHUP, &sa, NULL) < 0){
		err_quit("%s: can not restore SIGHUP default.");
	}
	sigfillset(&mask);
	if ((err = pthread_sigmask(SIG_BLOCK, &mask, NULL)) != 0){
		err_exit(err, "SIG_BLOCK error.");
	}
	/*
	* Create a thread to handle SIGHUP and SIGTERM signals 
	* from the user to this process
	*/
	err = pthread_create(&tid, NULL, thr_fn, 0);
	if (err != 0){
		err_exit(err, "can not create thread");
	}
	exit(0);
}
