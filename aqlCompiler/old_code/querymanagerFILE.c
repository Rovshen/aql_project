#include "apue.2e/include/apue.h"

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <syslog.h>
#include <fcntl.h>
#include <sys/resource.h>
#include <sys/stat.h>
#include <string.h>
#include <errno.h>
#include <pthread.h>
#include <stdarg.h>
#include <sys/types.h>


#define LOCKFILE "/var/run/aqlquerymanager.pid" /*file to lock to ensure only one daemon is running*/
#define LOCKMODE (S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH)

/*
* Created based on the daemon design described in the book by Stevens
* "Advanced Programming in the UNIX® Environment, Second Edition".
* Rovshen Nazarov. Baylor University. June 2014. All Rights Reserved.
*/

sigset_t mask;

/*
* Exit service on error.
*/
void error_quit(void)
{
	if (remove(LOCKFILE) != 0)
		syslog(LOG_ERR, "could not remove the file %s", LOCKFILE);
	closelog();
        exit(1);
}

/*
 Check if the daemon already running by using file lock and 
 writing pid into that file.
*/
int already_running(void)
{
	int file_descriptor, lock_stat;
	char buff[16];
	/* check if file exist*/
	if(access(LOCKFILE, F_OK) != -1 )
		return(1); /*process is already running*/
	else	/*create the file if it does not exist*/
		file_descriptor = open(LOCKFILE, O_CREAT, LOCKMODE);
	if (file_descriptor < 0){
		syslog(LOG_ERR,"can not create %s: %s", LOCKFILE, strerror(errno));
		exit(1);
	}
	file_descriptor = open(LOCKFILE, O_RDWR);
	ftruncate(file_descriptor, 0); /*remove any old data from the lockfile*/
	sprintf(buff, "%ld", (long)getpid());
	write(file_descriptor, buff, strlen(buff)+1);
	return(0);
}

/*
 Reread service configuration on restart.
*/
void reread(void)
{
	//read configuration updates
}

/*
 Get and process user signals to this service/daemon.
 supported signals are reload, stop.
*/
void *thr_fn(void *arg)
{
	int err, signo;

	for(;;){
		err = sigwait(&mask, &signo);
		if (err != 0){
			syslog(LOG_ERR,"sigwait failed");
			error_quit();
		}

		switch (signo){
		case SIGHUP:
			syslog(LOG_INFO, "Reloading configuration file");
			reread(); break;
		case SIGTERM:
			syslog(LOG_INFO, "Got exit signal. Stopping daemon.");
			exit(0);
		default:
			syslog(LOG_INFO, "Unexpected signal %d\n", signo);
		}
	}
	return(0);
}

void startquerymanager(const char *cmd)
{
	int			i, fd0, fd1, fd2;
	pid_t			pid, sid_new;
	struct rlimit		rl;
	struct sigaction	sa;

	/*Clear file creation mask.*/
	umask(0);
	/*Get max number of file descriptors.*/
	if (getrlimit(RLIMIT_NOFILE, &rl) < 0){
		syslog(LOG_ERR,"%s: can not get file limit.", cmd);
		error_quit();
	}
	/*Loose controlling TTY by cloning the process*/
	if ((pid = fork()) < 0){
		syslog(LOG_ERR,"%s: can not fork.", cmd);
		error_quit();
	}else if (pid != 0){
		exit(0); /*exit parent*/
	}
	sid_new = setsid(); /*create new session*/
	if (sid_new < 0){
		syslog(LOG_ERR,"cat not get new session.");
		error_quit();
	}
	/*Prevent future opens from allocating cotrolling TTY to this daemon*/
	sa.sa_handler = SIG_IGN;
	sigemptyset(&sa.sa_mask);
	sa.sa_flags = 0;
	if (sigaction(SIGHUP, &sa, NULL) < 0){
		syslog(LOG_ERR,"can not ignore SIGHUP.");
		error_quit();
	}
	if ((pid = fork()) < 0){
		syslog(LOG_ERR,"%s: can not fork.", cmd);
		error_quit();
	} else if (pid != 0){
		exit(0); /*exit parent*/
	}
	
	/*change working directory to root, to allow file systems to be unmounted*/
	if (chdir("/") < 0){
		syslog(LOG_ERR,"can not change directory to /");
		error_quit();
	}

	/*close all open file descriptors in the process*/
	if (rl.rlim_max == RLIM_INFINITY){
		rl.rlim_max = 1024;
	}
	for(i = 0; i < rl.rlim_max; i++){
		close(i);
	}

	/*Attach file descriptors to /dev/null*/
	fd0 = open("/dev/null", O_RDWR);
	fd1 = dup(0);
	fd2 = dup(0);
	/*init log file with identity, options and facility*/
	openlog(cmd, LOG_CONS, LOG_DAEMON);
	if (fd0 != 0 || fd1 != 1 || fd2 != 2){
		syslog(LOG_ERR,"unexpected file descriptors %d %d %d", fd0, fd1, fd2);
		error_quit();
	}
}

int main(int argc, char *argv[])
{
	int		err;
	pthread_t	tid;
	char		*cmd;
	struct sigaction sa;

	if ((cmd = strrchr(argv[0], '/')) == NULL){
		cmd = argv[0];
	} else {
		cmd++;
	}
	/*make the process daemon/service*/
	startquerymanager(cmd);

	/*Ensure that only one instance of the service is running.*/
	if (already_running()){
		syslog(LOG_ERR,"AQL Query Manager service already running");
		closelog();
		exit(1);
	}
	/*Restore SIGHUP and block all signals*/
	sa.sa_handler = SIG_DFL;
	sigemptyset(&sa.sa_mask);
	sa.sa_flags = 0;
	if (sigaction (SIGHUP, &sa, NULL) < 0){
		syslog(LOG_ERR,"can not restore SIGHUP default.");
		error_quit();
	}
	sigfillset(&mask);
	if ((err = pthread_sigmask(SIG_BLOCK, &mask, NULL)) != 0){
		syslog(LOG_ERR, "SIG_BLOCK error. %d", err);
                error_quit();
	}
	/*
	* Create a thread to handle SIGHUP and SIGTERM signals 
	* from the user to this process
	*/
	err = pthread_create(&tid, NULL, thr_fn, 0);
	if (err != 0){
		syslog(LOG_ERR, "can not create thread %d", err);
		error_quit();

	}
	
	closelog();
	exit(0);
}
