import java.util.*;
import java.io.*;

public class CodeGenerator {
	/*Uasage:./GET_COMMAND.sh <string command> <string path to the command lists>  (<int optimization level> |  )*/
	private static String COMMANDS_TO_EXEC = "commandMappers/GET_COMMAND.sh UNION commandMappers  1"; 
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if(args.length != 1){
			System.out.println("usage: CodeGenerator <AQL intermediate file>");
			System.exit(1);
		}		
 		try{
			Runtime runT = Runtime.getRuntime();
			Process processExec = runT.exec(COMMANDS_TO_EXEC);
			StreamProcessor errStream = new StreamProcessor(processExec.getErrorStream());
			StreamProcessor inpStream = new StreamProcessor(processExec.getInputStream());
			errStream.start();
			inpStream.start();
			int exitVal = processExec.waitFor();
			System.out.println("Command exit value: " + exitVal);
		}catch (Throwable e) {
			e.printStackTrace();
		}		
	}//end main 
}// end CodeGenerator class

class StreamProcessor extends Thread{
	InputStream inStream;
	
	public StreamProcessor(InputStream inInputStream) {
		this.inStream = inInputStream;
	}		
	public void run(){
		try{
			InputStreamReader inpStreamReader = new InputStreamReader(inStream);
			BufferedReader buffReader = new BufferedReader(inpStreamReader);
			String readLine = null;
			
			while( (readLine = buffReader.readLine()) != null ){
				System.out.println(">>" + readLine);
			}
		}catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}//end StreamProcessor class