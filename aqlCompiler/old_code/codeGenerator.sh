#!/bin/bash
#Code generator based on AIL (AQL Intermediate Language)
#constants
declare -r aqlBashCommand="aqlBashCommand";
declare -r commandMappersPath="commandMappers";
export commandMappersPath;
intermediateFile=$1;
queryFilesArray=();

usage(){
        echo "Usage: $0 <intermediateFile.AIL>"
        exit 1
}
splitIntoQueries(){
	cat "$intermediateFile" | awk -vRS="END" '{print $0>NR".aqlquery"}';
}
getQueryFiles(){
	queryFilesArray=(`ls *.aqlquery | tr s' ' '\n'`);
}

processEachQuery(){
for query in "${queryFilesArray[@]}"; do
	local name="arr$(echo $query | tr -d '.')";
	IFS=';' read -a $name <<< "`cat $query | grep -v '^$' | tr '\n' ';'`";
done

for query in "${queryFilesArray[@]}"; do
        local name="arr$(echo $query | tr -d '.')";
	refArr=$name[@]
	i=0;
	echo ${!refArr} | tr ':' '\n' | sed -e 's/^[0-9: ]*//'  
	local tempArr=(`echo ${!refArr} | tr ':' '\n' | sed -e 's/^[0-9: ]*//'`);
	for elem in "${!refArr}"; do		
		if [[ $elem != *.sh* ]]
                then
			#echo "$elem"; 
			echo "";
                fi
		if [[ $elem == *.sh* ]]
		then
			local command="`echo $elem | cut -d ' ' -f3 `";
			commandPriority="`echo $elem | cut -d ' ' -f2 `";
			#if it is first command in the query get two graphs
			if(($commandPriority == 1))
			then
				#echo "";
				echo "${tempArr[((i-2))]}";
				echo "${tempArr[((i-1))]}";
			else	
				#echo "";
				echo "${tempArr[((i-2))]}";
                                echo "${tempArr[((i-1))]}";
			fi
			#otheriwse get a graph and result of the previous command
			commandPosition="`echo $elem | cut -d ' ' -f1 `";
			#the command could be followed by the option number, where default is invoking MR
			result=$(."/$commandMappersPath/$command" option);
			echo "$commandPosition $commandPriority $result"
		fi
		((i+=1));
	done
	echo ""
done
}

[[ $# -ne 1 ]] && usage
splitIntoQueries
getQueryFiles
processEachQuery

#remove temp files
rm *.aqlquery;
