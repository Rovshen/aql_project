#!/bin/bash
inputAtt=$1
runnableJar="aqlserverapp-0.1v-jar-with-dependencies.jar"
clear
clear
echo "TESTING env HADOOP"
#start-dfs.sh
#start-yarn.sh
#hdfs dfsadmin -safemode leave
echo "Usage: $0 \"debug=true purgeOperationData=true purgeTools=true nullOptimization=true codeGenMode=single|all\""
# dev run with assertions
echo "

MUST RUN source ~/.bashrc ON SYSTEM REBOOT FOR GRAPHLAB TO WORK

"
while [ ! -f $runnableJar ]
do
  sleep 2
  echo "waiting.. for runnable"
done
ls -l $runnableJar
java -Xmx8g -Xms8g -ea -jar $runnableJar $inputAtt

# production run
#java -jar aqlserverapp-0.1v-jar-with-dependencies.jar
