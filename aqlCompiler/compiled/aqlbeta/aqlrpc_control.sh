#!/bin/bash
### BEGIN INIT INFO
# Provides:          RPC Query Manager controller
# Short-Description: Execute the query manager's Remote Procedures.
# Description:
### END INIT INFO

# Author Rovshen Nazarov. June 2014. Baylor University

DAEMON="querymanagerrpc_server"
AQL_NAMED_PIPE="/tmp/aqlquerymanager_namedpipe"

# Start Query Manager Service
do_start() {
        # Start the daemon
        echo -n "Starting $DAEMON"        
	# Create named pipe
	if [[ ! -p $AQL_NAMED_PIPE ]]; then
	    mkfifo $AQL_NAMED_PIPE
	else
		echo "RPC server is already running. Restart if needed"
	fi
        sudo ./$DAEMON &
        echo
}

# Stop Query Manager Service
do_stop() {
	local pid_array=(`ps -aelf | grep ./$DAEMON | grep -v "color" | cut -d' ' -f9`)
	for pid in "${pid_array[@]}"; do 
		sudo kill -9 $pid > /dev/null 2>&1
	done
	sudo rm -f $AQL_NAMED_PIPE
}

# perform action based on the call
case "$1" in
  start)
        do_start
        ;;
  stop)
        do_stop
        ;;
  restart)
	do_stop
	do_start
	;;
  *)
        echo "Usage: $0 start|stop|restart" >&2
        exit 3
        ;;
esac
exit 0
~             
