This project will contain tools that will be used for AQL - Analytical Query Language for NoSQL data processing.

AQL should be

Easily extensible
Highly object oriented
Modularity, Extensibility

See documentation at [this link](http://cs.ecs.baylor.edu/~nazarov/)

Tools:
Yacc Based Grammar Generator
resMeas.sh Resource Measurement API - for measuring what resources are available CPU, GPU, RAM, HDD, HDFS, Network Speed, etc

MySQL Ubuntu:

Problem: can't connect to local MySQL server through socket '/var/run/mysqld/mysqld.sock' [2]

Solution: The file should be created automatically when the MySql server starts

1) Check permissions for /var/run/mysqld/ should be mysql:mysql, if not doe below
sudo chown -R mysql:mysql /var/run/mysqld/
2) Restart the MySql server and mysqld.pid  mysqld.sock files will be created for you in the /var/run/mysqld/
    
sudo service mysql stop
sudo /etc/init.d/apparmor reload
sudo service mysql start

TO add mysql to start up permanently
sudo update-rc.d mysql defaults