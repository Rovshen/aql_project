
/**
* Use to send a datagram to a host but you don't know if it supports IPv4 or IPv6, use this function
* returns the address family returned by a DNS lookup
*
* @retval LIBSOCKET_IPv4 Host supports only IPv4
* @retval LIBSOCKET_IPv6 Host supports IPv6 (usually it supports IPv4 then, too)
* @retval <0 Error.
*/
int get_address_family(const char* hostname)


/**
 * @brief Create and connect a new TCP/IP socket
 *
 * This function returns a working client TCP/IP socket.
 *
 * @param host The host the socket will be connected to (everything resolvable, e.g. "::1", "8.8.8.8", "example.com")
 * @param service The host's port, either numeric or as service name ("http").
 * @param proto_osi3 `LIBSOCKET_IPv4` or `LIBSOCKET_IPv6`.
 * @param flags Flags to be passed to `socket(2)`. Most flags are Linux-only!
 *
 * @return A valid socket file descriptor.
 */
int create_inet_stream_socket(const char* host, const char* service, char proto_osi3, int flags)


/**
For TCP use proto_osi4 
 case LIBSOCKET_TCP:
 type = SOCK_STREAM;

*/

/**
to close both inet socket read and write
use 0011 to close read 0001 to close write 0010
*/
shutdown_inet_stream_socket(int sfd, int method)
