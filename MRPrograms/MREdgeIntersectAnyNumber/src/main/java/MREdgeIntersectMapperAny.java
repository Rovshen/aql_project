import java.io.IOException;
import java.util.StringTokenizer;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

//Templates must match default (LongWritable, Text) for input
//Templates must match values set in main for output
public class MREdgeIntersectMapperAny extends Mapper<Object, Text, Text, IntWritable>{
    private final Text edge = new Text();
    private final IntWritable zero = new IntWritable(0); 

    //Parameter types must match templates
    @Override
    public void map(final Object key, final Text value, final Context context
            ) throws IOException, InterruptedException {
        final StringTokenizer itr = new StringTokenizer(value.toString());
        while (itr.countTokens() >= 2) {
            edge.set(itr.nextToken() + " " + itr.nextToken());
        }
        context.write(edge,zero); 
    }
}
