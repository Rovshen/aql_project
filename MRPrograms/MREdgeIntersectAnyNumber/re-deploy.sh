#!/bin/bash

# default to run Maven (-o for offline)
# compiles, build and install the build result
testPath="/home/hduser/Desktop/aql_project/aqlCompiler/compiled/aqlbeta/tools/hadoop/"
execJar="MREdgeIntersectAny-0.0.1-SNAPSHOT-jar-with-dependencies.jar"
currDir="`pwd`"

mvn clean 
pid=$!
wait $pid
mvn package -DtestFailureIgnore=true
pid=$!
wait $pid
rm "$testPath"/"$execJar"
cp target/"$execJar" "$testPath"
