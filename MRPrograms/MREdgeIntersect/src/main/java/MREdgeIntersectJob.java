import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;
/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class MREdgeIntersectJob {

    public static void main(final String[] args) throws IOException {
        final Configuration conf = new Configuration();
        final String[] otherArgs = new GenericOptionsParser(conf, args).getRemainingArgs();
        if (otherArgs.length < 3) {
            // we allow arbitrary number of operands.
            System.err.println("Usage: edgeIntersect <dataPath1> (<dataPath2>)+ <resultData>");
            System.exit(2);
        }
        // - 1 because of the result operand
        final int operandCount = otherArgs.length - 1;
        conf.set("graphCount","" + operandCount);

        final Job job = new Job(conf, "Edge Intersect");
        job.setJarByClass(MREdgeIntersectJob.class);

        // Set the mapper & reduce class (always)
        // Set the combiner class if needed (or same as reducer)
        job.setMapperClass(MREdgeIntersectMapper.class);
        //        job.setCombinerClass(CountReducer.class);
        job.setReducerClass(MREdgeIntersectReducer.class);

        // Set the output key,value classes for the Mapper (always)
        // Must match the templates in the mapper class
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(IntWritable.class); 

        // Set the output key,value classes for the Reducer (always)
        // The input types of the reduce must match the output types if a combiner is used
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);

        try {
            // we allow arbitrary number of operands
            for (int i = 0; i < operandCount; i++) {
                FileInputFormat.addInputPath(job, new Path(otherArgs[i]));
            }

            // result operand is the last arg
            FileOutputFormat.setOutputPath(job, new Path(otherArgs[otherArgs.length - 1]));
            System.exit(job.waitForCompletion(true) ? 0 : 1);

        } catch (final IllegalArgumentException e) {
            e.printStackTrace();
            System.exit(2);
        } catch (final IOException e) {
            e.printStackTrace();
            System.exit(2);
        } catch (final ClassNotFoundException e) {
            e.printStackTrace();
            System.exit(2);
        } catch (final InterruptedException e) {
            e.printStackTrace();
            System.exit(2);
        }
    }

}
