import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

//Templates must match values set in main
public class MREdgeIntersectReducer extends Reducer<Text,IntWritable,Text,IntWritable>{
    private final IntWritable size = new IntWritable();

    //Parameter types must match templates
    @Override
    public void reduce(final Text key, final Iterable<IntWritable> values,
            final Context context
            ) throws IOException, InterruptedException {

        final Map<Integer,Integer> lookup = new HashMap<Integer,Integer>(10);
        final Configuration conf = context.getConfiguration();
        final int graphCount = Integer.parseInt(conf.get("graphCount"));
        int edgeCount = 0;

        //size.set(graphCount);
        //context.write(key,size);

        for (final IntWritable I : values) {
            edgeCount++;
        }
        size.set(edgeCount);
        //context.write(key,size);
        if (edgeCount == graphCount) {
            context.write(key,size);
        }
    }
}