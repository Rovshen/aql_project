
 @Override
    public List<OrderingCommandGroup> getOrderingCommandGroups(
            final Queue<Command> commands) {

        final List<OrderingCommandGroupBuilder> groupList = new ArrayList<>();
        final Map<CommandType, OrderingCommandGroupBuilder> commandGroupTemp = new HashMap<>();
        CommandType prevCommType = null; // previous ordering type in the
                                         // map
        int subQuerySeqNumber = Command.DEFAULT_SUB_QUERY_NUMBER;

        final Map<Integer, List<Command>> queryLevelCommandList = getLevelCommandListMap(commands);

        // process all sub query levels, main query is level -1
        for (final Integer level : queryLevelCommandList.keySet()) {

            final List<Command> commandsList = queryLevelCommandList.get(level);
            // process all commands from the same level in one pass
            for (final Command command : commandsList) {

                final CommandType cType = command.getCommandTypeObj();

                // if this is the first time we see this command type
                if (!commandGroupTemp.containsKey(cType)
                        || subQuerySeqNumber != command.getSubQueryNumber()) {

                    subQuerySeqNumber = command.getSubQueryNumber();
                    // store the previous group if any
                    if (prevCommType != null) {
                        groupList.add(commandGroupTemp.get(prevCommType));
                        // clear the map to get the next group
                        commandGroupTemp.clear();
                    }
                    prevCommType = cType;

                    // create an object to build a new group based on command
                    // type
                    final Queue<Command> commandsSameOrdering = new LinkedList<>();
                    final List<Operand> oSameGroup = new LinkedList<>();
                    final OrderingCommandGroupBuilder ocgb = new OrderingCommandGroup.OrderingCommandGroupBuilder(
                            oSameGroup, commandsSameOrdering, cType);
                    commandGroupTemp.put(cType, ocgb);
                }
                // we are looking on the same type
                logger.info("Command " + command.getCommandSeqNumber()
                        + " was added to the group of type "
                        + cType.getTypeName());
                commandGroupTemp.get(cType).getNestedCommands().add(command);
            } // end commands processing

            // store the last group
            if (prevCommType != null) {
                groupList.add(commandGroupTemp.get(prevCommType));
                commandGroupTemp.clear(); // clear the map
            }
        } // end level processing
          // build the command type groups
        final List<OrderingCommandGroup> commandGroups = buildCommandGroups(groupList);
        return commandGroups;
    }


/**
     * Create buckets/groups of the {@link Command}s based on the
     * {@link Command#getSubQueryNumber()}, which represents sub query level.
     * The sub query level for the main query is set to default at
     * {@link Command#DEFAULT_SUB_QUERY_NUMBER}.
     * 
     * @param commands
     * @return map level -> command list
     */
    private Map<Integer, List<Command>> getLevelCommandListMap(
            final Queue<Command> commands) {

        final Map<Integer, List<Command>> queryLevelCommandList = new TreeMap<>();

        for (final Command c : commands) {

            // if no entry for this level create one
            if (!queryLevelCommandList.containsKey(c.getSubQueryNumber())) {
                queryLevelCommandList.put(c.getSubQueryNumber(),
                        new ArrayList<Command>());
            }
            // add command to the level
            queryLevelCommandList.get(c.getSubQueryNumber());
        }

        return queryLevelCommandList;
    }
