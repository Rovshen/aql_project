/**
     * Subroutine. Stateless.
     * Add {@link Operand}s to {@link Command}. {@link Operand}s will have the
     * same order as defined in the operandsCopy {@link Deque}.
     * 
     * PRE: {@link CommandType} must be defined for {@link Command}s.
     * 
     * POST: replaces old {@link Operand}s in the {@link Command} if any with
     * the new set of {@link Operand}s.
     * 
     * NOTE: populating {@link Command}s with {@link Operand}s should be
     * separate from combining {@link Command}s based on
     * {@link CommandAlgorithm}
     * 
     * @param commands
     * @param operandsOrdered
     * @throws AQLException
     */
    private void addOperandsToCommands(final Queue<Command> commands,
            final Deque<Operand> operandsOrdered) throws AQLException {

        Operand intermedOperToSkip = null;
        // hold out operands based on sub query sequence number
        final Stack<Operand> operandsToHold = new Stack<>();

        // IDs retrieved in the reverse order e.g. id2 id1 for id1 DIFF id2
        for (final Command command : commands) {

            // put back any held out operands
            if (!operandsToHold.isEmpty()) {
                while (!operandsToHold.isEmpty()) {
                    // pop and push operand, dequeue acts as stack here
                    operandsOrdered.offerFirst(operandsToHold.pop());
                }
            }

            // Add back any interm operand
            if (intermedOperToSkip != null) {
                operandsOrdered.offerFirst(intermedOperToSkip);
            }
            intermedOperToSkip = null; // reuse the reference

            final List<Operand> operandsCurr = new ArrayList<>();
            if (command.getCommandTypeObj() == null) {
                throw new AQLException("CommandType cannot be null.");
            }

            // get operands for the command
            for (int i = 0; i < command.getCommandTypeObj().getMinNumbOper(); i++) {

                final Operand oToAdd = operandsOrdered.poll();

                // check if we shall hold out this operand
                if (holdOut(oToAdd)) {

                    operandsToHold.push(oToAdd);
                    i--; // increase loop by one step to get enough operands
                    continue; // get the next operand
                }
                // If interm result and not ready look ahead if enough ready
                // operands.
                if (oToAdd.getType() == OperandType.INTERMEDIATE_RESULT
                        && !oToAdd.getStatus()) {

                    final int operCountAhead = getOperandsCountUntilNextCommand(
                            operandsOrdered, command);
                    // check if enough ready operands
                    if (operCountAhead + operandsCurr.size() >= command
                            .getCommandTypeObj().getMinNumbOper()) {

                        // We have enough ready operands for this command.
                        intermedOperToSkip = oToAdd;
                        i--; // increase loop by one step to get enough operands
                        continue; // get the next operand
                    } else {

                        // add intermediate result to the curr command
                        operandsCurr.add(oToAdd);
                    }
                    // check the operand's parse line # is less than command's.
                } else if (oToAdd.getParseLineNumber() < command
                        .getParseLineNumber()) {

                    operandsCurr.add(oToAdd);

                } else { // do not allow other case

                    throw new AQLException("Incorrect Operand " + oToAdd
                            + " command is " + command);
                }
            }
            addOperandsToCommand(operandsCurr, command, operandsOrdered);
        } // end of populating this 
