#!/bin/bash

# default to run Maven (-o for offline)
# compiles, build and install the build result
testPath="/home/hduser/Desktop/aql_project/aqlCompiler/compiled/aqlbeta"
execJar="aqlserverapp-0.1v-jar-with-dependencies.jar"
projDir="/home/hduser/Desktop/aql_project/queryoptimizer"
currDir="`pwd`"

cd "$projDir"
rm "$testPath"/"$execJar"
mvn clean 
pid=$!
wait $pid
mvn package -DtestFailureIgnore=true
pid=$!
wait $pid
cp target/"$execJar" "$testPath"
# generate docs using maven
#mvn javadoc:javadoc
#xdg-open index.html
# copy docs to server
#scp -r target/site/apidocs/* nazarov@wind.ecs.baylor.edu:/home/faculty/nazarov/public_html/
# run on server in the folder /home/faculty/nazarov
# chmod -R 755 ~/public_html/
# done with docs
cd "$currDir"
