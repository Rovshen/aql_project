/**
 * This package provides services used by {@link edu.baylor.aql.services.intermlanggen.RequestParser} controller class
 * to perform parsing operations related to adding objects defined in entities package to the AQL system.
 * <p> 
 * Not all of the entities from the entities library need to be loaded. 
 * One can refer which entities are loaded based on the names of the service classes defined in this library.
 * <p> 
 * Any future extensions to AQL that will require parsing of additional object type, other than defined in entities
 * should be reflected in this library.
 */
package edu.baylor.aql.services.intermlanggen.load;