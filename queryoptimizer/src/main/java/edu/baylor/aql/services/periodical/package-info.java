/**
 * This package holds services that are designed to be invoked periodically, e.g. continuesly after some time interval or based on some schedule.
 * This services are similar to other periodical services, such as garbage collector in java.
 * This services provide way to delete unused or errand records and data from the system's metadata and storage.
 * This package can be extended with any other service that expected to run periodically.
 */
package edu.baylor.aql.services.periodical;