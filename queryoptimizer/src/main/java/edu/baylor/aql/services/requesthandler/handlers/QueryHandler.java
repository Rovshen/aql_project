package edu.baylor.aql.services.requesthandler.handlers;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.CancellationException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.log4j.Logger;

import edu.baylor.aql.bootstrap.AQLServerAppSingleton;
import edu.baylor.aql.bootstrap.BootstrapAQLServer;
import edu.baylor.aql.dataaccess.IMetadata;
import edu.baylor.aql.dataaccess.Metadata;
import edu.baylor.aql.dispatcher.AQLServerApp;
import edu.baylor.aql.entities.Element;
import edu.baylor.aql.entities.command.Command;
import edu.baylor.aql.entities.operands.Operand;
import edu.baylor.aql.entities.query.Query;
import edu.baylor.aql.services.commandexecutor.AbstractExecutor;
import edu.baylor.aql.services.commandexecutor.BashBatchExecutor;
import edu.baylor.aql.services.commandexecutor.CommandBatchExecutorController;
import edu.baylor.aql.services.commandexecutor.CommandExecutorController;
import edu.baylor.aql.services.intermlanggen.UtilParser;
import edu.baylor.aql.services.optimization.evaluating.IQuerySelectionService;
import edu.baylor.aql.services.optimization.evaluating.QuerySelectionService;
import edu.baylor.aql.services.query.IQueryService;
import edu.baylor.aql.services.query.QueryService;
import edu.baylor.aql.services.requesthandler.pipeline.CommandAlgorithmService;
import edu.baylor.aql.services.requesthandler.pipeline.ICommandAlgorithmService;
import edu.baylor.aql.services.requesthandler.pipeline.VisitorH;
import edu.baylor.aql.util.AQLException;
import edu.baylor.aql.util.Util;
import edu.baylor.aql.util.Util.ResultState;

/**
 * This service uses uses {@link CommandProcessor} to process each command in
 * the order defined by {@link CommandOrdering} service.
 * Process each AQL query composed of {@link Command}s and {@link Operand}s.
 * 
 * @author Rovshen Nazarov. Baylor University. Sep 17, 2014.All Rights Reserved.
 * 
 */
public final class QueryHandler extends VisitorH {

    /**
     * Used to log errors or information.
     */
    private static Logger logger = Logger.getLogger(QueryHandler.class
            .getName());
    private static final int POOL_SIZE = 10;
    private final ExecutorService execThreadPool;
    /**
     * AQL Server wide settings provided on the server start up.
     * Those settings define general settings that will be applied in different
     * places in the system.
     * <p>
     * The settings are accessed via singleton pattern, where the single
     * instance of the AQL Server is created on start up via
     * {@link BootstrapAQLServer} service and is accessed as needed.
     */
    private final AQLServerApp serverInstance = AQLServerAppSingleton
            .getInstance();

    public QueryHandler() {
        execThreadPool = Executors.newFixedThreadPool(POOL_SIZE);
    }

    @Override
    public void visit(final Element query, final CountDownLatch doneSignal)
            throws InterruptedException, AQLException {
        final IQuerySelectionService qSelectService = new QuerySelectionService();
        final IQueryService qService = new QueryService();

        try {

            Query queryToExec = null;
            // select query to execute based on the AQL server settings
            if ((AQLServerApp.Settings.CodeGenMode.ALL == serverInstance
                    .getSettings().getCodeGenMode())
                    && serverInstance.getSettings().isDebugMode()) {
                queryToExec = (Query) query;
            } else {
                // select highest progress query to execute
                queryToExec = qSelectService
                        .selectHighestProgressQuery((Query) query);
            }

            logger.info("\n\n\n\t\tThe query we chose to execute: "
                    + queryToExec);

            // process commands sequentially
            processSequencialBash(queryToExec, qService);

            // since in the caller we use the input Query
            // we need to update that input query
            qService.copyQuery(queryToExec, (Query) query);

            // an old way of processing commands in parallel
            // processBlockingList(queryToExec, qService);
        } finally {
            doneSignal.countDown();
        }
    }

    /**
     * Process the {@link Query} as a sequential bash command.
     * 
     * @param queryToExec
     * @param qService
     * @throws AQLException
     */
    private void processSequencialBash(final Query queryToExec,
            final IQueryService qService) throws AQLException {

        if (!queryToExec.isRepeated()) {
            final StringBuffer bashCommands = new StringBuffer();

            if (serverInstance.getSettings().isDebugMode()) {
                try {
                    processQueryDebugMode(queryToExec);
                } catch (final IOException e) {
                    throw new AQLException("Problem generating debug code "
                            + e.getMessage());
                }
            } else { // process query normally
                generateBashScriptQuery(bashCommands, queryToExec);
            }

            // process generated executables
            if ((!"".equals(bashCommands.toString()) && !queryToExec
                    .isErrState())
                    || serverInstance.getSettings().isDebugMode()) {
                executeBashFile(bashCommands, queryToExec);
            }

            if (!queryToExec.isErrState()) { // store final result
                qService.setResultForQuery(queryToExec);
            }
        }
    }

    /**
     * Generate either code for all {@link Query}ies or for a single
     * {@link Query}.
     * 
     * @param queryToExec
     * @throws AQLException
     * @throws IOException
     */
    private void processQueryDebugMode(final Query queryToExec)
            throws AQLException, IOException {

        final IMetadata dao = new Metadata();
        dao.openConnection();
        final String debugPathDir = dao.lookUpSystemSetting("debugPath",
                "linux");
        dao.closeConnection();

        // generate all queries's bash commands
        if (AQLServerApp.Settings.CodeGenMode.ALL == serverInstance
                .getSettings().getCodeGenMode()) {

            Query prev = queryToExec;
            long queryIndex = 1;
            do {
                debugRecordQueryCode(debugPathDir, queryToExec, queryIndex);
                queryIndex++;
            } while ((prev = prev.getNext()) != null);

        } else { // generate single query's bash commands
            debugRecordQueryCode(debugPathDir, queryToExec,
                    queryToExec.getQueryId());
        }

    }

    /**
     * Generate and record code of a {@link Query} into a given folder.
     * Note this method is expected to be used in debug mode only
     * 
     * @param debugPathDir
     * @param queryToExec
     * @param queryId
     * @throws AQLException
     * @throws IOException
     */

    private void debugRecordQueryCode(final String debugPathDir,
            final Query queryToExec, final long queryId) throws AQLException,
            IOException {

        final StringBuffer bashCommands = new StringBuffer();
        final StringBuilder debugPathDirQ = new StringBuilder(debugPathDir);
        final String extention = ".sh";
        debugPathDirQ.append(UtilParser.FOLDER_SEPARATOR_LINUX)
                .append(queryToExec.getClientSessionId())
                .append(UtilParser.FOLDER_SEPARATOR_LINUX).append(queryId)
                .append(extention);
        logger.info("Storing results to the file: " + debugPathDirQ);
        final File codeRecord = new File(debugPathDirQ.toString());

        final File parent = codeRecord.getParentFile();
        if (parent != null) { // create directory if does not exist
            parent.mkdirs();
        }
        if (!codeRecord.exists()) { // create the file if does not exits
            codeRecord.createNewFile();
        }
        generateBashScriptQuery(bashCommands, queryToExec);
        // store file ref for later management
        serverInstance.getSettings().getTempFiles().add(codeRecord);
        recordFile(codeRecord, bashCommands.toString());
    }

    /**
     * Write code to the file.
     * 
     * @param codeRecord
     * @param string
     * @throws IOException
     */
    private void recordFile(final File codeRecord, final String strToWrite)
            throws IOException {

        try (FileWriter writer = new FileWriter(codeRecord)) {
            writer.write(strToWrite);
            writer.flush();

        }
    }

    /**
     * Generate Code for the {@link Query}.
     * 
     * @param bashCommands
     * @param queryToExec
     * @throws AQLException
     */
    private void generateBashScriptQuery(final StringBuffer bashCommands,
            final Query queryToExec) throws AQLException {

        final Queue<Command> orderedCommands = queryToExec.getCommands();

        // Process command in the order.
        for (final Command command : orderedCommands) {
            if (!command.isStored()) {
                final CommandBatchExecutorController cExec = new CommandBatchExecutorController(
                        command, queryToExec, bashCommands);
                try {
                    cExec.process();
                } catch (InterruptedException | IOException e) {
                    queryToExec.setErrState(true);
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Executes the {@link Query} code and generates a temporary
     * using {@link File#createTempFile(String, String)}.
     * 
     * @param bashCommands
     * @param queryToExec
     * @throws AQLException
     */
    private void executeBashFile(final StringBuffer bashCommands,
            final Query queryToExec) throws AQLException {

        int execResult = Util.FAILED_STATUS;

        /**
         * TODO check this
         * Execute query command and check result. Generates temp file with the
         * executable. Only run the query if not in debug mode.
         */
        if (!serverInstance.getSettings().isDebugMode()) {
            final AbstractExecutor execCurrCommand = new BashBatchExecutor(
                    bashCommands.toString());
            final Future<Object> resultFutureCur = execThreadPool
                    .submit(execCurrCommand);
            try { // block here until result is ready
                execResult = (int) resultFutureCur.get();
            } catch (InterruptedException | ExecutionException e) {
                logger.error("Could not get result of the bash command."
                        + e.getMessage());
            }
        } else {
            execResult = Util.SUCCESS_STATUS;
        }
        recordBashCommandSuccess(execResult, bashCommands, queryToExec);
    }

    private void recordBashCommandSuccess(final int execResult,
            final StringBuffer bashCommands, final Query queryToExec)
            throws AQLException {

        final ICommandAlgorithmService caService = new CommandAlgorithmService();
        final Util.ResultState resState = new Util.ResultState();

        if (execResult == Util.SUCCESS_STATUS) {
            logger.info("Bash Command " + bashCommands.toString()
                    + " SUCCEEDED.");

            // record all operands
            for (final Command command : queryToExec.getCommands()) {
                if (!command.isStored()) {
                    caService.recordCommandResult(queryToExec, command);
                    if (command.getResult() == Util.FAILED_STATUS) {
                        throw new AQLException(
                                "Failed to store command result "
                                        + command.getResult().toString());
                    }
                    caService
                            .buildResultMessage(queryToExec, resState, command);
                    resState.msg.append(Util.LINE_SEPARATOR);
                }
            }
            sendUserMsg(queryToExec, resState.msg.toString());
        } else {
            resState.msg.append("Bash Command ")
                    .append(bashCommands.toString()).append(" FAILED.");
            queryToExec.setErrState(true);
            queryToExec.setStateMsg(resState.msg.toString());
            logger.error(resState.msg.toString());
        }
    }

    private void sendUserMsg(final Query queryToExec, final String string) {
        queryToExec.getClientSession().getChannel()
                .writeAndFlush(string + Util.LINE_SEPARATOR);
    }

    /**
     * Process the {@link Query} using producer consumer architecture, where
     * some {@link Command}s wait for other {@link Command}s to finish before
     * starting.
     * 
     * @param queryToExec
     * @param qService
     * @throws AQLException
     */
    @SuppressWarnings("unused")
    private void processBlockingList(final Query queryToExec,
            final IQueryService qService) throws AQLException {
        if (!queryToExec.isRepeated()) {

            // queryToExec.setOperandsWaiting(operandsWaiting);
            final Queue<Command> orderedCommands = queryToExec.getCommands();
            final List<Future<Util.ResultState>> futList = new ArrayList<>();
            final StringBuffer bashCommands = new StringBuffer();
            // Process command in the order.
            for (final Command command : orderedCommands) {
                if (!command.isStored()) {
                    @SuppressWarnings("deprecation")
                    final CommandExecutorController cExec = new CommandExecutorController(
                            command, queryToExec, bashCommands);
                    final Future<Util.ResultState> resFut = execThreadPool
                            .submit(cExec);
                    futList.add(resFut);
                }
            }
            getAndCheckCommandRes(futList, queryToExec);
            logger.info(bashCommands);
            if (!queryToExec.isErrState()) { // store final result
                qService.setResultForQuery(queryToExec);
            }
        }
    }

    private void getAndCheckCommandRes(final List<Future<ResultState>> futList,
            final Query queryToExec) throws AQLException {
        final StringBuilder msgAll = new StringBuilder();
        try {
            // wait for each command and get its results
            int index = 0;
            double progress = futList.size();
            for (; index < futList.size(); index++) {
                final Future<ResultState> future = futList.get(index);
                // TODO check results, compute query progress
                if (future != null) {
                    final Util.ResultState res = future.get();
                    if (res != null) {
                        if (res.status == Util.FAILED_STATUS) {
                            msgAll.append(res.msg.toString()
                                    + Util.LINE_SEPARATOR);
                            progress = index / progress; // compute progress
                            queryToExec.setQueryProgress(progress);
                            // stop other commands
                            index = errQuit(futList, queryToExec, index++);
                        } else {
                            // ok query progress forward
                            msgAll.append(res.msg.toString()
                                    + Util.LINE_SEPARATOR);
                        }
                    }
                }
            }
        } catch (final InterruptedException | ExecutionException
                | NullPointerException e) {
            logger.error(e.getMessage());
            e.printStackTrace();
            throw new AQLException("Problem processing commands "
                    + e.getMessage());
        } finally {
            sendUserMsg(queryToExec, msgAll.toString());
        }
    }

    private int errQuit(final List<Future<ResultState>> futList,
            final Query queryToExec, int index) {
        for (; index < futList.size(); index++) {
            final Future<ResultState> future = futList.get(index);
            if (!future.isDone()) {
                final boolean mayInterruptIfRunning = false;
                try {
                    future.cancel(mayInterruptIfRunning);
                } catch (final CancellationException e) {
                    logger.error(e.getMessage());
                }
            }
        }
        queryToExec.setErrState(true);
        return index;
    }

    @Override
    public Object call() throws Exception {
        return null;
    }
}
