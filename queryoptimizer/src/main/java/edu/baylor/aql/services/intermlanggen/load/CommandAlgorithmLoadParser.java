package edu.baylor.aql.services.intermlanggen.load;

import java.util.Set;

import org.apache.log4j.Logger;

import edu.baylor.aql.entities.command.CommandAlgorithm;
import edu.baylor.aql.entities.parsing.LoadMap;
import edu.baylor.aql.entities.query.IntermediateLanguageObject;
import edu.baylor.aql.services.intermlanggen.UtilParser;
import edu.baylor.aql.util.Util;

/**
 * This service parses {@link LoadMap} that contains elements of the
 * {@link CommandAlgorithm}.
 * 
 * @author Rovshen Nazarov. Baylor University. Jan 14, 2015.All Rights Reserved.
 * 
 */
public final class CommandAlgorithmLoadParser extends BasicLoadParser {

    private static Logger logger = Logger
            .getLogger(CommandAlgorithmLoadParser.class.getName());
    private final IntermediateLanguageObject ilo;

    private static final String ANY_NUMBER_OF_OPERANDS = "*";
    private static final int ANY_NUMBER_OF_OPERANDS_INT = Integer.MAX_VALUE;

    public CommandAlgorithmLoadParser(final IntermediateLanguageObject ilo) {
        super();
        this.ilo = ilo;
    }

    @Override
    public IntermediateLanguageObject call() {

        final LoadMap loadMap = (LoadMap) ilo.getReqestObj();
        final IntermediateLanguageObject iloRes = new IntermediateLanguageObject();
        iloRes.setParseStateCode(Util.SUCCESS_STATUS);
        final CommandAlgorithm ca = new CommandAlgorithm();

        final String path = loadMap.getAttValTable().get("path");
        final String commandTypeName = loadMap.getAttValTable().get(
                "commandtype");
        final String functionalType = loadMap.getAttValTable().get(
                "functionalType");
        final String commandTemplate = loadMap.getAttValTable().get(
                "commandtemplate");
        final String toolName = loadMap.getAttValTable().get("tool");
        /** check {@link OperandType} for supported types */
        final String resultOperandType = loadMap.getAttValTable().get(
                "resultOperandType");
        int operandCount = 0;
        int caPriority = 0;
        try {
            final String operandCountStr = loadMap.getAttValTable().get(
                    "operandcount");
            if (ANY_NUMBER_OF_OPERANDS.equals(operandCountStr)) {
                /**
                 * if * was provided meaning command algorithm can take any
                 * number of operands set the # of operands to MAX INT value.
                 */
                operandCount = CommandAlgorithm.MAX_OPERANDS;
            } else {
                operandCount = Integer.parseInt(operandCountStr);
            }
            caPriority = Integer.parseInt(loadMap.getAttValTable().get(
                    "priority"));
        } catch (final Exception e) {
            logger.error(e.getMessage());
        }

        /**
         * Keep only optional attributes.
         */
        final Set<String> attSetAll = loadMap.getAttValTable().keySet();
        attSetAll.removeAll(UtilParser.COMMAND_ALGO_REQUIRED_ATT_SET);

        // process all remaining optional attributes
        for (final String att : attSetAll) {
            final String val = loadMap.getAttValTable().get(att);
            ca.getAttributes().put(att, val);
        }

        if (path != null) {
            ca.setPath(path);
        } else {
            ca.setPath("");
            iloRes.getParseMsg().append("The path is needed.").append("\n");
            iloRes.setParseStateCode(Util.FAILED_STATUS);
        }
        if (commandTypeName != null) {
            ca.setCommandTypeName(commandTypeName);
        } else {
            iloRes.getParseMsg().append("The commandtype is needed.")
                    .append("\n");
            iloRes.setParseStateCode(Util.FAILED_STATUS);
        }
        if (functionalType != null) {
            ca.setFunctionalType(functionalType);
        } else {
            iloRes.getParseMsg().append("The functionalType is needed.")
                    .append("\n");
            iloRes.setParseStateCode(Util.FAILED_STATUS);
        }
        if (commandTemplate != null) {
            ca.setCommandTemplate(commandTemplate);
        } else {
            ca.setCommandTemplate("");
            iloRes.getParseMsg().append("The commandtemplate is needed.")
                    .append("\n");
            iloRes.setParseStateCode(Util.FAILED_STATUS);
        }
        if (toolName != null) {
            ca.setToolName(toolName);
        } else {
            ca.setToolName("");
            iloRes.getParseMsg().append("The toolName is needed.").append("\n");
            iloRes.setParseStateCode(Util.FAILED_STATUS);
        }

        if (operandCount > 0) {
            ca.setNumberOfOperands(operandCount);
        } else {
            iloRes.getParseMsg().append("The number of operands is needed.")
                    .append("\n");
            iloRes.setParseStateCode(Util.FAILED_STATUS);
        }

        if (caPriority >= 0) {
            ca.setPriority(caPriority);
        } else {
            iloRes.getParseMsg().append("Priority can only be positive value.")
                    .append("\n");
            iloRes.setParseStateCode(Util.FAILED_STATUS);
        }

        if (resultOperandType != null) {
            ca.setResultOperandType(resultOperandType);
        } else {
            iloRes.getParseMsg().append("The result operand type is needed.")
                    .append("\n");
            iloRes.setParseStateCode(Util.FAILED_STATUS);
        }
        iloRes.setReqestObj(ca);
        iloRes.setType(ilo.getObjType());
        return iloRes;
    }
}
