package edu.baylor.aql.services.query;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Queue;

import edu.baylor.aql.entities.client.ClientSession;
import edu.baylor.aql.entities.command.Command;
import edu.baylor.aql.entities.command.CommandAlgorithm;
import edu.baylor.aql.entities.operands.ComputedOperand;
import edu.baylor.aql.entities.operands.Operand;
import edu.baylor.aql.entities.query.Query;
import edu.baylor.aql.services.optimization.ordering.OrderingHandler;
import edu.baylor.aql.services.requesthandler.pipeline.Visitor;
import edu.baylor.aql.util.AQLException;

/**
 * This interface defines methods for {@link Query} object and its related
 * fields.
 * 
 * @author Rovshen Nazarov. Baylor University. Jan 14, 2015.All Rights Reserved.
 * 
 */
public interface IQueryService {
    /**
     * NOTE: The order and the number of {@link Operand}s that are added to the
     * {@link Command} depends on the following:
     * 1) Both {@link Command}s list and {@link Operand}s list are pre-sorted
     * based on their parse line numbers using custom {@link Comparator}.
     * <p>
     * 2) Any {@link Operand} that is still in the {@link Operand}'s list and
     * has {@link Operand#getParseLineNumber()} smaller than
     * {@link Command#getParseLineNumber()} is added to that {@link Command}.
     * <p>
     * Populates {@link Command}s with {@link ComputedOperand}s or
     * {@link Operand} s. This method is used for pre-processing for later use
     * with {@link OrderingHandler} and other services.
     * <p>
     * This method adds required number of {@link Operand}s based on
     * {@link CommandAlgorithm#setNumberOfOperands(Integer)} to each
     * {@link Command} in the command list member field.
     * <p>
     * This method creates a copy of the original {@link Operand}s to populate
     * each {@link Command} with desired number of {@link Operand}s and also
     * adds {@link ComputedOperand} for to the {@link Command} as a result.
     * <p>
     * If the Operand for command is result of another Command add
     * {@link ComputedOperand} operator.
     * <p>
     * Pre-condition. Each {@link Command} within {@link Query} should have been
     * assigned a {@link CommandAlgorithm}.
     * 
     * This method is non-blocking.
     * 
     * @param commands
     * 
     * @param operands
     * @throws AQLException
     */
    public void populateCommandsWithOperandsSequencial(final Query query)
            throws AQLException;

    /**
     * Check if all {@link Operand}s were loaded successfuly, e.g. loaded status
     * is true.
     * 
     * @param query
     * @param loadErrMsgLoad
     * @return numberOfErrors
     * @throws AQLException
     */
    public int checkOperandsLoaded(Query query, StringBuilder loadErrMsgLoad)
            throws AQLException;

    /**
     * Update all references in a given {@link Query} to a given {@link Operand}
     * .
     * The update is done based on the {@link Operand}'s command id.
     * 
     * NOTE: update is needed because later the other {@link Command}s will be
     * checked using their {@link Operand}'s ids to see if they are repeated
     * {@link Command}s.
     * 
     * @param query
     * @param result
     * @param repeatedResult
     * @throws AQLException
     */
    public void updateIntermResults(final Query query,
            final Long origComputedCommResult, final Long repeatedCommandResult)
            throws AQLException;

    /**
     * Compute {@link Query} progress based on {@link Command}'s result
     * {@link Operand}s.
     * 
     * @param query
     * @return
     * @throws AQLException
     */
    public double computeProgress(Query query) throws AQLException;

    /**
     * Set {@link Query} result {@link Operand} based on the result
     * {@link Operand} of
     * the last {@link Command} in the {@link Query}. And set
     * {@link Query#getProgress()} to 1.0
     * 
     * @param query
     * @throws AQLException
     */
    public void setResultForQuery(final Query query) throws AQLException;

    /**
     * Copy contents of from {@link Query} to the to {@link Query}.
     * This method is similar to {@link Query#clone()}, but differs in that the
     * to {@link Query} is updated instead of creating a copy of the from
     * {@link Query}. Also not that the {@link Object} in the from {@link Query}
     * will be referenced in the to {@link Query}, thus they will be shared.
     * 
     * @param from
     * @param to
     */
    public void copyQuery(Query from, Query to);

    /**
     * This is a helper function.
     * 
     * Sharing {@link Query} introduces race conditions, thus we only call
     * {@link Visitor}s on the clone {@link Query}, thus we need to process
     * {@link Query} separately. This method populates {@link Command}s with
     * {@link CommandAlgorithm}s and {@link Operand}s for the input
     * {@link Query}.
     * 
     * @param query
     * @throws AQLException
     */
    public void populateQueryWithDefaultCommandAlgo(Query q)
            throws AQLException;

    /**
     * Set numbering to {@link Command}s in the {@link Query} based on
     * {@link ClientSession#getCommandSessionId()} value.
     * 
     * @param q
     */
    public void numberCommands(Query q);

    /**
     * Each {@link Query} requires a unique {@link Queue} of {@link Command}s.
     * This method generates a clone of the input queue.
     * 
     * @param commands
     * @return
     */
    public Queue<Command> cloneCommandList(final Queue<Command> commands);

    /**
     * Link Queries to the head {@link Query} of the linked list.
     * 
     * @param queries
     * @param q
     *            Head of the linked list
     */
    public void linkQueries(List<Query> queries, Query q);

    /**
     * Given a {@link Query} replace all {@link Operand} with a given alias name
     * with {@link Query} result {@link Operand} from the provided {@link List}.
     * 
     * @param qNext
     * @param operandsMap
     */
    public void replaceAliasWithOperands(Query qNext,
            Map<Long, Operand> operandsMap) throws AQLException;

}
