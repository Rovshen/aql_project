package edu.baylor.aql.services.optimization.ordering;

import java.util.List;
import java.util.Queue;
import java.util.Set;

import edu.baylor.aql.entities.command.Command;
import edu.baylor.aql.entities.command.CommandType;
import edu.baylor.aql.entities.command.CommandType.OrderingType;
import edu.baylor.aql.entities.command.OrderingCommandGroup;
import edu.baylor.aql.entities.operands.Operand;
import edu.baylor.aql.entities.query.Query;

public interface IOrderingService {
    /**
     * Compute different possible {@link Operand} ordering based on
     * associativity and add {@link Query}s to the linked list in the
     * {@link Query} object.
     * 
     * @param query
     * @param orderingCommandGroup
     * @return listOfQueries
     */
    public List<Query> generOrderingsAssociative(Query query,
            OrderingCommandGroup orderingCommandGroup);

    /**
     * Compute different possible {@link Operand} ordering based on
     * commutativity and add {@link Query}s to the linked list in the
     * {@link Query} object.
     * 
     * @param query
     * @param orderingCommandGroup
     * @return listOfQueries
     */
    public void generOrderingsCommutative(final Query query,
            final OrderingCommandGroup orderingCommandGroup);

    /**
     * Compute different possible {@link Operand} ordering based on
     * associativity and commutativity and add {@link Query}s to the linked list
     * in the {@link Query} object.
     * 
     * @param query
     * @param orderingCommandGroup
     * @return listOfQueries
     */
    public List<Query> generOrderingsAssocAndCommutat(Query query,
            OrderingCommandGroup orderingCommandGroup);

    /**
     * PRE: {@link Command} must have {@link CommandType} object set and
     * {@link CommandType} must have {@link OrderingType} set.
     * <p>
     * A group will be specific by {@link OrderingType} and {@link CommandType}
     * and must contain commands that are on the same query level based on
     * {@link Command#getSubQueryNumber()} and follow each other sequentially.
     * <p>
     * The {@link OrderingType} to {@link OrderingCommandGroup} will not
     * generate duplicate {@link Command}s b/c if a {@link Command} obeys more
     * than one ordering law, then that is indicated by a combined
     * {@link OrderingType}s, such as
     * {@link OrderingType#ASSOCIATIVE_COMMUTATIVE} type, which indicates that
     * the {@link Command} follows associative, commutative, and a combination
     * of the two laws, and the 3 related orderings will be applied to such a
     * {@link Command} during orderings generation.
     * 
     * @param commands
     * @return orderingCommandMap {@link CommandType} maps to the {@link List}
     *         of {@link Command}s
     */
    public List<OrderingCommandGroup> getOrderingCommandGroups(
            final Queue<Command> commands);

    /**
     * The ordering of {@link Command}s in the list must be the same as the
     * ordering of the same {@link Command}s in the original {@link Query}.
     * 
     * @param orderingCommandGroups
     * @return
     */
    public Queue<Command> combineCommandsFromGroups(
            final List<OrderingCommandGroup> orderingCommandGroups);

    /**
     * For each group look at its {@link Operand} orderings and combine those
     * orderings with other {@link Operand} orderings of subsequent groups. Note
     * that the {@link OrderingCommandGroup}'s ordering in the list is important
     * and represents original ordering of {@link Command}s within a
     * {@link Query}.
     * 
     * @param orderingCommandGroups
     * @return
     */
    public Set<List<Long>> combineGroupsByOperands(
            List<OrderingCommandGroup> orderingCommandGroups);

}
