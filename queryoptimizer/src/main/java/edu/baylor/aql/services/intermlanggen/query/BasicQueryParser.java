package edu.baylor.aql.services.intermlanggen.query;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Stack;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.log4j.Logger;

import edu.baylor.aql.dataaccess.IMetadata;
import edu.baylor.aql.entities.Element;
import edu.baylor.aql.entities.command.Command;
import edu.baylor.aql.entities.command.CommandType;
import edu.baylor.aql.entities.operands.ComputedOperand;
import edu.baylor.aql.entities.operands.Operand;
import edu.baylor.aql.entities.query.IntermediateLanguageObject;
import edu.baylor.aql.entities.query.IntermediateLanguageObject.ParseObjType;
import edu.baylor.aql.entities.query.Query;
import edu.baylor.aql.services.intermlanggen.UtilParser;
import edu.baylor.aql.services.query.IQueryService;
import edu.baylor.aql.services.query.QueryService;
import edu.baylor.aql.services.requesthandler.pipeline.Visitor;
import edu.baylor.aql.util.AQLException;
import edu.baylor.aql.util.Util;

public class BasicQueryParser {

    public static class QueryTokens extends Element {

        private final List<String> queryTokens = new ArrayList<>();
        private StringBuilder request = new StringBuilder();

        /**
         * @return the request
         */
        public StringBuilder getRequest() {
            return request;
        }

        /**
         * @param request
         *            the request to set
         */
        public void setRequest(final StringBuilder request) {
            this.request = request;
        }

        /**
         * @return the queryTokens
         */
        public List<String> getQueryTokens() {
            return queryTokens;
        }

        @Override
        public void accept(final Visitor v, final CountDownLatch doneSignal)
                throws InterruptedException {
        }

        @Override
        public long insert(final IMetadata lookUp) throws AQLException {
            // TODO Auto-generated method stub
            return 0;
        }

        /*
         * (non-Javadoc)
         * 
         * @see java.lang.Object#toString()
         */
        @Override
        public String toString() {
            final StringBuilder builder = new StringBuilder();
            builder.append("QueryTokens [");
            if (queryTokens != null) {
                builder.append("queryTokens=").append(queryTokens).append(", ");
            }
            if (request != null) {
                builder.append("request=").append(request);
            }
            builder.append("]");
            return builder.toString();
        }

    }

    /**
     * System specific new line delimiter.
     */
    private static final String DELIMETER = System.lineSeparator();
    /**
     * Expected position of the element to retrieve, such as in the line 1:
     * Command UNION, we want to retreive UNION. This turns out to be true for
     * most parsing in the query.
     */
    protected static final int EXPECTED_ELEMENT_INDEX = 2;
    /**
     * Used to log errors or information.
     */
    private static Logger logger = Logger.getLogger(BasicQueryParser.class
            .getName());

    /**
     * This intermediate language object contains {@link QueryTokens}.
     */
    protected final IntermediateLanguageObject iloInst;
    /**
     * This service is used to operate AQL metadata. This service helps store
     * and retrieve metadata from the database.
     */
    protected final IMetadata dao;

    public BasicQueryParser(final IntermediateLanguageObject iloInst,
            final IMetadata dao) {
        this.iloInst = iloInst;
        this.dao = dao;
    }

    public IntermediateLanguageObject parse() throws Exception {

        final IQueryService qService = new QueryService();
        final IntermediateLanguageObject iloRes = new IntermediateLanguageObject();
        final Query query = new Query();
        final Queue<Command> commands = new LinkedList<>();
        final Stack<Integer> subQuerySeqNumbers = new Stack<>();
        final List<Long> operands = new ArrayList<>();
        query.setOperands(operands);
        query.setSubQuerySeqNumbers(subQuerySeqNumbers);
        query.setCommands(commands);
        final ConcurrentHashMap<Long, Operand> operMap = new ConcurrentHashMap<>();
        query.setOperMap(operMap);
        iloRes.setReqestObj(query);
        iloRes.setType(ParseObjType.QUERY);
        dao.openConnection();
        final List<CommandType> commandTypes = dao.lookUpCommandTypes();
        dao.closeConnection();

        if (commandTypes.isEmpty()) {
            // no commands
            logger.error("Could not retreive commands.");
            return iloRes;
        }
        final AtomicInteger numbOfExpCommResults = new AtomicInteger(0);
        final QueryTokens qt = (QueryTokens) iloInst.getReqestObj();
        query.setQueryRequest(qt.getRequest().toString());
        // Read data until the end query,
        // if error occurred return result so far.
        for (final String line : qt.getQueryTokens()) {
            if (processBasicCommand(line, iloRes, commandTypes,
                    numbOfExpCommResults)) {
                return iloRes;
            }
        }// end of query processing loop
        checkOperands(query, iloRes, qService);

        return iloRes;
    }

    /**
     * Check that all {@link Operand}s are loaded correctly, if not propagate an
     * error to the caller.
     * 
     * @param query
     * @param iloRes
     * @param qService
     * @throws AQLException
     */
    protected void checkOperands(final Query query,
            final IntermediateLanguageObject iloRes,
            final IQueryService qService) throws AQLException {

        final StringBuilder loadErrMsgLoad = new StringBuilder();
        final int numberOfErrors = qService.checkOperandsLoaded(query,
                loadErrMsgLoad);
        if (numberOfErrors > 0) {
            iloRes.getParseMsg().append(loadErrMsgLoad.toString())
                    .append(Util.LINE_SEPARATOR);
            iloRes.setParseStateCode(Util.FAILED_STATUS);
        }
    }

    /**
     * If returns true error has occurred, else no error.
     * 
     * @param line
     * @param iloRes
     * @param commandTypes
     * @param commands
     * @param operandIds
     * @param expResCount
     *            count expected number of results from other {@link Command}s.
     * @param minCommOperands
     *            Minimum number of {@link Operand}s expected by the
     *            {@link CommandType}.
     * @return true on error
     * @throws InterruptedException
     */
    protected boolean processBasicCommand(final String line,
            final IntermediateLanguageObject iloRes,
            final List<CommandType> commandTypes,
            final AtomicInteger expResCount) throws InterruptedException,
            AQLException {
        if (line.equals("END_QUERY")) {
            return false; // no error
        }

        final Query query = (Query) iloRes.getReqestObj();
        final Queue<Command> commands = query.getCommands();

        // add sub query sequence number
        if (line.contains("BEGIN_SUB_QUERY")) {
            final Integer subQSeqNum = UtilParser.getSubQuerySeqNumb(line);
            if (subQSeqNum == Util.FAILED_STATUS) {
                iloRes.setParseStateCode(Util.FAILED_STATUS);
                return true; // error
            }
            query.getSubQuerySeqNumbers().push(subQSeqNum);
        }
        // remove sub query sequence number
        if (line.contains("END_SUB_QUERY")) {
            final int id = query.getSubQuerySeqNumbers().pop();
            // logger.info("removing sub query id " + id);
        }
        // if encountered operand id push it to stack
        if (line.contains("OPERAND_ID")) {

            final String[] els = getElements(line, iloRes);
            // add sub query sequence number
            final Operand resOp = parseOperandId(els, iloRes);
            if (resOp == null) {
                throw new AQLException("Could not get operand");
            }
            query.getOperMap().put((long) resOp.hashCode(), resOp);
            query.getOperands().add((long) resOp.hashCode());
        } else if (line.contains("QUERY_ALIAS_NAME")) {

            // set this Query's alias name
            final String[] els = getElements(line, iloRes);
            final int aliasIndex = 2;
            if (els.length < aliasIndex + 1) {
                throw new IllegalArgumentException(
                        "operand line is does not have enough arguments.");
            }
            final String queryAliasStr = els[aliasIndex];
            query.setAliasName(queryAliasStr);
        } else {
            // process command
            Command command = null;
            for (final CommandType commandType : commandTypes) {
                if (line.toUpperCase().contains(
                        commandType.getTypeName().toUpperCase())) {
                    /**
                     * check minimum operands are supplied.
                     */
                    if (query.getOperands().size() + expResCount.get() < commandType
                            .getMinNumbOper()) {
                        iloRes.getParseMsg()
                                .append("Query, requires at least ")
                                .append(commandType.getMinNumbOper())
                                .append(" operands, but it had ")
                                .append(query.getOperands().size()
                                        + expResCount.get());
                        logger.info(iloRes.getParseMsg().toString());
                        iloRes.setParseStateCode(Util.FAILED_STATUS);
                        return true; // error
                    }
                    final int commandParseLineNumber = getCommandLineNumber(
                            line, iloRes);
                    command = new Command()
                            .setCommandType(commandType.getTypeName())
                            .setNumberOfOperands(commandType.getMinNumbOper())
                            .setCommandTypeObj(commandType)
                            .setParseLineNumber(commandParseLineNumber);
                    // add sub query sequence number if any
                    if (!query.getSubQuerySeqNumbers().isEmpty()) {
                        // the top of the stack number is the current sub query
                        // that is being parsed
                        command.setSubQueryNumber(query.getSubQuerySeqNumbers()
                                .peek());
                    }
                    commands.offer(command);
                    /** process another command */
                    expResCount.incrementAndGet();
                }
            }
            if (commands.isEmpty() || command == null) {
                if (!line.contains("QUERY")) {
                    iloRes.getParseMsg().append(
                            "Command Type for " + line + " is not loaded yet.");
                    iloRes.setParseStateCode(Util.FAILED_STATUS);
                    return true; // error
                }
            }
        }// end processing command
        return false; // no error
    }

    /**
     * Get the line number of the command element.
     * 
     * @param line
     * @param iloRes
     * @return int line number or {@link Util#FAILED_STATUS}.
     */
    protected int getCommandLineNumber(final String line,
            final IntermediateLanguageObject iloRes) {

        final String[] commandLineElements = getElements(line, iloRes);
        final int operandLineNumberIndex = 0;
        final int commandParseLineNumber = UtilParser
                .getNumber(commandLineElements[operandLineNumberIndex]);
        return commandParseLineNumber;
    }

    /**
     * Split a line and check if the number of String elements is as expected.
     * 
     * @param line
     * @param iloRes
     * @return return array of string elements in the input string. If number of
     *         elements is not as expected return empty array.
     */
    protected String[] getElements(final String line,
            final IntermediateLanguageObject iloRes) {
        final String[] elements = line.split(UtilParser.DEFAULT_ELEMENT_SEP);

        if (elements.length < UtilParser.EXP_LINE_ELEM_COUNT) {

            iloRes.getParseMsg().append(
                    "Parsing error. Not enough elements on the Operand line.");
            iloRes.setParseStateCode(Util.FAILED_STATUS);
        }
        return elements;
    }

    /**
     * 
     * @param line
     * @param iloRes
     * @return
     */
    protected String getElement(final String line,
            final IntermediateLanguageObject iloRes) {
        final String[] elements = line.split(UtilParser.DEFAULT_ELEMENT_SEP);
        if (checkElement(elements, EXPECTED_ELEMENT_INDEX, iloRes)) {
            return "";
        }
        final String el = elements[EXPECTED_ELEMENT_INDEX];
        return el;
    }

    /**
     * 
     * @param elements
     * @param expElementIndex
     * @param iloRes
     * @return
     */
    protected boolean checkElement(final String[] elements,
            final int expElementIndex, final IntermediateLanguageObject iloRes) {
        if (elements.length < expElementIndex + 1) {
            // wrong command
            iloRes.getParseMsg().append("Incorrect Command.");
            logger.error(iloRes.getParseMsg().toString());
            iloRes.setParseStateCode(Util.FAILED_STATUS);
            return true;
        }
        return false;
    }

    /**
     * Parse single operand. Note we invoke metadata access in this method.
     * This method parses an operand line, such as [1:, OPERAND_ID, graph_4] and
     * generates an {@link Operand} AQL object with all possible fields set.
     * <p>
     * Most important fields are parse line number and sub query sequence
     * number.
     * 
     * @param operandElements
     * @param ilo
     * @return
     * @throws NumberFormatException
     */
    protected Operand parseOperandId(final String[] operandElements,
            final IntermediateLanguageObject ilo) throws NumberFormatException {

        if (operandElements == null) {
            throw new IllegalArgumentException("operand line cannot be null");
        }
        // index of the operand's id section within operand line
        final int operandIdIndex = 2;
        // index of the operand's parse line number section within operand line
        final int operandLineNumberIndex = 0;
        // index of operand id within operand section of the operand line
        final int operandIdLineIndex = 1;
        // expected min element count in the operand id section of the operand
        // line
        final int expMinElCount = 2;
        if (operandElements.length < operandIdIndex + 1) {
            throw new IllegalArgumentException(
                    "operand line is does not have enough arguments.");
        }
        Operand resultOperand = null;
        final Query query = (Query) ilo.getReqestObj();

        // Get Operand from metadata based on the provided operand id
        final int parseLineNumber = UtilParser
                .getNumber(operandElements[operandLineNumberIndex]);
        final String operandIdStr = operandElements[operandIdIndex];

        final String[] operandIdLine = operandIdStr
                .split(UtilParser.DEFAULT_OPERAND_STRING_SEP);

        if (operandIdLine.length >= expMinElCount) { // 0 is the name, 1 is id

            final long operandId = Long
                    .parseLong(operandIdLine[operandIdLineIndex]);
            dao.openConnection(); // get operand from metadata
            resultOperand = dao.lookUpOperand(operandId);
            dao.closeConnection();
            resultOperand.setParseLineNumber(parseLineNumber);
            // add sub query sequence number if any
            if (!query.getSubQuerySeqNumbers().isEmpty()) {
                resultOperand.setSubQueryNumber(query.getSubQuerySeqNumbers()
                        .peek());
            }
        } else if (operandIdLine.length <= 1) { // 0 is the name
            // alias name does not have to have Id.
            resultOperand = new ComputedOperand();

            resultOperand.setAliasName(operandIdStr);
            // we need to give different ids to alias operands
            // logger.info(ilo.getClientSession());
            resultOperand.setId(ilo.getClientSession().getAliesSessionId()
                    .getAndIncrement());
            resultOperand.setParseLineNumber(parseLineNumber);
            // add sub query sequence number if any
            if (!query.getSubQuerySeqNumbers().isEmpty()) {
                resultOperand.setSubQueryNumber(query.getSubQuerySeqNumbers()
                        .peek());
            }
        }
        // logger.info(operands);
        return resultOperand;
    }
}
