package edu.baylor.aql.services.optimization.evaluating;

import java.util.concurrent.CountDownLatch;

import edu.baylor.aql.entities.Element;
import edu.baylor.aql.entities.command.Command;
import edu.baylor.aql.entities.command.CommandAlgorithm;
import edu.baylor.aql.entities.query.Query;
import edu.baylor.aql.entities.tools.Tool;
import edu.baylor.aql.services.requesthandler.pipeline.Visitor;

/**
 * This service class performs simple evaluation of a {@link Query}.
 * A {@link Query} contains {@link Command} and {@link CommandAlgorithm}.
 * 
 * @author Rovshen Nazarov. Baylor University. Sep 17, 2014.All Rights Reserved.
 * 
 */
public final class SimpleEvaluator extends Visitor {

    private Query query;
    private CountDownLatch doneSignal;

    /**
     * Return {@link Element}, which would be a list of ordered {@link Query}s.
     */
    @Override
    public Object call() throws Exception {
        try {
            final int estExecCost = 100; // 100 is the highest cost
            query.setEstExecCost(estExecCost);
        } finally {
            doneSignal.countDown();
        }
        return query;
    }

    /**
     * Perform simple evaluation by only using metadata information
     * to evaluate each execution {@link Query}.
     * 
     * {@link Query} represents a given merge of {@link Command}
     * {@link CommandAlgorithm} and {@link Tool} related data.
     * <p>
     * Write generated cost of each plan to corresponding
     * {@link CommandAlgorithm} objects.
     */
    @Override
    public void visit(final Element query, final CountDownLatch doneSignal)
            throws InterruptedException {
        this.query = (Query) query;
        this.doneSignal = doneSignal;
    }

}
