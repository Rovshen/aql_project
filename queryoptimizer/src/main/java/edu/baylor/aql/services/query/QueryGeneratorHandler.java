/**
 * 
 */
package edu.baylor.aql.services.query;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.log4j.Logger;

import edu.baylor.aql.bootstrap.AQLServerAppSingleton;
import edu.baylor.aql.bootstrap.BootstrapAQLServer;
import edu.baylor.aql.dispatcher.AQLServerApp;
import edu.baylor.aql.entities.Element;
import edu.baylor.aql.entities.query.Query;
import edu.baylor.aql.services.requesthandler.pipeline.Visitor;
import edu.baylor.aql.services.requesthandler.pipeline.VisitorH;
import edu.baylor.aql.util.AQLException;

/**
 * @author Rovshen Nazarov. Baylor University. Mar 17, 2015.All Rights Reserved.
 * 
 */
public class QueryGeneratorHandler extends VisitorH {

    /**
     * Used to log errors or information.
     */
    private static Logger logger = Logger.getLogger(QueryGeneratorHandler.class
            .getName());
    private static final int POOL_SIZE = 10;
    private final ExecutorService execThreadPool;
    /**
     * AQL Server wide settings provided on the server start up.
     * Those settings define general settings that will be applied in different
     * places in the system.
     * <p>
     * The settings are accessed via singleton pattern, where the single
     * instance of the AQL Server is created on start up via
     * {@link BootstrapAQLServer} service and is accessed as needed.
     */
    private final AQLServerApp serverInstance = AQLServerAppSingleton
            .getInstance();

    /**
     * 
     */
    public QueryGeneratorHandler() {
        execThreadPool = Executors.newFixedThreadPool(POOL_SIZE);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * edu.baylor.aql.services.requesthandler.pipeline.VisitorH#visit(edu.baylor
     * .aql.entities.Element, java.util.concurrent.CountDownLatch)
     */
    @Override
    public void visit(final Element query, final CountDownLatch doneSignal)
            throws InterruptedException, AQLException {

        try {
            final List<Future<Object>> resFutList = new ArrayList<>();
            final List<Query> queries = new ArrayList<>();
            final Query q = (Query) query;
            final IQueryService qService = new QueryService();

            if (serverInstance.getSettings().isNullOptimization()) {

                qService.numberCommands(q);
                qService.populateQueryWithDefaultCommandAlgo(q);
                for (final Visitor ohCurr : super.getVisitorsVert()) {
                    doneSignal.countDown();
                }
            } else { // full optimization

                // start different query generating services
                for (final Visitor ohCurr : super.getVisitorsVert()) {

                    q.clone().accept(ohCurr, doneSignal);
                    final Future<Object> resFut = execThreadPool.submit(ohCurr);
                    resFutList.add(resFut);
                }
                // combine all queries
                for (final Future<Object> future : resFutList) {

                    final Object queryListObj = future.get();
                    @SuppressWarnings("unchecked")
                    final List<Query> queryList = (List<Query>) queryListObj;
                    queries.addAll(queryList);
                }
                // check if any query has an error state
                for (final Query qC : queries) {
                    if (qC.isErrState()) {
                        logger.error(qC.getStateMsg());
                        throw new AQLException(qC.getStateMsg());
                    }
                }
                qService.linkQueries(queries, q);
                logger.info("\n\n\nTotal number of gererated queries by Query Generator handler is "
                        + queries.size());
            }
        } catch (final ExecutionException e) {
            e.printStackTrace();
        } finally {
            doneSignal.countDown(); // for this handler
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see edu.baylor.aql.services.requesthandler.pipeline.Visitor#call()
     */
    @Override
    public Object call() throws Exception {

        return null;
    }

}
