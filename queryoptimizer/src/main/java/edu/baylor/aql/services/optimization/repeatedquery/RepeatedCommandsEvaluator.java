package edu.baylor.aql.services.optimization.repeatedquery;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.log4j.Logger;

import edu.baylor.aql.entities.Element;
import edu.baylor.aql.entities.command.Command;
import edu.baylor.aql.entities.command.CommandType;
import edu.baylor.aql.entities.operands.Operand;
import edu.baylor.aql.entities.query.Query;
import edu.baylor.aql.services.query.IQueryService;
import edu.baylor.aql.services.query.QueryService;
import edu.baylor.aql.services.requesthandler.pipeline.Visitor;
import edu.baylor.aql.util.AQLException;
import edu.baylor.aql.util.Util;

/**
 * This service checks if some or all commands were already executed before in a
 * given query.
 * 
 * @author Rovshen Nazarov. Baylor University. Dec 19, 2014.All Rights Reserved.
 * 
 */
public final class RepeatedCommandsEvaluator extends Visitor {
    /**
     * Used to log errors or information.
     */
    private static Logger logger = Logger
            .getLogger(RepeatedCommandsEvaluator.class.getName());
    private Query query;
    private CountDownLatch doneSignal;

    private static final int POOL_SIZE = 1;
    private final ExecutorService execThreadPool;

    public RepeatedCommandsEvaluator() {
        execThreadPool = Executors.newFixedThreadPool(POOL_SIZE);
    }

    /**
     * FOREACH {@link Command} in the new {@link Query} &nbsp;Check if it was
     * executed before
     * &nbsp;IF it was executed before update its results and any reference to
     * the result {@link Operand}
     * <p>
     * IF all {@link Command} for a given {@link Query} were executed before AND
     * they all point to the same {@link Query} id get result {@link Operand} of
     * that query AND record the new {@link Query} as completed with the result
     * {@link Operand}.
     * <p>
     * Map {@link Command}s by {@link Operand} ids and size of {@link Operand}
     * list.
     * <p>
     * We check if {@link Command} was executed before by checking if all of its
     * {@link Operand}s were in the same {@link Command} with the same
     * {@link CommandType}.
     * <p>
     * Note that we verify if a {@link Command} was executed before by its first
     * {@link Operand}, and then get other operands of the {@link Command}s
     * where this first {@link Operand} was participating.
     */
    @Override
    public Object call() throws Exception {
        try {

            final IQueryService qService = new QueryService();
            final List<Future<Object>> resFutList = new ArrayList<>();

            Query qNext = query; // operate on the reference of the query
            /**
             * NOTE: need to open the connection only once as this service may
             * process hundred of thousand queries, creating same amount of DB
             * connections and exhausting the DB pool
             */
            logger.info("in RepeatedCommandsEvaluator");
            do { // for each linked query

                final Future<Object> resFut = execThreadPool
                        .submit(new RepeatedEvalProcessQuery(qNext, qService));
                resFutList.add(resFut);

            } while ((qNext = qNext.getNext()) != null);

            Util.waitForAllThreadsToTerminate(resFutList);

        } finally {
            doneSignal.countDown();
        }
        return null;
    }

    /**
     * The input query is the query we want to verify. Based on the input
     * {@link Query} we will try to find the {@link Query} or {@link Command}(s)
     * in the metadata.
     */
    @Override
    public void visit(final Element query, final CountDownLatch doneSignal)
            throws InterruptedException, AQLException {
        this.query = (Query) query;
        this.doneSignal = doneSignal;

    }

}
