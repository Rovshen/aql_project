package edu.baylor.aql.services.optimization.ordering;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.TreeMap;

import org.apache.log4j.Logger;

import com.google.common.collect.Collections2;

import edu.baylor.aql.entities.command.Command;
import edu.baylor.aql.entities.command.CommandType;
import edu.baylor.aql.entities.command.CommandType.OrderingType;
import edu.baylor.aql.entities.command.OrderingCommandGroup;
import edu.baylor.aql.entities.command.OrderingCommandGroup.OrderingCommandGroupBuilder;
import edu.baylor.aql.entities.operands.Operand;
import edu.baylor.aql.entities.operands.Operand.OperandState;
import edu.baylor.aql.entities.query.Query;

public class OrderingService implements IOrderingService {

    /**
     * Used to log errors or information.
     */
    private static Logger logger = Logger.getLogger(OrderingService.class
            .getName());

    /**
     * PRE: all {@link Command}s are {@link OrderingType#COMMUTATIVE}.
     * PRE: Some {@link Command}s in the {@link Query} may have different
     * {@link OrderingType}.
     * POST: input {@link Query} is not modified.
     * POST: possible orderings are added to the {@link OrderingCommandGroup}.
     */
    @Override
    public void generOrderingsCommutative(final Query query,
            final OrderingCommandGroup ocGroup) {
        assert (query != null);
        assert (ocGroup.getOperands() != null) : "Operands can not be null.";
        assert (ocGroup.getCommType().getOrderingType() == OrderingType.COMMUTATIVE || ocGroup
                .getCommType().getOrderingType() == OrderingType.ASSOCIATIVE_COMMUTATIVE) : "Ordering type must be commutative.";
        final Collection<List<Long>> possibleOrderings = generateCommutatOrderings(ocGroup
                .getOperands());
        for (final List<Long> operOrdering : possibleOrderings) {
            ocGroup.getPossibleOrderings().add(operOrdering);
        }
    }

    /**
     * Generate all possible orders of the {@link Operand}s as allowed by
     * the {@link OrderingType#COMMUTATIVE} law.
     * 
     * @param operands
     * @return possibleOrderings
     */
    private Collection<List<Long>> generateCommutatOrderings(
            final List<Long> operands) {

        final Collection<List<Long>> orderPerm = Collections2
                .orderedPermutations(operands);
        // logger.info("Number of permutation generated is " + orderPerm.size()
        // + " for the operand list of size " + operands.size() + "\n\t"
        // + Arrays.toString(orderPerm.toArray()));

        // + "\n\t" + Arrays.toString(orderPerm.toArray())
        return orderPerm;
    }

    @Override
    public List<Query> generOrderingsAssocAndCommutat(final Query query,
            final OrderingCommandGroup group) {
        return null;
    }

    /**
     * PRE: all {@link Command}s are {@link OrderingType#ASSOCIATIVE}.
     */
    @Override
    public List<Query> generOrderingsAssociative(final Query query,
            final OrderingCommandGroup group) {
        return null;

    }

    /**
     * A group will be specific by {@link OrderingType} and {@link CommandType}
     * and must contain commands that are on the same query level based on
     * {@link Command#getSubQueryNumber()} and follow each other sequentially.
     * <p>
     * The {@link OrderingType} to {@link OrderingCommandGroup} will not
     * generate duplicate {@link Command}s b/c if a {@link Command} obeys more
     * than one ordering law, then that is indicated by a combined
     * {@link OrderingType}s, such as
     * {@link OrderingType#ASSOCIATIVE_COMMUTATIVE} type, which indicates that
     * the {@link Command} follows associative, commutative, and a combination
     * of the two laws, and the 3 related orderings will be applied to such a
     * {@link Command} during orderings generation.
     * <p>
     * TODO 1) Problem with ordering when diff (Order Type none) present, 2)
     * ordering problem of grouping of commands, regardless of their subquery
     * status
     */
    @Override
    public List<OrderingCommandGroup> getOrderingCommandGroups(
            final Queue<Command> commands) {

        final List<OrderingCommandGroupBuilder> groupList = new ArrayList<>();
        final Map<CommandType, OrderingCommandGroupBuilder> commandGroupTemp = new HashMap<>();
        CommandType prevCommType = null; // previous ordering type in the
                                         // map
        int subQuerySeqNumber = Command.DEFAULT_SUB_QUERY_NUMBER;

        // process all commands from the same level in one pass
        for (final Command command : commands) {

            final CommandType cType = command.getCommandTypeObj();

            // if this is the first time we see this command type
            if (!commandGroupTemp.containsKey(cType)
                    || subQuerySeqNumber != command.getSubQueryNumber()) {

                subQuerySeqNumber = command.getSubQueryNumber();
                // store the previous group if any
                if (prevCommType != null) {
                    groupList.add(commandGroupTemp.get(prevCommType));
                    // clear the map to get the next group
                    commandGroupTemp.clear();
                }
                prevCommType = cType;

                // create an object to build a new group based on command
                // type
                final Queue<Command> commandsSameOrdering = new LinkedList<>();
                final List<Long> oSameGroup = new LinkedList<>();
                final OrderingCommandGroupBuilder ocgb = new OrderingCommandGroup.OrderingCommandGroupBuilder(
                        oSameGroup, commandsSameOrdering, cType);
                commandGroupTemp.put(cType, ocgb);
            }
            // we are looking on the same type
            logger.info("Command " + command.getCommandSeqNumber()
                    + " was added to the group of type " + cType.getTypeName());
            commandGroupTemp.get(cType).getNestedCommands().add(command);
        } // end commands processing

        // store the last group
        if (prevCommType != null) {
            groupList.add(commandGroupTemp.get(prevCommType));
            commandGroupTemp.clear(); // clear the map
        }

        // build the command type groups
        final List<OrderingCommandGroup> commandGroups = buildCommandGroups(groupList);
        return commandGroups;
    }

    /**
     * Create buckets/groups of the {@link Command}s based on the
     * {@link Command#getSubQueryNumber()}, which represents sub query level.
     * The sub query level for the main query is set to default at
     * {@link Command#DEFAULT_SUB_QUERY_NUMBER}.
     * 
     * @deprecated because we can not order commands into levels as we break
     *             grouping later.
     * 
     * @param commands
     * @return map level -> command list
     */
    @SuppressWarnings("unused")
    @Deprecated
    private Map<Integer, List<Command>> getLevelCommandListMap(
            final Queue<Command> commands) {

        final Map<Integer, List<Command>> queryLevelCommandList = new TreeMap<>();

        for (final Command c : commands) {

            // if no entry for this level create one
            if (!queryLevelCommandList.containsKey(c.getSubQueryNumber())) {
                queryLevelCommandList.put(c.getSubQueryNumber(),
                        new ArrayList<Command>());
            }
            // add command to the level
            queryLevelCommandList.get(c.getSubQueryNumber());
        }

        return queryLevelCommandList;
    }

    /**
     * Complete building {@link OrderingCommandGroup}s by adding result
     * {@link Operand} to the group.
     * 
     * Get {@link Operand}s from {@link Command}s and add them to the group.
     * Clear {@link Command}s.
     * 
     * Link groups by populating right most command of one group
     * and placing it result in the operands list of the other group.
     * 
     * @param orderingGroup
     * @return
     */
    private List<OrderingCommandGroup> buildCommandGroups(
            final List<OrderingCommandGroupBuilder> groupList) {

        final List<OrderingCommandGroup> orderingGroups = new ArrayList<>();

        for (final OrderingCommandGroupBuilder oCommGroupBuilder : groupList) {

            final Queue<Command> commands = oCommGroupBuilder
                    .getNestedCommands();
            logger.info("Number of commands in the group is " + commands.size());
            // logger.info(Arrays.toString(commands.toArray()));
            // get result of the last command
            final Command[] commArr = commands.toArray(new Command[0]);
            final Command lastC = commArr[commands.size() - 1];
            final Long gResult = lastC.getResult();
            // logger.info("Result operand for the command " +
            // lastC.getCommandSeqNumber() + " is " + gResult);
            oCommGroupBuilder.groupResult(gResult);

            final OrderingCommandGroup ocGroup = oCommGroupBuilder
                    .createOrderingCommandGroup();
            populateGroupsWithOperands(ocGroup);
            orderingGroups.add(ocGroup);
        }
        return orderingGroups;
    }

    /**
     * Populate an input {@link OrderingCommandGroup} with
     * {@link OrderingCommandGroup#getOperands()}.
     * 
     * PRE: The {@link Command}s in the group are expected to be populated
     * with {@link Operand}s.
     * 
     * Get {@link OperandState#LOADED} {@link Operand}s or recorded
     * {@link OperandState#INTERMEDIATE_RESULT} {@link Operand}s, and
     * store them in the group.
     * 
     * PRE: Do not allow to add to the group {@link Operand}s list
     * {@link Operand}s that are result {@link Operand}s of the {@link Command}s
     * in this {@link OrderingCommandGroup}.
     * 
     * POST: {@link OrderingCommandGroup}'s {@link Operand}s list is populated
     * 
     * @param ocGroup
     */
    private void populateGroupsWithOperands(final OrderingCommandGroup ocGroup) {

        final Set<Long> groupCommResults = new HashSet<>();
        for (final Command comm : ocGroup.getCommands()) {
            groupCommResults.add(comm.getResult());
        }
        for (final Command comm : ocGroup.getCommands()) {

            if (comm.getOperands().isEmpty()) {
                throw new IllegalStateException("Command "
                        + comm.getCommandSeqNumber() + " was empty");
            }
            for (final Long o : comm.getOperands()) {
                // add operand only if it is not result operand of another
                // command in this group, or any other group
                if (!groupCommResults.contains(o) && o > -1) {
                    ocGroup.getOperands().add(o);
                }
            }
        }
        logger.info("Number of operands in the group "
                + ocGroup.getCommType().getOrderingType().getLabel() + " is "
                + ocGroup.getOperands().size());
        // + "\n\t" + Arrays.toString(ocGroup.getOperands().toArray()));
    }

    /**
     * JUnit tests verify that the ordering of {@link Command}s in the original
     * {@link Query} is the same as in the Ordered {@link Query}.
     * 
     * @param orderingCommandGroups
     * @return
     */
    @Override
    public Queue<Command> combineCommandsFromGroups(
            final List<OrderingCommandGroup> orderingCommandGroups) {
        final Queue<Command> commands = new LinkedList<>();
        for (final OrderingCommandGroup ocg : orderingCommandGroups) {
            for (final Command c : ocg.getCommands()) {
                commands.add(c);
            }
        }
        return commands;
    }

    /**
     * Combine groups into list of {@link Operand}s.
     * TODO returns empty list
     */
    @Override
    public Set<List<Long>> combineGroupsByOperands(
            final List<OrderingCommandGroup> orderingCommandGroups)
            throws NullPointerException {

        final Set<List<Long>> diffOrderings = new HashSet<>();
        final int treeLevel = 0;
        // due to recursion we need to pass initial list
        final List<Long> emptList = new ArrayList<>();
        constructOrderingRecursively(diffOrderings, orderingCommandGroups,
                treeLevel, emptList);
        // logger.info("\n\n\ndiff operand Orderings: ");
        // for (final List<Long> o : diffOrderings) {
        // logger.info("\n\n" + Arrays.toString(o.toArray()));
        // }
        return diffOrderings;
    }

    /**
     * Construct a final {@link Operand} list , combining possible
     * {@link Operand} lists into a combinations for different
     * {@link OrderingCommandGroup}, while preserving the
     * {@link OrderingCommandGroup}'s sequence.
     * <p>
     * The result are a list of {@link List} of {@link Operand}s, where each
     * list could be used in the {@link Query}.
     * <p>
     * The number of possible combinations when we choose one item from each
     * list, such that the order of the lists matters, e.g. <a, b> <b,> <c, d>
     * will have 2 * 1 * 2 number of choices.
     * 
     * @param diffOrderings
     * @param orderingCommandGroups
     * @param treeLevel
     * @param branchList
     */
    private void constructOrderingRecursively(
            final Set<List<Long>> diffOrderings,
            final List<OrderingCommandGroup> orderingCommandGroups,
            final int treeLevel, final List<Long> branchList) {

        // stop cond for recursion after processing the last level of the tree
        if (treeLevel == orderingCommandGroups.size()) {
            diffOrderings.add(branchList);
            return;
        }

        // for each operand combination at this level, generate tree branching
        final Collection<List<Long>> orderings = orderingCommandGroups.get(
                treeLevel).getPossibleOrderings();
        for (final List<Long> operOrderings : orderings) {
            // logger.info("\n\norderings to combine "
            // + Arrays.toString(operOrderings.toArray()));
            // create different branch for one level down the tree
            final List<Long> newBranch = new ArrayList<>();
            newBranch.addAll(branchList);
            newBranch.addAll(new ArrayList<Long>(operOrderings));
            // go one level deeper in the tree
            constructOrderingRecursively(diffOrderings, orderingCommandGroups,
                    treeLevel + 1, newBranch);
        }
    }
}
