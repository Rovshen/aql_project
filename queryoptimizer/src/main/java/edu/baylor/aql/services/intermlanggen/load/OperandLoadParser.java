package edu.baylor.aql.services.intermlanggen.load;

import edu.baylor.aql.entities.client.ClientSession;
import edu.baylor.aql.entities.operands.Graph;
import edu.baylor.aql.entities.operands.Operand;
import edu.baylor.aql.entities.operands.Operand.OperandType;
import edu.baylor.aql.entities.parsing.LoadMap;
import edu.baylor.aql.entities.query.IntermediateLanguageObject;
import edu.baylor.aql.services.intermlanggen.load.operand.EstimatedParser;
import edu.baylor.aql.services.intermlanggen.load.operand.GraphParser;
import edu.baylor.aql.services.intermlanggen.load.operand.OperandParser;
import edu.baylor.aql.services.intermlanggen.load.operand.TableParser;
import edu.baylor.aql.services.intermlanggen.load.operand.UnstructuredParser;
import edu.baylor.aql.util.Util;

/**
 * This controller class is responsible for {@link Operand} loading and for
 * generating graph object with all required attributes. Once graph object
 * loaded successfully a record in flushed to metadata.
 * 
 * On load failure the graph status will be set to false indicating graph
 * creation failure.
 * 
 * @author Rovshen Nazarov. Baylor University. Aug 25, 2014.All Rights Reserved.
 * 
 */
public final class OperandLoadParser extends BasicLoadParser {

    private final IntermediateLanguageObject ilo;

    /**
     * Constructor that loads a graph with a given graph path for a given query
     * id. The graph loader loads a graph from
     * the specified path into HDFS and generates {@link Graph} object.
     * 
     * @param ilo
     *            The {@link Operand} with the initial path to load from
     * @param clientSession
     *            The {@link ClientSession} with query id, which corresponds to
     *            the client session id.
     */
    public OperandLoadParser(final IntermediateLanguageObject ilo) {
        this.ilo = ilo;
    }

    /**
     * This method is required by the Runnable interface and represents Thread
     * entry point.
     * 
     * This method loads the new {@link Operand} into HDFS and computes graph
     * attributes and estimates. Once the Operand was successfully stored to
     * HDFS the {@link Operand} object is inserted into
     * metadata.
     * 
     * Note if the {@link Operand} load fails: the {@link Operand} id will have
     * failedStatus and the {@link Operand} status will be set to false.
     * 
     * @return {@link Operand} Return newly generated graph object based on the
     *         loaded graph.
     */
    @Override
    public IntermediateLanguageObject call() {
        final LoadMap loadMap = (LoadMap) ilo.getReqestObj();
        IntermediateLanguageObject iloRes = new IntermediateLanguageObject();
        final String implType = loadMap.getAttValTable().get("impltype");

        OperandParser op = null;
        final OperandType opImplT = OperandType
                .getEnumByString(implType);
        if (opImplT != null) {
            switch (opImplT) {
            case COMP_OPERAND:
                op = new EstimatedParser();
                break;
            case GRAPH:
                op = new GraphParser();
                break;
            case TABLE:
                op = new TableParser();
                break;
            case UNSTRUCTURED:
                op = new UnstructuredParser();
                break;
            default:
                op = new EstimatedParser();
                break;
            }
            iloRes = op.parse(ilo);
            iloRes.setType(ilo.getObjType());
        } else {
            iloRes.setParseStateCode(Util.FAILED_STATUS);
            iloRes.getParseMsg().append("Could not find operand type ")
                    .append(Util.LINE_SEPARATOR);
        }
        return iloRes;
    }
}
