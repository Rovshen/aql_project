package edu.baylor.aql.services.optimization.ordering;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.log4j.Logger;

import edu.baylor.aql.bootstrap.AQLServerAppSingleton;
import edu.baylor.aql.bootstrap.BootstrapAQLServer;
import edu.baylor.aql.dispatcher.AQLServerApp;
import edu.baylor.aql.entities.Element;
import edu.baylor.aql.entities.query.Query;
import edu.baylor.aql.services.requesthandler.pipeline.Visitor;
import edu.baylor.aql.services.requesthandler.pipeline.VisitorH;
import edu.baylor.aql.util.AQLException;

public final class OrderingHandler extends VisitorH {

    /**
     * Used to log errors or information.
     */
    private static Logger logger = Logger.getLogger(OrderingHandler.class
            .getName());
    private static final int POOL_SIZE = 10;
    private final ExecutorService execThreadPool;
    /**
     * AQL Server wide settings provided on the server start up.
     * Those settings define general settings that will be applied in different
     * places in the system.
     * <p>
     * The settings are accessed via singleton pattern, where the single
     * instance of the AQL Server is created on start up via
     * {@link BootstrapAQLServer} service and is accessed as needed.
     */
    private final AQLServerApp serverInstance = AQLServerAppSingleton
            .getInstance();

    public OrderingHandler() {
        execThreadPool = Executors.newFixedThreadPool(POOL_SIZE);
    }

    @Override
    public Object call() throws Exception {

        return null;
    }

    @Override
    public void visit(final Element query, final CountDownLatch doneSignal)
            throws InterruptedException, AQLException {

        try {
            if (serverInstance.getSettings().isNullOptimization()) {
                // do nothing, we use default ordering
                // count down for the vertical visitors in the handler
                for (final Visitor v : super.getVisitorsVert()) {
                    doneSignal.countDown();
                }
            } else { // full optimization
                final List<Future<Object>> resFutList = new ArrayList<>();
                final List<Query> queries = new ArrayList<>();
                final Query q = (Query) query;

                for (final Visitor ohCurr : super.getVisitorsVert()) {

                    final Query qClone = q.clone();
                    qClone.accept(ohCurr, doneSignal);
                    final Future<Object> resFut = execThreadPool.submit(ohCurr);
                    resFutList.add(resFut);
                }
                for (final Future<Object> future : resFutList) {

                    final Object queryListObj = future.get();
                    @SuppressWarnings("unchecked")
                    final List<Query> queryList = (List<Query>) queryListObj;
                    queries.addAll(queryList);
                }
                logger.info("\n\n\nTotal number of gererated queries by ordering handler BEFORE linking is "
                        + queries.size());
                int i = 1;
                // link queries in linked list manner
                for (final Query nextQ : queries) {
                    // logger.info("\n\nquery " + i + " is: " + nextQ);
                    q.appendLast(nextQ);
                    i++;
                }
                logger.info("\n\n\nTotal number of gererated queries by ordering handler is "
                        + queries.size());
            }
        } catch (final ExecutionException e) {
            e.printStackTrace();
        } finally {
            doneSignal.countDown(); // for this handler
        }
    }
}
