package edu.baylor.aql.services.optimization.ordering;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Logger;

import edu.baylor.aql.entities.Element;
import edu.baylor.aql.entities.command.Command;
import edu.baylor.aql.entities.command.CommandAlgorithm;
import edu.baylor.aql.entities.command.CommandType.OrderingType;
import edu.baylor.aql.entities.command.OrderingCommandGroup;
import edu.baylor.aql.entities.operands.ComputedOperand;
import edu.baylor.aql.entities.operands.Operand;
import edu.baylor.aql.entities.query.Query;
import edu.baylor.aql.services.query.IQueryService;
import edu.baylor.aql.services.query.QueryService;
import edu.baylor.aql.services.requesthandler.pipeline.ICommandAlgorithmService;
import edu.baylor.aql.services.requesthandler.pipeline.VisitorQueries;
import edu.baylor.aql.util.AQLException;
import edu.baylor.aql.util.Util;

/**
 * This Service orders commands in an implementation specific manner.
 * Note the method will set operands for {@link Command} with
 * {@link ComputedOperand} as needed, but the method will not alter order
 * of Operand ids in the {@link BlockingDeque}.
 * 
 * Order {@link Operand}s in the {@link Query} based on each {@link Command}'s
 * {@link OrderingType}.
 * 
 * @author Rovshen Nazarov. Baylor University. Jan 27, 2015.All Rights Reserved.
 * 
 */
public final class OrderingTyped extends VisitorQueries {

    /**
     * Used to log errors or information.
     */
    private static Logger logger = Logger.getLogger(OrderingTyped.class
            .getName());
    /**
     * This {@link Query} is a clone of the original {@link Query}.
     */
    private Query query;
    private CountDownLatch doneSignal;

    private static final int POOL_SIZE = 1;
    private final ExecutorService execThreadPool;

    public OrderingTyped() {
        super();
        execThreadPool = Executors.newFixedThreadPool(POOL_SIZE);
    }

    /**
     * Order given Query {@link Command}s and {@link Operand}s based on the
     * ordering specified by the implementation.
     * 
     * return Plan with ordered commands based on specific implementation.
     * 
     * Process {@link Command}s of a {@link Query}. For each {@link Command} get
     * its default {@link CommandAlgorithm} using
     * {@link ICommandAlgorithmService}.
     * <p>
     * Then Using
     * {@link IQueryService#populateCommandsWithOperandsSequencial(Query)} we
     * populate {@link Command}s with {@link Operand}s. We need to do this, so
     * that later we can have {@link Operand}s within
     * {@link OrderingCommandGroup} for each group.
     * <p>
     * Then Using {@link IQueryService#getOrderingCommandGroups(Queue)} we get a
     * map that maps an {@link OrderingType} to a list of
     * {@link OrderingCommandGroup}s that support that {@link OrderingType}.
     * <p>
     * Note that the result of one ordering group is used as operand in another
     * ordering group thus we expect one group to be executed before the other
     * group is executed.
     * 
     * @throws AQLException
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Query> call() throws AQLException {

        final List<Query> queries = new ArrayList<>();
        final IQueryService qService = new QueryService();
        final IOrderingService oService = new OrderingService();
        final List<Future<Object>> resFutList = new ArrayList<>();

        try {
            Query qNext = query; // operate on the reference of the query

            logger.info("in OrderingTyped");
            do { // for each linked query

                final List<OrderingCommandGroup> orderingCommandGroupMap = oService
                        .getOrderingCommandGroups(qNext.getCommands());

                final Future<Object> resFut = execThreadPool
                        .submit(new OrderingGenerator(oService, qService,
                                orderingCommandGroupMap, qNext));
                resFutList.add(resFut);

            } while ((qNext = qNext.getNext()) != null);

            final List<Object> results = Util
                    .waitForAllThreadsToTerminate(resFutList);

            for (final Object object : results) {
                queries.addAll((List<Query>) object);
            }
            logger.info("Number of generated queries in typed ordering is: "
                    + queries.size());
        } catch (final InterruptedException e) {
            logger.info(e.getMessage());
            e.printStackTrace();
        } finally {
            doneSignal.countDown();
        }
        return queries;
    }

    @Override
    public void visit(final Element query, final CountDownLatch doneSignal)
            throws InterruptedException, AQLException {

        this.query = (Query) query;
        this.doneSignal = doneSignal;
    }

}
