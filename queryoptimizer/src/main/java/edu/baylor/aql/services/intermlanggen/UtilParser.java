package edu.baylor.aql.services.intermlanggen;

import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import edu.baylor.aql.util.Util;

/**
 * This is a parsing service. Most parsing done with {@link String} type.
 * TODO change static vars to enums
 * 
 * @author Rovshen Nazarov. Baylor University. Jan 14, 2015.All Rights Reserved.
 * 
 */
public final class UtilParser {

    /**
     * Used to log errors or information.
     */
    private static Logger logger = Logger.getLogger(UtilParser.class.getName());
    public static final String FOLDER_SEPARATOR_LINUX = "/";
    public static final String DEFAULT_ATT_VAL_STRING_SEP = ",";
    public static final String DEFAULT_OPERAND_STRING_SEP = "_";
    public static final String DEFAULT_TOKEN_STRING_SEP = "_";
    public static final String DEFAULT_DASH_PLACEHOLDER = "@";
    public static final String DEFAULT_ELEMENT_SEP = " ";
    /**
     * Expected number of elements on the operand line.
     */
    public static final int EXP_LINE_ELEM_COUNT = 3;
    private static final Pattern patternAtt = Pattern
            .compile("^[ 0-9:]*ATTR_TYPE=--");
    private static final Pattern patternVal = Pattern.compile("ATTR_VALUE=");
    public final static Set<String> COMMAND_ALGO_REQUIRED_ATT_SET = new HashSet<>();
    /**
     * Default number separator for the input lines from the Command Line
     * Parser.
     */
    private static final String DEFAULT_LINE_NUMBER_SEPR = ":";

    public UtilParser() {
        populateReqAtt();
    }

    private void populateReqAtt() {
        COMMAND_ALGO_REQUIRED_ATT_SET.add("type");
        COMMAND_ALGO_REQUIRED_ATT_SET.add("path");
        COMMAND_ALGO_REQUIRED_ATT_SET.add("commandtype");
        COMMAND_ALGO_REQUIRED_ATT_SET.add("functionalType");
        COMMAND_ALGO_REQUIRED_ATT_SET.add("commandtemplate");
        COMMAND_ALGO_REQUIRED_ATT_SET.add("operandcount");
    }

    /**
     * 
     * @param str
     *            to remove from
     * @param pattern
     *            to remove
     * @return substrNonMatch Substring remained after removing the match to the
     *         pattern
     */
    public static String removeTokenFromStr(final String str,
            final Pattern pattern) {
        String substrNonMatch = "";
        final Matcher matcher = pattern.matcher(str);
        if (matcher.find()) {
            final int beginNonMatch = matcher.end();
            final int endNonMatch = str.length();
            substrNonMatch = str.substring(beginNonMatch, endNonMatch);
        }
        return substrNonMatch;
    }

    /**
     * Replace all underscores with whitespaces.
     * 
     * @param val
     * @return
     */
    public static String replaceUnderScoreWithSpaces(final String val) {
        final String newStr = val.replaceAll("[_]", " ");
        return newStr;
    }

    /**
     * Trim single quotes if any
     * 
     * @param val
     * @return
     */
    public static String trimQuotes(final String val) {
        final StringBuilder sb = new StringBuilder(val);
        final Character quote = '\'';
        if (sb.charAt(0) == quote) {
            sb.deleteCharAt(0); // remove first quote
        }
        if (sb.charAt(sb.length() - 1) == quote) {
            sb.deleteCharAt(sb.length() - 1); // remove last quote
        }
        return sb.toString();
    }

    public static String[] getAttValueEntry(final String[] elements) {
        String[] entry = null;
        if (elements.length < 2) {
            logger.error("Incorrect att value pair " + elements.length);
            return entry;
        }
        final String attIn = elements[0];
        final String valIn = elements[1];
        final String att = UtilParser.removeTokenFromStr(attIn,
                UtilParser.patternAtt);
        final String val = UtilParser.removeTokenFromStr(valIn,
                UtilParser.patternVal);
        final String valTrimed = UtilParser.trimQuotes(val);
        entry = new String[] { att, valTrimed };

        return entry;
    }

    public String[] splitTokens(final String tokenString, final int expElCount) {
        final String[] elem = tokenString.split(DEFAULT_TOKEN_STRING_SEP);
        if (elem.length < expElCount) {
            logger.error("Not enough tokens in " + tokenString);
        }
        return elem;
    }

    @SuppressWarnings("unused")
    private static String replacePlaceHolderToDashes(final String val) {
        final String newStr = val.replaceAll("[@]", "-");
        return newStr;
    }

    /**
     * Removes a string from the graph id
     * that was added for readability.
     * 
     * @param combinedStr
     *            Two parts of string connected using separator, e.g. graph_23.
     * @param separator
     *            The char separator of the prefix from the id.
     * @return stringSecondPart.
     */
    public Long splitIDPrefix(final String combinedStr, final String separator) {
        Long stringSecondPart = 0L;
        final String[] parts = combinedStr.split(separator);
        if (parts.length < 2) {
            logger.error("The string does not contains the separator "
                    + separator);
        } else {
            stringSecondPart = Long.parseLong(parts[1]);
        }
        return stringSecondPart;
    }

    /**
     * Retrieve an integer from a string, such as "1:".
     * 
     * @param string
     * @return
     */
    public static int getNumber(final String tokenIn) {

        final int expNumbIndex = 0;
        final String elements[] = tokenIn.split(DEFAULT_LINE_NUMBER_SEPR);
        // logger.info(Arrays.toString(elements));
        int res = Util.FAILED_STATUS;

        res = Integer.parseInt(elements[expNumbIndex]);
        return res;
    }

    /**
     * Split the line and get the sub query sequence number.
     * 
     * @param line
     * @return {@link Util#FAILED_STATUS} on error else the sub query sequence
     *         number
     * @throws NumberFormatException
     */
    public static int getSubQuerySeqNumb(final String line)
            throws NumberFormatException {

        int res = Util.FAILED_STATUS;
        final int expElCount = 2;
        final int seqNumbIndex = 1;

        final String[] tokens = line.split(DEFAULT_ELEMENT_SEP);
        if (tokens.length < expElCount) {
            return res; // return error status
        }
        final String subQuerySeqNumbIdStr = tokens[seqNumbIndex];
        res = Integer.parseInt(subQuerySeqNumbIdStr);
        return res;
    }
}
