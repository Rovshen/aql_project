package edu.baylor.aql.services.requesthandler.pipeline;

import edu.baylor.aql.entities.operands.ComputedOperand;
import edu.baylor.aql.entities.operands.Graph;
import edu.baylor.aql.entities.operands.Operand;
import edu.baylor.aql.entities.operands.Operand.OperandType;
import edu.baylor.aql.entities.operands.Table;
import edu.baylor.aql.entities.operands.Unstructured;

/**
 * Get operand implementation object based on the {@link Operand}
 * {@link OperandType}
 * 
 * @author Rovshen Nazarov. Baylor University. Nov 14, 2014.All Rights Reserved.
 * 
 */
public final class OperandFactory {
    /**
     * Get in instance of an {@link Operand} based on the
     * {@link OperandType}
     * 
     * @param oit
     * @return operandObj
     */
    public static Operand getInstance(final OperandType oit) {
        Operand instance = new ComputedOperand();
        switch (oit) {
        case COMP_OPERAND: // already initialized
            break;
        case GRAPH:
            instance = new Graph();
            break;
        case TABLE:
            instance = new Table();
            break;
        case UNSTRUCTURED:
            instance = new Unstructured();
            break;
        default:
            break;
        }
        return instance;
    }
}
