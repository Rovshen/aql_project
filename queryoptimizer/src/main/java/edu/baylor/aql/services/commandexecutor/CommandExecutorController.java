package edu.baylor.aql.services.commandexecutor;

import io.netty.channel.Channel;

import java.io.IOException;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingDeque;

import org.apache.log4j.Logger;

import edu.baylor.aql.dataaccess.IMetadata;
import edu.baylor.aql.dataaccess.Metadata;
import edu.baylor.aql.entities.client.ClientSession;
import edu.baylor.aql.entities.command.Command;
import edu.baylor.aql.entities.command.CommandAlgorithm;
import edu.baylor.aql.entities.operands.Operand;
import edu.baylor.aql.entities.operands.Operand.OperandState;
import edu.baylor.aql.entities.query.Query;
import edu.baylor.aql.services.requesthandler.pipeline.CommandAlgorithmService;
import edu.baylor.aql.services.requesthandler.pipeline.ICommandAlgorithmService;
import edu.baylor.aql.util.AQLException;
import edu.baylor.aql.util.Util;
import edu.baylor.aql.util.Util.ResultState;

/**
 * This service class does some setting and preparation for the
 * {@link CommandExecutorController}. It does not wait for the result of the
 * {@link CommandExecutorController} and neither does its caller.
 * 
 * IT also processes single {@link Command}, such as UNION, INTER, etc. Use
 * Optimization package services to select best {@link CommandAlgorithm} for
 * most optimal {@link Command} processing.
 * 
 * This method prepares {@link Command} to be executed by the
 * {@link CommandExecutorController} All necessary {@link Operand}s are
 * retrieved from the metadata using {@link Metadata} using {@link Operand} Ids
 * and stored into current {@link Command}.
 * 
 * The number of operands for the {@link Command} depends on the number of
 * operands set for the {@link CommandAlgorithm} , which is defined in the
 * metadata.
 * 
 * This is a controller class that performs execution of a single
 * {@link Command}.
 * 
 * @deprecated see {@link CommandBatchExecutorController}.
 * 
 * @author Rovshen Nazarov. All rights Reserved.
 * 
 */
@Deprecated
public final class CommandExecutorController implements
        Callable<Util.ResultState> {

    private static Logger logger = Logger
            .getLogger(CommandExecutorController.class.getName());
    /**
     * Thread pool used to control threads.
     * Initialized on the object construction to the
     * fixed pool with predefined size.
     */
    private final ExecutorService execThreadPool;

    /**
     * List of {@link Operand}s that will be used to execute commands.
     */
    private final List<Long> operands;
    /**
     * The {@link Command} to execute by this controller.
     */
    private final Command currentCommand;

    /**
     * Database access class used to executed SQL related operations,
     * such as data retrieval and loading into in memory java objects.
     */
    private final IMetadata dao;
    /**
     * This queue is blocking because the graph Ids are generatated in similar
     * principal to consumer/producer pattern, where consumer are
     * {@link Command}s
     * and producer are other {@link Command}s. Thus some {@link Command}s block
     * until
     * the {@link Operand} they need is available.
     */
    private final BlockingDeque<Long> operandsWaiting;

    /**
     * Use the client session to write messages to the client
     */
    private final ClientSession clientSession;
    /**
     * All bash commands for the {@link Query}.
     */
    private final StringBuffer bashCommands;
    private final Query queryToExec;

    /**
     * Constructor.
     * This constructor takes required parameters needed for this
     * controller to process the command.
     * 
     * @param currentCommand
     *            The command to process.
     * @param operandIds
     *            Blocking Queue of the ids of the operands for the current
     *            command.
     * @param queryToExec
     *            The {@link ClientSession} that has {@link Channel} to write to
     *            the client via.
     * @param bashCommands
     */
    public CommandExecutorController(final Command currentCommand,
            final Query queryToExec, final StringBuffer bashCommands) {
        // DOMConfigurator.configure("./config/log4j-config.xml");
        if (currentCommand == null || queryToExec == null) {
            logger.error("Arguments can not be null.");
        }
        this.queryToExec = queryToExec;
        this.bashCommands = bashCommands;
        this.currentCommand = currentCommand;
        operands = currentCommand.getOperands();
        execThreadPool = Executors.newFixedThreadPool(10);
        // operandsWaiting = queryToExec.getOperandsWaiting();
        // TODO this class is not expected to be used
        operandsWaiting = new LinkedBlockingDeque<>(50);
        clientSession = queryToExec.getClientSession();
        dao = new Metadata();
    }

    /**
     * This method is required by the {@link Runnable} interface
     * and is used as the entry point for this controller.
     * In this method the current command is stored into the Queue commands
     * and that later executed using
     * {@link CommandExecutorController#executeCommand()} method.
     * 
     * Upon successful execution of the command the result {@link Operand} is
     * generated
     * initialized and stored into the metadata.
     * 
     * If the operands needed for the {@link Command} are not available an error
     * message
     * is produced and send to the client.
     * 
     * @throws InterruptedException
     * @throws AQLException
     * @throws IOException
     */
    @Override
    public Util.ResultState call() throws InterruptedException, AQLException,
            IOException {
        final Util.ResultState resState = new Util.ResultState();
        resState.status = Util.SUCCESS_STATUS;
        final ICommandAlgorithmService caService = new CommandAlgorithmService();
        logger.info("processing command: "
                + currentCommand.getCommandSeqNumber());

        try {
            prepareCommand(operandsWaiting, currentCommand, resState);
            caService.waitForOperands(queryToExec, operandsWaiting,
                    currentCommand, resState);
            // logger.info(currentCommand);
            currentCommand.setCommand(caService.substPlaceHolders(queryToExec,
                    currentCommand.getCommandAlgo(), currentCommand));
            if (currentCommand.getCommand().equals("")) {
                throw new AQLException("command to execute cannot be empty");
            }
            if (!checkOperandsStatus()) {
                generateOperStatFailErrState(resState);
            }
            bashCommands.append(currentCommand.getCommand()).append(
                    Util.LINE_SEPARATOR);
            final int execResult = executeCommand(currentCommand);
            if (execResult == Util.SUCCESS_STATUS
                    && resState.status != Util.FAILED_STATUS) {
                logger.info("Command " + currentCommand.getCommand()
                        + " SUCCEEDED.");
                caService.recordCommandResult(queryToExec, currentCommand);
                if (currentCommand.getResult() == Util.FAILED_STATUS) {
                    resState.msg.append("Could not get last id ");
                    resState.status = Util.FAILED_STATUS;
                } else {
                    try {
                        logger.info(currentCommand.getResult());
                        operandsWaiting.putLast(currentCommand.getResult());
                    } catch (final InterruptedException e) {
                        throw new InterruptedException(
                                "Command execution was interrupted");
                    }
                    caService.buildResultMessage(queryToExec, resState,
                            currentCommand);
                }
            } else {
                resState.msg.append("Command ")
                        .append(currentCommand.getCommand()).append(" FAILED.");
                resState.status = Util.FAILED_STATUS;
                logger.error(resState.msg.toString());
            }
        } finally {
            final Util util = new Util();
            util.shutdownAndAwaitTermination(execThreadPool);
        }
        return resState;
    }

    /**
     * Generates error message due to failed status of some operands.
     * Reuses StringBuilder object msg that is expected to be cleared for
     * reuse before this method invoked.
     * 
     * Add to the error message ids of the {@link Operand}s that caused the
     * {@link Command} termination.
     * 
     * @param resState
     */
    private void generateOperStatFailErrState(final ResultState resState) {
        // generate error message for status fail
        resState.msg.append("Problem executing command ").append(
                currentCommand.getCommandType());
        for (final Long operand : operands) {
            resState.msg.append(" due to operands ").append(
                    queryToExec.getOperMap().get(operand).getReadableId());
        }
        resState.msg.append(Util.LINE_SEPARATOR);
        resState.status = Util.FAILED_STATUS;
    }

    /**
     * Check if any status of the operands is negative, which indicates that the
     * graph was loaded or generated with
     * errors.
     * 
     * @return status of operands
     */
    private boolean checkOperandsStatus() {
        // Check Operands status in metadata
        boolean operandsStatus = true;
        for (final Long operand : operands) {
            operandsStatus = operandsStatus
                    && queryToExec.getOperMap().get(operand).getStatus();
        }
        return operandsStatus;
    }

    /**
     * Wait for intermediate results to be generated. Once processed
     * successfully check if the command has all required
     * graphs generated and ready for use.
     * 
     * Was replaced by the {@link BlockingQueue} for the same effect.
     * This method was introduced to simulate producer/consumer design pattern.
     * 
     * @throws InterruptedException
     */
    @SuppressWarnings("unused")
    @Deprecated
    private void waitForCommandsToFinish(final Operand operand1,
            final Operand operand2, final Queue<Command> commandsToExecute) {

        final int waitTimeMls = 5000; // 5 sec
        dao.openConnection();
        try {
            while (!(operand1.getStatus() && operand2.getStatus())) {
                synchronized (commandsToExecute) {
                    commandsToExecute.wait(waitTimeMls);
                }
                operand1.setStatus(dao.lookUpOperandStatus(operand1.getId()));
                operand2.setStatus(dao.lookUpOperandStatus(operand2.getId()));
                if (operand1.getStatus() && operand2.getStatus()) {
                    synchronized (commandsToExecute) {
                        commandsToExecute.offer(currentCommand);
                    }
                }
                final int milliseconds = 2000; // sleep 2 sec.
                Thread.sleep(milliseconds);
            }
        } catch (final InterruptedException e) {
        } finally {
            dao.closeConnection();
        }
    }

    /**
     * Execute {@link Command} using {@link BashExecutor} and check its result
     * using {@link Future}.
     * 
     * @return int Result status of executing command using {@link BashExecutor}
     *         .
     */
    private int executeCommand(final Command command) {

        int result = Util.FAILED_STATUS;
        // execute query command and check result
        final AbstractExecutor execCurrCommand;
        if (!"".equals(command.getCommand())) {
            execCurrCommand = new BashExecutor(command.getCommand());
            final Future<Object> resultFutureCur = execThreadPool
                    .submit(execCurrCommand);
            try { // block here until result is ready
                result = (int) resultFutureCur.get();
            } catch (InterruptedException | ExecutionException e) {
                logger.error("Could not get result of the executing command with callable thread."
                        + e.getMessage());
            }
        }
        return result;
    }

    /**
     * Prepare to process single {@link Command}, such as UNION, INTER, etc. Use
     * best {@link CommandAlgorithm} for most optimal {@link Command}
     * processing.
     * <p>
     * This method prepares {@link Command} to be executed by the
     * {@link CommandExecutorController} All necessary {@link Operand}s are
     * retrieved from the metadata using {@link Metadata} using OperandIds and
     * stored into current {@link Command}.
     * <p>
     * The number of operands for the {@link Command} depends on the number of
     * operands set for the {@link CommandAlgorithm}, which is defined in the
     * metadata.
     * 
     * @param command
     * @param operandsWaiting2
     * @param resState
     * @throws IOException
     * @throws InterruptedException
     * @throws AQLException
     */
    private void prepareCommand(final BlockingDeque<Long> operandsWaiting2,
            final Command command, final ResultState resState)
            throws IOException, InterruptedException, AQLException {
        CommandAlgorithm commandAlgo = null;
        if (command == null) {
            resState.status = Util.FAILED_STATUS;
        } else {
            commandAlgo = command.getCommandAlgo();
            if (commandAlgo == null) {
                resState.status = Util.FAILED_STATUS;
                resState.msg.append("Command Algorithm was null for "
                        + command.getCommandType());
            }
            dao.openConnection();
            final String hdfsResultPath = dao.lookUpSystemSetting("resultPath",
                    "hdfs");
            dao.closeConnection();
            if (hdfsResultPath.equals("")) {
                resState.status = Util.FAILED_STATUS;
                resState.msg
                        .append("could not get result path from the settings");
            }
            final StringBuilder resultGraphPath = new StringBuilder(
                    hdfsResultPath).append("/")
                    .append(clientSession.getSessionId()).append("/")
                    .append(command.getCommandType()).append("_")
                    .append(command.getCommandSeqNumber()).append("/");
            final Operand resO = queryToExec.getOperMap().get(
                    command.getResult());

            resO.setType(OperandState.INTERMEDIATE_RESULT.getLabel())
                    .setInitialPath(resultGraphPath.toString())
                    .setLoadedPath(resultGraphPath.toString());
            // TODO tell how long approx processing should take
            logger.info("Prepared command result operand "
                    + command.getResult() + Util.LINE_SEPARATOR);
        }
    }

}
