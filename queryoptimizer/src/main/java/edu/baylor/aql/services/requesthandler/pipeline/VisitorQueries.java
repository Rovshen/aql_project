package edu.baylor.aql.services.requesthandler.pipeline;

import java.util.List;

import edu.baylor.aql.entities.query.Query;
import edu.baylor.aql.util.AQLException;

/**
 * This Service generates {@link Query} {@link List}.
 * 
 * @author Rovshen Nazarov. Baylor University. Oct 10, 2014.All Rights Reserved.
 * 
 */
public abstract class VisitorQueries extends Visitor {

    /**
     * Generate {@link Query} {@link List}.
     * 
     * @return {@link List} of {@link Query}ies.
     * @throws AQLException
     */
    @Override
    public abstract List<Query> call() throws AQLException;

}
