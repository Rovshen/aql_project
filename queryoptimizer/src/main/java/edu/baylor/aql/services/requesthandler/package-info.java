/**
 * This package contains services that are used to structure API based on Visitor pattern. 
 * The key service interfaces are {@link edu.baylor.aql.services.requesthandler.pipeline.Visitor}, {@link edu.baylor.aql.services.requesthandler.pipeline.VisitorH},
 * and {@link edu.baylor.aql.entities.Element}. Together they are used to structure service providing in such a way that it is easy to add a new {@link edu.baylor.aql.services.requesthandler.pipeline.Visitor}
 * to perform additional service to the object that implements {@link edu.baylor.aql.entities.Element} abstract class.
 * The Purpose of {@link edu.baylor.aql.services.requesthandler.pipeline.VisitorH} is to invoke one or more {@link edu.baylor.aql.services.requesthandler.pipeline.Visitor}
 * to perform services on the provided object.
 * <p>
 * This allows modularity and extensibility of a group of services that are performed on a set of similar objects.
 * E.g. most of the services that implement {@link edu.baylor.aql.services.requesthandler.pipeline.Visitor} abstract class are in the optimization library.
 * <p>
 * Also the idea of handlers is that those are controller classes that invoke {@link edu.baylor.aql.services.requesthandler.pipeline.Visitor} services via {@link edu.baylor.aql.services.requesthandler.pipeline.VisitorH}
 * service. The reason we have two different visitor service classes is because we want an {@link edu.baylor.aql.entities.Element} to be visited by the chain of visitors sequentially, 
 * and we want an {@link edu.baylor.aql.entities.Element} to be visited concurrently by the set of visitors vertically.
 */
package edu.baylor.aql.services.requesthandler;