/**
 * This package contains Services used for estimating values related to an {@link edu.baylor.aql.entities.operands.Operand} based on some {@link edu.baylor.aql.entities.command.Command}
 * with some {@link edu.baylor.aql.entities.operands.Operand}s.
 * The estimation results does not have to be exact and will be used for analyzes of the possible {@link edu.baylor.aql.entities.query.Query} execution time.  
 * <p>
 * This package may also contain estimation services to estimate run-time of a {@link edu.baylor.aql.entities.command.CommandAlgorithm} on a 
 * given {@link edu.baylor.aql.entities.tools.Tool} 
 * using well known statistical estimation techniques.
 */
package edu.baylor.aql.services.optimization.estimating;

