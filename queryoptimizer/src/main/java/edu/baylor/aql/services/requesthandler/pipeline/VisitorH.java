package edu.baylor.aql.services.requesthandler.pipeline;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import edu.baylor.aql.entities.Element;
import edu.baylor.aql.util.AQLException;

/**
 * This abstract class is desgned to be extended by the processor services that
 * have one or more {@link Visitor} services to run.
 * 
 * @author Rovshen Nazarov. Baylor University. Jan 14, 2015.All Rights Reserved.
 * 
 */
public abstract class VisitorH extends Visitor {
    /**
     * Visit concrete Elements.
     * Add other elements to visit as needed.
     * 
     * @param query
     */
    private final List<Visitor> visitorsVert = new ArrayList<>();
    /**
     * Number of threads for fixed pull executor.
     */
    private static final int POOL_SIZE = 20;
    private final ExecutorService execThreadPool = Executors
            .newFixedThreadPool(POOL_SIZE);

    /**
     * @return the execThreadPool
     */
    public ExecutorService getExecThreadPool() {
        return execThreadPool;
    }

    @Override
    public abstract void visit(final Element query,
            final CountDownLatch doneSignal) throws InterruptedException,
            AQLException;

    /**
     * @return the visitorsVert
     */
    public List<Visitor> getVisitorsVert() {
        return visitorsVert;
    }

    /**
     * @param visitorsVert
     *            the visitorsVert to set
     */
    public void addVisitorsVert(final Visitor v) {
        visitorsVert.add(v);
    }

}
