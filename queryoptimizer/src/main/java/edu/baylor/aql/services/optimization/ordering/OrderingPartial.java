package edu.baylor.aql.services.optimization.ordering;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import edu.baylor.aql.entities.Element;
import edu.baylor.aql.entities.command.Command;
import edu.baylor.aql.entities.command.CommandAlgorithm;
import edu.baylor.aql.entities.query.Query;
import edu.baylor.aql.services.requesthandler.pipeline.VisitorQueries;
import edu.baylor.aql.util.AQLException;

/**
 * Generates partial command ordering.
 * Just uses default {@link CommandAlgorithm}s with use provided ordering.
 * Default {@link CommandAlgorithm}s have priority 0.
 * 
 * @author Rovshen Nazarov. Baylor University. Oct 10, 2014.All Rights Reserved.
 * 
 */
public final class OrderingPartial extends VisitorQueries {

    @SuppressWarnings("unused")
    private Query query;
    private CountDownLatch doneSignal;

    public OrderingPartial() {

    }

    /**
     * Return commands in the same order as received. Add default
     * {@link CommandAlgorithm}s to the {@link Command}s.
     * 
     * @throws AQLException
     */
    @Override
    public List<Query> call() throws AQLException {
        final List<Query> queries = new ArrayList<>();
        try {
            // do nothing default ordering is already established
        } finally {
            doneSignal.countDown();
        }
        return queries;
    }

    @Override
    public void visit(final Element query, final CountDownLatch doneSignal)
            throws InterruptedException {
        this.query = (Query) query;
        this.doneSignal = doneSignal;
    }
}
