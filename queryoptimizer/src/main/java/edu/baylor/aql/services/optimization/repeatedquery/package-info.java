/**
 * This package contains services used to identify {@link edu.baylor.aql.entities.command.Command}s that have been executed before and for which there is available result.
 * The services use metadata information and {@link edu.baylor.aql.entities.operands.Operand}s from the {@link edu.baylor.aql.entities.query.Query} to
 * identify what {@link edu.baylor.aql.entities.command.Command}s the {@link edu.baylor.aql.entities.operands.Operand}s participated in before.
 * If the {@link edu.baylor.aql.entities.operands.Operand}s for a given {@link edu.baylor.aql.entities.command.Command} participated in another
 * {@link edu.baylor.aql.entities.command.Command} executed earlier and that old {@link edu.baylor.aql.entities.command.Command} and new {@link edu.baylor.aql.entities.command.Command}
 * have the same {@link edu.baylor.aql.entities.command.CommandType} and {@link edu.baylor.aql.entities.operands.Operand}s, then the new {@link edu.baylor.aql.entities.command.Command}
 * is marked and updated as a repeated {@link edu.baylor.aql.entities.command.Command} by the services in this package.
 * <p>
 * Identifying repeated {@link edu.baylor.aql.entities.command.Command}s allows speed up execution of {@link edu.baylor.aql.entities.query.Query}s that contain {@link edu.baylor.aql.entities.command.Command}s
 * executed in other {@link edu.baylor.aql.entities.query.Query}s, which is part of optimization module. 
 */
package edu.baylor.aql.services.optimization.repeatedquery;