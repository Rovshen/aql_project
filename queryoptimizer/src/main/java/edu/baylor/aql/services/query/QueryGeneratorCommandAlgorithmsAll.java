/**
 * 
 */
package edu.baylor.aql.services.query;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.CountDownLatch;
import java.util.logging.Logger;

import edu.baylor.aql.dataaccess.IMetadata;
import edu.baylor.aql.dataaccess.Metadata;
import edu.baylor.aql.entities.Element;
import edu.baylor.aql.entities.command.Command;
import edu.baylor.aql.entities.command.CommandAlgorithm;
import edu.baylor.aql.entities.command.CommandType;
import edu.baylor.aql.entities.query.Query;
import edu.baylor.aql.services.requesthandler.pipeline.VisitorQueries;
import edu.baylor.aql.util.AQLException;

/**
 * This service generates all possible combinations of {@link CommandAlgorithm}s
 * for a given {@link Command} list based on the {@link CommandType} of each
 * {@link Command}.
 * 
 * We have potentially exponential growth of number of Queries with respect to
 * number of possible combinations of CommandAlgorithms, where order matters.
 * 
 * E.g. a INTER b UNION c, given MRinter, GLinter, Xinter, and MRunion, GLunion,
 * Xunion command algorithms. We get 9 queries.
 * 
 * In general we have multiplicative growth in number of queries, where we
 * multiply # of CommAlgo per Command together.
 * 
 * @author Rovshen Nazarov. Baylor University. Mar 17, 2015.All Rights Reserved.
 */

public class QueryGeneratorCommandAlgorithmsAll extends VisitorQueries {

    /**
     * Used to log errors or information.
     */
    private static Logger logger = Logger
            .getLogger(QueryGeneratorCommandAlgorithmsAll.class.getName());
    /**
     * This {@link Query} is a clone of the original {@link Query}.
     */
    private Query query;
    private CountDownLatch doneSignal;

    /**
     * 
     */
    public QueryGeneratorCommandAlgorithmsAll() {
    }

    /*
     * (non-Javadoc)
     * 
     * @see edu.baylor.aql.services.requesthandler.pipeline.Visitor#call()
     */
    @Override
    public List<Query> call() throws AQLException {

        final List<Query> queriesAll = new ArrayList<>();
        final IMetadata dao = new Metadata();
        final Queue<Command> commands = query.getCommands();
        final Queue<Command> emptyCommands = new LinkedList<>();

        // we will re-populate query with commands later
        query.setCommands(emptyCommands);
        // init all queries
        queriesAll.add(query);

        try {
            dao.openConnection();
            for (final Command c : commands) {

                final List<Query> queriesCommand = new ArrayList<>();
                final List<CommandAlgorithm> commAlgoList = dao
                        .lookUpCommandAlgosByCommandType(c.getCommandType());

                for (final CommandAlgorithm cAlgo : commAlgoList) {
                    // generate possible queries for this command + command
                    // algorithm pair
                    c.setCommandAlgo(cAlgo);
                    final List<Query> queriesCA = generateQueries(c, queriesAll);
                    queriesCommand.addAll(queriesCA);
                }
                // remove previous incomplete queries
                queriesAll.clear();
                // add new complete up to a given command queries
                queriesAll.addAll(queriesCommand);
                // logger.info(Arrays.toString(queriesAll.toArray()));
            }

            dao.closeConnection();

            logger.info("Number of generated queries in query generator for all Command algorithms is: "
                    + queriesAll.size());

            final IQueryService qService = new QueryService();

            // populate a query's commands with operands
            for (final Query q : queriesAll) {

                qService.numberCommands(q);
                // populate query initially with operands
                qService.populateCommandsWithOperandsSequencial(q);
            }

        } catch (final AQLException e) {
            // close db on error
            dao.closeConnection();
            // assign error state to the first query
            queriesAll.get(0).setErrState(true);
            queriesAll.get(0).setStateMsg(e.getMessage());
            // e.printStackTrace();
        } finally {
            doneSignal.countDown();
        }
        return queriesAll;
    }

    /**
     * Generate {@link Query}s for the {@link Command}, {@link CommandAlgorithm}
     * pair.
     * 
     * If there are some previous {@link Query}s make their copies and add them
     * to the result {@link List}.
     * 
     * @param c
     * @param queries
     * @return {@link List} of {@link Query}s
     */
    private List<Query> generateQueries(final Command c,
            final List<Query> queries) {

        final List<Query> resQList = new ArrayList<>();

        for (final Query q : queries) {

            // create clone from existing query
            final Query cloneQ = q.clone();

            cloneQ.getCommands().add(c.clone());
            resQList.add(cloneQ);
        }
        return resQList;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * edu.baylor.aql.services.requesthandler.pipeline.Visitor#visit(edu.baylor
     * .aql.entities.Element, java.util.concurrent.CountDownLatch)
     */
    @Override
    public void visit(final Element query, final CountDownLatch doneSignal)
            throws InterruptedException, AQLException {

        this.query = (Query) query;
        this.doneSignal = doneSignal;

    }

}
