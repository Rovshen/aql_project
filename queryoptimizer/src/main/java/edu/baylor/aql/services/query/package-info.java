/**
 * This package contains classes that perform {@link Query} related non optimization services, 
 */
/**
 * @author Rovshen Nazarov. Baylor University. Mar 17, 2015.All Rights Reserved.
 *
 */
package edu.baylor.aql.services.query;

