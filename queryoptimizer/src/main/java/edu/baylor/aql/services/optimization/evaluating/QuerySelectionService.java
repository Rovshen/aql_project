package edu.baylor.aql.services.optimization.evaluating;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import edu.baylor.aql.entities.Element;
import edu.baylor.aql.entities.command.Command;
import edu.baylor.aql.entities.command.CommandAlgorithm;
import edu.baylor.aql.entities.query.Query;
import edu.baylor.aql.util.AQLException;

public class QuerySelectionService implements IQuerySelectionService {

    /**
     * Used to log errors or information.
     */
    @SuppressWarnings("unused")
    private static Logger logger = Logger.getLogger(QuerySelectionService.class
            .getName());

    @Override
    public Query selectHighestProgressQuery(final Query queryIn)
            throws AQLException {
        Query optimalQ = queryIn; // default optimal is the input query
        Query prev = queryIn;
        double highestProgress = 0.0;
        final List<Query> sameProgressQList = new ArrayList<>();
        do {
            if (prev.getProgress() > highestProgress) {
                sameProgressQList.clear();
                // clear prev queries
                sameProgressQList.add(prev); // store curr query
                highestProgress = prev.getProgress();
                optimalQ = prev;
            } else if (prev.getProgress() == highestProgress) {
                // add query with the same progress to the list
                sameProgressQList.add(prev);
            }
            // logger.info(prev);
        } while ((prev = prev.getNext()) != null);

        if (sameProgressQList.size() > 1) {
            // select one of the queries with the same progress
            optimalQ = selectQueryBasedOnCAPriority(sameProgressQList);
        }

        if (optimalQ.getProgress() > queryIn.getProgress()) {
            return optimalQ;
        } else {
            return queryIn;
        }
    }

    /**
     * Select a {@link Query} based on the
     * {@link CommandAlgorithm#getPriority()}. The {@link Query} whose
     * {@link Command}s have the highest priority, which is indicated by the
     * lowest sum of the {@link Command} priorities will be returned.
     * 
     * @param sameProgressQList
     * @return
     */
    private Query selectQueryBasedOnCAPriority(
            final List<Query> sameProgressQList) throws AQLException {

        // logger.info(Arrays.asList(sameProgressQList.toArray()));
        Query result = new Query();
        // set at lowest priority
        long priortySumSmallest = Integer.MAX_VALUE;

        for (final Query q : sameProgressQList) {
            long priorSumCurr = 0; // set at highest priority
            for (final Command c : q.getCommands()) {
                priorSumCurr += c.getCommandAlgo().getPriority();
            }

            // if all commands priorities's sum is smaller, update
            if (priorSumCurr < priortySumSmallest) {
                // update result query and highest priority sum
                priortySumSmallest = priorSumCurr;
                result = q;
            }
        }
        return result;
    }

    @Override
    public Query selectLowestCostQuery(final Element queryin) {
        Query queryToExec = (Query) queryin;
        Query queryCurr = queryToExec;
        final int lowestCost = -1;
        do {
            if (queryCurr.getEstExecCost() < lowestCost) {
                queryToExec = queryCurr;
            }
        } while ((queryCurr = queryToExec.getNext()) != null);
        return queryToExec;
    }
}
