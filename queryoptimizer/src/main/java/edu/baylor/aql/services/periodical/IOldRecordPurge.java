package edu.baylor.aql.services.periodical;

import edu.baylor.aql.services.commandexecutor.BashExecutor;

/**
 * This service performs cleaning tasks on the system used by AQL.
 * 
 * @author Rovshen Nazarov. Baylor University. Jan 14, 2015.All Rights Reserved.
 * 
 */
public interface IOldRecordPurge {
    /**
     * Removes all records in the provided folder on HDFS system.
     * Return {@link Boolean#TRUE} on success.
     * Uses {@link BashExecutor} to execute HDFS command.
     * 
     * @param folderName
     */
    public boolean removeAllInFolderRecords(String folderName);
}
