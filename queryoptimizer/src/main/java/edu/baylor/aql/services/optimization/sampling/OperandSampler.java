package edu.baylor.aql.services.optimization.sampling;

import java.io.File;

import org.apache.log4j.Logger;

/**
 * Sampling service.
 * 
 * This service class performs precise computations on physical graph object.
 * This service could be used to perform precise computations on graph if graph
 * size is small enough.
 * 
 * This service estimates graph density, avg # of neighbors,
 * # of nodes, # of edges, result graph size estimate and assumes
 * graph uniform distribution
 * 
 * @author Rovshen Nazarov. Baylor University. Aug 25, 2014.All Rights Reserved.
 * 
 */
public final class OperandSampler implements Sampler {

    private static Logger logger = Logger.getLogger(OperandSampler.class
            .getName());
    private final static long SIZE_MB_IN_GB = 1024;

    /**
     * Assumption is made that the graph in its own folder, e.g.
     * mygraphpath/mygraphfolder/mygraph.
     * Thus a graph is either split into parts in the mygraphfolder folder
     * or graph is a single file, mygraph.
     * 
     * @param graphPath
     *            either file path or folder path
     * @return size of a file or all files in folder in MB
     */
    public long computeSize(final String graphPath) {
        if (graphPath == null) {
            logger.error("Graph path cannot be null.");
            return 0;
        }
        long size = 0L;
        final File dir = new File(graphPath);
        if (dir.isDirectory()) {
            final File[] innerFiles = dir.listFiles();
            for (final File file : innerFiles) {
                // calculate size recursively
                size += file.length();
            }
        } else {
            size = dir.length(); // in bytes
        }
        // convert to Megabytes
        size = (size / SIZE_MB_IN_GB) / SIZE_MB_IN_GB;

        return size;
    }
}
