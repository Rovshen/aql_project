package edu.baylor.aql.services.commandexecutor;

import io.netty.channel.Channel;

import java.io.IOException;

import org.apache.log4j.Logger;

import edu.baylor.aql.dataaccess.IMetadata;
import edu.baylor.aql.dataaccess.Metadata;
import edu.baylor.aql.entities.client.ClientSession;
import edu.baylor.aql.entities.command.Command;
import edu.baylor.aql.entities.command.CommandAlgorithm;
import edu.baylor.aql.entities.operands.Operand;
import edu.baylor.aql.entities.operands.Operand.OperandState;
import edu.baylor.aql.entities.query.Query;
import edu.baylor.aql.services.requesthandler.pipeline.CommandAlgorithmService;
import edu.baylor.aql.services.requesthandler.pipeline.ICommandAlgorithmService;
import edu.baylor.aql.util.AQLException;
import edu.baylor.aql.util.Util;
import edu.baylor.aql.util.Util.ResultState;

/**
 * This service class does some setting and preparation for running
 * {@link Query} bash script.
 * <p>
 * It processes all {@link Command}s, such as UNION, INTER, etc from the
 * {@link Query} and stored them as a bash sript to be run later.
 * <p>
 * This method prepares each {@link Command} from the {@link Query} to be
 * executed by the {@link BashExecutor}. All necessary {@link Operand}s are
 * retrieved from the metadata using {@link Metadata} using {@link Operand} Ids
 * and stored into current {@link Command}.
 * <p>
 * The number of operands for the {@link Command} depends on the number of
 * operands set for the {@link CommandAlgorithm} , which is defined in the
 * metadata.
 * <p>
 * This is a controller class that performs execution of all {@link Command}s
 * for a given {@link Query}.
 * <p>
 * 
 * @author Rovshen Nazarov. All rights Reserved.
 * 
 */
public final class CommandBatchExecutorController {

    private static Logger logger = Logger
            .getLogger(CommandBatchExecutorController.class.getName());

    /**
     * The {@link Command} to execute by this controller.
     */
    private final Command currentCommand;

    /**
     * Database access class used to executed SQL related operations,
     * such as data retrieval and loading into in memory java objects.
     */
    private final IMetadata dao;

    /**
     * Use the client session to write messages to the client
     */
    private final ClientSession clientSession;
    /**
     * All bash commands for the {@link Query}.
     */
    private final StringBuffer bashCommands;
    /**
     * Query related to {@link Command} to execute.
     */
    private final Query queryToExec;

    /**
     * Constructor.
     * This constructor takes required parameters needed for this
     * controller to process the command.
     * 
     * @param currentCommand
     *            The command to process.
     * @param operandIds
     *            Blocking Queue of the ids of the operands for the current
     *            command.
     * @param queryToExec
     *            The {@link ClientSession} that has {@link Channel} to write to
     *            the client via.
     * @param bashCommands
     */
    public CommandBatchExecutorController(final Command currentCommand,
            final Query queryToExec, final StringBuffer bashCommands) {
        if (currentCommand == null || queryToExec == null) {
            logger.error("Arguments can not be null.");
        }
        this.bashCommands = bashCommands;
        this.currentCommand = currentCommand;
        this.queryToExec = queryToExec;
        clientSession = queryToExec.getClientSession();
        dao = new Metadata();
    }

    /**
     * This method is required by the {@link Runnable} interface
     * and is used as the entry point for this controller.
     * In this method the current command is stored into the Queue commands
     * and that later executed using
     * {@link CommandExecutorController#executeCommand()} method.
     * 
     * Upon successful execution of the command the result {@link Operand} is
     * generated
     * initialized and stored into the metadata.
     * 
     * If the operands needed for the {@link Command} are not available an error
     * message
     * is produced and send to the client.
     * 
     * @throws InterruptedException
     * @throws AQLException
     * @throws IOException
     */
    public Util.ResultState process() throws InterruptedException,
            AQLException, IOException {
        final Util.ResultState resState = new Util.ResultState();
        resState.status = Util.SUCCESS_STATUS;
        final ICommandAlgorithmService caService = new CommandAlgorithmService();
        // logger.info("processing command: "
        // + currentCommand.getCommandSeqNumber());

        prepareCommandResultPath(currentCommand, resState);
        caService.updateResultOperandRefer(queryToExec, currentCommand,
                resState);
        // logger.info(currentCommand);
        currentCommand.setCommand(caService.substPlaceHolders(queryToExec,
                currentCommand.getCommandAlgo(), currentCommand));
        if (currentCommand.getCommand().equals("")) {
            throw new AQLException("command to execute cannot be empty");
        }
        bashCommands.append(currentCommand.getCommand()).append(
                Util.LINE_SEPARATOR);
        // logger.info(bashCommands.toString());
        return resState;
    }

    /**
     * Prepare to process single {@link Command}, such as UNION, INTER, etc. Use
     * best {@link CommandAlgorithm} for most optimal {@link Command}
     * processing.
     * <p>
     * This method prepares {@link Command} to be executed by the
     * {@link BashBatchExecutor} All necessary {@link Operand}s are retrieved
     * from the metadata using {@link Metadata} using OperandIds and stored into
     * current {@link Command}.
     * <p>
     * The number of operands for the {@link Command} depends on the number of
     * operands set for the {@link CommandAlgorithm}, which is defined in the
     * metadata.
     * 
     * PRE: {@link Command}'s result {@link Operand} cannot be null.
     * 
     * @param command
     * @param resState
     * @throws IOException
     * @throws InterruptedException
     * @throws AQLException
     */
    private void prepareCommandResultPath(final Command command,
            final ResultState resState) throws IOException,
            InterruptedException, AQLException {
        CommandAlgorithm commandAlgo = null;
        if (command == null) {
            resState.status = Util.FAILED_STATUS;
        } else {
            commandAlgo = command.getCommandAlgo();
            if (commandAlgo == null) {
                resState.status = Util.FAILED_STATUS;
                resState.msg.append("Command Algorithm was null for "
                        + command.getCommandType());
            }
            dao.openConnection();
            final String hdfsResultPath = dao.lookUpSystemSetting("resultPath",
                    "hdfs");
            dao.closeConnection();
            if (hdfsResultPath.equals("")) {
                resState.status = Util.FAILED_STATUS;
                resState.msg
                        .append("could not get result path from the settings");
            }
            final StringBuilder resultGraphPath = new StringBuilder(
                    hdfsResultPath).append("/")
                    .append(clientSession.getSessionId()).append("/")
                    .append(command.getCommandType()).append("_")
                    .append(command.getCommandSeqNumber()).append("/");
            if (command.getResult() == null) { // log err
                logger.error("Command's result Operand cannot be null.");
                throw new AQLException( // user err
                        "Command's result Operand cannot be null.");
            }
            final Operand resultO = queryToExec.getOperMap().get(
                    command.getResult());
            resultO.setType(OperandState.INTERMEDIATE_RESULT.getLabel())
                    .setInitialPath(resultGraphPath.toString())
                    .setLoadedPath(resultGraphPath.toString());
            // TODO tell how long approx processing should take
            // logger.info("Prepared command result operand "
            // + command.getResult() + Util.LINE_SEPARATOR);
        }
    }
}
