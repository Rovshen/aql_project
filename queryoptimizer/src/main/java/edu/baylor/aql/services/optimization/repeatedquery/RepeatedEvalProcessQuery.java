package edu.baylor.aql.services.optimization.repeatedquery;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import org.apache.log4j.Logger;

import edu.baylor.aql.dataaccess.IMetadata;
import edu.baylor.aql.dataaccess.Metadata;
import edu.baylor.aql.entities.command.Command;
import edu.baylor.aql.entities.operands.Operand;
import edu.baylor.aql.entities.query.Query;
import edu.baylor.aql.services.query.IQueryService;
import edu.baylor.aql.util.AQLException;
import edu.baylor.aql.util.Util;

public class RepeatedEvalProcessQuery implements Callable<Object> {

    private static Logger logger = Logger
            .getLogger(RepeatedEvalProcessQuery.class.getName());

    private final IQueryService qService;
    private final Query query;
    private final IMetadata dao;

    public RepeatedEvalProcessQuery(final Query qNext,
            final IQueryService qService) {
        query = qNext;
        this.qService = qService;
        dao = new Metadata();
    }

    @Override
    public Object call() throws Exception {
        processQuery(qService, query);
        return null;
    }

    /**
     * PRE: {@link Command}s are expected to have at least one {@link Operand}.
     * 
     * @param qService
     * @param qClone
     * @throws AQLException
     */
    private void processQuery(final IQueryService qService, final Query qClone)
            throws AQLException {

        dao.openConnection();

        // logger.info("\n\n\nrepeated command processing query: " + qClone);
        final Command lastStoredCommand = processSingleQuery(qClone);
        final int completedCommandCount = countCompletedCommands(qClone);

        // logger.info("\n\n\ncompleted commands count " + completedCommandCount
        // + " query command size " + qClone.getCommands().size());
        if (completedCommandCount == qClone.getCommands().size()) {
            // logger.info("Stored Last Command id: " +
            // lastStoredCommand.getId());
            queryComplete(qClone, lastStoredCommand);
        }
        // update query progress
        if (completedCommandCount >= 0) {
            qClone.setQueryProgress((double) completedCommandCount
                    / qClone.getCommands().size());
        }
        // logger.info("\n\nupdated by repeated service query: " + qClone);
        dao.closeConnection();
    }

    /**
     * Count number of commands in the query that were executed before and do
     * not need to be run.
     * 
     * @param q
     * @return
     */
    private int countCompletedCommands(final Query q) {

        int complCommCount = 0;

        for (final Command c : q.getCommands()) {
            if (c.isStored()) {
                complCommCount++;
            }
        }
        return complCommCount;
    }

    private Command processSingleQuery(final Query qClone) throws AQLException {

        Command lastStoredCommand = null;

        for (final Command command : qClone.getCommands()) {
            Command commandStored = null;
            if (command.getOperands().size() < 1) {
                logger.error("A command is expected to have at least one Operand.");
                throw new AQLException(
                        "A command is expected to have at least one Operand.");
            }
            final Long operandFirst = command.getOperands().get(0);
            boolean commandsEqual = false;

            final List<Long> commandStoredIds = dao
                    .lookUpCommandIdsByOperandId(operandFirst);
            // logger.info("Ids of the stored commands " + commandStoredIds);
            final List<Command> commandStoredList = new ArrayList<>();
            for (final Long commandStoredIdC : commandStoredIds) {
                commandStoredList.add(dao.lookUpCommandById(commandStoredIdC));
            }
            // logger.info(commandStoredList);
            int index = 0;
            while (!commandsEqual && (index < commandStoredList.size())) {

                final Command commandStoredC = commandStoredList.get(index);
                if (commandStoredC.getOperands().size() == command
                        .getOperands().size()) {
                    if (command
                            .getCommandTypeObj()
                            .getTypeName()
                            .equals(commandStoredC.getCommandTypeObj()
                                    .getTypeName())) {
                        final int diff = checkCommDifference(command,
                                commandStoredC, qClone);
                        if (diff == 0) {
                            // command operand sets are the same
                            commandsEqual = true;
                            commandStored = commandStoredC;
                            lastStoredCommand = commandStoredC;
                            // logger.info(commandStoredC);
                        }
                    }
                }
                index++;
            }
            // logger.info("Stored command " + commandStored);
            if (commandsEqual) {
                updateRepeatedCommandResult(command, commandStored, qService,
                        qClone);
                // logger.info("Updated Command: " + command);
            }
        }
        return lastStoredCommand;
    }

    /**
     * Check if two {@link Command}s have the same set of {@link Operand}s using
     * {@link Operand}'s id for comparison.
     * 
     * NOTE: we need to compare the {@link Operand}s in the order as some of the
     * {@link Command}s may be order sensitive.
     * 
     * Pre: the size of operands must be the same.
     * 
     * @param command
     * @param commandStored
     * @param qClone
     * @return
     */
    private int checkCommDifference(final Command command,
            final Command commandStored, final Query q) {

        // set diff to 0 and change if two sets are different
        int diff = 0;
        final List<Long> operandIdSetCurr = new ArrayList<>();
        for (final Long key : command.getOperands()) {
            operandIdSetCurr.add(q.getOperMap().get(key).getId());
        }
        final List<Long> operandIdSetStor = commandStored.getOperands();
        logger.info("Id list new command: " + operandIdSetCurr);
        logger.info("Id list stored command: " + operandIdSetStor);
        for (int i = 0; i < operandIdSetCurr.size(); i++) {
            final long currOpId = operandIdSetCurr.get(i);
            final long stOpId = operandIdSetStor.get(i);
            if (currOpId != stOpId) {
                diff++;
            }
        }
        logger.info("Id set difference: " + diff);
        return diff;
    }

    /**
     * 
     * @param command
     * @param commandStored
     * @param qService
     * @param qClone
     * @throws AQLException
     */
    private void updateRepeatedCommandResult(final Command command,
            final Command commandStored, final IQueryService qService,
            final Query qClone) throws AQLException {

        final Long result = command.getResult();
        final Long repeatedResult = commandStored.getResult();

        // logger.info(result);
        // logger.info(repeatedResult);
        qService.updateIntermResults(qClone, result, repeatedResult);
        command.setStored(true).setId(commandStored.getId());
    }

    /**
     * NOTE: this method is only invoked if all {@link Command} of the given
     * {@link Query} are repeated.
     * <p>
     * If all commands were executed before and they related to the same query
     * id get result of that query id.
     * 
     * @param query2
     * @param lastStoredCommand
     * @throws AQLException
     */
    private void queryComplete(final Query queryIn,
            final Command lastStoredCommand) throws AQLException {

        final long queryStoredId = dao.lookUpQueryId(lastStoredCommand.getId());
        logger.info("Stored Query id: " + queryStoredId);
        final Query queryStored = dao.lookUpQuery(queryStoredId);

        if (queryStored.getCommands().size() == queryIn.getCommands().size()) {

            if (queryStored.getResultOperand() != Util.FAILED_STATUS) {

                queryIn.setResultOperand(queryStored.getResultOperand());
                queryIn.setQueryProgress(queryStored.getProgress()); // completed
                queryIn.setRepeated(true);
            } else {
                throw new AQLException(
                        "Could not get query result opearnd for the query id "
                                + queryStoredId);
            }
        } else {
            /**
             * The two queries differ, but the input query is complete.
             */
            queryIn.setResultOperand(lastStoredCommand.getResult());
            queryIn.setQueryProgress(1.0); // completed
            queryIn.setRepeated(true);
        }
    }

}
