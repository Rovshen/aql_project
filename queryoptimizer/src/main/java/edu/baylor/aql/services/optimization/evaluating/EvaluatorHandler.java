package edu.baylor.aql.services.optimization.evaluating;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Logger;

import edu.baylor.aql.bootstrap.AQLServerAppSingleton;
import edu.baylor.aql.bootstrap.BootstrapAQLServer;
import edu.baylor.aql.dispatcher.AQLServerApp;
import edu.baylor.aql.entities.Element;
import edu.baylor.aql.entities.query.Query;
import edu.baylor.aql.services.requesthandler.pipeline.Visitor;
import edu.baylor.aql.services.requesthandler.pipeline.VisitorH;
import edu.baylor.aql.util.AQLException;

/**
 * Run different evaluators on each {@link Query} ordering.
 * 
 * @author Rovshen Nazarov. Baylor University. Dec 3, 2014.All Rights Reserved.
 * 
 */
public final class EvaluatorHandler extends VisitorH {

    /**
     * Used to log errors or information.
     */
    private static Logger logger = Logger.getLogger(EvaluatorHandler.class
            .getName());
    private static final int POOL_SIZE = 10;
    private final ExecutorService execThreadPool;
    /**
     * AQL Server wide settings provided on the server start up.
     * Those settings define general settings that will be applied in different
     * places in the system.
     * <p>
     * The settings are accessed via singleton pattern, where the single
     * instance of the AQL Server is created on start up via
     * {@link BootstrapAQLServer} service and is accessed as needed.
     */
    private final AQLServerApp serverInstance = AQLServerAppSingleton
            .getInstance();

    public EvaluatorHandler() {
        execThreadPool = Executors.newFixedThreadPool(POOL_SIZE);
    }

    /**
     * Sets cost value for each {@link Query} based on estimated
     * time to execute the {@link Query}.
     */
    @Override
    public Object call() throws InterruptedException {
        return null;
    }

    @Override
    public void visit(final Element query, final CountDownLatch doneSignal)
            throws InterruptedException, AQLException {

        logger.info("in evaluator handler");
        try {
            if (serverInstance.getSettings().isNullOptimization()) {
                // do nothing
                // count down for the vertical visitors in the handler
                for (final Visitor v : super.getVisitorsVert()) {
                    doneSignal.countDown();
                }
            } else { // full optimization
                final List<Future<Object>> resFutList = new ArrayList<>();
                for (final Visitor v : super.getVisitorsVert()) {
                    // do not run a service on different objects as the service
                    // has
                    // a state
                    // be careful with a state change of the shared object
                    // and be careful with done signal count down latch
                    query.accept(v, doneSignal);
                    final Future<Object> resFut = execThreadPool.submit(v);
                    resFutList.add(resFut);
                    // wait for all visitors to finish
                    for (final Future<Object> future : resFutList) {
                        try {
                            future.get();
                        } catch (final ExecutionException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            logger.info("leaving evaluator handler");
        } finally {
            doneSignal.countDown(); // for this handler
        }
    }

}
