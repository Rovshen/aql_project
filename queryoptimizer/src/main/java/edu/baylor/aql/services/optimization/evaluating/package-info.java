/**
 * This package contains services that are used to evaluate a given operation in form of a {@link edu.baylor.aql.entities.query.Query} or a {@link edu.baylor.aql.entities.command.Command}.
 * <p>
 * The goal of an evaluation service is to evaluate how long it will take to run a given {@link edu.baylor.aql.entities.command.Command} or a {@link edu.baylor.aql.entities.query.Query}
 * and to generate a cost based on that evaluation. Evaluation shall involve some metrics gathered and stored related to the {@link edu.baylor.aql.entities.tools.Tool} to be used
 * {@link edu.baylor.aql.entities.command.CommandAlgorithm} to be used and {@link edu.baylor.aql.entities.operands.Operand}s involved in {@link edu.baylor.aql.entities.command.Command}
 * execution. For a {@link edu.baylor.aql.entities.query.Query} evaluation will involved the same analyzes as for the {@link edu.baylor.aql.entities.command.Command} performed on a collection
 * of {@link edu.baylor.aql.entities.command.Command}s. 
 */
package edu.baylor.aql.services.optimization.evaluating;

