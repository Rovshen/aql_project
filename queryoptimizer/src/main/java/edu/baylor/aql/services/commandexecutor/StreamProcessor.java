package edu.baylor.aql.services.commandexecutor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.log4j.Logger;

/**
 * Reads provided stream and prints results of the stream to the {@link Logger}.
 * This is a helper service that is used for a stream processing.
 * 
 * @author Rovshen Nazarov. Baylor University. Jun 24, 2014.All Rights Reserved.
 * 
 */
public final class StreamProcessor implements Runnable {
    private final InputStream inStream;
    private static Logger logger = Logger.getLogger(StreamProcessor.class
            .getName());

    /**
     * Constructor that that initializes input stream for this stream processor.
     * 
     * @param inInputStream
     */
    public StreamProcessor(final InputStream inInputStream) {
        // DOMConfigurator.configure("./config/log4j-config.xml");
        if (inInputStream == null)
            logger.error("The inInputStream is NULL");
        inStream = inInputStream;
    }

    /**
     * This method is required by the Thread class.
     * This method reads from the class's input stream using
     * {@link BufferedReader} until it reaches null and writes read data into
     * the logger.
     */
    @Override
    public void run() {
        try {
            final BufferedReader buffReader = new BufferedReader(
                    new InputStreamReader(inStream));
            String readLine = null;
            while ((readLine = buffReader.readLine()) != null) {
                System.out.println(">>" + readLine);
            }
        } catch (final IOException e) {
            logger.error(e.getMessage());
        }
    }
}
