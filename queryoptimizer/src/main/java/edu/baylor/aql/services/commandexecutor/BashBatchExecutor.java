package edu.baylor.aql.services.commandexecutor;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.concurrent.Callable;

import org.apache.log4j.Logger;
import org.omg.SendingContext.RunTime;

import edu.baylor.aql.entities.command.Command;
import edu.baylor.aql.util.AQLException;
import edu.baylor.aql.util.Util;

/**
 * This service is responsible for executing bash script with one or more bash
 * commands.
 * It captures error and input stream of the bash execution to be executed and
 * evaluated command exit value.
 * 
 * This service uses Callable interface, which is a recent extension to the
 * Runnable interface to support result return from the service thread.
 * 
 * @author Rovshen Nazarov. Baylor University. Aug 25, 2014.All Rights Reserved.
 * 
 */
public final class BashBatchExecutor extends AbstractExecutor {

    private static Logger logger = Logger.getLogger(BashBatchExecutor.class
            .getName());
    /**
     * Bash that this bash executor will execute.
     */
    private String bashToExecute;
    private static final String ENV = "bash";
    private static final String BASH_SCRIPT = "#!/bin/bash";
    private static final String BASH_EXT = ".sh";

    /**
     * Constructor.
     * This constructor creates a BashExecutor object with a given
     * command to execute.
     * 
     * @param bashToExec
     *            {@link Command} to execute by this bash executor.
     */
    public BashBatchExecutor(final String bashToExec) {
        bashToExecute = bashToExec;
    }

    /**
     * This method executes given bash command and returns result status of the
     * execution.
     * During execution input and error streams of the executed bash command are
     * captured by {@link StreamProcessor} and printed to the standard output.
     * This method uses {@link RunTime} to process bash command and it uses
     * {@link Process} to get
     * error and input streams.
     * 
     * @param bashToExecute
     *            Command to execute.
     * @return Return status, 0 success, 1 failed.
     */
    @Override
    public int execute() throws AQLException {

        if (bashToExecute == null) {
            return Util.FAILED_STATUS;
        }
        File bashScriptFile = null;
        try {
            bashToExecute = BASH_SCRIPT + Util.LINE_SEPARATOR + bashToExecute;
            bashScriptFile = createBashScript(bashToExecute);
            final ProcessBuilder pb = new ProcessBuilder(ENV,
                    bashScriptFile.getAbsolutePath());
            logger.info("\n\n\nExecuting command " + bashToExecute);
            logger.info("From the file  " + bashScriptFile.getAbsolutePath()
                    + "\n\n");

            final Process processExec = pb.start();

            final StreamProcessor errStream = new StreamProcessor(
                    processExec.getErrorStream());
            final StreamProcessor inpStream = new StreamProcessor(
                    processExec.getInputStream());
            final Thread errT = new Thread(errStream);
            final Thread inT = new Thread(inpStream);
            errT.start();
            inT.start();
            final int exitVal = processExec.waitFor();
            logger.info("Command exit value: " + exitVal);
            if (exitVal == 0) {
                return Util.SUCCESS_STATUS;
            }
        } catch (final Throwable e) {
            logger.error("\n\nFailed to execute command " + bashToExecute
                    + "\nError msg: " + e.getMessage());
        } finally {
            if (bashScriptFile != null)
                bashScriptFile.delete();
        }
        return Util.FAILED_STATUS;

    }

    private File createBashScript(final String bashToExecuteIn)
            throws IOException {
        final String fileName = "bashScript";
        final File tempScript = File.createTempFile(fileName, BASH_EXT);
        final Writer streamWriter = new OutputStreamWriter(
                new FileOutputStream(tempScript));
        try (final PrintWriter prWriter = new PrintWriter(streamWriter)) {
            prWriter.println(bashToExecuteIn);
        }
        return tempScript;
    }

    /**
     * This method is required by the {@link Callable} object.
     * This method runs {@link BashExecutor#execute(String)} and returns the
     * result of executing that method.
     */
    @Override
    public Object call() throws Exception {
        return execute();
    }
}
