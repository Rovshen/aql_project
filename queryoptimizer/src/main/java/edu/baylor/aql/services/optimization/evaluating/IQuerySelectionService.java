package edu.baylor.aql.services.optimization.evaluating;

import edu.baylor.aql.entities.Element;
import edu.baylor.aql.entities.command.CommandAlgorithm;
import edu.baylor.aql.entities.query.Query;
import edu.baylor.aql.util.AQLException;

public interface IQuerySelectionService {

    /**
     * Select {@link Query} with the highest progress, if more than one query
     * have the same progress return the head {@link Query}.
     * 
     * If more than one {@link Query} has the same progress select the one with
     * the {@link CommandAlgorithm}s that have lowest priority number (which is
     * highest priority).
     * 
     * @param queryIn
     * @return
     * @throws AQLException
     */
    public Query selectHighestProgressQuery(Query queryIn) throws AQLException;

    /**
     * Select Query orderings with the lowest cost.
     * 
     * @param queryin
     * @return
     */
    public Query selectLowestCostQuery(final Element queryin);

}
