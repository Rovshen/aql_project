/**
 * This package contains core services that extend {@link edu.baylor.aql.services.requesthandler.pipeline.Visitor} abstract class
 * and operate on {@link edu.baylor.aql.entities.Element} entity abstract object and its subclassses.
 * <p>
 * Each of the services in this package provides an API service to contribute to optimization of the code to be generated.
 * This package contains other packages that are composed of services and packages specific to a certain area of optimization.
 * The services in this package are invoked through {@link edu.baylor.aql.services.requesthandler.pipeline.EPipelineSingleton} object.
 * The services invocation involves visitor pattern structure and involves {@link edu.baylor.aql.services.requesthandler.pipeline.Visitor}s and
 * {@link edu.baylor.aql.services.requesthandler.pipeline.VisitorH}s.
 * <p>
 * When modifying the services and packages in this library, special care should be taken to expose only through API only those services that are needed
 * and hide other methods to allow modification of implementation.
 * As this system evolves some implementations may be substituted by faster or more robust implementations, yet the interfaces that those implementations provided
 * are expected to remain the same.
 */
package edu.baylor.aql.services.optimization;

