package edu.baylor.aql.services.requesthandler.pipeline;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingDeque;

import org.apache.log4j.Logger;

import edu.baylor.aql.dataaccess.IMetadata;
import edu.baylor.aql.dataaccess.Metadata;
import edu.baylor.aql.entities.command.Command;
import edu.baylor.aql.entities.command.CommandAlgorithm;
import edu.baylor.aql.entities.operands.ComputedOperand;
import edu.baylor.aql.entities.operands.Operand;
import edu.baylor.aql.entities.operands.Operand.OperandState;
import edu.baylor.aql.entities.operands.Operand.OperandType;
import edu.baylor.aql.entities.query.Query;
import edu.baylor.aql.util.AQLException;
import edu.baylor.aql.util.Util;
import edu.baylor.aql.util.Util.ResultState;

/**
 * This service performs operations for the {@link CommandAlgorithm} object and
 * its fields.
 * 
 * @author Rovshen Nazarov. Baylor University. Jan 14, 2015.All Rights Reserved.
 * 
 */
public final class CommandAlgorithmService implements ICommandAlgorithmService {

    /**
     * Used to log errors or information.
     */
    private static Logger logger = Logger
            .getLogger(CommandAlgorithmService.class.getName());
    /**
     * This service is used to operate AQL metadata. This service helps store
     * and retrieve metadata from the database.
     */
    private final IMetadata dao;
    /**
     * Cache {@link CommandAlgorithm}s. This field should be updated when new
     * {@link CommandAlgorithm}s are added.
     */
    private static Map<String, List<CommandAlgorithm>> commAlgoCached = new HashMap<>();
    /**
     * Control access to cached {@link CommandAlgorithm}s.
     */
    private final Object mutex = new Object();

    public CommandAlgorithmService() {
        dao = new Metadata();
    }

    /**
     * Clear {@link CommandAlgorithm}'s cache.
     * Need to use synchronization because operating on a static object, which
     * represents a state shared by many instances of this service.
     */
    public void clearCachedCommAlgo() {
        synchronized (mutex) {
            commAlgoCached.clear();
        }
    }

    /**
     * Get {@link CommandAlgorithm} with highest priority
     * 
     * @throws AQLException
     */
    @Override
    public CommandAlgorithm getHighestPriorityCA(final Command command)
            throws AQLException {

        final int priorityPrev = -1; // any lower than 0 value
        CommandAlgorithm resultCA = null;

        if (command == null) {
            logger.error("The command can not be NULL");
            return null;
        }
        final List<CommandAlgorithm> commands = getAllCommandAlgos(command
                .getCommandType());
        if (commands.isEmpty()) { // if no tools define for commandType
            logger.error("Could not get command algorithms for command "
                    + command.getCommandType());
            return null;
        }
        for (final CommandAlgorithm commandC : commands) {

            final int priorityCur = commandC.getPriority();
            if (priorityCur > priorityPrev) {
                resultCA = commandC;
            }
        }
        return resultCA;
    }

    /**
     * Get and cache {@link CommandAlgorithm}s
     * 
     * @throws AQLException
     */
    @Override
    public CommandAlgorithm getDefaultCA(final Command command)
            throws AQLException {

        final int defaultPriority = 0;
        if (command == null) {
            logger.error("The command can not be NULL");
            return null;
        }
        final List<CommandAlgorithm> commandAlgos = getAllCommandAlgos(command
                .getCommandType());
        if (commandAlgos.isEmpty()) { // if no tools define for commandType
            logger.error("Could not get command algorithms for command "
                    + command.getCommandType());
            return null;
        }
        for (final CommandAlgorithm commandC : commandAlgos) {
            final int priority = commandC.getPriority();
            if (priority == defaultPriority) {
                return commandC; // return default command
            }
        }
        if (!commandAlgos.isEmpty()) {
            // if no default return the first one
            return commandAlgos.get(0);
        }
        return null;
    }

    /**
     * On retrieve from metadata cache {@link CommandAlgorithm}s.
     * 
     * @param commandType
     * @return
     * @throws AQLException
     */
    private List<CommandAlgorithm> getAllCommandAlgos(final String commandType)
            throws AQLException {
        List<CommandAlgorithm> commands = new ArrayList<>();
        if (commAlgoCached.get(commandType) != null) {
            commands = commAlgoCached.get(commandType);
        } else {
            dao.openConnection();
            commands = dao.lookUpCommandAlgosByCommandType(commandType);
            // cache results
            commAlgoCached.put(commandType, commands);
            dao.closeConnection();
        }
        return commands;
    }

    @Override
    public String substPlaceHolders(final Query q,
            final CommandAlgorithm commandAlgo, final Command currentCommand)
            throws AQLException {
        // logger.info(currentCommand);
        String resultCommand = "";
        if (commandAlgo == null || currentCommand == null) {
            logger.error("Arguments can not be null.");
            return resultCommand;
        }
        if (currentCommand.getResult() == null
                || commandAlgo.getCommandTemplate() == null) {
            logger.error("Arguments can not be null.");
            return resultCommand;
        }

        if (commandAlgo.getTool() == null) {
            throw new AQLException("Need to provide tool");
        }
        if (commandAlgo.getTool().getPrefix() == null
                || commandAlgo.getTool().getPath() == null) {
            throw new AQLException("Need to provide tool");
        }
        final StringBuilder commandTemplBuilder = new StringBuilder(commandAlgo
                .getTool().getPrefix()).append(" ")
                .append(commandAlgo.getTool().getPath())
                .append(commandAlgo.getPath())
                .append(commandAlgo.getCommandTemplate());
        resultCommand = commandTemplBuilder.toString();
        Integer i = 1;
        for (final Long oId : currentCommand.getOperands()) {
            final Operand operand = q.getOperMap().get(oId);
            if (operand == null) {
                throw new AQLException("Could not find operand.");
            }
            if (operand.getLoadedPath() == null) {
                throw new AQLException("Loaded path cannot be empty for "
                        + operand.toString());
            }
            resultCommand = resultCommand.replace(
                    ICommandAlgorithmService.OPERAND_PLACEHOLDER + i,
                    operand.getLoadedPath());
            i++;
        }
        resultCommand = resultCommand.replace(
                ICommandAlgorithmService.RESULT_OPERAND_PLACEHOLDER, q
                        .getOperMap().get(currentCommand.getResult())
                        .getLoadedPath());

        return resultCommand;
    }

    /**
     * Blocking method.
     */
    @Override
    public void waitForOperands(final Query q,
            final BlockingDeque<Long> operandsWaiting, final Command command,
            final ResultState resState) throws InterruptedException,
            AQLException {

        final CommandAlgorithm commandAlgo = command.getCommandAlgo();
        final List<Long> operandsResTemp = new ArrayList<>();
        command.setEstOper(findIntermedOperands(q, command.getOperands()));
        logger.info("Done estimating operands");
        populateWaitOperands(q, operandsResTemp, operandsWaiting, command
                .getEstOper().size());
        if (command.getEstOper().size() < operandsResTemp.size()) {
            resState.msg.append("Not enough Operands for ")
                    .append(command.getCommandType()).append(" expected ")
                    .append(commandAlgo.getNumberOfOperands());
            logger.error(resState.toString());
            resState.status = Util.FAILED_STATUS;
        }

        if (!replaceCompOperandsWithResultOper(q, command, operandsResTemp,
                operandsWaiting)) {
            resState.status = Util.FAILED_STATUS;
            resState.msg.append(" Could not get all expected operands ");
        }
        // logger.info(command);
        resState.status = Util.SUCCESS_STATUS;
    }

    private void populateWaitOperands(final Query q,
            final List<Long> operandsResTemp,
            final BlockingDeque<Long> operandsWaiting, final int countOperToWait)
            throws InterruptedException, AQLException {
        if (countOperToWait == 0) {
            return; // nothing to wait for
        }
        // may block here
        for (int i = 0; i < countOperToWait; i++) {
            logger.info("waiting for operands " + countOperToWait);
            final Long op = operandsWaiting.takeFirst();
            logger.info("Received operand " + op);
            if (Operand.OperandType.COMP_OPERAND == q.getOperMap().get(op)
                    .getImplementationType()) {
                throw new AQLException("Unexcpected Computed Operand Type "
                        + q.getOperMap().get(op).getImplementationType()
                                .getLabel());
            }
            operandsResTemp.add(op);
        }
        logger.info("DONE waiting");

    }

    /**
     * Blocking method.
     * 
     * Get {@link Operand} that have the same
     * {@link Command#getCommandSeqNumber()} as {@link ComputedOperand}.
     * <p>
     * If none available block and wait.
     * 
     * @param command
     * @param operandsResTemp
     * @param operandsWaiting
     * @throws InterruptedException
     * @throws AQLException
     */
    private boolean replaceCompOperandsWithResultOper(final Query q,
            final Command command, final List<Long> operandsResTemp,
            final BlockingDeque<Long> operandsWaiting)
            throws InterruptedException, AQLException {

        // check if desired operands were produced
        int operandsReceived = 0;
        while (operandsReceived < command.getEstOper().size()) {
            /**
             * We expect one result operand to match one computed operand
             */
            for (final Long operandRes : operandsResTemp) {
                boolean match = false;
                for (Long operandEst : command.getEstOper()) {
                    if (q.getOperMap().get(operandRes).getCommand()
                            .getCommandSeqNumber() == (q.getOperMap()
                            .get(operandEst).getCommand().getCommandSeqNumber())) {
                        operandEst = operandRes;
                        operandsReceived++;
                        match = true;
                    }
                }
                if (!match) { // if not what we were waiting for put it back
                    operandsWaiting.putLast(operandRes);
                }
            }
            operandsResTemp.clear();
            final int remaining = command.getEstOper().size()
                    - operandsReceived;
            populateWaitOperands(q, operandsResTemp, operandsWaiting, remaining);
        }
        return true; // success

    }

    /**
     * Check if {@link OperandType#COMP_OPERAND} and based on that add
     * {@link ComputedOperand}s to {@link Command}.
     * 
     * @param list
     * @return
     */
    private List<Long> findIntermedOperands(final Query q, final List<Long> list)
            throws AQLException {
        final List<Long> estOper = new ArrayList<>();

        for (final Long oId : list) {

            // logger.info(OperandState.INTERMEDIATE_RESULT.getLabel().equals(
            // operand.getType().getLabel()));

            final int minCommResId = -2;
            if (oId <= minCommResId) {
                estOper.add(oId);
                logger.info("Operand to wait for " + oId);
            } // TODO uncomment for debugging
              // logger.info(estOper);
        }

        return estOper;
    }

    /**
     * Update references to the result {@link Operand} of the current
     * {@link Command}.
     */
    @Override
    public void updateResultOperandRefer(final Query queryToExec,
            final Command currentCommand, final ResultState resState)
            throws AQLException {
        final Long resOpId = currentCommand.getResult();
        // status=false, type=intermediate result, commandId=0
        for (final Command commC : queryToExec.getCommands()) {
            if (commC.getCommandSeqNumber() == currentCommand
                    .getCommandSeqNumber()) {
                continue; // skip self
            }
            for (final Long oId : commC.getOperands()) {
                if (resOpId.equals(oId)) {
                    queryToExec
                            .getOperMap()
                            .get(oId)
                            .setLoadedPath(
                                    queryToExec.getOperMap().get(resOpId)
                                            .getLoadedPath());
                }
            }
        }
    }

    @Override
    public void recordCommandResult(final Query q, final Command currentCommand)
            throws AQLException {

        final Operand result = q.getOperMap().get(currentCommand.getResult());
        result.setStatus(true);
        result.setId(Util.FAILED_STATUS);
        result.setType(OperandState.INTERMEDIATE_RESULT.getLabel());

        dao.openConnection();
        result.setId(result.insert(dao));
        dao.closeConnection();

        q.getOperMap().put((long) result.hashCode(), result);
        currentCommand.setResult((long) result.hashCode());

        // logger.info("Result Operanad " + result);
    }

    @Override
    public void buildResultMessage(final Query q, final ResultState resState,
            final Command currentCommandIn) throws AQLException {
        resState.msg.append("The result of the operation ")
                .append(currentCommandIn.getCommandType()).append(" between ");
        for (final Long operand : currentCommandIn.getOperands()) {
            resState.msg.append(q.getOperMap().get(operand).getReadableId())
                    .append(" ");
        }
        resState.msg.append("is ").append(
                q.getOperMap().get(currentCommandIn.getResult())
                        .getReadableId());
    }
}
