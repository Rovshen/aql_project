/**
 * This package contains services that implement  {@link edu.baylor.aql.services.requesthandler.pipeline.Visitor} abstract class.
 * The goal of services in this package is to provide some services to entities implementing {@link edu.baylor.aql.entities.Element} abstract class.
 * The services in this package are grouped together based on service type by {@link edu.baylor.aql.services.requesthandler.pipeline.VisitorH} and
 * invoked through {@link edu.baylor.aql.services.requesthandler.pipeline.EPipelineSingleton}.
 * <p>
 * This package also contains service that are applied to a specific entity objects to perform 
 * operations related to those entities or related to state of those entities.
 * Special caution should be taken, when modifying those services as many other services depend on them
 * for performing common operations on some entities. 
 * <p>
 * http://refcardz.dzone.com/refcardz/design-patterns
 * VISITOR pattern allows for one or more operation to be applied to a set of 
 * objects at runtime, decoupling the operations from the object structure.  
 * Gang of Four Design patterns. Use When: 
 * 1) An object structure must have many unrelated operations performed upon it. 
 * 2) The object structure can't change but operations performed on it can. 
 * 3) Operations must be performed on the concrete classes of an object structure. 
 * 4)Exposing internal state or operations of the object structure is acceptable.
 * Ordering handler, estimation handler and evaluation handler will 
 * implement visitor interface those visitors will visit Query object and will return or not 
 * return some results after the visit
 */
package edu.baylor.aql.services.requesthandler.pipeline;