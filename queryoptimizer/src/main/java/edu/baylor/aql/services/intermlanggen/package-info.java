/**
 * This package provides AQL request related parsing and its services are invoked by {@link edu.baylor.aql.dispatcher.SessionManager}.
 * This package provides two parsing libraries load parser and query parser. Those libraries are invoked in the {@link edu.baylor.aql.services.intermlanggen.RequestParser}
 * controller class based on request type.
 * This package generates {@link edu.baylor.aql.entities.query.IntermediateLanguageObject}.
 * The type of the object contained in the {@link edu.baylor.aql.entities.query.IntermediateLanguageObject} depends on the client request's type.
 */
package edu.baylor.aql.services.intermlanggen;