package edu.baylor.aql.services.requesthandler.pipeline;

import java.util.concurrent.BlockingDeque;

import edu.baylor.aql.entities.command.Command;
import edu.baylor.aql.entities.command.CommandAlgorithm;
import edu.baylor.aql.entities.operands.Operand;
import edu.baylor.aql.entities.query.Query;
import edu.baylor.aql.util.AQLException;
import edu.baylor.aql.util.Util.ResultState;

/**
 * This interface defines {@link CommandAlgorithm} services.
 * 
 * @author Rovshen Nazarov. Baylor University. Dec 3, 2014.All Rights Reserved.
 * 
 */
public interface ICommandAlgorithmService {
    public static String OPERAND_PLACEHOLDER = "dataPath";
    public static String RESULT_OPERAND_PLACEHOLDER = "resultPath";

    /**
     * Gets default execution command, which is identified in command metadata
     * by priority level 0.
     * Each command is expected to have default command.
     * 
     * @param command
     * @return null or {@link CommandAlgorithm}
     * @throws AQLException
     */
    public CommandAlgorithm getDefaultCA(Command command) throws AQLException;

    /**
     * Pre-condition, {@link Operand}'s data is loaded from the metadata.
     * 
     * Substitute real values in place of place holders. This method is used to
     * replace place holders of the command
     * string in the {@link CommandAlgorithm} template with the {@link Operand}
     * related values in the {@link Command}.
     * 
     * Replace placeholder elements with
     * the command and command algorithm arguments.
     * Thus, we make necessary substitutions to generic command
     * 
     * @param commandAlgo
     *            {@link CommandAlgorithm} The algorithm that will be used to
     *            execute command.
     * @param currentCommand
     *            {@link Command} Command being processed.
     * 
     * @return String result of the replacement of the place holders with the
     *         {@link Operand}s values.
     * @throws AQLException
     */
    public String substPlaceHolders(Query q,
            final CommandAlgorithm commandAlgo, final Command currentCommand)
            throws AQLException;

    /**
     * Pre-conditions, result {@link Operand} is set to the
     * {@link CommandAlgorithm}'s result operand type.
     * 
     * Blocking method.
     * 
     * Check there is enough Operands available to run Command.
     * If not enough arguments, then takeFirst will block until
     * operand ids are available.
     * <p>
     * This design follows producer/consumer pattern.
     * <p>
     * Another way to implement this is to do a check per period if the status
     * of expected operand has changed.
     * 
     * TODO we could combine commands here if {@link CommandAlgorithm} permits
     * executing more than 2 {@link Operand}s at the same time.
     * 
     * @param commandAlgo
     * @param operandIds
     * @param command
     * @param clientSession
     * @throws InterruptedException
     * @throws AQLException
     */
    public void waitForOperands(final Query q,
            final BlockingDeque<Long> operandsWaiting, final Command command,
            final ResultState resState) throws InterruptedException,
            AQLException;

    /**
     * Update all references in the {@link Query} to the result {@link Operand}
     * of the {@link Command}.
     * 
     * @param queryToExec
     * @param currentCommand
     * @param resState
     */
    public void updateResultOperandRefer(Query queryToExec,
            Command currentCommand, ResultState resState) throws AQLException;

    /**
     * Store {@link Command} result {@link Operand} in the metadata.
     * 
     * @param currentCommand
     * @throws AQLException
     */
    public void recordCommandResult(Query q, final Command currentCommand)
            throws AQLException;

    /**
     * Build result message using string builder and command attributes.
     * Gives the user the id of the result {@link Operand} and shows which
     * command generated that result.
     * 
     * @param resState
     */
    public void buildResultMessage(final Query q, final ResultState resState,
            final Command currentCommandIn) throws AQLException;

    /**
     * Get {@link CommandAlgorithm} with highest priority, where highest
     * priority is the largest {@link CommandAlgorithm#getPriority()} number.
     * 
     * @param command
     * @return
     * @throws AQLException
     */
    public CommandAlgorithm getHighestPriorityCA(final Command command)
            throws AQLException;
}
