package edu.baylor.aql.services.intermlanggen;

import io.netty.channel.Channel;

import java.io.File;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.log4j.Logger;

import edu.baylor.aql.dataaccess.IMetadata;
import edu.baylor.aql.dataaccess.Metadata;
import edu.baylor.aql.entities.Element;
import edu.baylor.aql.entities.client.ClientSession;
import edu.baylor.aql.entities.operands.Operand;
import edu.baylor.aql.entities.parsing.LoadMap;
import edu.baylor.aql.entities.query.IntermediateLanguageObject;
import edu.baylor.aql.entities.query.IntermediateLanguageObject.ParseObjType;
import edu.baylor.aql.entities.query.IntermediateLanguageObject.ParseRequestType;
import edu.baylor.aql.entities.query.Query;
import edu.baylor.aql.services.intermlanggen.load.BasicLoadParser;
import edu.baylor.aql.services.intermlanggen.load.CommandAlgorithmLoadParser;
import edu.baylor.aql.services.intermlanggen.load.CommandTypeLoadParser;
import edu.baylor.aql.services.intermlanggen.load.OperandLoadParser;
import edu.baylor.aql.services.intermlanggen.load.ToolLoadParser;
import edu.baylor.aql.services.intermlanggen.query.BasicQueryParser;
import edu.baylor.aql.services.intermlanggen.query.BasicQueryParser.QueryTokens;
import edu.baylor.aql.services.intermlanggen.query.CompoundQueryParser;
import edu.baylor.aql.services.intermlanggen.query.UsingQueryParser;
import edu.baylor.aql.util.Util;

/**
 * This service class reads the AQL query request from the client using Netty
 * framework, the request data structure is read one line at a time and each
 * line is examined. If the line represents start of known data structure, such
 * as load request, then all the lines until the load end symbol will be read
 * into {@link LoadMap}. Same is done for the query, except that the lines are
 * read into the {@link QueryTokens} object.
 * <p>
 * Once the request is read into an appropriate object, it can be parsed. There
 * are two routines in this class, one that invokes query parser and the other
 * one that invokes load parser.
 * <p>
 * There are more than one parser for each request type, and invoked parser type
 * depends on the read request type defined by {@link ParseObjType}.
 * <p>
 * Once request read and parsed the result object is passed with
 * {@link IntermediateLanguageObject} to the caller.
 * 
 * @author Rovshen Nazarov. All Rights Reserved.
 */
public final class RequestParser implements Callable<Object> {

    /**
     * Used to log errors or information.
     */
    private static Logger logger = Logger.getLogger(RequestParser.class
            .getName());

    /**
     * This pool is used for concurrent execution of client requests such as
     * queries and loading of the {@link Operand} s.
     */
    private final ExecutorService execThreadPool;

    /**
     * Client session specific state, such as client id, client channel,
     * etc.
     */
    private final ClientSession clientSession;

    /**
     * Number of threads for fixed pull executor.
     */
    private static final int POOL_SIZE = 10;
    /**
     * This service is used to operate AQL metadata. This service helps store
     * and retrieve metadata from the database.
     */
    private final IMetadata dao;

    /**
     * Constructor for the client session manager. This manager/controller
     * listens to the client transactions such as query and {@link Operand}s
     * loading and invokes other services and controllers to handle those
     * requests.
     * 
     * @param clientSession
     *            {@link ClientSession}
     */
    public RequestParser(final ClientSession clientSession) {
        if (clientSession == null) {
            logger.error("Query Optimizer arguments can not be NULL");
        }
        this.clientSession = clientSession;
        execThreadPool = Executors.newFixedThreadPool(POOL_SIZE);
        dao = new Metadata();
    }

    /**
     * This method is an entry point for the client session. This method
     * listens to the clients request until the client terminates the
     * session, which signaled with the null returned from the FIFO pipe.
     * 
     * During the session client can make load or query requests, both of
     * which are handled differently.
     * 
     * To write result statuses of requests back to the client {@link Channel}
     * stored in {@link ClientSession} is used. All used
     * threads by the client session manager are terminated using
     * {@link Util#shutdownAndAwaitTermination(ExecutorService)} method.
     * 
     * @throws Exception
     */
    @Override
    public Object call() throws Exception {
        IntermediateLanguageObject ilo = new IntermediateLanguageObject();

        final boolean result = performHandshake(clientSession);
        if (!result) {
            ilo.getParseMsg()
                    .append("Failed to perform handshake with the client with session id ")
                    .append(clientSession.getSessionId());
            logger.error(ilo.getParseMsg().toString());
            return ilo; // exit thread.
        }
        try {
            logger.info("Request Parser is reading from client with session id "
                    + clientSession.getSessionId());

            // we use blocking queue
            final String msgFromClient = clientSession.getMsgsFromClient()
                    .take();
            // BEGIN | END | BEGIN_LOAD | END_LOAD
            if (msgFromClient.equals("BEGIN_QUERY")) {
                ilo = readAQLquery(msgFromClient);
                ilo.setClientSession(clientSession);
                ilo = parseAQLquery(ilo);
                ilo.setParseType(ParseRequestType.QUERY);
            } else if (msgFromClient.equals("BEGIN_LOAD")) {
                ilo = readLoad(msgFromClient);
                ilo.setClientSession(clientSession);
                ilo = parseLoading(ilo);
                if (ilo != null) {
                    ilo.setParseType(ParseRequestType.LOAD);
                } else {
                    ilo = new IntermediateLanguageObject();
                    ilo.getParseMsg().append("not supported load operation")
                            .append(Util.LINE_SEPARATOR);
                    ilo.setParseStateCode(Util.FAILED_STATUS);
                }
            }
        } catch (final InterruptedException | ExecutionException e) {
            logger.error("Problem reading form the client " + e.getMessage());
            e.printStackTrace();
        } finally {
            final Util util = new Util();
            util.shutdownAndAwaitTermination(execThreadPool);
        }
        return ilo;
    }

    /**
     * Read an {@link Operand} Object to be loaded.
     * 
     * @param lineIn
     * @return operand
     * @throws InterruptedException
     */
    private IntermediateLanguageObject readLoad(final String lineIn)
            throws InterruptedException {
        String line = lineIn;
        // Read data until the end load
        logger.info(Util.LINE_SEPARATOR + Util.LINE_SEPARATOR + line);
        final IntermediateLanguageObject ilo = new IntermediateLanguageObject();
        final LoadMap loadMap = new LoadMap();

        while (!line.equals("END_LOAD")) {
            if (!clientSession.getMsgsFromClient().isEmpty()) {
                line = clientSession.getMsgsFromClient().take();
                logger.info(line);
                if (line.contains("ATTR_TYPE=--")) { // skip non att value data
                    final String[] elements = line
                            .split(UtilParser.DEFAULT_ATT_VAL_STRING_SEP);
                    final String[] mapEntry = UtilParser
                            .getAttValueEntry(elements);
                    if (mapEntry == null) {
                        ilo.setParseStateCode(Util.FAILED_STATUS);
                        ilo.getParseMsg()
                                .append("att values were not correct.");
                        return ilo;
                    }
                    loadMap.addAttVal(mapEntry);
                }
            }
        }
        final Element loadObj = loadMap;
        ilo.setReqestObj(loadObj);
        return ilo;
    }

    /**
     * Read a full AQL {@link Query} from the client.
     * The logic is simple read each line from the client and store it into an
     * array, if there is using command in the query mark that.
     * 
     * @param msgFromClient
     * @return
     * @throws InterruptedException
     */
    private IntermediateLanguageObject readAQLquery(final String msgFromClient)
            throws InterruptedException {
        final IntermediateLanguageObject ilo = new IntermediateLanguageObject();
        ilo.setType(ParseObjType.QUERY);
        final QueryTokens qt = new QueryTokens();
        qt.getRequest().append(msgFromClient).append(Util.LINE_SEPARATOR);
        String line = msgFromClient;
        logger.info(Util.LINE_SEPARATOR + Util.LINE_SEPARATOR + line);
        // Read data until the end query
        while (!line.equals("END_QUERY")) {
            if (!clientSession.getMsgsFromClient().isEmpty()) {
                line = clientSession.getMsgsFromClient().take();
                logger.info(line);
                qt.getQueryTokens().add(line);
                qt.getRequest().append(line).append(Util.LINE_SEPARATOR);
                if (line.toUpperCase().contains("BEGIN_USING")
                        && ParseObjType.COMPOUND_QUERY != ilo.getObjType()) {
                    ilo.setType(ParseObjType.USING_QUERY);
                }
                if (line.toUpperCase().contains("QUERY_ALIAS_NAME")) {
                    ilo.setType(ParseObjType.COMPOUND_QUERY);
                }
            }// end of query processing loop
        }
        ilo.setReqestObj(qt);
        return ilo;
    }

    /**
     * This method is used to perform AQL protocol specific handshake
     * between connecting client and the AQL server.
     * 
     * @param client
     * @return
     */
    private boolean performHandshake(final ClientSession client) {
        // TODO Perform handshake with the client following defined
        // protocol
        return true;
    }

    /**
     * Parse the query tokens based on the query type.
     * 
     * @param ilo
     * @return
     * @throws Exception
     */
    private IntermediateLanguageObject parseAQLquery(
            final IntermediateLanguageObject ilo) throws Exception {
        BasicQueryParser bqp = null;
        IntermediateLanguageObject iloNew = null;
        switch (ilo.getObjType()) {
        case QUERY:
            bqp = new BasicQueryParser(ilo, dao);
            break;
        case USING_QUERY:
            bqp = new UsingQueryParser(ilo, dao);
            break;
        case COMPOUND_QUERY:
            bqp = new CompoundQueryParser(ilo, dao);
            break;
        default:
            // none
            break;

        }
        if (bqp != null) {
            iloNew = bqp.parse();
            if (iloNew == null) {
                errStateObj(iloNew);
            }
        } else {
            errStateObj(iloNew);
        }
        if (iloNew != null) {
            iloNew.setType(ilo.getObjType());
        }
        return iloNew;
    }

    private void errStateObj(IntermediateLanguageObject iloNew) {
        iloNew = new IntermediateLanguageObject();
        iloNew.setParseStateCode(Util.FAILED_STATUS);
        iloNew.setParseType(ParseRequestType.QUERY);
    }

    /**
     * This method processes the loading request. Client can load any class
     * that extends {@link Operand} abstract class.
     * 
     * The client load request is processed until the end of load request is
     * reached. The loaded {@link Operand} is stored in the metadata and the
     * loaded {@link Operand}'s human readable id returned to the client.
     * 
     * Load the graph and compute necessary {@link Operand} properties.
     * 
     * @param ilo
     *            - {@link Operand} with the user provided attributes.
     * @throws ExecutionException
     * @throws InterruptedException
     */
    private IntermediateLanguageObject parseLoading(
            final IntermediateLanguageObject ilo) throws InterruptedException,
            ExecutionException {

        final LoadMap map = (LoadMap) ilo.getReqestObj();
        // to store result of the load parsing.
        IntermediateLanguageObject iloParsed = null;
        String typeVal = "";
        final String type = "type";
        if (map.getAttValTable().containsKey(type)) {
            typeVal = map.getAttValTable().get(type);
        }
        final ParseObjType parseT = ParseObjType.getEnumByString(typeVal);

        if (parseT != null) {
            ilo.setType(parseT);
            final BasicLoadParser loader = getLoadParser(parseT, ilo);
            if (loader != null) {
                final Future<Object> resultFuture = execThreadPool
                        .submit(loader);
                // blocks until gets result
                iloParsed = (IntermediateLanguageObject) resultFuture.get();
                if (iloParsed.getParseStateCode() != Util.FAILED_STATUS) {
                    setQueryIdForOperand(loader, iloParsed);
                } else {
                    iloParsed.getParseMsg().append("for the ")
                            .append(iloParsed.getObjType()).append(" loading.");
                }
            }
        }
        return iloParsed;
    }

    private void setQueryIdForOperand(final BasicLoadParser loader,
            final IntermediateLanguageObject iloParsed) {
        if (loader instanceof OperandLoadParser) {
            final String queryId = clientSession.getSessionId().toString();
            ((Operand) iloParsed.getReqestObj()).setQueryId(queryId);
        }
    }

    /**
     * We have different load parsers for different objects we can load.
     * Based on the type decide which load parser to invoke.
     * 
     * @param parseT
     * @param ilo
     * @return
     */
    private BasicLoadParser getLoadParser(final ParseObjType parseT,
            final IntermediateLanguageObject ilo) {
        BasicLoadParser loader = null;
        switch (parseT) {
        case LOAD_COMMANDALGORITHM:
            loader = new CommandAlgorithmLoadParser(ilo);
            break;
        case LOAD_COMMANDTYPE:
            loader = new CommandTypeLoadParser(ilo);
            break;
        case LOAD_OPERAND:
            loader = new OperandLoadParser(ilo);
            break;
        case LOAD_TOOL:
            loader = new ToolLoadParser(ilo);
            break;
        default: // none
            break;
        }
        return loader;
    }

    /**
     * Wait for file. This method waits for a given file specified amount of
     * time. The wait is done using while loop with file existence check. If
     * the file does not exist, the Thread.wait is invoked to wait
     * predefined period of time.
     * 
     * @param waitFile
     *            String name of the file to wait for.
     * @param sleepMillisec
     *            The time to sleep in milliseconds.
     * @param sleepMaxMillisec
     *            Maximum time the invoker is ok with waiting for.
     */
    @SuppressWarnings("unused")
    private void waitUntilFileExists(final String waitFile,
            final int sleepMillisec, final int sleepMaxMillisec) {
        if (waitFile == null || sleepMillisec == 0 || sleepMaxMillisec == 0) {
            logger.error("The file to wait for or its path must be not NULL");
            return;
        }
        final File fileToWaitFor = new File(waitFile);
        int sleftSofFar = 0;
        logger.info("File to wait for " + waitFile);
        while (!fileToWaitFor.exists() && (sleftSofFar < sleepMaxMillisec)) {
            try {
                Thread.sleep(sleepMillisec);
                sleftSofFar += sleepMillisec;
                logger.info("Still waiting for file " + waitFile);
            } catch (final InterruptedException e) {
                return;
            }
        }
    }

}
