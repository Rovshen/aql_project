/**
 * This is the core package of the Query Execution Server.
 * This package has many sub-packages that perform different services required for 
 * processing incoming request and executing it.
 * <p>
 * The services package is a collection of the following modules:
 * <ul> 
 * <li> Parsing defined in intermlanggen package, which stands for intermediate language generator.
 * <li> Request handling and processing is defined in the requesthandler package.
 * <li> Loading related services defined by loading package. It contains services used during AQL 
 * related loading operations. Note this services are invoked after parsing of the request.
 * <li> Collection of optimization modules is defined in the optimization library. It contains estimating, 
 * evaluating, ordering, repeated query, and sampling services used to execute {@link edu.baylor.aql.entities.query.Query} faster.
 * <li> Periodical package contains AQL system support services for removal of marked for deletion data from the storage system.
 * </ul>  
 */
package edu.baylor.aql.services;

