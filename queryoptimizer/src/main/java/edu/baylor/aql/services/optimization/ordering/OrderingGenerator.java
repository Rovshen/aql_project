package edu.baylor.aql.services.optimization.ordering;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.logging.Logger;

import edu.baylor.aql.entities.command.Command;
import edu.baylor.aql.entities.command.CommandType.OrderingType;
import edu.baylor.aql.entities.command.OrderingCommandGroup;
import edu.baylor.aql.entities.operands.Operand;
import edu.baylor.aql.entities.query.Query;
import edu.baylor.aql.services.query.IQueryService;
import edu.baylor.aql.util.AQLException;

public class OrderingGenerator implements Callable<Object> {

    /**
     * Used to log errors or information.
     */
    @SuppressWarnings("unused")
    private static Logger logger = Logger.getLogger(OrderingTyped.class
            .getName());
    private final IOrderingService oService;
    private final IQueryService qService;
    private final List<OrderingCommandGroup> orderingCommandGroupMap;
    private final Query query;

    public OrderingGenerator(final IOrderingService oService,
            final IQueryService qService,
            final List<OrderingCommandGroup> orderingCommandGroupMap,
            final Query query) {
        this.oService = oService;
        this.qService = qService;
        this.orderingCommandGroupMap = orderingCommandGroupMap;
        this.query = query;
    }

    @Override
    public List<Query> call() throws Exception {

        return generateOrderings(oService, qService, orderingCommandGroupMap,
                query);
    }

    /**
     * Populate each query with the ordered operands
     * <p>
     * For each {@link OrderingCommandGroup} generate possible {@link Operand}
     * list orderings using {@link IOrderingService} with appropriate ordering
     * method based on the {@link OrderingType} of the
     * {@link OrderingCommandGroup}.
     * <p>
     * Combine generated {@link Operand} list orderings of each
     * {@link OrderingCommandGroup} into {@link Query}s generating many
     * Sequential linkages between {@link OrderingCommandGroup}. Thus if the
     * {@link Query} is broken into 4 {@link OrderingCommandGroup} if each
     * {@link OrderingCommandGroup} has 2 possible {@link Operand} list
     * orderings, we will be able to combine those 4 groups into a Query.
     * <p>
     * Return list of {@link Query}s for different orderings.
     * 
     * @param oService
     * @param qService
     * @param orderingCommandGroups
     * @param qNext
     * @return queries
     * @throws AQLException
     */
    private List<Query> generateOrderings(final IOrderingService oService,
            final IQueryService qService,
            final List<OrderingCommandGroup> orderingCommandGroups,
            final Query q) throws AQLException {

        final List<Query> queries = new ArrayList<>();

        // generate orderings for OrderingCommandGroup
        for (final OrderingCommandGroup oCommGroup : orderingCommandGroups) {
            // below modifies content of the oCommGroup
            processCommandsSameOrdering(oService, oCommGroup);
        }
        // combine together OrderingCommandGroup
        final Set<List<Long>> operandsOrdered = oService
                .combineGroupsByOperands(orderingCommandGroups);

        final Queue<Command> commands = q.getCommands();

        // generate all possible ordering Queries for a given group
        for (final List<Long> operandsOrder : operandsOrdered) {
            logger.info(Arrays.toString(operandsOrder.toArray()));
            // create clone from the base query
            final Query cloneQ = query.clone();
            final Queue<Command> commandsClone = qService
                    .cloneCommandList(commands);
            // remove cloned commands and reset them
            cloneQ.setCommands(commandsClone);
            cloneQ.getOperands().clear(); // remove cloned operands
            cloneQ.setOperands(operandsOrder);

            // re-populate qeuery's commands with a operands ordered
            // based on ordering
            qService.populateCommandsWithOperandsSequencial(cloneQ);
            // logger.info("\n\nafter " + cloneQ.toString());
            queries.add(cloneQ);
        }
        return queries;
    }

    /**
     * In the {@link OrderingCommandGroup} all commands have the same
     * {@link OrderingType} process them based on that ordering.
     * <p>
     * Use query service {@link IOrderingService} to generate a list of
     * {@link Operand} orderings and add them to {@link OrderingCommandGroup}.
     * 
     * POST: {@link OrderingCommandGroup} updated with {@link Operand}s and
     * different orderings of these {@link Operand}s.
     * 
     * {@link OrderingCommandGroup} are linked based on the
     * {@link OrderingCommandGroup#getGroupResult()} of the previous
     * {@link OrderingCommandGroup}.
     * 
     * @param oService
     * @param orderingCommandGroup
     */
    private void processCommandsSameOrdering(final IOrderingService oService,
            final OrderingCommandGroup orderingCommandGroup) {

        switch (orderingCommandGroup.getCommType().getOrderingType()) {
        case ASSOCIATIVE:
            oService.generOrderingsAssociative(query, orderingCommandGroup);
            break;
        case ASSOCIATIVE_COMMUTATIVE:
            oService.generOrderingsAssociative(query, orderingCommandGroup);
            oService.generOrderingsCommutative(query, orderingCommandGroup);
            oService.generOrderingsAssocAndCommutat(query, orderingCommandGroup);
            break;
        case COMMUTATIVE:
            oService.generOrderingsCommutative(query, orderingCommandGroup);
            break;
        case NONE: // default sequential ordering is used
            orderingCommandGroup.getPossibleOrderings().add(
                    orderingCommandGroup.getOperands());
            break;
        default: // no action
            break;
        }
    }

}
