package edu.baylor.aql.services.requesthandler.pipeline;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import edu.baylor.aql.dataaccess.Metadata;
import edu.baylor.aql.entities.command.Command;
import edu.baylor.aql.entities.command.CommandAlgorithm;
import edu.baylor.aql.entities.operands.Operand;
import edu.baylor.aql.entities.query.Query;
import edu.baylor.aql.entities.tools.Tool;
import edu.baylor.aql.services.optimization.evaluating.EvaluatorHandler;

/**
 * This is a recommended way to create singleton by one of the major Java
 * contributors Joshua Bloch. The approach was discussed during the talk at
 * Google I/O 2008.
 * 
 * @author Rovshen Nazarov. Baylor University. Nov 12, 2014.All Rights Reserved.
 * 
 */
public enum EPipelineSingleton {
    /**
     * The LOAD and QUERY pipelines use {@link EvaluatorHandler}s to select
     * {@link CommandAlgorithm} that will run in least amount of time with
     * bounded resources.
     * <p>
     * The plan generation step in the {@link Query} pipeline includes
     * generating different plans for different {@link Tool} with different
     * configuration and optimization parameters set that are specific for each
     * {@link CommandAlgorithm}.
     * <p>
     * 1) This class uses {@link MySQLService} service to load
     * {@link CommandAlgorithm} to generate all possible execution combinations
     * of a given {@link Query} using {@link Command}s, {@link Operand}s.
     * <p>
     * 2) The checks if the results for given commands and operands already
     * exist in the metadata and if so the result of the command immediately
     * substituted in place of command in all execution plans.
     * 
     * @author Rovshen Nazarov. Baylor University. Aug 25, 2014.All Rights
     *         Reserved.
     */
    LOAD(new ConcurrentLinkedQueue<VisitorH>()), QUERY(
            new ConcurrentLinkedQueue<VisitorH>());

    private final Queue<VisitorH> pipeline;

    EPipelineSingleton(final Queue<VisitorH> pipeline) {
        this.pipeline = pipeline;
    }

    public Queue<VisitorH> getPipeLineInstance() {
        return pipeline;
    }

    public void addToPipeLineH(final VisitorH v) {
        pipeline.add(v);
    }
}
