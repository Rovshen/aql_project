package edu.baylor.aql.services.optimization.sampling;

/**
 * The tool used to sample other tools
 * to generate performance metadata for those tools,
 * such as average running time for different operand sizes.
 * 
 * @author Rovshen Nazarov. Baylor University. Jan 14, 2015.All Rights Reserved.
 * 
 */
public interface Sampler {

}
