/**
 * This package contains specific parsing services used for each specific implementation type of the abstract {@link edu.baylor.aql.entities.operands.Operand} entity.
 * Each implementation type of the {@link edu.baylor.aql.entities.operands.Operand} will require different parsing, yet general structure of each of the services defined
 * in this package are similar. Thus when this package is extended the same similar structure should be used.
 * <p>
 * This package is invoked by the higher level {@link edu.baylor.aql.services.intermlanggen.load.OperandLoadParser} and he choice of specific parser invokation is done 
 * based on {@link edu.baylor.aql.entities.operands.Operand.OperandType}.
 */
package edu.baylor.aql.services.intermlanggen.load.operand;