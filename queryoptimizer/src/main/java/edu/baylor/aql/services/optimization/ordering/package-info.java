/**
 * This package contains services for generating different order of {@link edu.baylor.aql.entities.operands.Operand}s position, and grouping.
 * The ordering services are {@link edu.baylor.aql.entities.command.CommandType.OrderingType} specific and for different {@link edu.baylor.aql.entities.command.CommandType.OrderingType}
 * different orderings are generated. 
 * <p>
 * The orderings represent linked list of {@link edu.baylor.aql.entities.query.Query} objects with {@link edu.baylor.aql.entities.command.Command}s 
 * grouped or not. The {@link edu.baylor.aql.entities.operands.Operand}s of those {@link edu.baylor.aql.entities.command.Command}s may have different position
 * depending on the rules defined for specific {@link edu.baylor.aql.entities.command.CommandType.OrderingType} law.
 * <p>
 * There are two aspects involved in the ordering {@link edu.baylor.aql.entities.command.CommandType.OrderingType} rules
 * and {@link edu.baylor.aql.entities.operands.Operand.OperandType} rules.
 * Some {@link edu.baylor.aql.entities.operands.Operand}s follow the general ordering rules like associativity and commutativity, but some may not.
 * Thus when extending or modifying this library, special care should be taken to obey all the rules involved in generation of possible 
 *  {@link edu.baylor.aql.entities.operands.Operand} ordering and grouping within a {@link edu.baylor.aql.entities.query.Query} and/or {@link edu.baylor.aql.entities.command.Command}.
 *  <p>
 *  Note also that some {@link edu.baylor.aql.entities.command.CommandAlgorithm}s allow {@link edu.baylor.aql.entities.operands.Operand} grouping under one {@link edu.baylor.aql.entities.command.Command}
 *  instead of several identical {@link edu.baylor.aql.entities.command.Command}s. Such {@link edu.baylor.aql.entities.command.CommandAlgorithm}s are known to have
 *  variable number of {@link edu.baylor.aql.entities.operands.Operand}s.      
 */
package edu.baylor.aql.services.optimization.ordering;

