package edu.baylor.aql.services.intermlanggen.load.operand;

import java.io.File;

import org.apache.log4j.Logger;

import edu.baylor.aql.entities.operands.Graph;
import edu.baylor.aql.entities.operands.Operand;
import edu.baylor.aql.entities.operands.Operand.OperandState;
import edu.baylor.aql.entities.parsing.LoadMap;
import edu.baylor.aql.entities.query.IntermediateLanguageObject;
import edu.baylor.aql.util.Util;

public final class GraphParser implements OperandParser {
    private static Logger logger = Logger
            .getLogger(GraphParser.class.getName());

    @Override
    public IntermediateLanguageObject parse(final IntermediateLanguageObject ilo) {
        final IntermediateLanguageObject iloRes = new IntermediateLanguageObject();
        iloRes.setParseStateCode(Util.SUCCESS_STATUS);
        final LoadMap loadMap = (LoadMap) ilo.getReqestObj();
        final Operand o = new Graph();
        o.setId(Util.FAILED_STATUS);
        Double density = 0.0;
        Long estEdgesCount = 0l;
        Long estNodesCount = 0l;
        final String initialPath = loadMap.getAttValTable().get("path");
        try {
            density = Double.parseDouble(loadMap.getAttValTable()
                    .get("density"));
            estEdgesCount = Long.parseLong(loadMap.getAttValTable().get(
                    "edgecount"));
            estNodesCount = Long.parseLong(loadMap.getAttValTable().get(
                    "nodecount"));
        } catch (final Exception e) {
            logger.error(e.getMessage());
            iloRes.setReqestObj(o);
            iloRes.getParseMsg().append("Problem parsing Graph.")
                    .append(Util.LINE_SEPARATOR);
            iloRes.setParseStateCode(Util.FAILED_STATUS);
        }
        if (initialPath != null) {
            o.setInitialPath(initialPath);
            final File file = new File(initialPath);
            o.setFile(file);
        } else {
            iloRes.getParseMsg().append("The path is needed.")
                    .append(Util.LINE_SEPARATOR);
            iloRes.setParseStateCode(Util.FAILED_STATUS);
        }
        o.setType(OperandState.LOADED.getLabel());

        // below data is optional
        if (density != 0.0) {
            ((Graph) o).setDensity(density);
        }
        if (estEdgesCount != 0l) {
            ((Graph) o).setEstEdgesCount(estEdgesCount);
        }
        if (estNodesCount != 0l) {
            ((Graph) o).setEstNodesCount(estNodesCount);
        }
        iloRes.setReqestObj(o);

        return iloRes;
    }
}
