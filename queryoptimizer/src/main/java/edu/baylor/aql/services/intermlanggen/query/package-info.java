/**
 * This package defines services used to parse AQL query and produce {@link edu.baylor.aql.entities.query.Query} object that will be stored in {@link edu.baylor.aql.entities.query.IntermediateLanguageObject}.
 * At the moment there are two query types, but if necessary this package can be extended to support more types. The base query type is designed to be {@link edu.baylor.aql.services.intermlanggen.query.BasicQueryParser}
 * and all new types of query parsing are expected to extend that class.
 */
package edu.baylor.aql.services.intermlanggen.query;