package edu.baylor.aql.services.loading;

import java.util.concurrent.CountDownLatch;

import org.apache.log4j.Logger;

import edu.baylor.aql.entities.Element;
import edu.baylor.aql.entities.tools.Tool;
import edu.baylor.aql.services.requesthandler.pipeline.Visitor;
import edu.baylor.aql.util.Util;

/**
 * This service loads into the AQL system provided {@link Tool}.
 * 
 * @author Rovshen Nazarov. Baylor University. Jan 14, 2015.All Rights Reserved.
 * 
 */
public final class ToolLoader extends Visitor {

    private static Logger logger = Logger.getLogger(ToolLoader.class.getName());
    private Element tool;
    private CountDownLatch doneSignal;

    @Override
    public Element call() throws Exception {
        // System.out.println("ToolLoader");
        if (!(tool instanceof Tool)) {
            doneSignal.countDown();
            return null;
        }
        Tool t;
        try {
            t = (Tool) tool;
            super.getDao().openConnection();
            storeTool(t);
            super.getDao().closeConnection();
            String msg = "";
            if (Util.FAILED_STATUS == t.getId()) {
                msg = "Could not load the tool: " + t.getName()
                        + Util.LINE_SEPARATOR;
                logger.error(msg);
                t.setErrState(true);
            } else {
                msg = "Generated Tool's id is: " + t.getId();
                logger.info(msg);
            }
            t.setStateMsg(msg);
        } finally {
            doneSignal.countDown();
        }
        return t;
    }

    @Override
    public void visit(final Element tool, final CountDownLatch doneSignal)
            throws InterruptedException {
        this.doneSignal = doneSignal;
        this.tool = tool;
    }

    /**
     * Load {@link Element} to the database.
     * This method stores in memory the object represented by the
     * {@link Element} to the metadata. It also saves in-memory auto generated
     * id for later references.
     * 
     * If the object could not be inserted, than the object id is set to
     * FAILED_STATUS.
     * 
     */
    private void storeTool(final Tool t) {
        t.setId((long) Util.FAILED_STATUS);
        t.setId((t.insert(super.getDao())));
    }

}
