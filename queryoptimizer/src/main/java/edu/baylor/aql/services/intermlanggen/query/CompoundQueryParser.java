package edu.baylor.aql.services.intermlanggen.query;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Stack;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.log4j.Logger;

import edu.baylor.aql.dataaccess.IMetadata;
import edu.baylor.aql.entities.command.Command;
import edu.baylor.aql.entities.command.CommandType;
import edu.baylor.aql.entities.operands.Operand;
import edu.baylor.aql.entities.query.IntermediateLanguageObject;
import edu.baylor.aql.entities.query.IntermediateLanguageObject.ParseObjType;
import edu.baylor.aql.entities.query.Query;
import edu.baylor.aql.services.query.IQueryService;
import edu.baylor.aql.services.query.QueryService;
import edu.baylor.aql.util.AQLException;

/**
 * This parser processes the {@link Query} that consists of several
 * {@link Query}ies that are separated by comma and linked via aliases.
 * 
 * @author Rovshen Nazarov. Baylor University. Mar 23, 2015.All Rights Reserved.
 * 
 */
public final class CompoundQueryParser extends BasicQueryParser {

    /**
     * Used to log errors or information.
     */
    private static Logger logger = Logger.getLogger(CompoundQueryParser.class
            .getName());
    private final UsingQueryParser uQParser;

    public CompoundQueryParser(final IntermediateLanguageObject ilo,
            final IMetadata dao) {
        super(ilo, dao);
        uQParser = new UsingQueryParser(ilo, dao);
    }

    @Override
    public IntermediateLanguageObject parse() throws Exception {

        final IQueryService qService = new QueryService();
        final IntermediateLanguageObject iloRes = new IntermediateLanguageObject();
        iloRes.setClientSession(super.iloInst.getClientSession());
        final QueryTokens qt = (QueryTokens) super.iloInst.getReqestObj();

        super.dao.openConnection();

        final List<QueryTokens> qTokensList = splitQueryTokens(qt);
        final List<Query> queries = new ArrayList<>();
        final Query head = new Query();
        final Queue<Command> commandsH = new LinkedList<>();
        final ConcurrentHashMap<Long, Operand> operMapH = new ConcurrentHashMap<>();
        final Stack<Integer> subQuerySeqNumbersH = new Stack<>();
        final List<Long> operandsH = new ArrayList<>();
        head.setOperands(operandsH);
        head.setSubQuerySeqNumbers(subQuerySeqNumbersH);
        head.setOperMap(operMapH);
        head.setCommands(commandsH);
        head.setQueryRequest(qt.getRequest().toString());

        // create a Query from each QueryTokens
        for (final QueryTokens qtCurr : qTokensList) {

            final Query query = new Query();
            final Queue<Command> commands = new LinkedList<>();
            final ConcurrentHashMap<Long, Operand> operMap = new ConcurrentHashMap<>();
            final Stack<Integer> subQuerySeqNumbers = new Stack<>();
            final List<Long> operands = new ArrayList<>();
            query.setOperands(operands);
            query.setSubQuerySeqNumbers(subQuerySeqNumbers);
            query.setOperMap(operMap);
            query.setCommands(commands);
            query.setQueryRequest(qt.getRequest().toString());
            queries.add(query);
            // process this query
            iloRes.setReqestObj(query);
            if (parseSingleQuery(iloRes, qService, qtCurr)) {
                return iloRes; // return on error
            }
        }
        // logger.info(Arrays.toString(queries.toArray()));
        qService.linkQueries(queries, head);
        iloRes.setReqestObj(head);

        super.dao.closeConnection();
        return iloRes;
    }

    /**
     * Split {@link Query}ies by {@link ParseObjType#COMPOUND_QUERY} Split
     * {@link Query} based on ALIAS_NAME
     * 
     * @param qt
     * 
     * @return
     */
    private List<QueryTokens> splitQueryTokens(final QueryTokens qt) {

        final List<QueryTokens> qTokensList = new ArrayList<>();
        final List<String> tokensQ = new ArrayList<>();

        for (final String token : qt.getQueryTokens()) {

            tokensQ.add(token);
            if (token.toUpperCase().contains("QUERY_ALIAS_NAME")) {
                // end of current Query save this query's tokens and start
                // another query's tokens
                final QueryTokens qtCurr = new QueryTokens();
                qtCurr.getQueryTokens().addAll(tokensQ);
                qtCurr.setErrState(qt.isErrState()).setStateMsg(
                        qt.getStateMsg());
                qtCurr.setRequest(qt.getRequest());
                qTokensList.add(qtCurr);
                tokensQ.clear(); // reuse array list
            }
        }
        // add last query if any
        if (!tokensQ.isEmpty()) {
            final QueryTokens qtCurr = new QueryTokens();
            qtCurr.getQueryTokens().addAll(tokensQ);
            qtCurr.setErrState(qt.isErrState()).setStateMsg(qt.getStateMsg());
            qtCurr.setRequest(qt.getRequest());
            qTokensList.add(qtCurr);
        }
        return qTokensList;
    }

    /**
     * Parse a single {@link Query} within a {@link ParseObjType#COMPOUND_QUERY}
     * 
     * @param iloRes
     * @param qService
     * @param qt
     * @return return false if all OK, true if any problem.
     * @throws AQLException
     * @throws InterruptedException
     */
    private boolean parseSingleQuery(final IntermediateLanguageObject iloRes,
            final IQueryService qService, final QueryTokens qt)
            throws AQLException, InterruptedException {

        if (processQueryTokens(iloRes, qt)) {
            return true; // return on error
        }
        super.checkOperands((Query) iloRes.getReqestObj(), iloRes, qService);
        return false;
    }

    /**
     * 
     * 
     * @param iloRes
     * @param qt
     * @return return false if all OK, true if any problem.
     * @throws AQLException
     * @throws InterruptedException
     */
    private boolean processQueryTokens(final IntermediateLanguageObject iloRes,
            final QueryTokens qt) throws AQLException, InterruptedException {

        final List<CommandType> commandTypes = dao.lookUpCommandTypes();
        if (commandTypes.isEmpty()) {
            // wrong command encountered
            logger.error("No Commands Types loaded.");
            return true; // return on error
        }
        final AtomicInteger numbOfExpCommResults = new AtomicInteger(0);
        final int queryTokensSize = qt.getQueryTokens().size();
        // Read data until the end query
        for (int index = 0; index < queryTokensSize; index++) {
            final String line = qt.getQueryTokens().get(index);
            // process as using command
            if (line.toUpperCase().contains("BEGIN_USING")) {
                iloRes.setType(ParseObjType.USING_QUERY);
                index = uQParser.processUsingCommand(qt.getQueryTokens(),
                        index, commandTypes, iloRes);
            } else {
                // if not using command process as basic command
                if (super.processBasicCommand(line, iloRes, commandTypes,
                        numbOfExpCommResults)) {
                    return true; // return on error
                }
            }
        }
        return false; // all ok
    }
}
