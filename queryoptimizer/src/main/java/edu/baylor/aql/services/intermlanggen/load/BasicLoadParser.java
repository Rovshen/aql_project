package edu.baylor.aql.services.intermlanggen.load;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import edu.baylor.aql.dataaccess.IMetadata;
import edu.baylor.aql.dataaccess.Metadata;
import edu.baylor.aql.util.hdfds.HDFSService;

/**
 * This abstract class defines common attributes and mehtods for Load Parsing.
 * 
 * @author Rovshen Nazarov. Baylor University. Jan 14, 2015.All Rights Reserved.
 * 
 */
public abstract class BasicLoadParser implements Callable<Object> {

    private final ExecutorService execThreadPool;
    private final IMetadata dao;
    private final HDFSService hdfsService;

    public BasicLoadParser() {
        final int poolSize = 10;
        execThreadPool = Executors.newFixedThreadPool(poolSize);
        dao = new Metadata();
        hdfsService = new HDFSService(execThreadPool);
    }

    /**
     * @return the execThreadPool
     */
    public ExecutorService getExecThreadPool() {
        return execThreadPool;
    }

    /**
     * @return the dao
     */
    public IMetadata getDao() {
        return dao;
    }

    /**
     * @return the hdfsService
     */
    public HDFSService getHdfsService() {
        return hdfsService;
    }

    @Override
    abstract public Object call();

}
