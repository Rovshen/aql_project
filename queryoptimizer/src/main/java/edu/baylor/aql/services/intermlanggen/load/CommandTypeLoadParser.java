package edu.baylor.aql.services.intermlanggen.load;

import org.apache.log4j.Logger;

import edu.baylor.aql.entities.command.CommandType;
import edu.baylor.aql.entities.command.CommandType.OrderingType;
import edu.baylor.aql.entities.parsing.LoadMap;
import edu.baylor.aql.entities.query.IntermediateLanguageObject;
import edu.baylor.aql.util.Util;

/**
 * This service parses {@link LoadMap} that contains elements of the
 * {@link CommandType}.
 * 
 * @author Rovshen Nazarov. Baylor University. Jan 14, 2015.All Rights Reserved.
 * 
 */
public final class CommandTypeLoadParser extends BasicLoadParser {

    private static Logger logger = Logger.getLogger(CommandTypeLoadParser.class
            .getName());
    private final IntermediateLanguageObject ilo;

    public CommandTypeLoadParser(final IntermediateLanguageObject ilo) {
        super();
        this.ilo = ilo;
    }

    @Override
    public IntermediateLanguageObject call() {
        final LoadMap loadMap = (LoadMap) ilo.getReqestObj();
        final IntermediateLanguageObject iloRes = new IntermediateLanguageObject();
        iloRes.setParseStateCode(Util.SUCCESS_STATUS);
        final CommandType ct = new CommandType();
        for (final String key : loadMap.getAttValTable().keySet()) {
            logger.info("key: " + key + "; value: "
                    + loadMap.getAttValTable().get(key));
        }
        final String name = loadMap.getAttValTable().get("name");
        final String orderingType = loadMap.getAttValTable().get("ordering");
        final String isUsing = loadMap.getAttValTable().get("isUsing");

        int numberOfOperands = 0;
        try {
            numberOfOperands = Integer.parseInt(loadMap.getAttValTable().get(
                    "minoperandscount"));
        } catch (final Exception e) {
            // ignore
        }
        if (name != null) {
            ct.setTypeName(name);
        } else {
            ct.setTypeName("");
            iloRes.getParseMsg().append("The name is needed.").append("\n");
            iloRes.setParseStateCode(Util.FAILED_STATUS);
        }
        if (orderingType != null) {
            final OrderingType oType = OrderingType
                    .getEnumByString(orderingType);
            if (oType == null) {
                iloRes.getParseMsg().append("The ordering type ")
                        .append(orderingType).append(" is not supported")
                        .append("\n");
                iloRes.setParseStateCode(Util.FAILED_STATUS);
            } else {
                ct.setOrderingType(oType);
            }
        } else { // allow no ordering
            ct.setOrderingType(OrderingType.NONE);
        }
        if (numberOfOperands != 0) {
            ct.setNumberOfOperands(numberOfOperands);
        } else {
            iloRes.getParseMsg()
                    .append("The minimum number of operands is needed.")
                    .append("\n");
            iloRes.setParseStateCode(Util.FAILED_STATUS);
        }
        if (isUsing != null) {

            ct.setUsingCommandType(("true".equals(isUsing.toLowerCase())) ? Boolean.TRUE
                    : Boolean.FALSE);
        } else {
            ct.setUsingCommandType(Boolean.FALSE);
            iloRes.getParseMsg().append("Set as not a using command type.")
                    .append("\n");
        }
        iloRes.setReqestObj(ct);
        iloRes.setType(ilo.getObjType());
        return iloRes;
    }
}
