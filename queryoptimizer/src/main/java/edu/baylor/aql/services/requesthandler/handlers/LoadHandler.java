package edu.baylor.aql.services.requesthandler.handlers;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import edu.baylor.aql.entities.Element;
import edu.baylor.aql.entities.query.IntermediateLanguageObject;
import edu.baylor.aql.entities.query.IntermediateLanguageObject.ParseObjType;
import edu.baylor.aql.services.requesthandler.pipeline.Visitor;
import edu.baylor.aql.services.requesthandler.pipeline.VisitorH;
import edu.baylor.aql.util.Util;

/**
 * This service class defines top level processing service that invoces
 * {@link Visitor} services. After all {@link Visitor} services completed, this
 * service check results and updates the state of the visited {@link Element}
 * object.
 * 
 * @author Rovshen Nazarov. Baylor University. Jan 14, 2015.All Rights Reserved.
 * 
 */
public final class LoadHandler extends VisitorH {

    private static Logger logger = Logger
            .getLogger(LoadHandler.class.getName());

    /**
     * Get {@link IntermediateLanguageObject} and based on its
     * {@link ParseObjType} decide, which type of load to perform.
     */
    @Override
    public void visit(final Element load, final CountDownLatch doneSignal)
            throws InterruptedException {
        doneSignal.countDown(); // count down for this visitor
        final List<Future<Object>> futures = new ArrayList<>();
        try {
            final List<Visitor> pipeline = this.getVisitorsVert();
            for (final Visitor v : pipeline) {
                v.visit(load, doneSignal); // sets values in visitors
                final Future<Object> fut = super.getExecThreadPool().submit(v);
                futures.add(fut);
            }
        } catch (final Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        } finally {
            try {
                // wait for all
                doneSignal.await(Util.MAX_WAITING_MIN, TimeUnit.MINUTES);
            } catch (final InterruptedException e) {
                // do nothing
            }
            // visitors to finish
            final StringBuilder msg = new StringBuilder(load.getStateMsg());
            boolean errOcc = false;

            for (final Future<Object> future : futures) {
                Element el = null;
                try {
                    el = (Element) future.get();
                } catch (final ExecutionException e) {
                    // do nothing
                }
                if (el != null) {
                    if (el.isErrState()) {
                        logger.info(el.getStateMsg());
                        if (!load.isErrState() && el.isErrState()) {
                            errOcc = true;
                        }
                        msg.append(el.getStateMsg());
                    }
                }
            }
            load.setErrState(errOcc);
            // load.setStateMsg(msg.toString());
        }
    }

    @Override
    public Object call() throws Exception {
        return null;
    }

}
