package edu.baylor.aql.services.loading;

import java.util.concurrent.CountDownLatch;

import org.apache.log4j.Logger;

import edu.baylor.aql.entities.Element;
import edu.baylor.aql.entities.command.CommandType;
import edu.baylor.aql.services.requesthandler.pipeline.Visitor;
import edu.baylor.aql.util.AQLException;
import edu.baylor.aql.util.Util;

/**
 * This service loads into the AQL system provided {@link CommandType}.
 * 
 * @author Rovshen Nazarov. Baylor University. Jan 14, 2015.All Rights Reserved.
 * 
 */
public final class CommandTypeLoader extends Visitor {

    private static Logger logger = Logger.getLogger(CommandTypeLoader.class
            .getName());
    private Element commType;
    private CountDownLatch doneSignal;

    @Override
    public Element call() throws Exception {
        // System.out.println("CommandTypeLoader");
        if (!(commType instanceof CommandType)) {
            doneSignal.countDown();
            return null;
        }
        CommandType ct = null;
        try {
            ct = (CommandType) commType;
            final StringBuilder sb = new StringBuilder();

            super.getDao().openConnection();
            storeCommandType(ct);
            super.getDao().closeConnection();
            if (Util.FAILED_STATUS == ct.getId()) {
                sb.append("Could not load the Command Type: ")
                        .append(ct.getTypeName()).append("\n");
                logger.error(sb.toString());
                ct.setErrState(true);
            } else {
                sb.append("Generated Command Type's id is: ")
                        .append(ct.getId()).append("\n");
                logger.info(sb.toString());
            }
            ct.setStateMsg(sb.toString());
        } finally {
            doneSignal.countDown();
        }
        return ct;
    }

    @Override
    public void visit(final Element commType, final CountDownLatch doneSignal)
            throws InterruptedException {
        this.doneSignal = doneSignal;
        this.commType = commType;
    }

    /**
     * Load {@link Element} to the database.
     * This method stores in memory the object represented by the
     * {@link Element} to the metadata. It also saves in-memory auto generated
     * id for later references.
     * 
     * If the object could not be inserted, than the object id is set to
     * FAILED_STATUS.
     * 
     */
    private void storeCommandType(final CommandType ct) throws AQLException {
        ct.setId((long) Util.FAILED_STATUS);
        ct.setId(ct.insert(super.getDao()));
    }
}
