package edu.baylor.aql.services.requesthandler.pipeline;

import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;

import edu.baylor.aql.dataaccess.IMetadata;
import edu.baylor.aql.dataaccess.Metadata;
import edu.baylor.aql.entities.Element;
import edu.baylor.aql.util.AQLException;

/**
 * This is the abstract class to be extended by services that will perform
 * operations on the sub-classes of the {@link Element} abstract class.
 * 
 * @author Rovshen Nazarov. Baylor University. Jan 14, 2015.All Rights Reserved.
 * 
 */
public abstract class Visitor implements Callable<Object> {

    /**
     * This field is used by subclasses to store data to the metadata.
     */
    private final IMetadata dao = new Metadata();

    /**
     * @return the dao
     */
    public IMetadata getDao() {
        return dao;
    }

    @Override
    public abstract Object call() throws Exception;

    public abstract void visit(final Element query,
            final CountDownLatch doneSignal) throws InterruptedException,
            AQLException;
}
