/**
 * This package contains controller services that control or handle processing of AQL request.
 * Each controller/handler must extend {@link edu.baylor.aql.services.requesthandler.pipeline.VisitorH} and
 * will invoke and check results of one or more {@link edu.baylor.aql.services.requesthandler.pipeline.Visitor} services.
 *<p>
 *Extend {@link edu.baylor.aql.services.requesthandler.pipeline.VisitorH} to add a handler for a new AQL request type, and attached it to an appropriate {@link edu.baylor.aql.services.requesthandler.pipeline.EPipelineSingleton}.
 *When {@link edu.baylor.aql.dispatcher.SessionManager} receives AQL request it invokes appropriate {@link edu.baylor.aql.services.requesthandler.pipeline.EPipelineSingleton} 
 *based on {@link edu.baylor.aql.entities.query.IntermediateLanguageObject.ParseRequestType}.
 *<p>
 *{@link edu.baylor.aql.services.requesthandler.pipeline.EPipelineSingleton} is the general way to invoke hierarchy of related {@link edu.baylor.aql.services.requesthandler.pipeline.VisitorH} with a list of {@link edu.baylor.aql.services.requesthandler.pipeline.Visitor} services.
 *It also allows adding dynamically new {@link edu.baylor.aql.services.requesthandler.pipeline.Visitor} to existing {@link edu.baylor.aql.services.requesthandler.pipeline.VisitorH}.
 *{@link edu.baylor.aql.services.requesthandler.pipeline.EPipelineSingleton} is populated at AQL Query Exectution Server start up in the {@link edu.baylor.aql.dispatcher.AQLServerApp} controller.
 */
package edu.baylor.aql.services.requesthandler.handlers;