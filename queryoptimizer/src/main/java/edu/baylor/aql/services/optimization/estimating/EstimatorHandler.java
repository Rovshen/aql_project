package edu.baylor.aql.services.optimization.estimating;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import edu.baylor.aql.bootstrap.AQLServerAppSingleton;
import edu.baylor.aql.bootstrap.BootstrapAQLServer;
import edu.baylor.aql.dispatcher.AQLServerApp;
import edu.baylor.aql.entities.Element;
import edu.baylor.aql.entities.operands.ComputedOperand;
import edu.baylor.aql.entities.query.Query;
import edu.baylor.aql.services.requesthandler.pipeline.Visitor;
import edu.baylor.aql.services.requesthandler.pipeline.VisitorH;
import edu.baylor.aql.util.AQLException;

/**
 * This service runs needed estimator for each {@link ComputedOperand} in the
 * {@link Query}
 * 
 * @author Rovshen Nazarov. Baylor University. Nov 7, 2014.All Rights Reserved.
 * 
 */
public final class EstimatorHandler extends VisitorH {

    private static final int POOL_SIZE = 10;
    private final ExecutorService execThreadPool;
    /**
     * AQL Server wide settings provided on the server start up.
     * Those settings define general settings that will be applied in different
     * places in the system.
     * <p>
     * The settings are accessed via singleton pattern, where the single
     * instance of the AQL Server is created on start up via
     * {@link BootstrapAQLServer} service and is accessed as needed.
     */
    private final AQLServerApp serverInstance = AQLServerAppSingleton
            .getInstance();

    public EstimatorHandler() {
        execThreadPool = Executors.newFixedThreadPool(POOL_SIZE);
    }

    @Override
    public Object call() throws InterruptedException {
        return null;
    }

    @Override
    public void visit(final Element query, final CountDownLatch doneSignal)
            throws InterruptedException, AQLException {
        try {
            if (serverInstance.getSettings().isNullOptimization()) {
                // do nothing
                // count down for the vertical visitors in the handler
                for (final Visitor v : super.getVisitorsVert()) {
                    doneSignal.countDown();
                }
            } else { // full optimization
                for (final Visitor v : super.getVisitorsVert()) {
                    // be careful with a state change of the shared object
                    // and be careful with done signal count down latch
                    query.accept(v, doneSignal);
                    execThreadPool.submit(v);
                }
            }
        } finally {
            doneSignal.countDown(); // for this handler
        }
    }
}
