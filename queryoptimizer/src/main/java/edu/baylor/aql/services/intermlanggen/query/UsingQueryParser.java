package edu.baylor.aql.services.intermlanggen.query;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Stack;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.log4j.Logger;

import edu.baylor.aql.dataaccess.IMetadata;
import edu.baylor.aql.entities.command.Command;
import edu.baylor.aql.entities.command.CommandType;
import edu.baylor.aql.entities.operands.Operand;
import edu.baylor.aql.entities.query.IntermediateLanguageObject;
import edu.baylor.aql.entities.query.IntermediateLanguageObject.ParseObjType;
import edu.baylor.aql.entities.query.Query;
import edu.baylor.aql.services.intermlanggen.UtilParser;
import edu.baylor.aql.services.query.IQueryService;
import edu.baylor.aql.services.query.QueryService;

/**
 * This is a specific type of the {@link BasicQueryParser} because it needs to
 * perform the same parsing of the client request. In addition to what
 * {@link BasicQueryParser} does this parser has additional parsing step, where
 * the using query section is parsed.
 * 
 * @author Rovshen Nazarov. Baylor University. Jan 14, 2015.All Rights Reserved.
 * 
 */
public final class UsingQueryParser extends BasicQueryParser {

    /**
     * Used to log errors or information.
     */
    private static Logger logger = Logger.getLogger(UsingQueryParser.class
            .getName());

    public UsingQueryParser(final IntermediateLanguageObject ilo,
            final IMetadata dao) {
        super(ilo, dao);
    }

    @Override
    public IntermediateLanguageObject parse() throws Exception {

        final IQueryService qService = new QueryService();
        final IntermediateLanguageObject iloRes = new IntermediateLanguageObject();
        final Query query = new Query();
        final Queue<Command> commands = new LinkedList<>();
        final ConcurrentHashMap<Long, Operand> operMap = new ConcurrentHashMap<>();
        final Stack<Integer> subQuerySeqNumbers = new Stack<>();
        final List<Long> operands = new ArrayList<>();
        query.setOperands(operands);
        query.setSubQuerySeqNumbers(subQuerySeqNumbers);
        query.setOperMap(operMap);
        query.setCommands(commands);
        iloRes.setReqestObj(query);
        iloRes.setType(ParseObjType.USING_QUERY);
        super.dao.openConnection();
        final List<CommandType> commandTypes = dao.lookUpCommandTypes();
        dao.closeConnection();
        if (commandTypes.isEmpty()) {
            // wrong command encountered
            logger.error("No Commands Types loaded.");
            return iloRes;
        }
        final AtomicInteger numbOfExpCommResults = new AtomicInteger(0);
        final QueryTokens qt = (QueryTokens) super.iloInst.getReqestObj();
        query.setQueryRequest(qt.getRequest().toString());
        final int queryTokensSize = qt.getQueryTokens().size();
        // Read data until the end query
        for (int index = 0; index < queryTokensSize; index++) {
            final String line = qt.getQueryTokens().get(index);
            // process as using command
            if (line.toUpperCase().contains("BEGIN_USING")) {
                index = processUsingCommand(qt.getQueryTokens(), index,
                        commandTypes, iloRes);
            } else {
                // if not using command process as basic command
                if (super.processBasicCommand(line, iloRes, commandTypes,
                        numbOfExpCommResults)) {
                    return iloRes; // return on error
                }
            }
        }
        super.checkOperands(query, iloRes, qService);
        return iloRes;
    }

    /**
     * Return the updated index position after parsing using command. Note the
     * position corresponds to the last processed line, thus if used in must be
     * incremented to read the next line.
     * <p>
     * Add sub query id if any to the operands and the command from the
     * subQueryIds stack
     * 
     * @param queryTokens
     * @param index
     *            the line where we found using command start marker
     * @param commandTypes
     * @param iloRes
     * @return
     * @throws InterruptedException
     */
    protected int processUsingCommand(final List<String> queryTokens,
            int index, final List<CommandType> commandTypes,
            final IntermediateLanguageObject iloRes)
            throws InterruptedException {

        final Query query = (Query) iloRes.getReqestObj();
        final Queue<Command> commands = ((Query) iloRes.getReqestObj())
                .getCommands();
        String line = "";
        final Command usingCommand = new Command();
        index++; // prepare to read the next line of the using command data
                 // structure
        line = getNextLine(index, queryTokens);
        // read using command related data structure line by line until the end
        // marker
        while (!line.toUpperCase().contains("END_USING")) {
            // process using command with attributes
            if (line.contains("CUST_COMMAND_NAME")) {
                final String usingCommName = getElement(line, iloRes);
                final int commandParseLineNumber = getCommandLineNumber(line,
                        iloRes);

                for (final CommandType commandType : commandTypes) {
                    // populate using command based on the matched type
                    if (usingCommName.contains(commandType.getTypeName())) {
                        // add parse line number
                        usingCommand
                                .setCommandType(commandType.getTypeName())
                                .setCommandTypeObj(commandType)
                                .setNumberOfOperands(
                                        commandType.getMinNumbOper())
                                .setParseLineNumber(commandParseLineNumber);
                        // add sub query sequence number if any
                        if (!query.getSubQuerySeqNumbers().isEmpty()) {
                            // the top of the stack number is the current sub
                            // query that is being parsed
                            usingCommand.setSubQueryNumber(query
                                    .getSubQuerySeqNumbers().peek());
                        }
                    }
                }
            }// end of process custom name
             // process attributes and possible operands
            if (line.contains("ATTR_TYPE")) {
                final String[] elements = line
                        .split(UtilParser.DEFAULT_ATT_VAL_STRING_SEP);
                final String[] attVal = UtilParser.getAttValueEntry(elements);
                usingCommand.addAttVal(attVal);
            }
            // operands inside brackets, such as custComm( operand1, operand2 )
            if (line.contains("OPERAND_ID")) {

                final String[] els = getElements(line, iloRes);
                // add sub query sequence number
                final Operand resOp = super.parseOperandId(els, iloRes);
                query.getOperMap().put((long) resOp.hashCode(), resOp);
                query.getOperands().add((long) resOp.hashCode());
            }
            index++; // get the next line to process from using command
            line = getNextLine(index, queryTokens);
        }
        // logger.info(usingCommand);
        commands.offer(usingCommand);
        return index;
    }

    /**
     * Get the line from the list given the index.
     * 
     * @param index
     * @param queryTokens
     * @return
     */
    private String getNextLine(final int index, final List<String> queryTokens) {
        String line = "";
        if (index < queryTokens.size()) {
            line = queryTokens.get(index);
        }
        return line;
    }
}
