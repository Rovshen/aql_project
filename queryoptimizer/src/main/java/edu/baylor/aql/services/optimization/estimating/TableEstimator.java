package edu.baylor.aql.services.optimization.estimating;

import java.util.concurrent.CountDownLatch;

import edu.baylor.aql.entities.Element;
import edu.baylor.aql.entities.operands.Operand.OperandType;
import edu.baylor.aql.entities.query.Query;
import edu.baylor.aql.services.requesthandler.pipeline.Visitor;

public final class TableEstimator extends Visitor {

    private static final OperandType estType = OperandType.TABLE;
    private CountDownLatch doneSignal;
    @SuppressWarnings("unused")
    private Query query;

    /**
     * @return the esttype
     */
    public static OperandType getEsttype() {
        return estType;
    }

    @Override
    public Object call() throws Exception {
        try {

        } finally {
            doneSignal.countDown();
        }
        return null;
    }

    @Override
    public void visit(final Element query, final CountDownLatch doneSignal)
            throws InterruptedException {
        this.doneSignal = doneSignal;
        this.query = (Query) query;

    }
}
