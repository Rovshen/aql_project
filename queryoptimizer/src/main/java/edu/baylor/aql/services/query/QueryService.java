package edu.baylor.aql.services.query;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Deque;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.Stack;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import edu.baylor.aql.dataaccess.IMetadata;
import edu.baylor.aql.dataaccess.Metadata;
import edu.baylor.aql.entities.command.Command;
import edu.baylor.aql.entities.command.CommandAlgorithm;
import edu.baylor.aql.entities.command.CommandType;
import edu.baylor.aql.entities.operands.ComputedOperand;
import edu.baylor.aql.entities.operands.Operand;
import edu.baylor.aql.entities.operands.Operand.OperandState;
import edu.baylor.aql.entities.query.Query;
import edu.baylor.aql.services.optimization.ordering.IOrderingService;
import edu.baylor.aql.services.requesthandler.pipeline.CommandAlgorithmService;
import edu.baylor.aql.services.requesthandler.pipeline.ICommandAlgorithmService;
import edu.baylor.aql.services.requesthandler.pipeline.OperandFactory;
import edu.baylor.aql.util.AQLException;
import edu.baylor.aql.util.Util;

/**
 * This implementation of hte {@link IQueryService} interface has implementation
 * of the services for the {@link Query}.
 * 
 * @author Rovshen Nazarov. Baylor University. Jan 14, 2015.All Rights Reserved.
 * 
 */
public final class QueryService implements IQueryService {
    /**
     * Used to log errors or information.
     */
    private static Logger logger = Logger.getLogger(QueryService.class
            .getName());

    private static final IMetadata dao = new Metadata();

    /**
     * NOTE: The order and the number of {@link Operand}s that are added to the
     * {@link Command} depends on the following:
     * 1) Both {@link Command}s list and {@link Operand}s list are pre-sorted
     * based on their parse line numbers using custom {@link Comparator}.
     * <p>
     * 2) Any {@link Operand} that is still in the {@link Operand}'s list and
     * has {@link Operand#getParseLineNumber()} smaller than
     * {@link Command#getParseLineNumber()} is added to that {@link Command}.
     * 
     * PRE: both {@link Operand}s and {@link Command}'s list are expected to be
     * sorted based on their parse line numbers.
     */
    @Override
    public void populateCommandsWithOperandsSequencial(final Query query)
            throws AQLException {
        assert (query != null) : "Query can not be null.";
        assert (query.getOperands() != null) : "Operands can not be null.";
        // operands are populated during parsing and have just IDs
        // modify copy not original operands
        final Deque<Long> operandsCopy = new LinkedList<>(query.getOperands());
        addOperandsToCommands(operandsCopy, query);
        setResultForQuery(query);
        // logger.info(query);
    }

    /**
     * Set {@link Query} result {@link Operand} based on the result
     * {@link Operand} of
     * the last {@link Command} in the {@link Query}. And set
     * {@link Query#getProgress()} to 1.0
     * 
     * @param query
     * @throws AQLException
     */
    @Override
    public void setResultForQuery(final Query query) throws AQLException {
        final Command[] commands = query.getCommands().toArray(new Command[0]);
        if (commands.length < 1) {
            throw new AQLException("At least one command must be specified");
        }
        final Command lastCommand = commands[commands.length - 1];
        query.setResultOperand(lastCommand.getResult());
    }

    /**
     * Subroutine. Stateless.
     * Add {@link Operand}s to {@link Command}. {@link Operand}s will have the
     * same order as defined in the operandsCopy {@link Deque}.
     * 
     * PRE: {@link CommandType} must be defined for {@link Command}s.
     * 
     * POST: replaces old {@link Operand}s in the {@link Command} if any with
     * the new set of {@link Operand}s.
     * 
     * NOTE: populating {@link Command}s with {@link Operand}s should be
     * separate from combining {@link Command}s based on
     * {@link CommandAlgorithm}
     * 
     * @param operandsCopy
     * @param query
     * @throws AQLException
     */
    private void addOperandsToCommands(final Deque<Long> operandsCopy,
            final Query query) throws AQLException {

        final Queue<Command> commands = query.getCommands();

        // hold out operands based on sub query sequence number
        final Stack<Long> operandsToHold = new Stack<>();

        // IDs retrieved in the reverse order e.g. id2 id1 for id1 DIFF id2
        for (final Command command : commands) {

            // put back any held out operands
            while (!operandsToHold.isEmpty()) {
                // pop and push operand, dequeue acts as stack here
                operandsCopy.offerFirst(operandsToHold.pop());
            }

            if (command.getCommandTypeObj() == null) {
                throw new AQLException("CommandType cannot be null.");
            }
            populateCommandWithOper(command, operandsCopy, operandsToHold,
                    query);

        } // end of populating this command, do same for the next ones
          // logger.info(operandsCopy);
          // last command leaves its result operand
          // if more than two operands, we got more operands,
          // than commands supports
        if (operandsCopy.size() > 1) {
            throw new AQLException(
                    "There were more operands than commands could take");
        }
    }

    /**
     * Add {@link Query}'s {@link Operand}s to the {@link Command}.
     * 
     * @param command
     * @param operandsCopy
     * @param operandsToHold
     * @param query
     * @throws AQLException
     */
    private void populateCommandWithOper(final Command command,
            final Deque<Long> operandsCopy, final Stack<Long> operandsToHold,
            final Query query) throws AQLException {

        if (command.getCommandTypeObj().isUsingCommandType()) {
            populateCommandWithOperUsing(command, operandsCopy, operandsToHold,
                    query);
        } else {
            populateCommandWithOperBinary(command, operandsCopy,
                    operandsToHold, query);
        }
    }

    /**
     * Just pick up all {@link Operand}s on the same level.
     * We make assumption that the below is not allowed:
     * 
     * from (from a UNION b) using c();
     * Note that this is allowed
     * from (from a UNION b) as X using c(X);
     * 
     * Also we have two cases:
     * 
     * 1)The number of desired operands is known
     * 2) The number of desired operands is NOT known (* - any)
     * 
     * @param command
     * @param operandsCopy
     * @param operandsToHold
     * @param query
     * @throws AQLException
     */
    private void populateCommandWithOperUsing(final Command command,
            final Deque<Long> operandsCopy, final Stack<Long> operandsToHold,
            final Query query) throws AQLException {

        final List<Long> operandsCurr = new ArrayList<>();
        // any number of operands on same level
        if (CommandAlgorithm.MAX_OPERANDS == command.getCommandAlgo()
                .getNumberOfOperands()) {

            boolean sameLevel = true;
            // get operands for the command on same level
            while (sameLevel) {
                final Long oToAdd = operandsCopy.poll();

                if (oToAdd == null) {
                    sameLevel = false; // stop adding operands
                    continue; // next step
                }

                // check if we shall hold out this operand
                if (holdOutOperandNotSameLevel(command,
                        query.getOperMap().get(oToAdd))) {

                    operandsToHold.push(oToAdd);
                    continue; // get the next operand
                }
                operandsCurr.add(oToAdd);
            }// end of getting operands for any case
        } else { // fixed number of operands case

            // get operands for the command
            for (int i = 0; i < command.getCommandAlgo().getNumberOfOperands(); i++) {

                final Long oToAdd = operandsCopy.poll();

                if (oToAdd == null) {
                    throw new AQLException(
                            "Not enough arguments for the command "
                                    + command.getCommandType());
                }

                // check if we shall hold out this operand
                if (holdOutOperandNotSameLevel(command,
                        query.getOperMap().get(oToAdd))) {
                    operandsToHold.push(oToAdd);
                    i--; // increase loop by one step to get enough operands
                    continue; // get the next operand
                }
                operandsCurr.add(oToAdd);
            }// end of getting operands
        }
        addOperandsToSingleCommand(operandsCurr, command, operandsCopy, query);

    }

    private void populateCommandWithOperBinary(final Command command,
            final Deque<Long> operandsCopy, final Stack<Long> operandsToHold,
            final Query query) throws AQLException {

        final List<Long> operandsCurr = new ArrayList<>();

        // we use this flag to allow adding operands from different levels
        boolean doHoldOut = true;
        // get operands for the command
        for (int addedCount = 1; addedCount <= command.getCommandAlgo()
                .getNumberOfOperands(); addedCount++) {

            final Long oToAdd = operandsCopy.poll();
            // GIVE SECOND CHANCE
            if (oToAdd == null) {
                if (!operandsToHold.isEmpty()) {
                    // put back any held out operands
                    while (!operandsToHold.isEmpty()) {
                        operandsCopy.offerFirst(operandsToHold.pop());

                    }
                    // this are last operands so do not hold them out
                    doHoldOut = false;
                    // since this check cost a loop iteration
                    addedCount--; // increase loop by one step
                    continue;
                }
                throw new AQLException("Not enough arguments for the command "
                        + command.getCommandType() + " operands added were "
                        + operandsCurr);
            }
            final Operand o = query.getOperMap().get(oToAdd);
            // check if we shall hold out this operand
            if (holdOutOperandNotSameLevel(command, o) && doHoldOut) {

                // if not same level check parse line number
                // if (addResultOperand(operandsCopy, command, o, addedCount)
                // && (operandsCopy.isEmpty() && addedCount == 2)) {
                if (addResultOperand(query, operandsCopy, command, o,
                        addedCount)) {
                    operandsCurr.add(oToAdd);
                } else {
                    operandsToHold.push(oToAdd);
                    addedCount--; // increase loop by one step to get enough
                                  // operands
                    continue; // get the next operand
                }
            } else { // if do not need to hold out just add
                // logger.info("\n\n" + query.getOperMap().get(oToAdd));
                operandsCurr.add(oToAdd);
            }
        }
        addOperandsToSingleCommand(operandsCurr, command, operandsCopy, query);

    }

    /**
     * This case is only for result operand of sub query.
     * 
     * If not same level check if it is result operand.
     * If it is result operand check if parse line number smaller,
     * than command parse line number
     * 
     * TODO
     * We have two cases assuming used for BINARY command:
     * 
     * 1) we need one more operand (normal case)
     * 2) we need two more operands (special case).
     * We need to do look ahead to see if the next based by parse
     * line number operand is the last operand smaller than command, if so we
     * add this operand, otherwise we do not add it.
     * 
     * @param query
     * 
     * @param operandsCopy
     * 
     * @param command
     * @param operand
     * @param addedCount
     * @return true add to operands, false do not add to operands
     */
    private boolean addResultOperand(final Query query,
            final Deque<Long> operandsCopy, final Command command,
            final Operand operand, final int addedCount) {

        // first case we need one operand
        // the operand parse line number is smaller and it is result operand
        if (addedCount == 2) {
            if ((command.getParseLineNumber() > operand.getParseLineNumber())
                    && (OperandState.INTERMEDIATE_RESULT == operand.getType())) {
                return true; // add to operands
            }
        }

        // second case we need two operands
        // if we need two operands do special check
        if (addedCount == 1) {
            // do look ahead if the next operand can be added to command.
            int canAdd = 0;
            if ((command.getParseLineNumber() > operand.getParseLineNumber())
                    && (OperandState.INTERMEDIATE_RESULT == operand.getType())) {
                canAdd++;
                // logger.info("\n\n" + operand);
                // if can add more than two do not add this operand
                for (final Long oId : operandsCopy) {
                    final Operand oC = query.getOperMap().get(oId);
                    if ((command.getParseLineNumber() > oC.getParseLineNumber())) {
                        // logger.info(oC);
                        canAdd++;
                    }
                }
                // logger.info("Can add " + canAdd + "; operands left "
                // + operandsCopy.size());
                if (canAdd == 2) {
                    return true; // add to operands
                }
            }
        }

        return false; // do not add to operands
    }

    /**
     * This method is an attempt to fix sub - query operand population.
     * 
     * If no more {@link Operand}s for the {@link Command} that are on the same
     * level, get result {@link Operand}s that are one or two levels below
     * current command.
     * 
     * This handles cases
     * 
     * (sub query level 1 (sub query level 2) UNION (sub query level 3) )
     * (sub query level 1 operand UNION (sub query level 2) )
     * (sub query level 1 (sub query level 2) UNION operand )
     * command has level 1
     * 
     * @param command
     * 
     * @param operandsCopy
     * @param query
     * @return
     */
    @SuppressWarnings("unused")
    private Long getResultOfInnerMostSubQuery(final Command command,
            final Deque<Long> operandsCopy, final Query query) {
        // we use descending iterator, but we could use normal one as well
        // descending iter will help find results of innermost sub queries
        // faster as we expect them to be in the end of the queue.
        final Iterator<Long> operIter = operandsCopy.descendingIterator();
        while (operIter.hasNext()) {
            final Long currVal = operIter.next();
            final int operSubQNumb = query.getOperMap().get(currVal)
                    .getSubQueryNumber();

            if ((command.getSubQueryNumber() == (operSubQNumb + 1))
                    || (command.getSubQueryNumber() == (operSubQNumb + 2))) {
                operandsCopy.remove(currVal);
                return currVal;
            }
        }
        return null;
    }

    /**
     * Check if we should hold out current {@link Operand} from adding to the
     * current {@link Command}. The decision is based on the both object's sub
     * query sequence number. If {@link Command#getSubQueryNumber()} <=
     * {@link Operand#getSubQueryNumber()} no need to hold
     * out, else hold out the {@link Operand} from adding
     * to this {@link Command}.
     * 
     * @param command
     * @param operand
     * @return true - hold out, false - do not
     */
    private boolean holdOutOperandNotSameLevel(final Command command,
            final Operand operand) {

        // the operand in the same level
        if (command.getSubQueryNumber() == operand.getSubQueryNumber()) {
            return false; // do not hold out
        }

        return true; // hold out

    }

    /**
     * Add current list of {@link Operand}s to the {@link Command} and add the
     * result {@link Operand} of the {@link Command} to the {@link Operand}s
     * dequeue if that {@link Operand} is not already in the
     * {@link Query#getOperands()} list. It could be in the list if the
     * {@link IOrderingService#combineGroupsByOperands(List)} produces such an
     * ordering of the {@link Operand}s lists.
     * 
     * @param operandsCurr
     * @param command
     * @param operandsCopy
     * @param query
     */
    private void addOperandsToSingleCommand(final List<Long> operandsCurr,
            final Command command, final Deque<Long> operandsCopy,
            final Query query) throws AQLException {

        command.setOperands(operandsCurr);

        final Operand resultO = OperandFactory.getInstance(command
                .getCommandAlgo().getResultOperandType());
        if (resultO == null) {
            throw new AQLException("Could not initialize operand of type "
                    + command.getCommandAlgo().getResultOperandType()
                            .getLabel());
        }
        resultO.setType(OperandState.INTERMEDIATE_RESULT.getLabel())
                .setCommand(command)
                .setSubQueryNumber(command.getSubQueryNumber())
                .setParseLineNumber(command.getParseLineNumber());

        resultO.setId(query.getClientSession().getCommandResultOperId()
                .getAndDecrement());
        query.getOperMap().put((long) resultO.hashCode(), resultO);
        command.setResult((long) resultO.hashCode());
        // place results for curr command at the head of the queue
        // if this operand is not already in the Operands list of the query.
        // this case is possible for orderings population
        if (!query.getOperands().contains((long) resultO.hashCode())) {
            operandsCopy.offerFirst((long) resultO.hashCode());
        } else {
            throw new AQLException("Duplicate member operand being added: "
                    + resultO.hashCode());
        }

    }

    /**
     * Look ahead how many {@link OperandState#LOADED} or stored
     * {@link OperandState#INTERMEDIATE_RESULT} {@link Operand}s can be taken
     * from the {@link Operand} dequeue up to
     * the next {@link Command} in respect to the input current {@link Command}.
     * <p>
     * Count each {@link OperandState#LOADED} {@link Operand} or recorded
     * {@link OperandState#INTERMEDIATE_RESULT} {@link Operand}.
     * 
     * @param RemainingOperands
     * @param commandCurr
     *            current {@link Command}
     * @return count of operands from start to first command
     */
    @SuppressWarnings("unused")
    private int getOperandsCountUntilNextCommand(
            final Deque<Operand> RemainingOperands, final Command commandCurr) {

        int count = 0;
        for (final Operand o : RemainingOperands) {
            // similar to check in
            // OrderingService.populateGroupsWithOperands(ocGroup)
            if (OperandState.INTERMEDIATE_RESULT != o.getType()
                    || (o.getStatus() && o.getInitPath() != null)) {
                // this operand is before the next command, so count it in
                if (o.getParseLineNumber() < commandCurr.getParseLineNumber()) {
                    count++;
                } else {
                    // no need to look further as ordering insures increase in
                    // parse line number
                    return count;
                }
            }
        }
        return count;
    }

    /**
     * Get the {@link Command} that comes after the current {@link Command}.
     * 
     * @param commands
     * @param commandCurr
     * @return empty or the next Command
     */

    @SuppressWarnings("unused")
    private Command getNextCommand(final Queue<Command> commands,
            final Command commandCurr) {
        final Iterator<Command> iter = commands.iterator();
        while (iter.hasNext()) {
            if (iter.next().equals(commandCurr)) {
                if (iter.hasNext()) {
                    return iter.next();
                }
            }
        }
        return new Command();
    }

    @Override
    public int checkOperandsLoaded(final Query q,
            final StringBuilder loadErrMsgLoad) throws AQLException {

        final List<Long> operandIds = q.getOperands();
        int numberOfErrors = 0;
        for (final Long oId : operandIds) {
            final Operand operand = q.getOperMap().get(oId);
            if (operand == null) {
                throw new AQLException("The operand cannot be null");
            }
            // check if operand exists if it is not an alias
            if (!operand.getStatus() && operand.getAliasName() == null) {
                loadErrMsgLoad.append("Could not find ")
                        .append(operand.getReadableId())
                        .append(" in the metadata").append(Util.LINE_SEPARATOR);
                logger.error(loadErrMsgLoad.toString());
                numberOfErrors++;
            }
        }
        return numberOfErrors;
    }

    /**
     * Iterate through all {@link Command}s within {@link Query}.
     * For each {@link Command} iterate through all {@link Operand}s.
     * Compare current {@link Operand} and {@link ComputedOperand} based on
     * computedOperand's command id.
     * If the two {@link Operand} have the same command id replace the current
     * operand with the repeatedCommandResult.
     * 
     * @throws AQLException
     */
    @Override
    public void updateIntermResults(final Query query,
            final Long origComputedCommResultId,
            final Long repeatedCommandResultId) throws AQLException {

        for (final Command command : query.getCommands()) {
            // logger.info("\n\n\n"
            // + Arrays.toString(query.getCommands().toArray()));
            if (command.getResult().equals(origComputedCommResultId)) {
                // this is the command whose result we update in other commands
                // update reference id
                final Operand o = updateOperandReference(query,
                        repeatedCommandResultId, command.getResult());
                command.setResult((long) o.hashCode());
            }
            // logger.info("\n\n\ncopyOrigComputedCommResultId: "
            // + origComputedCommResultId);
            for (final Long operandId : command.getOperands()) {

                // logger.info("operandId: " + operandId);
                if (operandId.equals(origComputedCommResultId)) {

                    // logger.info("\n\n\noperand before update " + operandId);
                    // update reference id
                    final Operand o = updateOperandReference(query,
                            repeatedCommandResultId, operandId);

                    updateListEntry(command.getOperands(), operandId,
                            (long) o.hashCode());

                    // logger.info("command operands: " + command.getOperands()
                    // + "\n\n\n");
                }
            }
        }
    }

    /**
     * Replace fromVal with toVal in the {@link List} of {@link Long} values.
     * 
     * @param list
     * @param fromVal
     * @param toVal
     */
    private void updateListEntry(final List<Long> list, final Long fromVal,
            final Long toVal) {

        final int index = list.indexOf(fromVal);
        list.set(index, toVal);
    }

    /**
     * Update Operands map and add new reference to the command
     */
    /**
     * If operand's command sequence number is the same as command
     * sequence number of the operand being updated and
     * If the operand is ready to be used, e.g. it is a real object
     * stored somewhere, and recorded as ready to be executed in the
     * command in the metadata, then
     * Copy repeated command result operand data to the matched
     * operand.
     * Compare and updated {@link Operand} if needed
     * 
     */
    /**
     * Copy values of one {@link Operand} to the other {@link Operand}.
     * NOTE: this operation is recommended instead of the to = from
     * {@link Operand} assignment.
     * 
     * @param fromOp
     * @param toOp
     * @return
     */
    public Operand updateOperandReference(final Query q, final Long fromOpId,
            final Long toOpId) {

        if (fromOpId == null || toOpId == null || q == null) {
            throw new IllegalArgumentException("Input operand cannot be null");
        }
        dao.openConnection();
        final Operand fromOp = dao.lookUpOperand(fromOpId);
        dao.closeConnection();
        final Operand toOp = q.getOperMap().get(toOpId);

        fromOp
        // TODO check setting sub query number and parse line number
        // does not break anything
        .setSubQueryNumber(toOp.getSubQueryNumber())
                .setParseLineNumber(toOp.getParseLineNumber())
                .setCommand(toOp.getCommand())
                .setAliasName(toOp.getAliasName());
        // logger.info("Operand retrieved " + fromOp);
        // logger.info("Operand old " + toOp);
        // expand operands map with repeated operand results
        q.getOperMap().put((long) fromOp.hashCode(), fromOp);
        return fromOp;
    }

    @Override
    public double computeProgress(final Query query) throws AQLException {
        double succCommandCount = 0.0;
        for (final Command command : query.getCommands()) {
            // logger.info(command);
            // check if result Operand is null
            if (command.getResult() == null) {
                throw new AQLException("command result operand cannot be null "
                        + command.getResult());
            }
            final int minOperId = 1;
            if (command.getResult() >= minOperId) {
                succCommandCount++;
            }
        }
        return succCommandCount / query.getCommands().size();
    }

    @Override
    public void copyQuery(final Query from, final Query qTo) {
        if (!from.equals(qTo)) {
            qTo.getOperands().clear();
            qTo.setOperands(new ArrayList<>(from.getOperands()));
            qTo.setSubQuerySeqNumbers(from.getSubQuerySeqNumbers());
        }
        qTo.setCommands(from.getCommands())
                .setClientSession(from.getClientSession())
                .setOperMap(
                        (ConcurrentHashMap<Long, Operand>) from.getOperMap())
                .setClientSessionId(from.getClientSessionId())
                .setResultOperand(
                        (from.getResultOperand() != null) ? from
                                .getResultOperand() : null)
                .setRepeated(from.isRepeated())
                .setQueryProgress(from.getProgress())
                .appendLast(from.getNext())
                .setEstExecCost(from.getEstExecCost())
                .setQueryId(from.getQueryId())
                .setAliasName(from.getAliasName())
                .setQueryRequest(from.getQueryRequest())
                .setErrState(from.isErrState()).setStateMsg(from.getStateMsg());
        ;
    }

    @Override
    public void populateQueryWithDefaultCommandAlgo(final Query q)
            throws AQLException {

        final ICommandAlgorithmService caService = new CommandAlgorithmService();
        // order of setting Command Algo and populating operands matters
        for (final Command command : q.getCommands()) {
            final CommandAlgorithm commAlgo = caService.getDefaultCA(command);
            if (commAlgo != null) {
                command.setCommandAlgo(commAlgo);
            } else {
                throw new AQLException("Command Algorithm must be provided");
            }
        }
        populateCommandsWithOperandsSequencial(q);
    }

    @Override
    public void numberCommands(final Query q) {

        for (final Command command : q.getCommands()) {
            command.setCommandSeqNumber(q.getClientSession()
                    .getCommandSessionId().getAndIncrement());
        }
    }

    @Override
    public Queue<Command> cloneCommandList(final Queue<Command> commands) {
        final Queue<Command> cloneArr = new LinkedList<>();
        for (final Command command : commands) {
            cloneArr.add(command.clone());
        }
        return cloneArr;
    }

    @Override
    public void linkQueries(final List<Query> queries, final Query head) {

        int i = 1;
        boolean firstQuery = true;

        // link queries in linked list manner
        // head is at the input query
        for (final Query nextQ : queries) {
            // logger.info("\n\nquery " + i + " is: " + nextQ);

            // process first query in special order
            if (firstQuery) {
                // we need to copy first query to the input query
                copyQuery(nextQ, head);
                head.setAliasName(nextQ.getAliasName());
                firstQuery = false;
            } else {
                head.appendLast(nextQ);
            }
            i++;
        }
    }

    @Override
    public void replaceAliasWithOperands(final Query qNext,
            final Map<Long, Operand> operandsMap) throws AQLException {

        final Set<String> aliasNamesAll = new HashSet<>();
        for (final Long o : operandsMap.keySet()) {
            // logger.info(operandsMap.get(o));
            aliasNamesAll.add(operandsMap.get(o).getAliasName());
        }
        final List<Long> operandsTo = qNext.getOperands();
        // logger.info(operandsTo);
        for (final Long oTo : operandsTo) {

            if (qNext.getOperMap().get(oTo).getAliasName() != null) {
                if (aliasNamesAll.contains(qNext.getOperMap().get(oTo)
                        .getAliasName())) {
                    final Operand oFrom = getOperandByAliasName(operandsMap,
                            qNext.getOperMap().get(oTo).getAliasName());
                    if (oFrom == null) {
                        throw new AQLException(
                                "Could not get operand by alias name getOperandByAliasName");
                    }

                    // logger.info(oFrom);
                    updateOperandReference(qNext, oFrom.getId(), oTo);
                    updateListEntry(qNext.getOperands(), oTo, oFrom.getId());
                    // logger.info("updated map is " + qNext.getOperMap());
                }
            }
        }
    }

    /**
     * Get an {@link Operand} from a given {@link List} by its
     * {@link Operand#getAliasName()}.
     * 
     * @param operandsMap
     * @param aliasName
     * @return
     */
    private Operand getOperandByAliasName(final Map<Long, Operand> operandsMap,
            final String aliasName) {

        for (final Long o : operandsMap.keySet()) {
            if (aliasName.equals(operandsMap.get(o).getAliasName())) {
                return operandsMap.get(o);
            }
        }
        return null;
    }
}
