package edu.baylor.aql.services.loading;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.log4j.Logger;

import edu.baylor.aql.entities.Element;
import edu.baylor.aql.entities.operands.Operand;
import edu.baylor.aql.entities.operands.Operand.OperandState;
import edu.baylor.aql.entities.operands.Operand.OperandType;
import edu.baylor.aql.services.intermlanggen.UtilParser;
import edu.baylor.aql.services.requesthandler.pipeline.Visitor;
import edu.baylor.aql.util.AQLException;
import edu.baylor.aql.util.Util;
import edu.baylor.aql.util.hdfds.HDFSService;

/**
 * This service loads into the AQL system provided {@link Operand}. This service
 * copies the {@link Operand} from it's original location to the HDFS file
 * system.
 * 
 * @author Rovshen Nazarov. Baylor University. Jan 14, 2015.All Rights Reserved.
 * 
 */
public final class OperandLoader extends Visitor {

    private static Logger logger = Logger.getLogger(OperandLoader.class
            .getName());
    private final ExecutorService execThreadPool;
    private final HDFSService hdfsService;
    private Element operand;
    private CountDownLatch doneSignal;

    public OperandLoader() {
        final int poolSize = 10;
        execThreadPool = Executors.newFixedThreadPool(poolSize);
        hdfsService = new HDFSService(execThreadPool);

    }

    @Override
    public Element call() throws Exception {
        // System.out.println("OperandLoader");
        if (!(operand instanceof Operand)) {
            doneSignal.countDown();
            return null;
        }
        Operand o = null;
        try {
            o = (Operand) operand;

            super.getDao().openConnection();
            final String hdfsLoadPath = getLoadPath(o);
            super.getDao().closeConnection();
            // may be an error because the folder already exits
            final int resultMkdir = hdfsService.createDirOnHDFS(hdfsLoadPath);
            if (resultMkdir == Util.FAILED_STATUS) {
                logger.info("folder may already exist.");
            }
            final int resCopy = hdfsService.copyToHDFS(o.getInitPath(),
                    hdfsLoadPath);
            if (resCopy != Util.SUCCESS_STATUS) {
                final String errCopy = "Could not copy graph to HDFS ";
                operand.setStateMsg(errCopy);
                logger.error(errCopy);
                o.setId(Util.FAILED_STATUS);
            } else {
                final long status = hdfsService.verifyFileExistsOnHDFS(o,
                        hdfsLoadPath);
                if (status == Util.SUCCESS_STATUS) {
                    o.setStatus(true); // status used to build sql in the object
                    super.getDao().openConnection();
                    storeLoadedOperand(o);
                    super.getDao().closeConnection();
                    checkMetadataInsertState(o);
                }
            }
        } finally {
            doneSignal.countDown();
        }
        return o;
    }

    private void checkMetadataInsertState(final Operand o) {
        String msg = "";
        if (Util.FAILED_STATUS == o.getId()) {
            msg = "Could not load the operand: " + o.getReadableId()
                    + " from path " + o.getInitPath();
            logger.error(msg);
            o.setErrState(true);
            o.setStatus(false); // load failed
        } else {
            o.setStatus(true); // loaded successfully
            msg = "Generated operand id is: " + o.getReadableId();
            logger.info(msg);
        }
        o.setStateMsg(msg);
    }

    /**
     * Could be used to load any of the {@link OperandType}
     * 
     * @return
     */
    @Override
    public void visit(final Element operand, final CountDownLatch doneSignal)
            throws InterruptedException {
        this.operand = operand;
        this.doneSignal = doneSignal;
    }

    private String getLoadPath(final Operand o) throws AQLException {

        final String loadPath = super.getDao().lookUpSystemSetting("loadPath",
                "hdfs")
                + UtilParser.FOLDER_SEPARATOR_LINUX + o.getQueryId();
        return loadPath;
    }

    /**
     * Load {@link Element} to the database.
     * This method stores in memory the object represented by the
     * {@link Element} to the metadata. It also saves in-memory auto generated
     * id for later references.
     * 
     * If the object could not be inserted, than the object id is set to
     * FAILED_STATUS.
     * 
     */
    private void storeLoadedOperand(final Operand o) {
        o.setType(OperandState.LOADED.getLabel());
        o.setId(Util.FAILED_STATUS);
        o.setId(o.insert(super.getDao()));
    }

}
