package edu.baylor.aql.services.periodical;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.log4j.Logger;

import edu.baylor.aql.entities.command.Command;
import edu.baylor.aql.services.commandexecutor.AbstractExecutor;
import edu.baylor.aql.services.commandexecutor.BashExecutor;
import edu.baylor.aql.util.Util;

/**
 * This implementation of the {@link IOldRecordPurge} interface purges old
 * records from the underling storage system.
 * 
 * @author Rovshen Nazarov. Baylor University. Dec 21, 2014.All Rights Reserved.
 * 
 */
public final class OldRecordPurge implements IOldRecordPurge {

    private static Logger logger = Logger.getLogger(OldRecordPurge.class
            .getName());
    /**
     * Thread pool used to control threads.
     * Initialized on the object construction to the
     * fixed pool with predefined size.
     */
    private final ExecutorService execThreadPool;

    public OldRecordPurge() {
        final int poolSize = 5;
        execThreadPool = Executors.newFixedThreadPool(poolSize);
    }

    @Override
    public boolean removeAllInFolderRecords(final String folderName) {

        final String allElem = "/*";
        final String hdfsCommand = "hadoop fs -rm -r " + folderName + allElem;
        final Command commandHDFS = new Command();
        commandHDFS.setCommand(hdfsCommand);
        int result = Util.FAILED_STATUS;
        final AbstractExecutor execCurrCommand;
        execCurrCommand = new BashExecutor(commandHDFS.getCommand());
        final Future<Object> resultFutureCur = execThreadPool
                .submit(execCurrCommand);
        try { // block here until result is ready
            result = (int) resultFutureCur.get();
        } catch (InterruptedException | ExecutionException e) {
            logger.error("Could not get result of the executing command with callable thread."
                    + e.getMessage());
        }
        if (result != Util.SUCCESS_STATUS) {
            return false;
        }
        return true;
    }

}
