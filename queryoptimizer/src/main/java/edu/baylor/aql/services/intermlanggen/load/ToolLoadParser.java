package edu.baylor.aql.services.intermlanggen.load;

import edu.baylor.aql.entities.parsing.LoadMap;
import edu.baylor.aql.entities.query.IntermediateLanguageObject;
import edu.baylor.aql.entities.tools.Tool;
import edu.baylor.aql.util.Util;

/**
 * This controller is used to load a tool. This controller
 * is invoked when the client executes tool load command.
 * 
 * This controller stored the new tool into the tools's root
 * folder and generates {@link Tool} object.
 * Also initial sampling is performed on a newly loaded tool
 * to generate initial tool metadata record.
 * 
 * @author Rovshen Nazarov. Baylor University. Sep 1, 2014.All Rights Reserved.
 * 
 */
public final class ToolLoadParser extends BasicLoadParser {

    private final IntermediateLanguageObject ilo;

    public ToolLoadParser(final IntermediateLanguageObject ilo) {
        super();
        this.ilo = ilo;
    }

    @Override
    public IntermediateLanguageObject call() {
        final LoadMap loadMap = (LoadMap) ilo.getReqestObj();
        final IntermediateLanguageObject iloRes = new IntermediateLanguageObject();
        final Tool t = new Tool();

        final String path = loadMap.getAttValTable().get("path");
        final String name = loadMap.getAttValTable().get("name");
        final String prefix = loadMap.getAttValTable().get("prefix");
        final String execenv = loadMap.getAttValTable().get("execenv");
        final String comment = loadMap.getAttValTable().get("comment");
        iloRes.setParseStateCode(Util.SUCCESS_STATUS);
        if (name != null) {
            t.setName(name);
        } else {
            t.setName("");
            iloRes.getParseMsg().append("The name is needed.").append("\n");
            iloRes.setParseStateCode(Util.FAILED_STATUS);
        }
        if (path != null) {
            t.setPath(path);
        } else {
            t.setPath("");
            iloRes.getParseMsg().append("The path is needed.").append("\n");
            iloRes.setParseStateCode(Util.FAILED_STATUS);
        }
        if (prefix != null) {
            t.setPrefix(prefix);
        } else {
            t.setPrefix("");
            iloRes.getParseMsg().append("The prefix is needed.").append("\n");
            iloRes.setParseStateCode(Util.FAILED_STATUS);
        }
        if (execenv != null) {
            t.setExecutionEnvironment(execenv);
        } else {
            t.setExecutionEnvironment("");
            iloRes.getParseMsg().append("The execenv is needed.").append("\n");
            iloRes.setParseStateCode(Util.FAILED_STATUS);
        }
        if (comment != null) {
            t.setComment(comment);
        } else {
            t.setComment("");
        }
        iloRes.setReqestObj(t);
        iloRes.setType(ilo.getObjType());
        return iloRes;
    }
}
