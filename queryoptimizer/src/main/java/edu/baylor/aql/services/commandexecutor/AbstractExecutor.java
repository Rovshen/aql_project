package edu.baylor.aql.services.commandexecutor;

import java.util.concurrent.Callable;

import edu.baylor.aql.entities.command.Command;
import edu.baylor.aql.util.AQLException;

/**
 * General service class that will be extended by platform or OS
 * specific executors.
 * 
 * The goal of the executor service is to receive and execute {@link Command}
 * and return execution result status to the caller.
 * 
 * @author Rovshen Nazarov. Baylor University. Aug 25, 2014.All Rights Reserved.
 * 
 */

public abstract class AbstractExecutor implements Callable<Object> {

    /**
     * Execute given command in the String form.
     * 
     * @return command Execution result status.
     *         Where 1 equals failed,
     *         and 0 equals success.
     * @throws AQLException
     */
    public abstract int execute() throws AQLException;
}
