/**
 * This package contains services that execute loading of the entity objects generated after parsing loading request.
 * Loading involves making a record of the new entity in metadata, and copying loaded entity if needed to the data storage used by the AQL Query Execution Server.
 * If a new entity object needs to be parsed and loaded both the specific parser needs to be added to the intermlanggen library and to this library.
 * There are some common behaviour and interface of the loading services, thus when a new loader is added it should use similar
 * structure and implement similar call interface as the other loader services.  
 */
package edu.baylor.aql.services.loading;

