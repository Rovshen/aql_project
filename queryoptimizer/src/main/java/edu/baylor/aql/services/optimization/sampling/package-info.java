/**
 * This package contains services that perform sampling of {@link edu.baylor.aql.entities.tools.Tool}s, {@link edu.baylor.aql.entities.command.CommandAlgorithm}s,
 * {@link edu.baylor.aql.entities.operands.Operand}s, or any other objects that have some properties outside of this system that can be sampled.
 * The properties to be sampled could be size, speed of execution, time of execution, properties related to data structure, and any other measurable properties.
 * <p>
 * {@link edu.baylor.aql.entities.command.CommandAlgorithm} sampler uses data generated specifically for sampling purposes and contains sizes and volumes
 * that cover desired ranges.
 * <p>
 * Results of sampling is stored in the metadata to be used by other services.
 */
package edu.baylor.aql.services.optimization.sampling;

