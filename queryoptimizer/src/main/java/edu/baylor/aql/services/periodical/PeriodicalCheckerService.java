package edu.baylor.aql.services.periodical;


/**
 * This checker is used to perform necessary cleaning,
 * such as removing graphs that are marked for removal,
 * removing graphs that failed during creation and
 * any other services necessary for maintaining ACID
 * properties.
 * 
 * 
 * @author Rovshen Nazarov. Baylor University. Aug 25, 2014.All Rights Reserved.
 * 
 */
public final class PeriodicalCheckerService implements Runnable {

    /**
     * Empty constructor.
     */
    public PeriodicalCheckerService() {

    }

    /**
     * This class is required by the {@link Runnable} class.
     * At the moment this method does nothing.
     * This method represents an entry point for the checker service.
     */
    @Override
    public void run() {
        // TODO Auto-generated method stub

    }

}
