package edu.baylor.aql.services.loading;

import java.util.concurrent.CountDownLatch;

import org.apache.log4j.Logger;

import edu.baylor.aql.entities.Element;
import edu.baylor.aql.entities.command.CommandAlgorithm;
import edu.baylor.aql.entities.command.CommandType;
import edu.baylor.aql.entities.tools.Tool;
import edu.baylor.aql.services.requesthandler.pipeline.Visitor;
import edu.baylor.aql.util.AQLException;
import edu.baylor.aql.util.Util;

/**
 * This service loads into the AQL system provided {@link CommandAlgorithm}.
 * 
 * @author Rovshen Nazarov. Baylor University. Jan 14, 2015.All Rights Reserved.
 * 
 */
public final class CommandAlgorithmLoader extends Visitor {
    private static Logger logger = Logger
            .getLogger(CommandAlgorithmLoader.class.getName());
    private Element commandAlgo;
    private CountDownLatch doneSignal;

    @Override
    public Element call() throws Exception {

        // System.out.println("CommandAlgorithmLoader");
        if (!(commandAlgo instanceof CommandAlgorithm)) {
            doneSignal.countDown();
            return null;
        }
        CommandAlgorithm ca = null;
        try {
            ca = (CommandAlgorithm) commandAlgo;
            final StringBuilder sb = new StringBuilder();

            super.getDao().openConnection();
            // get tool id
            final Tool tool = super.getDao().lookUpToolByName(ca.getToolName());
            checkLoad(tool.getId(), sb, ca);
            // need to safe tool otherwise load false
            ca.setToolId(tool.getId()).setTool(tool);
            // get command type id
            final CommandType commandType = super.getDao()
                    .lookUpCommandTypeByName(ca.getCommandTypeName());
            checkLoad(commandType.getId(), sb, ca);
            ca.setCommandType(commandType);
            if (!ca.isErrState()) {
                storeCommandAlgo(ca);
            }
            super.getDao().closeConnection();
            if (Util.FAILED_STATUS == ca.getId()) {
                sb.append("Could not load the Command Algorithm: ")
                        .append(ca.getCommandTypeName())
                        .append(Util.LINE_SEPARATOR);
                logger.error(sb.toString());
                ca.setErrState(true);
            } else {
                sb.append("Generated Command Algorithm's id is: ").append(
                        ca.getId());
                logger.info(sb.toString());
            }
            commandAlgo.setStateMsg(sb.toString());
            // TODO
            // 1 check if the loaded command algorithm already exists (clue that
            // it exists is that there will be no path provided)

            // 2 if CA does not exists based on the command type, use
            // appropriate CA copyier

            // 3 verify that command algo executes normally

            if (ca.getCommandTypeName().equals("graphlab")) {
                @SuppressWarnings("unused")
                // TODO add tool specific CA loading
                final CommandTypeLoader graphLab = new CommandTypeLoader();
            }
        } finally {
            doneSignal.countDown();
        }
        return ca;
    }

    private void checkLoad(final Long loadStatus, final StringBuilder sb,
            final CommandAlgorithm ca) {
        if (loadStatus == Util.FAILED_STATUS) {
            sb.append("Could not load the Command Algorithm with name ")
                    .append(ca.getToolName());
            logger.error(sb.toString());
            ca.setErrState(true);
        }
    }

    @Override
    public void visit(final Element commandAlgo, final CountDownLatch doneSignal)
            throws InterruptedException {
        this.commandAlgo = commandAlgo;
        this.doneSignal = doneSignal;
    }

    /**
     * Load {@link Element} to the database.
     * This method stores in memory the object represented by the
     * {@link Element} to the metadata. It also saves in-memory auto generated
     * id for later references.
     * 
     * If the object could not be inserted, than the object id is set to
     * FAILED_STATUS.
     * 
     * @throws AQLException
     * 
     */
    private void storeCommandAlgo(final CommandAlgorithm ca)
            throws AQLException {
        ca.setId((long) Util.FAILED_STATUS);
        ca.setId((ca.insert(super.getDao())));
    }

}
