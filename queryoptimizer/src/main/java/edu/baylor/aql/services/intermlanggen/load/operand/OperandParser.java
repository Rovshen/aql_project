package edu.baylor.aql.services.intermlanggen.load.operand;

import edu.baylor.aql.entities.operands.Operand.OperandType;
import edu.baylor.aql.entities.query.IntermediateLanguageObject;

public interface OperandParser {

    /**
     * Process {@link IntermediateLanguageObject} and retrieve necessary
     * attributes for each {@link OperandType}.
     * 
     * @param ilo
     * @return
     */
    public IntermediateLanguageObject parse(IntermediateLanguageObject ilo);
}
