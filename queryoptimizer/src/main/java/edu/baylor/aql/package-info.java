/**
 * This is the top level package, please start exploring package structure from {@link edu.baylor.aql.dispatcher.AQLServerApp} class. 
 * <p>
 * The AQL server application is a framework for executing Big Data tools for unstructured data.
 * To analyze and understand package structure start with {@link edu.baylor.aql.dispatcher.AQLServerApp}.
 * 
 * @see {@link edu.baylor.aql.dispatcher.AQLServerApp}
 */
package edu.baylor.aql;

