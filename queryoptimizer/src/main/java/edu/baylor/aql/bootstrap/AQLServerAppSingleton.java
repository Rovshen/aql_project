package edu.baylor.aql.bootstrap;

import edu.baylor.aql.dispatcher.AQLServerApp;

/**
 * This class performs thread safe lazy initialization of the singleton instance
 * of the {@link AQLServerApp} class.
 * 
 * The {@link AQLServerApp} object initialization is based on
 * initialization-on-demand holder idiom.
 * 
 * @author Rovshen Nazarov. Baylor University. Feb 21, 2015.All Rights Reserved.
 * 
 */
public class AQLServerAppSingleton {

    /**
     * Use JVM class loader's static initialization synchronization. This
     * initialization provides free synchronization that is
     * performed by the JVM's loader.
     * 
     * JVM loader guarantees that all static initialization has completed before
     * the class is used.
     * 
     * @author Rovshen Nazarov. Baylor University. Feb 21, 2015.All Rights
     *         Reserved.
     * 
     */
    private static class InstanceInitWrapper {

        private static AQLServerApp SERVER_APP_INSTANCE = new AQLServerApp();
    }

    /**
     * Make constructor of this singleton service private, so nobody can
     * initialize it.
     */
    private AQLServerAppSingleton() {
    }

    /**
     * Use the private wrapper class to initiate the static instance of the
     * {@link AQLServerApp}.
     * 
     * Because we use {@link InstanceInitWrapper} only once within this method,
     * we are guaranteed that the {@link AQLServerApp} object is initialized
     * only once.
     * 
     * @return {@link AQLServerApp}
     */
    public static AQLServerApp getInstance() {

        return InstanceInitWrapper.SERVER_APP_INSTANCE;
    }
}
