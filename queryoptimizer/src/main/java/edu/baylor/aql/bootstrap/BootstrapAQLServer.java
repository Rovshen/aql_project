package edu.baylor.aql.bootstrap;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import edu.baylor.aql.dispatcher.AQLServerApp;

/**
 * This class starts the {@link AQLServerApp}, which in terms
 * starts processing of client requests.
 * 
 * @author Rovshen Nazarov. Baylor University. Oct 30, 2014.All Rights Reserved.
 * 
 */
public final class BootstrapAQLServer {

    static Logger logger = Logger.getLogger(BootstrapAQLServer.class.getName());

    public static void main(final String[] args) {

        final Map<String, String> options = new HashMap<>();

        System.out.println(Arrays.toString(args));

        if (args.length > 0) {
            getOptions(args, options);
        } else {
            System.out
                    .println("You can start AQL with options: "
                            + "<debug=true> <purgeOperationData=true> <purgeTools=true> <nullOptimization=true> <codeGenMode=(single|all)>");
        }

        final int poolSize = 10; // number of concurrent clients
        final AQLServerApp serverApp = AQLServerAppSingleton.getInstance();
        serverApp.setPoolSize(poolSize).getSettings().setOptions(options);
        final Thread aqlServer = new Thread(serverApp);
        aqlServer.start();
    }

    /**
     * Get and store command line options.
     * 
     * @param args
     * @param options
     */
    private static void getOptions(final String[] args,
            final Map<String, String> options) {

        final String ATT_VAL_SEP = "=";
        // we expect name value pair for AQL Server settings
        for (final String setting : args) {
            final String[] attVal = setting.split(ATT_VAL_SEP);
            if (attVal.length < 2) {
                throw new IllegalArgumentException("Expected: option=value");
            } else {
                options.put(attVal[0], attVal[1]);
            }
        }
        // displayDebug(options);
    }

    @SuppressWarnings("unused")
    private static void displayDebug(final Map<String, String> options) {
        for (final Map.Entry<String, String> entry : options.entrySet()) {
            System.out.println("att: " + entry.getKey() + "; val: "
                    + entry.getValue());
        }

    }
}
