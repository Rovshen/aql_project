package edu.baylor.aql.dispatcher;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import edu.baylor.aql.bootstrap.AQLServerAppSingleton;
import edu.baylor.aql.dataaccess.IMetadata;
import edu.baylor.aql.dataaccess.Metadata;
import edu.baylor.aql.entities.Element;
import edu.baylor.aql.entities.client.ClientSession;
import edu.baylor.aql.entities.command.Command;
import edu.baylor.aql.entities.operands.Operand;
import edu.baylor.aql.entities.query.IntermediateLanguageObject;
import edu.baylor.aql.entities.query.IntermediateLanguageObject.ParseObjType;
import edu.baylor.aql.entities.query.IntermediateLanguageObject.ParseRequestType;
import edu.baylor.aql.entities.query.Query;
import edu.baylor.aql.services.intermlanggen.RequestParser;
import edu.baylor.aql.services.query.IQueryService;
import edu.baylor.aql.services.query.QueryService;
import edu.baylor.aql.services.requesthandler.pipeline.EPipelineSingleton;
import edu.baylor.aql.services.requesthandler.pipeline.VisitorH;
import edu.baylor.aql.util.AQLException;
import edu.baylor.aql.util.Util;

/**
 * This class is a controller class responsible for processing user requests,
 * until the user requests a termination or until an error occurs on either user
 * side or serve side.
 * This controller invokes several services.
 * <p>
 * This controller is checking the client state {@link ClientSession} and if the
 * client is still connected, it invokes {@link RequestParser} service to try to
 * receive and parse client request.
 * <p>
 * If the parser parsed a request successfully, based on the
 * {@link ParseRequestType} this controller decides to invoke one of the request
 * processing pipelines defined with {@link EPipelineSingleton}.
 * <p>
 * For the pipe corresponding to the {@link ParseRequestType} this controller
 * invokes {@link VisitorH} handlers to invoke specific services that are needed
 * to fulfilling client request.
 * <p>
 * If the {@link ParseRequestType} is a {@link ParseRequestType#QUERY}, then
 * after successful completion of the request processing by the corresponding
 * {@link EPipelineSingleton}, this controller records {@link Query} results for
 * later reference and provides result information to the client via underlying
 * communication channel.
 * 
 * 
 * @author Rovshen Nazarov. Baylor University. Jan 25, 2015.All Rights Reserved.
 * 
 */
final class SessionManager implements Runnable {

    /**
     * Used to log errors or information.
     */
    private static Logger logger = Logger.getLogger(SessionManager.class
            .getName());
    /**
     * Number of threads for fixed pull executor.
     */
    private static final int POOL_SIZE = 10;
    private final ExecutorService execThreadPool;
    private final ClientSession clientSession;
    private final IMetadata dao;

    public SessionManager(final ClientSession clientSession) {
        this.clientSession = clientSession;
        execThreadPool = Executors.newFixedThreadPool(POOL_SIZE);
        dao = new Metadata();
    }

    /**
     * Read from the client until he or she exits or the stream is
     * terminated
     */
    @Override
    public void run() {

        @SuppressWarnings("unused")
        final AQLServerApp serverInstance = AQLServerAppSingleton.getInstance();

        try {
            while (clientSession.isConnected()) {
                try {
                    /** read from the client until client exits */
                    final RequestParser rp = new RequestParser(clientSession);
                    final Future<Object> resStat = execThreadPool.submit(rp);
                    // parsing may block while waiting for client messages
                    final IntermediateLanguageObject parseRes = getReqObj(resStat);
                    // check client state after parsing
                    if (!clientSession.isConnected()) {
                        continue; // skip processing
                    }
                    if (parseRes == null) {
                        continue; // skip processing
                    }
                    if (parseRes.getParseStateCode() == Util.FAILED_STATUS) {
                        clientSession.getChannel().writeAndFlush(
                                parseRes.getParseMsg().toString()
                                        + Util.LINE_SEPARATOR);
                        continue; // skip processing
                    }
                    // clear messages
                    parseRes.getParseMsg().setLength(0);
                    parseRes.getReqestObj().setStateMsg("");
                    if (ParseRequestType.LOAD.equals(parseRes.getParseType())) {

                        final Queue<VisitorH> loadPipe = EPipelineSingleton.LOAD
                                .getPipeLineInstance();
                        processPipe(parseRes, loadPipe);
                        clientSession.getChannel().writeAndFlush(
                                parseRes.getReqestObj().getStateMsg()
                                        + Util.LINE_SEPARATOR);
                    } else if (ParseRequestType.QUERY.equals(parseRes
                            .getParseType())) {

                        if (ParseObjType.COMPOUND_QUERY.equals(parseRes
                                .getObjType())) { // compound query

                            processCompoundQuery(parseRes);

                        } else { // simple query
                            final Queue<VisitorH> queryPipe = EPipelineSingleton.QUERY
                                    .getPipeLineInstance();
                            processSingleQuery(parseRes, queryPipe);
                            // Once query processed record it in metadata
                            recordQuery((Query) parseRes.getReqestObj());
                        }
                    }
                    logger.info(parseRes.getReqestObj().getStateMsg());
                } catch (final AQLException e) { // Recoverable Exception
                    logger.error("AQLException occured while processing client session."
                            + e.getMessage());
                    e.printStackTrace();
                    sendErrMsgToClient("Server experienced problems due to the following problem: "
                            + e.getMessage());
                } catch (final ExecutionException e) {
                    logger.error("Exception occured while processing client session."
                            + e.getMessage());
                    e.printStackTrace();
                    clientSession.getChannel().writeAndFlush(
                            "Expirienced unexpected server problems. Please try to resubmit your query. "
                                    + Util.LINE_SEPARATOR);
                }
            }// end while
            logger.info("Stopped processing client with id: "
                    + clientSession.getSessionId() + Util.LINE_SEPARATOR);
        } catch (final InterruptedException e) {
            logger.error("Interrupted while waiting for result."
                    + e.getMessage());
        } finally {
            final Util util = new Util();
            util.shutdownAndAwaitTermination(execThreadPool);
        }
    }

    private void sendErrMsgToClient(final String msg) {
        clientSession.getChannel().writeAndFlush(msg + Util.LINE_SEPARATOR);
    }

    /**
     * Process {@link Query} that consists of several comma separated
     * {@link Query}ies.
     * 
     * @param parseRes
     * @throws InterruptedException
     * @throws AQLException
     */
    private void processCompoundQuery(final IntermediateLanguageObject parseRes)
            throws InterruptedException, AQLException {

        final IQueryService qService = new QueryService();
        final Map<Long, Operand> operandsMap = new HashMap<>();
        final Query q = (Query) parseRes.getReqestObj();
        final List<Query> queries = linkedListToArrayList(q);

        for (final Query qNext : queries) {

            final Queue<VisitorH> queryPipe = EPipelineSingleton.QUERY
                    .getPipeLineInstance();
            parseRes.setReqestObj(qNext);
            if (qNext.getAliasName() == null && qNext.getNext() == null) {
                // this is the last Query, thus give it
                // alias objects if any
                if (!operandsMap.isEmpty()) {
                    // logger.info(operandsMap);
                    // for (final Long o : qNext.getOperands()) {
                    // logger.info(qNext.getOperMap().get(o));
                    // }

                    qService.replaceAliasWithOperands(qNext, operandsMap);
                }
            }
            processSingleQuery(parseRes, queryPipe);
            // Once query processed record it in metadata
            if (qNext.getAliasName() != null) {
                final Long o = recordQuery(qNext);
                if (o != null) {
                    qNext.getOperMap().get(o)
                            .setAliasName(qNext.getAliasName());
                    // logger.info("\n\n\n\n" + qNext.getOperMap().get(o));
                    operandsMap.put(
                            (long) qNext.getOperMap().get(o).hashCode(), qNext
                                    .getOperMap().get(o));
                }
            } else {
                recordQuery(qNext);
            }
        }
    }

    /**
     * 
     * @param q
     * @return
     */
    private List<Query> linkedListToArrayList(final Query q) {

        final List<Query> queries = new ArrayList<>();
        Query qN = q;

        do { // for each linked query
            queries.add(qN);
        } while ((qN = qN.getNext()) != null);

        for (final Query qNext : queries) {
            qNext.setNext(null); // break linked list
        }
        return queries;
    }

    /**
     * 
     * @param parseRes
     * @param queryPipe
     * @throws InterruptedException
     * @throws AQLException
     */
    private void processSingleQuery(final IntermediateLanguageObject parseRes,
            final Queue<VisitorH> queryPipe) throws InterruptedException,
            AQLException {

        final Query query = (Query) parseRes.getReqestObj();
        query.setClientSession(clientSession);
        processPipe(parseRes, queryPipe);
    }

    /**
     * Return result {@link Operand} if any.
     * 
     * @param query
     * @return
     * @throws AQLException
     */
    private Long recordQuery(final Query query) throws AQLException {

        final IQueryService qs = new QueryService();
        if (query.isErrState()) { // problem with query
            clientSession
                    .getChannel()
                    .writeAndFlush(
                            Util.LINE_SEPARATOR
                                    + "Problem processing query. There was an error during executing the query."
                                    + Util.LINE_SEPARATOR);
            return null;
        } else { // all good
            if (!query.isRepeated()) {
                final double progress = qs.computeProgress(query);
                query.setQueryProgress(progress);
                dao.openConnection();
                final long queryId = query.insert(dao);
                for (final Command command : query.getCommands()) {
                    dao.storeCommand(command, queryId, query);
                }
                dao.closeConnection();
            }
            clientSession.getChannel().writeAndFlush(
                    Util.LINE_SEPARATOR
                            + "Result of the query is: "
                            + query.getOperMap().get(query.getResultOperand())
                                    .getReadableId() + Util.LINE_SEPARATOR);
            return query.getResultOperand();
        }
    }

    /**
     * Process pipe with visitors visiting an {@link Element} object
     * horizontally and for each horizontal {@link VisitorH} vertically.
     * 
     * @param parseRes
     * @param processingPipe
     * @throws InterruptedException
     * @throws AQLException
     */
    private void processPipe(final IntermediateLanguageObject parseRes,
            final Queue<VisitorH> processingPipe) throws InterruptedException,
            AQLException {

        for (final VisitorH visitorH : processingPipe) {
            final int numbOfThreads = visitorH.getVisitorsVert().size();
            final CountDownLatch doneSignal = new CountDownLatch(
                    numbOfThreads + 1); // 1 for the horiz visit

            try {
                parseRes.getReqestObj().accept(visitorH, doneSignal);
                // wait for object to be processed
                if (!doneSignal.await(Util.MAX_WAITING_MIN, TimeUnit.MINUTES)) {
                    logger.error("Terminated wait for Handler "
                            + visitorH.getClass().getName());
                }
            } catch (final InterruptedException | AQLException e) {
                // clientSession.getChannel().writeAndFlush(
                // "Invalid AQL request: " + e.getMessage()
                // + Util.LINE_SEPARATOR);
                // logger.error(e.toString());
                e.printStackTrace();
                throw new AQLException(e.getMessage());
            }
        }
    }

    private IntermediateLanguageObject getReqObj(final Future<Object> resStat)
            throws AQLException, InterruptedException, ExecutionException {
        // blocks here, until gets result
        final Object parseResObj = resStat.get();
        IntermediateLanguageObject parseRes = new IntermediateLanguageObject();
        if (parseResObj instanceof IntermediateLanguageObject) {
            parseRes = (IntermediateLanguageObject) parseResObj;
        } else {
            logger.error("Wrone parse result obj.");
        }
        return parseRes;
    }
}
