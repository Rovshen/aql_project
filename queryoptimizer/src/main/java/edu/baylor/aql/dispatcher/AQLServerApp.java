/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.baylor.aql.dispatcher;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Queue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import edu.baylor.aql.dataaccess.IMetadata;
import edu.baylor.aql.dataaccess.Metadata;
import edu.baylor.aql.entities.client.ClientSession;
import edu.baylor.aql.entities.command.CommandAlgorithm;
import edu.baylor.aql.entities.operands.Operand;
import edu.baylor.aql.entities.query.Query;
import edu.baylor.aql.entities.tools.Tool;
import edu.baylor.aql.persistance.communication.NettyServerBootstrap;
import edu.baylor.aql.persistance.communication.NettyStringServerInBoundHandler;
import edu.baylor.aql.services.intermlanggen.RequestParser;
import edu.baylor.aql.services.loading.CommandAlgorithmLoader;
import edu.baylor.aql.services.loading.CommandTypeLoader;
import edu.baylor.aql.services.loading.OperandLoader;
import edu.baylor.aql.services.loading.ToolLoader;
import edu.baylor.aql.services.optimization.estimating.EstOperandEstimator;
import edu.baylor.aql.services.optimization.estimating.EstimatorHandler;
import edu.baylor.aql.services.optimization.estimating.GraphEstimator;
import edu.baylor.aql.services.optimization.estimating.TableEstimator;
import edu.baylor.aql.services.optimization.estimating.UnstructuredEstimator;
import edu.baylor.aql.services.optimization.evaluating.EvaluatorHandler;
import edu.baylor.aql.services.optimization.evaluating.SimpleEvaluator;
import edu.baylor.aql.services.optimization.ordering.OrderingHandler;
import edu.baylor.aql.services.optimization.ordering.OrderingPartial;
import edu.baylor.aql.services.optimization.ordering.OrderingTyped;
import edu.baylor.aql.services.optimization.repeatedquery.RepeatedCommandsEvaluator;
import edu.baylor.aql.services.periodical.IOldRecordPurge;
import edu.baylor.aql.services.periodical.OldRecordPurge;
import edu.baylor.aql.services.query.QueryGeneratorCommandAlgorithmsAll;
import edu.baylor.aql.services.query.QueryGeneratorHandler;
import edu.baylor.aql.services.requesthandler.handlers.LoadHandler;
import edu.baylor.aql.services.requesthandler.handlers.QueryHandler;
import edu.baylor.aql.services.requesthandler.pipeline.EPipelineSingleton;
import edu.baylor.aql.services.requesthandler.pipeline.Visitor;
import edu.baylor.aql.services.requesthandler.pipeline.VisitorH;
import edu.baylor.aql.util.AQLException;
import edu.baylor.aql.util.Util;

/**
 * This class is the starting point for the AQL Query Execution module. It
 * performs necessary initializations, and configurations.
 * <p>
 * When the client connects a {@link ClientSession} of that client is passed to
 * a {@link SessionManager} dedicated to that client. The {@link SessionManager}
 * parses client requests and engages necessary services to fulfill those
 * requests.
 * <p>
 * This class loads system-settings, sets-up global logger system based on the
 * XML configurations. To do such initialization this class requires that both
 * system-settings and logger configuration files are located in the same
 * directory as the ALQ Query Execution Server jar file, and be placed under
 * config folder.
 * <p>
 * Communication with clients is maintained using a {@link Socket}s and
 * maintained by the {@link NettyStringServerInBoundHandler}.
 * 
 * The license for this project shall be Apache License, v2.0 since we use Netty
 * and its license requires that.
 * 
 * TODO State specifically for how long objects for entities and services live.
 * TODO Expand JUnit test coverage.
 * 
 * @author Rovshen Nazarov. Baylor University. Aug 25, 2014.All Rights Reserved.
 * 
 */
public final class AQLServerApp implements Runnable {

    /**
     * Placeholder class for the settings.
     * 
     * @author Rovshen Nazarov. Baylor University. Feb 21, 2015.All Rights
     *         Reserved.
     * 
     */
    public static class Settings {

        public enum CodeGenMode {
            SINGLE("single"), ALL("all");

            private final String label;

            /**
             * Constructor to set the label of this enumeration object.
             * 
             * @param label
             *            The label that we want to set for this
             *            instance of the ImplementationType.
             */
            CodeGenMode(final String label) {
                this.label = label;
            }

            public String getLabel() {
                return label;
            }

            @Override
            public String toString() {
                return label;
            }

            /**
             * Return CodeGenMode object given the String code. Note that
             * the String code must be a member of the enumeration If the
             * code is not part of predefined enumeration null is returned.
             * 
             * @param code
             *            String code that is defined for this enum type
             * @return type The type based on the String code. if String
             *         code unknown return null.
             */
            public static CodeGenMode getEnumByString(final String code) {
                for (final CodeGenMode type : CodeGenMode.values()) {
                    if (type.getLabel().equals(code)) {
                        return type;
                    }
                }
                return null;
            }
        }

        /**
         * File descriptors for recording debug code.
         */
        private final List<File> tempFiles = new ArrayList<>();
        /**
         * If true additional logging is turned on and Code Execution is
         * disabled.
         */
        private boolean debugMode;
        /**
         * If true all generated during execution {@link Operand}s and related
         * to
         * those records are removed from
         * the metadata using {@link IMetadata} and from the backing store.
         */
        private boolean purgeOperationData;
        /**
         * If true all {@link Tool}s related settings and records are removed
         * from
         * the metadata using {@link IMetadata} and from the backing store.
         */
        private boolean purgeTools;
        /**
         * Default is false, if set to true
         * no optimization performed and result is generated based on the
         * {@link Query} with default {@link CommandAlgorithm}s.
         */
        private boolean nullOptimization;
        /**
         * Two modes are supported: single and all. Note, this option is only
         * valid
         * if the the {@link AQLServerApp#debugMode} is true. If single mode is
         * selected, then only one code will be generated per {@link Query}. If
         * all
         * is selected, then all possible code for a given {@link Query} will be
         * generated. Note in both cases the debug mode must be true for this
         * option
         * to be considered.
         */
        private CodeGenMode codeGenMode;

        /**
         * @return the tempFiles
         */
        public final List<File> getTempFiles() {
            return tempFiles;
        }

        /**
         * @param options
         *            the options to set
         */
        public final void setOptions(final Map<String, String> options) {

            if (options.size() > 0) {
                // ./runAQLServer.sh debug=true purgeOperationData=true
                // purgeTools=true codeGenMode=all
                debugMode = Boolean.parseBoolean(options.get("debug"));
                purgeOperationData = Boolean.parseBoolean(options
                        .get("purgeOperationData"));
                purgeTools = Boolean.parseBoolean(options.get("purgeTools"));
                codeGenMode = CodeGenMode.getEnumByString(options
                        .get("codeGenMode")); // single/all
                nullOptimization = Boolean.parseBoolean(options
                        .get("nullOptimization"));
            } else {
                debugMode = false;
                purgeOperationData = false;
                purgeTools = false;
                nullOptimization = false;
                codeGenMode = CodeGenMode.SINGLE; // single is default
            }
            logger.info("settings debugMode: " + debugMode);
            logger.info("settings purgeOperationData: " + purgeOperationData);
            logger.info("settings purgeTools: " + purgeTools);
            logger.info("settings nullOptimization: " + nullOptimization);
            logger.info("settings codeGenMode: " + codeGenMode);
        }

        /**
         * @return the debugMode
         */
        public final boolean isDebugMode() {
            return debugMode;
        }

        /**
         * @return the purgeGeneratedData
         */
        public final boolean isPurgeOperationData() {
            return purgeOperationData;
        }

        /**
         * @return the purgeTools
         */
        public final boolean isPurgeTools() {
            return purgeTools;
        }

        /**
         * @return the nullOptimization
         */
        public final boolean isNullOptimization() {
            return nullOptimization;
        }

        /**
         * @param nullOptimization
         *            the nullOptimization to set
         */
        public final void setNullOptimization(final boolean nullOptimization) {
            this.nullOptimization = nullOptimization;
        }

        /**
         * @return the codeGenMode
         */
        public final CodeGenMode getCodeGenMode() {
            return codeGenMode;
        }
    }

    static Logger logger = Logger.getLogger(AQLServerApp.class.getName());
    /**
     * This service is used to process each new client.
     */
    private ExecutorService threadPool;
    private final IMetadata dao;
    @SuppressWarnings("unused")
    /**
     * Used to perform check points for all client sessions
     * to clear metadata if needed and re-run some operations if needed on restart.
     */
    private final ScheduledExecutorService cronJobs; // for check points
    /**
     * The {@link File} object that the in memory property will be saved to.
     */
    private File propFile;
    /**
     * Number of threads to be used by this controller class.
     */
    private int poolSize;

    private final Settings settings = new Settings();

    /**
     * The constructor initializes logger using predefined configuration for
     * output destination and related formats.
     * 
     * @param poolSize
     * @param options
     *            The AQL server configuration options, such as debug mode, etc.
     */
    public AQLServerApp() {
        // configure logger from xml conf file
        DOMConfigurator.configure("./config/log4j-config.xml");
        cronJobs = Executors.newSingleThreadScheduledExecutor();
        dao = new Metadata();
    }

    /**
     * @return the settings
     */
    public final Settings getSettings() {
        return settings;
    }

    /**
     * @return the poolSize
     */
    public final int getPoolSize() {
        return poolSize;
    }

    /**
     * @param poolSize
     *            the poolSize to set
     * @return
     */
    public final AQLServerApp setPoolSize(final int poolSize) {
        this.poolSize = poolSize;
        threadPool = Executors.newFixedThreadPool(poolSize);
        return this;
    }

    /**
     * Perform initializations and loadings.
     * NOTE: Ordering within pipes matters.
     */
    private void init() {
        // populate Query pipeline vertically

        // generate many queries for each command algorithm
        final VisitorH genQueryH = new QueryGeneratorHandler();
        // Generate queries for all command algorithms
        final Visitor genQuerVP = new QueryGeneratorCommandAlgorithmsAll();
        genQueryH.addVisitorsVert(genQuerVP);

        // generate many orderings, default single
        final VisitorH ordH = new OrderingHandler();
        // default ordering
        final Visitor ordVP = new OrderingPartial();
        // command type such as associative based ordering
        final Visitor ordVTyped = new OrderingTyped();
        ordH.addVisitorsVert(ordVP);
        ordH.addVisitorsVert(ordVTyped);
        // perform different estimations for unknown values
        final VisitorH estH = new EstimatorHandler();
        // specific estimators
        final Visitor estEstOp = new EstOperandEstimator();
        final Visitor estGraph = new GraphEstimator();
        final Visitor estTable = new TableEstimator();
        final Visitor estUnstr = new UnstructuredEstimator();
        estH.addVisitorsVert(estEstOp);
        estH.addVisitorsVert(estGraph);
        estH.addVisitorsVert(estTable);
        estH.addVisitorsVert(estUnstr);
        // perform evaluation of different orderings
        final VisitorH evalH = new EvaluatorHandler();
        // simple evaluator, just use the single ordering given
        final Visitor evalS = new SimpleEvaluator();
        // check if query is already computed
        final Visitor evalRepeatVC = new RepeatedCommandsEvaluator();
        evalH.addVisitorsVert(evalS);
        evalH.addVisitorsVert(evalRepeatVC);
        // no vertical visitors yet
        final VisitorH qProcessH = new QueryHandler();

        // populate Query horizontally
        EPipelineSingleton.QUERY.addToPipeLineH(genQueryH);
        EPipelineSingleton.QUERY.addToPipeLineH(ordH);
        EPipelineSingleton.QUERY.addToPipeLineH(estH);
        EPipelineSingleton.QUERY.addToPipeLineH(evalH);
        EPipelineSingleton.QUERY.addToPipeLineH(qProcessH);

        // populate Load pipeline vertically
        final VisitorH loadH = new LoadHandler();
        final Visitor loadT = new ToolLoader();
        final Visitor loadCA = new CommandAlgorithmLoader();
        final Visitor loadCT = new CommandTypeLoader();
        final Visitor loadO = new OperandLoader();
        loadH.addVisitorsVert(loadT);
        loadH.addVisitorsVert(loadCA);
        loadH.addVisitorsVert(loadCT);
        loadH.addVisitorsVert(loadO);

        // populate Load pipeline horizontally
        EPipelineSingleton.LOAD.addToPipeLineH(loadH);
    }

    /**
     * This is an entry point for the AQL server This method deals
     * with the incoming connections and delegates them to the
     * {@link RequestParser} for further processing.
     */
    @Override
    public final void run() {

        final Util util = new Util();
        final int port = 8010; // TODO port should be read from settings.
        SessionManager csManager;
        final NettyServerBootstrap serverBootstrap = new NettyServerBootstrap(
                port);
        serverBootstrap.run();
        final List<ClientSession> startedClientSessions = new ArrayList<>();

        try {
            init(); // do any needed initialization
            cleanRecords(); // clean any data based on start settings

            while (!Thread.interrupted()) {
                /**
                 * for each {@link ClientSession} start
                 * {@link ClientSessionManager} if not already started
                 */
                if (!serverBootstrap.getClientSessions().isEmpty()) {
                    for (final ClientSession clientSession : serverBootstrap
                            .getClientSessions()) {
                        if (!startedClientSessions.contains(clientSession)) {
                            startedClientSessions.add(clientSession);
                            csManager = new SessionManager(clientSession);
                            threadPool.execute(csManager);
                        }
                    }
                    /**
                     * Synchronize started client sessions and active client
                     * sessions lists.
                     */
                    startedClientSessions.retainAll(serverBootstrap
                            .getClientSessions());
                }
            }
        } catch (final AQLException e) {
            e.printStackTrace();
        } finally { // terminate threads and close pipe reader
            logger.info("Completed Reading from the network ");
            serverBootstrap.terminateServer();
            stopPipeLine(EPipelineSingleton.QUERY.getPipeLineInstance());
            stopPipeLine(EPipelineSingleton.LOAD.getPipeLineInstance());
            util.shutdownAndAwaitTermination(threadPool);
        }
    }

    /**
     * Clean old records from HDFS.
     * 
     * @throws AQLException
     */
    private void cleanRecords() throws AQLException {

        final IOldRecordPurge recordPurgeService = new OldRecordPurge();

        dao.openConnection();
        final String resPathDir = dao.lookUpSystemSetting("resultPath", "hdfs");
        final String loadPathDir = dao.lookUpSystemSetting("loadPath", "hdfs");
        final String toolPathDir = dao.lookUpSystemSetting("toolLoadPath",
                "hdfs");

        if (getSettings().isPurgeOperationData()) {
            logger.info("Purging operation data.");
            cleanResultOperandRecords(recordPurgeService, resPathDir,
                    loadPathDir);
        }

        if (getSettings().isPurgeTools()) {
            logger.info("Purging tools");
            cleanToolRecords(recordPurgeService, toolPathDir);
        }
        dao.closeConnection();
    }

    /**
     * Removes from HDFS
     * 
     * Tools
     * 
     * Removes from Metadata
     * 
     * Tools
     * CommandAlgorithms
     * CommandTypes
     * 
     * @param recordPurgeService
     * @param toolPathDir
     */
    private void cleanToolRecords(final IOldRecordPurge recordPurgeService,
            final String toolPathDir) {

        // remove from metadata
        dao.removeAllLoadRecordsTools();
        dao.removeAllLoadRecordsCommandAlgos();
        dao.removeAllLoadRecordsCommandTypes();

        // remove from backing storage
        final boolean isToolPurged = recordPurgeService
                .removeAllInFolderRecords(toolPathDir);
        if (!isToolPurged) {
            logger.error("Could not clear previous records tools.");
        } else {
            logger.info("Successfully cleared:" + toolPathDir);
        }
    }

    private void cleanResultOperandRecords(
            final IOldRecordPurge recordPurgeService, final String resPathDir,
            final String loadPathDir) {

        // remove from metadata
        dao.removeAllQueryExecutionRecords();

        // remove from backing storage
        final boolean isLoadPurged = recordPurgeService
                .removeAllInFolderRecords(loadPathDir);
        if (!isLoadPurged) {
            logger.error("Could not clear previous records loaded data.");
        } else {
            logger.info("Successfully cleared:" + loadPathDir);
        }

        // remove from backing storage
        final boolean isResPurged = recordPurgeService
                .removeAllInFolderRecords(resPathDir);

        if (!isResPurged) {
            logger.error("Could not clear previous records generated result Operands.");
        } else {
            logger.info("Successfully cleared:" + resPathDir);
        }
    }

    /**
     * 
     * @param processingPipe
     */
    private void stopPipeLine(final Queue<VisitorH> processingPipe) {

        settings.getTempFiles().clear();
        for (final VisitorH visitorH : processingPipe) {
            final Util util = new Util();
            util.shutdownAndAwaitTermination(visitorH.getExecThreadPool());
        }
    }

    /**
     * This method loads property file in memory. The {@link Properties} object
     * holds client session ids. If the property file could not be found, this
     * method creates a new property file with pre-defined name.
     * 
     * @param fileName
     * @param prop
     */
    @SuppressWarnings("unused")
    @Deprecated
    private void loadProperty(final String fileName, final Properties prop) {
        propFile = new File(fileName);
        try (InputStream instrPropStream = new FileInputStream(propFile)) {
            if (propFile.exists()) {

                prop.loadFromXML(instrPropStream);
                // prop.list(System.out);
            } else {
                propFile.createNewFile();
            }
        } catch (final IOException e) {
            logger.error("Could not load into memory property file.");
            e.printStackTrace();
        }
    }

    /**
     * Update property file. This method stores in memory {@link Properties}
     * object into the pre-defined property file.
     * 
     * @param prop
     */
    @SuppressWarnings("unused")
    @Deprecated
    private void updateProperty(final Properties prop) {
        try (OutputStream outputPropStream = new FileOutputStream(propFile)) {
            prop.storeToXML(outputPropStream, null); // save property to a file
        } catch (final IOException e) {
            logger.error("Could not update property file.");
            e.printStackTrace();
        }
    }
}
