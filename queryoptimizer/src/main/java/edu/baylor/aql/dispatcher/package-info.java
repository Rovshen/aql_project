/**
 * This package contains two main service classes {@link edu.baylor.aql.dispatcher.AQLServerApp} and 
 * {@link edu.baylor.aql.dispatcher.SessionManager}. First performs system level management, 
 * second performs client level management. 
 * <p>
 * The first one, {@link edu.baylor.aql.dispatcher.AQLServerApp} is used for initiating 
 * AQL Query Execution Server, and accepting and dispatching incoming interprocess requests. 
 * It also used to start asynchronous {@link java.net.Socket} server Netty for performing client-server communication.
 * <p>
 * The second, {@link edu.baylor.aql.dispatcher.SessionManager} is used for processing client session, where some data 
 * related to the state of the session is stored into {@link edu.baylor.aql.entities.client.ClientSession} object.
 * This service performs main functionalities of the AQL Query execution system. It uses {@link edu.baylor.aql.services.requesthandler.pipeline.EPipelineSingleton}
 * to invoke pipeline of services related to a particular request type defined by {@link edu.baylor.aql.entities.query.IntermediateLanguageObject.ParseRequestType}.
 */
package edu.baylor.aql.dispatcher;