/**
 * This package is designed to contain a helper services that are expected to be used across different modules of this system.
 * The services must be general enough, and should not have any state, or depend on any state or any events.
 * <p>
 * This package also contains {@link edu.baylor.aql.util.AQLException}, which is this system specific exception.
 */
package edu.baylor.aql.util;

