package edu.baylor.aql.util;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

/**
 * This general service class contains general methods that may be
 * used by different controllers and/or services.
 * 
 * Any method in this class must be general.
 * 
 * @author Rovshen Nazarov. Baylor University. Aug 25, 2014.All Rights Reserved.
 * 
 */
public final class Util {

    public static class ResultState {
        final public StringBuilder msg = new StringBuilder();
        public int status;
    }

    @SuppressWarnings("unused")
    private static Logger logger = Logger.getLogger(Util.class.getName());
    // 3 min in millisec
    public static final int MAX_WAITING_MIN = 1;
    public static final int FAILED_STATUS = -1;
    public static final int SUCCESS_STATUS = 0;
    public static final String WOKRING_DIR = System.getProperty("user.dir");
    /**
     * System specific new line delimiter.
     */
    public static final String LINE_SEPARATOR = System.lineSeparator();

    /**
     * Helper function to get current time.
     * 
     * @return Date in a String, e.g. 2014/06/26 16:07:18
     */
    public static synchronized String getCurrDate() {
        final DateFormat dateFormat = new SimpleDateFormat(
                "yyyy/MM/dd HH:mm:ss");
        final Date date = new Date();
        if (dateFormat == null || date == null)
            return null;
        return dateFormat.format(date);
    }

    /**
     * This method was demonstrated on Oracle website for ExecutorService
     * interface documentation. The code snippet used
     * as was presented on the web page.
     * http://docs.oracle.com/javase/7/docs/api/java/util/concurrent/
     * ExecutorService.html
     * 
     * @param pool
     */
    public void shutdownAndAwaitTermination(final ExecutorService pool) {

        pool.shutdown(); // Disable new tasks from being submitted
        try {
            // Wait a while for existing tasks to terminate
            boolean termStat = false;
            while (!termStat) { // wait until all queued services are terminated
                termStat = pool.awaitTermination(60, TimeUnit.SECONDS);
                // TODO uncomment for debugging
                // logger.info("Pool is still running");
            }
            // TODO uncomment for debugging
            // logger.info("Pool terminated succesfully.");

        } catch (final InterruptedException ie) {
            // (Re-)Cancel if current thread also interrupted
            pool.shutdownNow();
        }
    }

    /**
     * Read content of a file and return {@link String} representation of the
     * content using {@link Charset} provided.
     * 
     * @param pathToFile
     * @param fileEncoding
     * @return
     * @throws IOException
     */
    public String readFileContentAsString(final String pathToFile,
            final Charset fileEncoding) throws IOException {
        String resStr = "";
        final byte[] encodedContent = Files.readAllBytes(Paths.get(pathToFile));
        resStr = new String(encodedContent, fileEncoding);
        return resStr;
    }

    /**
     * Get current date value
     * 
     * @return String of the current date in format yyyy-MM-dd-HH-mm-ss
     */
    public static String getCurrDateAsStringId() {

        final DateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd-HH-mm-ss");
        final Date date = new Date();
        if (dateFormat == null || date == null) {
            return "";
        }
        final String dateStr = dateFormat.format(date);
        return dateStr;
    }

    public static List<Object> waitForAllThreadsToTerminate(
            final List<Future<Object>> resFutList) throws InterruptedException {

        final List<Object> results = new ArrayList<>();

        for (final Future<Object> future : resFutList) {
            try {
                final Object res = future.get();
                results.add(res);
            } catch (final ExecutionException e) {
                e.printStackTrace();
            }
        }
        return results;
    }
}
