package edu.baylor.aql.util;

/**
 * AQL exception. This is a general exception designed to give a user reasons
 * for request processing failure.
 * 
 * @author Rovshen Nazarov. Baylor University. Dec 4, 2014.All Rights Reserved.
 * 
 */
public final class AQLException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = 1802197599292336373L;

    public AQLException() {
    }

    public AQLException(final String msg) {
        super(msg);
    }

    public AQLException(final Throwable arg0) {
        super(arg0);
    }

    public AQLException(final String arg0, final Throwable arg1) {
        super(arg0, arg1);
    }

    public AQLException(final String arg0, final Throwable arg1,
            final boolean arg2, final boolean arg3) {
        super(arg0, arg1, arg2, arg3);
    }

}
