package edu.baylor.aql.util.hdfds;

import java.util.Queue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

import org.apache.log4j.Logger;

import edu.baylor.aql.entities.command.Command;
import edu.baylor.aql.entities.operands.Operand;
import edu.baylor.aql.services.commandexecutor.AbstractExecutor;
import edu.baylor.aql.services.commandexecutor.BashExecutor;
import edu.baylor.aql.services.intermlanggen.UtilParser;
import edu.baylor.aql.util.Util;

/**
 * This class performs HDFS system related operations,
 * such as checking if file exists, writing file to HDFS,
 * executing an HDFS command, etc.
 * 
 * @author Rovshen Nazarov. Baylor University. Aug 25, 2014.All Rights Reserved.
 * 
 */
public final class HDFSService {

    private static Logger logger = Logger
            .getLogger(HDFSService.class.getName());
    /**
     * Thread pool used to execute HDFS related services.
     * The thread pool is initialized externally and passed to this service for
     * use.
     */
    private final ExecutorService execThreadPool;
    private final static String hdfsSpace = "_";

    /**
     * Constructor that takes initialized thread pool to
     * use for HDFS services.
     * 
     * @param execThreadPool
     *            Thread pool passed by the creator of this object.
     */
    public HDFSService(final ExecutorService execThreadPool) {
        this.execThreadPool = execThreadPool;
    }

    /**
     * Check if file exists on HDFS Hadoop MapReduce related file system
     * This method uses hadoop filesystem listing command using the file path
     * provided.
     * 
     * The HDFS command is executed using {@link BashExecutor} and based on its
     * result the appropriate boolean value returned to the caller.
     * 
     * @param filePathGraph
     *            String the name of the file/folder to check for on HDFS.
     * 
     * @return status true file exists, false file does not exist on HDFS.
     */
    public boolean checkIfFileExistsHDFS(final String filePathGraph) {

        if (filePathGraph == null || execThreadPool == null) {
            logger.error("The file to wait for or its path must be not NULL");
            return false;
        }
        int result = Util.FAILED_STATUS;
        final StringBuilder commandHDFS = new StringBuilder();
        final AbstractExecutor execCommand;
        commandHDFS.append("hadoop fs -ls ").append(filePathGraph);
        final Command command = new Command();
        command.setCommand(commandHDFS.toString());
        execCommand = new BashExecutor(command.getCommand());
        final Future<Object> resultFuture = execThreadPool.submit(execCommand);

        try {
            result = (int) resultFuture.get();// blocks here until receives
                                              // result
        } catch (InterruptedException | ExecutionException e) {
            logger.error("Could not get result of the Callable thread."
                    + e.getMessage());
        }
        if (result != 0) {
            logger.info("File does not exist " + commandHDFS + "\nResult "
                    + result);
            return false;
        }
        return true;
    }

    /**
     * Execute HDFS Command.
     * This method executes HDFS command provided by the caller
     * using {@link BashExecutor} and returns its result to
     * the caller.
     * 
     * @param commandHDFS
     *            String HDFS command to execute. This command is
     *            expected to be executed on Linux environment.
     * @return status: 0 ok, -1 fail
     */
    public int executeHDFSCommand(final String commandHDFS) {

        int resultSuccess = Util.FAILED_STATUS;
        final Command command = new Command();
        command.setCommand(commandHDFS.toString());
        final AbstractExecutor execCommandAfterWait = new BashExecutor(
                command.getCommand());
        final Future<Object> future = execThreadPool
                .submit(execCommandAfterWait);

        try {
            resultSuccess = (int) future.get();// get blocks here until receives
                                               // the result.
        } catch (InterruptedException | ExecutionException e) {
            logger.error("Could not get result of the Callable thread."
                    + e.getMessage());
        }
        return resultSuccess;
    }

    /**
     * Creates a file on HDFS using Linux like touch command.
     * This method uses {@link HDFSService#executeHDFSCommand(String)} method
     * of this service to execute Hadoop touch command that creates a file
     * if it does not exists and is similar to Linux touch command.
     * 
     * @param fileName
     *            String the name of the file to create on HDFS.
     */
    public void createFileOnHDFS(final String fileName) {
        final String commandHDFS = "hadoop fs -touchz " + fileName;
        final int resultSuccess = executeHDFSCommand(commandHDFS);
        if (resultSuccess != 0) {
            logger.error("Could not create " + fileName + " file on hdfs.");
        }
    }

    /**
     * Create query folder on HDFS. To create query path hdfs load path is used,
     * which represents the default load path
     * defined in system wide AQL settings and a sub folder for the current
     * client session represented by the query id.
     * 
     * @param commandHDFS
     *            {@link StringBuilder} object used to construct HDFS command.
     * @param hdfsLoadPath
     *            This {@link String} object represents the path, where
     *            the directory should be created on HDFS
     *            and this path represents default root path concatenated with
     *            query id.
     * @return int result of the executed HDFS command. Default value is the
     *         FAILED_STATUS
     */
    public int createDirOnHDFS(final String hdfsLoadPath) {

        int resultSuccess = Util.FAILED_STATUS;
        final String commandHDFS = "hadoop fs -mkdir " + hdfsLoadPath;
        resultSuccess = executeHDFSCommand(commandHDFS);

        return resultSuccess;
    }

    /**
     * Copy file or directory to HDFS. This method copies the graph that the
     * client wants to load from the initial path
     * to the HDFS load path.
     * 
     * @param commandHDFS
     *            {@link StringBuilder} object used to construct HDFS command.
     * @param hdfsLoadPath
     *            This {@link String} object represents the path, where
     *            the directory should be created on HDFS
     *            and this path represents default root path concatenated with
     *            query id.
     * @return int result of the executed HDFS command. Default value is the
     *         FAILED_STATUS
     */
    public int copyToHDFS(final String initPath, final String hdfsLoadPath) {

        /**
         * HDFS requires underscore instead of space.
         */
        final String initPathHDFSSpace = initPath.replaceAll(" ", hdfsSpace);
        int resultSuccess = Util.FAILED_STATUS;
        final String commandHDFS = "hadoop fs -copyFromLocal "
                + initPathHDFSSpace + " " + hdfsLoadPath;
        resultSuccess = executeHDFSCommand(commandHDFS);

        return resultSuccess;
    }

    /**
     * Check file was copied to HDFS. This method verifies if the graph being
     * loaded was copied properly
     * from the original file system to the HDFS.
     * The verification uses graph name to check if the graph was copied, if the
     * graph is a directory
     * we use directory name as the graph name.
     * 
     * TODO if the file is already in HDFS shall we copy it or leave it where it
     * is?
     * 
     * @param commandHDFS
     *            {@link StringBuilder} object used to construct HDFS command.
     * @param hdfsLoadPath
     *            This {@link String} object represents the path, where
     *            the directory should be created on HDFS
     *            and this path represents default root path concatenated with
     *            query id.
     * @return 0 or FAILED_STATUS, which is the value other than 0. 0 represents
     *         success.
     */
    public long verifyFileExistsOnHDFS(final Operand loadedOperand,
            final String hdfsLoadPath) {
        // build Operand load path that we will use later.
        final String commandHDFS = hdfsLoadPath
                + UtilParser.FOLDER_SEPARATOR_LINUX
                + loadedOperand.getOperandName();
        final boolean fileExists = checkIfFileExistsHDFS(commandHDFS);

        if (!fileExists) {
            logger.error("Could not load graph to hfds "
                    + commandHDFS.toString());
            return Util.FAILED_STATUS;
        } else {
            loadedOperand.setLoadedPath(commandHDFS.toString());
            return Util.SUCCESS_STATUS;
        }
    }

    /**
     * DEPRECATED, now database is used to check {@link Operand}'s statuses.
     * 
     * Wait for commands generating files (Operands) to finish. Once files
     * generated
     * check if the command has necessary files (Operands) to be executed.
     * 
     * @param commandsToExecute
     * @param command
     * @param successFilePathGraph1
     * @param successFilePathGraph2
     * @param execThreadPool
     * @throws InterruptedException
     */
    @SuppressWarnings("unused")
    @Deprecated
    private void waitForCommandsToFinishHDFS(
            final Queue<String> commandsToExecute, final String command,
            final String successFilePathGraph1,
            final String successFilePathGraph2,
            final ExecutorService execThreadPool) throws InterruptedException {

        boolean graph1Exists = false;
        boolean graph2Exists = false;
        final int waitTimeMls = 5000; // 5 sec
        while (!(graph1Exists && graph2Exists)) {
            synchronized (commandsToExecute) {
                commandsToExecute.wait(waitTimeMls);
            }
            graph1Exists = checkIfFileExistsHDFS(successFilePathGraph1);
            graph2Exists = checkIfFileExistsHDFS(successFilePathGraph2);

            if (graph1Exists && graph2Exists) {
                synchronized (commandsToExecute) {
                    commandsToExecute.offer(command);
                }
            }
        }
    }
}
