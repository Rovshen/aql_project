package edu.baylor.aql.dataaccess;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

import org.apache.log4j.Logger;

import com.google.common.base.Joiner;

import edu.baylor.aql.bootstrap.AQLServerAppSingleton;
import edu.baylor.aql.entities.client.ClientSession;
import edu.baylor.aql.entities.command.Command;
import edu.baylor.aql.entities.command.CommandAlgorithm;
import edu.baylor.aql.entities.command.CommandType;
import edu.baylor.aql.entities.command.CommandType.OrderingType;
import edu.baylor.aql.entities.operands.ComputedOperand;
import edu.baylor.aql.entities.operands.Graph;
import edu.baylor.aql.entities.operands.Operand;
import edu.baylor.aql.entities.operands.Table;
import edu.baylor.aql.entities.operands.Unstructured;
import edu.baylor.aql.entities.query.Query;
import edu.baylor.aql.entities.tools.Tool;
import edu.baylor.aql.util.AQLException;
import edu.baylor.aql.util.Util;

//Notice, do not import com.mysql.jdbc.*
//or you will have problems!

/**
 * This service class provides access to metadata. Use this service to retrieve
 * metadata from database into objects and
 * to store metadata objects into database.
 * 
 * If you use this service use "openConnection" method right before you retrieve
 * the needed metdata objects and use
 * "closeDBConnection" method immediately after you have retrieved all necessary
 * objects.
 * 
 * The database connection should be used per thread. One thread may have many
 * statements to execute.
 * 
 * TODO SECURITY: sanity values before executing select, insert, update to
 * prevent SQL injections.
 * TODO Security: replace * for select to specific fields select that we need
 * TODO Security: replace + var + in query with set ? 1, 2 to prevent SQL
 * injection
 * TODO create Metadata access/modification interface and have this class
 * implement it
 * 
 * @author Rovshen Nazarov. Baylor University. Aug 25, 2014.All Rights Reserved.
 * 
 */
public final class Metadata implements IMetadata {

    private static Logger logger = Logger.getLogger(Metadata.class.getName());

    // JDBC driver name
    private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    private static final String HOST = "localhost";
    private static final String PORT = "3306";
    private static final String DATABASE_NAME = "aql_db";
    private static final String DB_URL = "jdbc:mysql://" + HOST + ":" + PORT
            + "/" + DATABASE_NAME;
    // DB credentials
    private static final String USER = "aql_system"; // data base user
    private static final String PASS = "aql"; // data base password
    private static final int FAILD_STATUS = -1;

    private static final String OPERANDS_ID_SEP = ",";

    private Connection conn;
    private volatile boolean status = false;

    /**
     * Create a single instance of the {@link Connection} object.
     * 
     * @author Rovshen Nazarov. Baylor University. Mar 21, 2015.All Rights
     *         Reserved.
     * 
     */
    private final static class DBConnectionSingleton {

        private final Connection conn;

        /**
         * This loader ensures that the instance is only loaded once guaranteed
         * by
         * JVM class loader.
         * 
         * @author Rovshen Nazarov. Baylor University. Mar 21, 2015.All Rights
         *         Reserved.
         * 
         */
        private final static class DBInstanceLoader {
            static DBConnectionSingleton INSTANCE = new DBConnectionSingleton();
        }

        private DBConnectionSingleton() {

            Connection tmpConn = null;

            try { // Register JDBC driver
                Class.forName(JDBC_DRIVER).newInstance();
                tmpConn = DriverManager.getConnection(DB_URL, USER, PASS);

            } catch (InstantiationException | IllegalAccessException
                    | ClassNotFoundException | SQLException e) {
                logger.error("Problems creating SQL connection "
                        + e.getMessage());
                e.printStackTrace();
            } finally {
                conn = tmpConn;
            }

        }

        public final static Connection getInstance() {

            return DBInstanceLoader.INSTANCE.conn;
        }
    }

    /** Database connection object */

    /**
     * Empty constructor.
     */
    public Metadata() {
    }

    /*
     * (non-Javadoc)
     * 
     * @see edu.baylor.aql.dataaccess.ILookUp#openConnection()
     */
    @Override
    public void openConnection() {

        // TODO push this creation to singleton object and pattern
        if (!status) {
            // TODO uncomment for debugging
            // logger.info("Opening DB connection.");
            conn = DBConnectionSingleton.getInstance();
        } else {
            // TODO uncomment for debugging
            // logger.info("DB connection is already open.");
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see edu.baylor.aql.dataaccess.ILookUp#closeDBConnection()
     */
    @Override
    public void closeConnection() {
        if (status) {
            if (conn != null) {
                try {
                    // TODO conn.close();
                    status = false;
                } catch (final Exception e) {
                    logger.error("Problems closing connection "
                            + e.getMessage());
                } finally {
                    // TODO conn = null;
                    status = false;
                }
                // TODO uncomment for debugging
                // logger.info("Database connection closed.");
            }
        }
    }

    /**
     * Execute SQL select command using sql prepared statement. Prepared
     * statement is known to be faster than normal sql
     * statement.
     * 
     * @param sqlCommand
     *            Select command to execute.
     * @return ResultSet Database's returned result set after executing select
     *         command. Read {@link ResultSet} for
     *         possible results.
     */
    private ResultSet executeSqlCommandSelect(final String sqlCommand) {

        ResultSet resultSet = null;
        if (sqlCommand == null || ("".equals(sqlCommand))) {
            logger.error("SQL command can not be empty");
            return null;
        }
        // TODO uncomment for debugging
        // logger.info("SQL command is: " + sqlCommand);
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = conn.prepareStatement(sqlCommand);
            resultSet = preparedStatement.executeQuery();
        } catch (final SQLException e) {
            logger.error("Problems executing sql " + sqlCommand
                    + e.getMessage());
            e.printStackTrace();
        }
        return resultSet;
    }

    /**
     * Executes SQL command that does not return result value, e.g. UPDATE, or
     * DELETE. Yet There is still result
     * returned to indicate the completion status of the SQL command.
     * 
     * @param sqlCommand
     *            String representation of SQL command to execute
     * @return int status Status of the executed SQL command. Default is set to
     *         failed status defined in this class.
     */
    public int executeSqlCommandUpdate(final String sqlCommand) {

        int result = FAILD_STATUS;
        if (sqlCommand == null || sqlCommand.equals("")) {
            logger.error("SQL command can not be empty");
            return result;
        }
        if (AQLServerAppSingleton.getInstance().getSettings().isDebugMode()) {
            logger.info("SQL command is: " + sqlCommand);
        }
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = conn.prepareStatement(sqlCommand);
            result = preparedStatement.executeUpdate();

        } catch (final SQLException e) {
            logger.error("Problems executing sql " + sqlCommand
                    + e.getMessage());
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Executes INSERT. This method is used to execute SQL Insert command. Last
     * automatically generated key is returned
     * by this method. Since the Auto Generated key is for the sqlCommand that
     * we execute we assume that the sqlCommand
     * inserts only one row and returned generated keys from database will have
     * only one entry.
     * 
     * @param sqlCommand
     *            SQL Insert command to execute (we assume one insert in
     *            performed only).
     * @return int Single auto generated key from the last insert command.
     */
    public final long execSqlCommandInsert(final String sqlCommand) {
        logger.info(sqlCommand);
        long result = FAILD_STATUS;
        if (sqlCommand == null || sqlCommand.equals("")) {
            logger.error("SQL command can not be empty");
            return result;
        }
        if (AQLServerAppSingleton.getInstance().getSettings().isDebugMode()) {
            // logger.info("SQL command is: " + sqlCommand);
        }
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = conn.prepareStatement(sqlCommand,
                    Statement.RETURN_GENERATED_KEYS);
            preparedStatement.executeUpdate();
            final ResultSet keys = preparedStatement.getGeneratedKeys();
            if (keys.next()) {
                result = keys.getInt(1);
            }
        } catch (final SQLException | NullPointerException e) {
            logger.error("Problems executing sql " + sqlCommand
                    + e.getMessage());
            result = Util.FAILED_STATUS;
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Helper SQL method to show all entries in a given result set.
     * 
     * @param resultSet
     *            Result set to display values for
     * @throws SQLException
     */
    @SuppressWarnings("unused")
    private final void showResults(final ResultSet resultSet)
            throws SQLException {
        final List<String> columnNames = getTableColumns(resultSet);
        writeResultSet(resultSet, columnNames);
    }

    /**
     * This method generates a list of the table column names for the result set
     * of the SQL select statement.
     * 
     * @param resultSet
     *            A result set returned after executing SQL select statement.
     * @return Returns list of the names of the table columns.
     * @throws SQLException
     */
    private List<String> getTableColumns(final ResultSet resultSet)
            throws SQLException {

        final List<String> columnNames = new ArrayList<>();
        // Get metadata from the SQL result
        System.out.println("The columns in the table "
                + resultSet.getMetaData().getTableName(1) + " are:");
        // column numnber starts at 1
        for (int i = 1; i <= resultSet.getMetaData().getColumnCount(); i++) {
            final String colName = resultSet.getMetaData().getColumnName(i);
            System.out.print(colName + " ");
            columnNames.add(colName);
        }
        System.out.println();

        return columnNames;
    }

    /**
     * This method writes to standard output result of the SQL select statement.
     * 
     * @param resultSet
     *            The result set of executing SQL select statement.
     * @param columnNames
     *            List of column names
     * @throws SQLException
     */
    private void writeResultSet(final ResultSet resultSet,
            final List<String> columnNames) throws SQLException {

        System.out.println("The entries in the table are:");
        while (resultSet.next()) {
            // access columns via column names
            for (final String columnName : columnNames) {
                System.out.print(resultSet.getString(columnName) + " ");
            }
            System.out.println();
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see edu.baylor.aql.dataaccess.ILookUp#getOperand(long)
     */
    @Override
    public final Operand lookUpOperand(final long operandId) {
        Operand operand = new ComputedOperand();
        // set graph to failed initially
        operand.setStatus(false).setId(FAILD_STATUS);
        if (operandId == Util.FAILED_STATUS) {
            // logger.info("No Operand stored for the id " + operandId);
            return operand;
        }
        final String sqlCommand = "select * from operand where operandId = "
                + operandId + ";";

        try (ResultSet resultSet = executeSqlCommandSelect(sqlCommand)) {
            if (resultSet.next()) {
                operand.setImplementationType(resultSet
                        .getString("implementationType"));

                switch (operand.getImplementationType()) {
                case COMP_OPERAND:
                    operand = getEstAtt(operandId);
                    break;
                case GRAPH:
                    operand = getGraphAtt(operandId);
                    break;
                case TABLE:
                    operand = getTableAtt(operandId);
                    break;
                case UNSTRUCTURED:
                    break;
                default:
                    break;
                }
                operand.setId(operandId);
                addCommonAtt(resultSet, operand);
            } else {
                // logger.info("No Operand stored for the id " + operandId);
            }
        } catch (final SQLException e) {
            logger.error("Problems executing sql " + sqlCommand
                    + e.getMessage());
            e.printStackTrace();
        }
        return operand;
    }

    private void addCommonAtt(final ResultSet resultSet, final Operand operand)
            throws SQLException {
        operand.setType(resultSet.getString("type"));
        operand.setImplementationType(resultSet.getString("implementationType"));
        final String pathInitial = resultSet.getString("initialPath");
        final String pathLoaded = resultSet.getString("loadedPath");
        operand.setCustomName(resultSet.getString("customName"));
        operand.setInitialPath(pathInitial);
        operand.setLoadedPath(pathLoaded);
        if (resultSet.getInt("status") == 1) {
            operand.setStatus(true);
        } else {
            operand.setStatus(false);
        }
        final Command command = new Command();
        command.setCommandSeqNumber((int) resultSet.getLong("commandId"));
        operand.setCommand(command);
        operand.setCreated(resultSet.getDate("created"));
    }

    private Operand getEstAtt(final Long operandId) throws SQLException {
        final ComputedOperand operand = new ComputedOperand();

        return operand;
    }

    private Operand getTableAtt(final Long operandId) throws SQLException {
        final Table operand = new Table();
        final String sqlCommand = "select * from table where operandId = "
                + operandId + ";";

        try (ResultSet resultSet = executeSqlCommandSelect(sqlCommand)) {
            if (resultSet.next()) {
                operand.setEstRowCount(resultSet.getLong("estRowCount"));
            } else {
                logger.error("Could not get data for Table with id "
                        + operandId);
            }
        }
        return operand;
    }

    private Operand getGraphAtt(final Long operandId) throws SQLException {
        final Graph operand = new Graph();
        final String sqlCommand = "select * from graph where operandId = "
                + operandId + ";";

        try (ResultSet resultSet = executeSqlCommandSelect(sqlCommand)) {
            if (resultSet.next()) {
                operand.setEstNodesCount(resultSet.getLong("estNodesCount"));
                operand.setEstEdgesCount(resultSet.getLong("estEdgesCount"));
                operand.setSize(resultSet.getLong("size"));
                operand.setDensity(resultSet.getDouble("density"));
            } else {
                // logger.info("No data for graph with id " + operandId);
            }
        }
        return operand;
    }

    /*
     * (non-Javadoc)
     * 
     * @see edu.baylor.aql.dataaccess.ILookUp#getSystemSetting(java.lang.String,
     * java.lang.String)
     */
    @Override
    public final String lookUpSystemSetting(final String name, final String type)
            throws AQLException {
        String result = "";
        final String sqlCommand = "select value from operating_system_settings where name = '"
                + name + "' and type = '" + type + "';";
        try (ResultSet resultSet = executeSqlCommandSelect(sqlCommand)) {
            if (resultSet.next()) {
                // access columns via column name
                result = resultSet.getString("value");
            } else {
                final String msg = "Could not get value for " + name + " "
                        + type;
                logger.error(msg);
                throw new AQLException(msg);

            }
        } catch (final SQLException e) {
            logger.error("Problems executing sql " + sqlCommand
                    + e.getMessage());
            e.printStackTrace();
        }
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see edu.baylor.aql.dataaccess.ILookUp#getOperandStatus(java.lang.Long)
     */
    @Override
    public boolean lookUpOperandStatus(final Long operandId) {
        boolean result = false;
        if (operandId == null) {
            logger.error("Operand id can not be null ");
            return result;
        }
        final String sqlCommand = "select status from operand where id = "
                + operandId + ";";
        try (ResultSet resultSet = executeSqlCommandSelect(sqlCommand)) {
            if (resultSet.next()) {
                // access columns via column name
                if (resultSet.getInt("status") == 1) {
                    result = true;
                }
            } else {
                logger.error("Could not get status for graph with id "
                        + operandId);
            }
        } catch (final SQLException e) {
            logger.error("Problems executing sql " + sqlCommand
                    + e.getMessage());
            e.printStackTrace();
        }
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see edu.baylor.aql.dataaccess.ILookUp#getCommandAlgos(java.lang.String)
     */
    @Override
    public final List<CommandAlgorithm> lookUpCommandAlgosByCommandType(
            final String commandType) throws AQLException {

        final List<CommandAlgorithm> commandAlgos = new ArrayList<>();
        if (commandType == null) {
            logger.error("commandType can not be null ");
            return commandAlgos;
        }
        // TODO read also attrib_key_value_command_algo table
        final StringBuilder sqlCommand = new StringBuilder(
                "select commandAlgorithmId, toolId, functionalType, priority, path, commandTemplate, ")
                .append("ca.created, ca.numberOfOperants, inputOperandType, ct.name, resultOperandType, avrRunTime, avgPerformance, avgMemoryRequirement, avgCPURequirement ")
                .append(" from command_algorithm ca, command_type ct ")
                .append(" where ct.commandTypeId = ca.commandTypeId and ct.name = '")
                .append(commandType).append("';");
        try (ResultSet resultSet = executeSqlCommandSelect(sqlCommand
                .toString())) {
            while (resultSet.next()) {
                final CommandAlgorithm currCommandAlgo = new CommandAlgorithm();
                final long commandAlgoId = resultSet
                        .getLong("commandAlgorithmId");
                currCommandAlgo
                        .setId(commandAlgoId)
                        .setFunctionalType(
                                resultSet.getString("functionalType"))
                        .setPriority(resultSet.getInt("priority"))
                        .setPath(resultSet.getString("path"))
                        .setNumberOfOperands(
                                resultSet.getInt("ca.numberOfOperants"))
                        .setCommandTemplate(
                                resultSet.getString("commandTemplate"))
                        .setOperandsType(
                                resultSet.getString("inputOperandType"))
                        .setResultOperandType(
                                resultSet.getString("resultOperandType"))
                        .setToolId(resultSet.getLong("toolId"))
                        .setCreated(resultSet.getDate("created"))
                        .setAvrRunTime(resultSet.getDouble("avrRunTime"))
                        .setAvgPerformance(resultSet.getInt("avgPerformance"))
                        .setAvgMemoryRequirement(
                                resultSet.getInt("avgMemoryRequirement"))
                        .setAvgCPURequirement(
                                resultSet.getInt("avgCPURequirement"));
                final Tool tool = lookUpTool(currCommandAlgo.getToolId());
                currCommandAlgo.setTool(tool);
                lookUpComandAlgoAttVal(commandAlgoId,
                        currCommandAlgo.getAttributes());
                commandAlgos.add(currCommandAlgo);
            }
        } catch (final SQLException e) {
            logger.error("Problems executing sql " + sqlCommand
                    + e.getMessage());
            e.printStackTrace();
        }
        if (commandAlgos.size() == 0) {
            throw new AQLException(
                    "No command algorithms fo the command type: " + commandType);
        }
        return commandAlgos;
    }

    @Override
    public CommandAlgorithm lookUpCommandAlgoById(final long commandAlgoId)
            throws AQLException {

        final CommandAlgorithm commandAlgoRes = new CommandAlgorithm();

        if (commandAlgoId == Util.FAILED_STATUS) {
            logger.error("commandAlgoId must be provided.");
            return commandAlgoRes;
        }

        final StringBuilder sqlCommand = new StringBuilder(
                "select commandAlgorithmId, toolId, functionalType, priority, path, commandTemplate, ")
                .append("ca.created, ca.numberOfOperants, inputOperandType, resultOperandType, avrRunTime, avgPerformance, avgMemoryRequirement, avgCPURequirement ")
                .append(" from command_algorithm ca")
                .append(" where commandAlgorithmId = ").append(commandAlgoId)
                .append(";");
        try (ResultSet resultSet = executeSqlCommandSelect(sqlCommand
                .toString())) {
            if (resultSet.next()) {

                commandAlgoRes
                        .setId(resultSet.getLong("commandAlgorithmId"))
                        .setFunctionalType(
                                resultSet.getString("functionalType"))
                        .setPriority(resultSet.getInt("priority"))
                        .setPath(resultSet.getString("path"))
                        .setNumberOfOperands(
                                resultSet.getInt("ca.numberOfOperants"))
                        .setCommandTemplate(
                                resultSet.getString("commandTemplate"))
                        .setOperandsType(
                                resultSet.getString("inputOperandType"))
                        .setResultOperandType(
                                resultSet.getString("resultOperandType"))
                        .setToolId(resultSet.getLong("toolId"))
                        .setCreated(resultSet.getDate("created"))
                        .setAvrRunTime(resultSet.getDouble("avrRunTime"))
                        .setAvgPerformance(resultSet.getInt("avgPerformance"))
                        .setAvgMemoryRequirement(
                                resultSet.getInt("avgMemoryRequirement"))
                        .setAvgCPURequirement(
                                resultSet.getInt("avgCPURequirement"));
                final Tool tool = lookUpTool(commandAlgoRes.getToolId());
                commandAlgoRes.setTool(tool);
                lookUpComandAlgoAttVal(commandAlgoId,
                        commandAlgoRes.getAttributes());
            } else {
                throw new AQLException(
                        "Could not get command algorithm for the id "
                                + commandAlgoId);
            }
        } catch (final SQLException e) {
            logger.error("Problems executing sql " + sqlCommand
                    + e.getMessage());
            e.printStackTrace();
        }
        return commandAlgoRes;
    }

    /**
     * Populate give Map with attribute value for the given
     * {@link CommandAlgorithm}'s
     * id.
     * 
     * @param commandAlgoId
     * @param attValMap
     */
    private void lookUpComandAlgoAttVal(final long commandAlgoId,
            final Map<String, String> attValMap) {

        final StringBuilder sqlCommand = new StringBuilder();
        sqlCommand
                .append("SELECT required, attrName, attrValue, created from attrib_key_value_command_algo where commandAlgorithmId = ")
                .append(commandAlgoId).append(";");
        try (ResultSet resultSet = executeSqlCommandSelect(sqlCommand
                .toString())) {
            while (resultSet.next()) {
                // TODO map required to the att value key somehow
                @SuppressWarnings("unused")
                final int required = resultSet.getInt("required");
                final String attrName = resultSet.getString("attrName");
                final String attrValue = resultSet.getString("attrValue");
                @SuppressWarnings("unused")
                final Date created = resultSet.getDate("created");
                // TODO what if more than one value per key? hash key collision?
                attValMap.put(attrName, attrValue);
            }
            logger.info("CommandAlgo attMap: " + attValMap);
        } catch (final SQLException e) {
            logger.error("Problems executing sql " + sqlCommand
                    + e.getMessage());
            e.printStackTrace();
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see edu.baylor.aql.dataaccess.ILookUp#getTool(java.lang.Long)
     */
    @Override
    public final Tool lookUpTool(final Long toolId) throws AQLException {
        final Tool tool = new Tool();
        if (toolId == 0) {
            logger.error("commandType can not be null ");
            return tool;
        }
        final StringBuilder sqlCommand = new StringBuilder(
                "select * from tool where toolId = " + toolId + ";");
        try (ResultSet resultSet = executeSqlCommandSelect(sqlCommand
                .toString())) {
            if (resultSet.next()) {
                tool.setId(toolId)
                        .setName(resultSet.getString("name"))
                        .setPrefix(resultSet.getString("prefix"))
                        .setPath(resultSet.getString("path"))
                        .setExecutionEnvironment(
                                resultSet.getString("executionEnvironment"))
                        .setComment(resultSet.getString("comment"))
                        .setCreated(resultSet.getDate("created"));
            } else {
                throw new AQLException("could not get tool with id " + toolId);
            }
        } catch (final SQLException e) {
            logger.error("Problems executing sql " + sqlCommand
                    + e.getMessage());
            e.printStackTrace();
        }
        return tool;
    }

    /*
     * (non-Javadoc)
     * 
     * @see edu.baylor.aql.dataaccess.ILookUp#getCommandTypes()
     */
    @Override
    public List<CommandType> lookUpCommandTypes() throws AQLException {
        final List<CommandType> commandTypes = new ArrayList<>();
        final String sqlCommand = "select * from command_type;";
        try (ResultSet resultSet = executeSqlCommandSelect(sqlCommand)) {
            if (resultSet == null) {
                return commandTypes;
            }
            while (resultSet.next()) {
                final CommandType commandType = new CommandType();
                getCommandTypeFromResultSet(resultSet, commandType);
                commandTypes.add(commandType);
            }
        } catch (final SQLException e) {
            logger.error("Problems executing sql " + sqlCommand
                    + e.getMessage());
            e.printStackTrace();
        }
        if (commandTypes.size() == 0) {
            throw new AQLException("No command types recorded in the system.");
        }
        return commandTypes;
    }

    /*
     * (non-Javadoc)
     * 
     * @see edu.baylor.aql.dataaccess.ILookUp#getToolByName(java.lang.String)
     */
    @Override
    public Tool lookUpToolByName(final String toolName) {
        final Tool t = new Tool();
        t.setId((long) Util.FAILED_STATUS);
        if (toolName == null) {
            logger.error("toolName can not be null ");
            return t;
        }
        final StringBuilder sqlCommand = new StringBuilder(
                "select * from tool where name = '").append(toolName).append(
                "';");
        try (ResultSet resultSet = executeSqlCommandSelect(sqlCommand
                .toString())) {
            if (resultSet.next()) {
                t.setId(resultSet.getLong("toolId"))
                        .setName(toolName)
                        .setPrefix(resultSet.getString("prefix"))
                        .setPath(resultSet.getString("path"))
                        .setExecutionEnvironment(
                                resultSet.getString("executionEnvironment"))
                        .setCreated(resultSet.getDate("created"));
            }
        } catch (final SQLException e) {
            logger.error("Problems executing sql " + sqlCommand
                    + e.getMessage());
            e.printStackTrace();
        }
        return t;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * edu.baylor.aql.dataaccess.ILookUp#getCommandTypeByName(java.lang.String)
     */
    @Override
    public CommandType lookUpCommandTypeByName(final String commandTypeName)
            throws AQLException {
        final CommandType ct = new CommandType();
        ct.setId((long) Util.FAILED_STATUS);
        if (commandTypeName == null) {
            logger.error("commandTypeName can not be null ");
            return ct;
        }
        final StringBuilder sqlCommand = new StringBuilder(
                "select * from command_type ").append("where name = '")
                .append(commandTypeName).append("';");
        try (ResultSet resultSet = executeSqlCommandSelect(sqlCommand
                .toString())) {
            if (resultSet.next()) {
                getCommandTypeFromResultSet(resultSet, ct);
            } else {
                throw new AQLException(
                        "Could not find command type with the name "
                                + commandTypeName);
            }
        } catch (final SQLException e) {
            logger.error("Problems executing sql " + sqlCommand
                    + e.getMessage());
            e.printStackTrace();
        }
        return ct;
    }

    private void getCommandTypeFromResultSet(final ResultSet resultSet,
            final CommandType commandType) throws SQLException {

        commandType
                .setId(resultSet.getLong("commandTypeId"))
                .setTypeName(resultSet.getString("name"))
                .setOrderingType(
                        OrderingType.getEnumByString(resultSet
                                .getString("commandTypeOrderingLaw")))
                .setNumberOfOperands(resultSet.getInt("numberOfOperants"))

                .setUsingCommandType(
                        (resultSet.getInt("isUsing") == 1) ? Boolean.TRUE
                                : Boolean.FALSE)

                .setCreated(resultSet.getDate("created"));
        logger.info(commandType);
    }

    /*
     * (non-Javadoc)
     * 
     * @see edu.baylor.aql.dataaccess.ILookUp#getCommandIds(long)
     */
    @Override
    public List<Long> lookUpCommandIdsByOperandId(final long operandId)
            throws AQLException {
        final List<Long> commandIds = new ArrayList<>();
        if (operandId == Util.FAILED_STATUS) {
            logger.error("operandId must be provided.");
            return commandIds;
        }
        final StringBuilder sqlCommand = new StringBuilder(
                "select * from command_operands where operandId = ").append(
                operandId).append(";");
        try (ResultSet resultSet = executeSqlCommandSelect(sqlCommand
                .toString())) {
            while (resultSet.next()) {
                commandIds.add(resultSet.getLong("commandId"));
            }
        } catch (final SQLException e) {
            logger.error("Problems executing sql " + sqlCommand
                    + e.getMessage());
            e.printStackTrace();
        }
        // if (commandIds.size() == 0) {
        // logger.info("None of the stored commands are associated with the operand id "
        // + operandId);
        // }
        return commandIds;
    }

    /**
     * Get full command with operands and all their values.
     * NOTE: Command id is unique and identifies a single command.
     * 
     * Only use this method to retrieve stored {@link Command}s.
     * 
     * @param commandId
     * @return
     * @throws AQLException
     */
    @Override
    public Command lookUpCommandById(final long commandId) throws AQLException {
        final Command command = new Command();
        if (commandId == Util.FAILED_STATUS) {
            logger.error("commandId must be provided.");
            return command;
        }
        final StringBuilder sqlCommand = new StringBuilder(
                "select * from command where commandId = ").append(commandId)
                .append(";");
        long commandAlgoId = Util.FAILED_STATUS;
        String operandsList = "";

        try (ResultSet resultSet = executeSqlCommandSelect(sqlCommand
                .toString())) {
            if (resultSet.next()) {
                command.setStored(true)
                        .setCommandSeqNumber(
                                resultSet.getInt("commandSeqNumber"))
                        .setCommand(resultSet.getString("commandStr"))
                        .setCreated(resultSet.getDate("created"));
                command.setResult(resultSet.getLong("resultOperandId"));
                command.setCommandTypeObj(
                        this.lookUpCommandTypeById(resultSet
                                .getLong("commandTypeId"))).setId(commandId);
                operandsList = resultSet.getString("operandsList");
                commandAlgoId = resultSet.getLong("commandAlgorithmId");

                command.setCommandAlgo(lookUpCommandAlgoById(commandAlgoId));
            } else {
                throw new AQLException("Could not find command with the id "
                        + commandId);
            }
            // get all operands for a given command
            final List<Long> operands = strToOperandsByOperIdList(operandsList);
            command.setOperands(operands);

        } catch (final SQLException e) {
            logger.error("Problems executing sql " + sqlCommand
                    + e.getMessage());
            e.printStackTrace();
        }
        return command;
    }

    private List<Long> strToOperandsByOperIdList(final String operandIdListStr)
            throws AQLException {

        final List<Long> operands = new ArrayList<>();
        if ("".equals(operandIdListStr)) {
            logger.error("operandIdListStr must be provided.");
            return operands;
        }

        final String[] operandsSrArr = operandIdListStr.split(OPERANDS_ID_SEP);
        for (final String oStr : operandsSrArr) {
            operands.add(Long.parseLong(oStr));
        }
        if (operands.size() == 0) {
            throw new AQLException(
                    "Could not get operands for the operand ids "
                            + operandIdListStr);
        }
        return operands;
    }

    /*
     * (non-Javadoc)
     * 
     * @see edu.baylor.aql.dataaccess.ILookUp#getCommandTypeById(long)
     */
    @Override
    public CommandType lookUpCommandTypeById(final long commandTypeId)
            throws AQLException {
        final CommandType ct = new CommandType();
        ct.setId((long) Util.FAILED_STATUS);
        if (commandTypeId == Util.FAILED_STATUS) {
            logger.error("commandTypeId must be provided.");
            return ct;
        }
        final StringBuilder sqlCommand = new StringBuilder(
                "select * from command_type where commandTypeId = ").append(
                commandTypeId).append(";");
        try (ResultSet resultSet = executeSqlCommandSelect(sqlCommand
                .toString())) {
            if (resultSet.next()) {
                getCommandTypeFromResultSet(resultSet, ct);
            } else {
                throw new AQLException("Could not find command type with id "
                        + commandTypeId);
            }
        } catch (final SQLException e) {
            logger.error("Problems executing sql " + sqlCommand
                    + e.getMessage());
            e.printStackTrace();
        }
        return ct;
    }

    /*
     * (non-Javadoc)
     * 
     * @see edu.baylor.aql.dataaccess.ILookUp#getQuery(long)
     */
    @Override
    public Query lookUpQuery(final long queryId) throws AQLException {
        final Query query = new Query();
        if (queryId == Util.FAILED_STATUS) {
            logger.error("queryId must be provided.");
            return query;
        }
        final StringBuilder sqlCommand = new StringBuilder(
                "select * from query ").append("where queryId = " + queryId
                + ";");
        try (ResultSet resultSet = executeSqlCommandSelect(sqlCommand
                .toString())) {
            if (resultSet.next()) {
                query.setRepeated(true)
                        .setClientSessionId(
                                resultSet.getLong("clientSessionId"))
                        .setQueryRequest(resultSet.getString("queryRequest"))
                        .setQueryProgress(resultSet.getDouble("queryProgress"));
                query.setResultOperand(resultSet.getLong("operandIdResult"));
            } else {
                throw new AQLException("Could not find query with id "
                        + queryId);
            }
            query.setCommands(lookUpCommandsForQuery(queryId));
        } catch (final SQLException e) {
            logger.error("Problems executing sql " + sqlCommand
                    + e.getMessage());
            e.printStackTrace();
        }
        return query;
    }

    private Queue<Command> lookUpCommandsForQuery(final long queryId)
            throws SQLException, AQLException {
        final Queue<Command> commands = new LinkedList<>();
        final StringBuilder sqlCommand = new StringBuilder(
                "select * from command  where queryId = ").append(queryId)
                .append(";");
        try (ResultSet resultSet = executeSqlCommandSelect(sqlCommand
                .toString())) {
            while (resultSet.next()) {
                final Command currComm = this.lookUpCommandById(resultSet
                        .getLong("commandId"));
                commands.add(currComm);
            }
        }
        if (commands.size() == 0) {
            throw new AQLException(
                    "Could not get commands for the query with id " + queryId);
        }
        return commands;
    }

    /*
     * (non-Javadoc)
     * 
     * @see edu.baylor.aql.dataaccess.ILookUp#getQueryId(long)
     */
    @Override
    public long lookUpQueryId(final long commandId) {
        long queryId = Util.FAILED_STATUS;
        if (commandId == Util.FAILED_STATUS) {
            logger.error("commandId must be provided.");
            return queryId;
        }
        final StringBuilder sqlCommand = new StringBuilder(
                "select queryId from command where commandId = ").append(
                commandId).append(";");
        try (ResultSet resultSet = executeSqlCommandSelect(sqlCommand
                .toString())) {
            if (resultSet.next()) {
                queryId = resultSet.getLong("queryId");
            }
        } catch (final SQLException e) {
            logger.error("Problems executing sql " + sqlCommand
                    + e.getMessage());
            e.printStackTrace();
        }
        return queryId;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * edu.baylor.aql.dataaccess.ILookUp#execCommandInsert(edu.baylor.aql.entities
     * .command.Command, long)
     */
    @Override
    public void storeCommand(final Command command, final long queryId,
            final Query q) {
        final StringBuilder sb = new StringBuilder("");
        if (command == null) {
            logger.error("command must be provided.");
            return;
        }
        if (!command.isStored()) {
            final List<Long> operIdsArr = new ArrayList<>();

            // Get updated ids for operands.
            final long resOId = q.getOperMap().get(command.getResult()).getId();
            for (final Long oIdOld : command.getOperands()) {
                final long oIdNew = q.getOperMap().get(oIdOld).getId();
                operIdsArr.add(oIdNew);
            }
            // build opearnds list string concat by comma
            final Joiner joiner = Joiner.on(OPERANDS_ID_SEP).skipNulls();
            final String operandsListStr = joiner.join(operIdsArr);

            sb.append(
                    "INSERT INTO  command (queryId, operandsList, resultOperandId, commandTypeId, commandAlgorithmId, commandSeqNumber, commandStr)")
                    .append("VALUES (")
                    .append(queryId)
                    .append(",'")
                    .append(operandsListStr)
                    .append("',")
                    .append(resOId)
                    .append(", ")

                    .append(command.getCommandTypeObj() == null ? null
                            : command.getCommandTypeObj().getId())
                    .append(", ")
                    .append(command.getCommandAlgo() == null ? null : command
                            .getCommandAlgo().getId()).append(", ")

                    .append(command.getCommandSeqNumber()).append(", '")
                    .append(command.getCommand()).append("');\n");
            final long commandId = this.execSqlCommandInsert(sb.toString());

            for (final Long oId : operIdsArr) {

                sb.setLength(0); // clear string builder
                sb.append(
                        "INSERT INTO  command_operands (operandId, commandId)")
                        .append("VALUES (").append(oId).append(", ")
                        .append(commandId).append(");\n");
                this.execSqlCommandInsert(sb.toString());
            }

        }
    }

    @Override
    public long store(final ClientSession clientSession) {
        final StringBuilder builder = new StringBuilder();
        int statusInt = 0;
        if (clientSession.isConnected()) {
            statusInt = 1;
        }
        builder.append("INSERT INTO  session (remoteAddress, isConnected)")
                .append("VALUES ('")
                .append(clientSession.getRemoteAddress().toString())
                .append("', ").append(statusInt).append(");");
        final long res = this.execSqlCommandInsert(builder.toString());
        return res;
    }

    @Override
    public int update(final ClientSession clientSession) {
        final StringBuilder builder = new StringBuilder();
        int statusInt = 0;
        if (clientSession.isConnected()) {
            statusInt = 1;
        }
        builder.append("UPDATE session SET isConnected = ")
                .append(statusInt)
                .append(", ")
                .append(" lastQueryId = ")
                .append((clientSession.getLastQuery() == null) ? null
                        : clientSession.getLastQuery().getQueryId())
                .append(", ").append(" useExperimentalTools = ")
                .append(clientSession.isUseExperimentalTools() ? 1 : 0)
                .append(", ").append(" commandCount = ")
                .append(clientSession.getCommandSessionId())
                .append(" WHERE sessionId = ")
                .append(clientSession.getSessionId()).append(";");
        final int res = this.executeSqlCommandUpdate(builder.toString());
        return res;

    }

    @Override
    public long store(final CommandAlgorithm ca) throws AQLException {

        final StringBuilder sb = new StringBuilder();
        sb.append(
                "INSERT INTO  command_algorithm (resultOperandType, numberOfOperants, commandTypeId, toolId,  priority, path, functionalType, commandTemplate)")
                .append("VALUES ('")
                .append(ca.getResultOperandType().getLabel())
                .append("',")
                .append(ca.getNumberOfOperands())
                .append(", ")
                .append(ca.getCommandType().getId())
                .append(", ")
                .append(ca.getToolId())
                .append(", ")
                .append(ca.getPriority())
                .append(", '")
                .append(ca.getPath())
                .append("', '")
                .append(ca.getFunctionalType() != null ? ca.getFunctionalType()
                        .getLabel() : null).append("', '")
                .append(ca.getCommandTemplate()).append("');");

        final long res = this.execSqlCommandInsert(sb.toString());
        final List<String> attValSQL = getInsertComandAlgoAttVal(res, ca);
        for (final String sqlInsert : attValSQL) {
            execSqlCommandInsert(sqlInsert);
        }

        return res;
    }

    /**
     * Store an SQL insert statement for each optional attribute value pair.
     * 
     * @param commandAlgorithmId
     * @return
     */
    private List<String> getInsertComandAlgoAttVal(
            final Long commandAlgorithmId, final CommandAlgorithm ca) {

        final List<String> attValSQLList = new ArrayList<>();
        final Set<String> allAtt = ca.getAttributes().keySet();
        clearLoadedAttList(allAtt);

        final StringBuilder sb = new StringBuilder();
        for (final String att : allAtt) {
            final String val = ca.getAttributes().get(att);
            sb.setLength(0);
            sb.append(
                    "INSERT INTO  attrib_key_value_command_algo (commandAlgorithmId, attrName, attrValue)")
                    .append("VALUES ").append("(").append(commandAlgorithmId)
                    .append(", '").append(att).append("', '").append(val)
                    .append("');");
            attValSQLList.add(sb.toString());
        }

        return attValSQLList;
    }

    /**
     * Remove loaded attributes that are already in the metadata.
     * 
     * @param allAtt
     */
    private void clearLoadedAttList(final Set<String> allAtt) {
        final Set<String> attToExclude = new HashSet<>();
        attToExclude.add("tool");
        attToExclude.add("path");
        attToExclude.add("operandcount");
        attToExclude.add("type");
        attToExclude.add("commandtemplate");
        allAtt.removeAll(attToExclude);
    }

    /**
     * Record {@link OrderingType} associated with the input {@link CommandType}
     * 
     * PRE: Result {@link OrderingType} of {@link CommandType} is defined.
     * PRE: Command Type Id for the {@link CommandType} is supplied.
     * 
     * @return
     */
    @Override
    public long store(final CommandType ct) {
        final StringBuilder sb = new StringBuilder();
        sb.append(
                "INSERT INTO  command_type (name, commandTypeOrderingLaw, numberOfOperants, isUsing)")
                .append("VALUES ('").append(ct.getTypeName()).append("', '")
                .append(ct.getOrderingType().getLabel()).append("', ")
                .append(ct.getMinNumbOper())
                // if using command store 1, 0 otherwise
                .append(", ").append(ct.isUsingCommandType() ? 1 : 0)

                .append(");");
        final long res = this.execSqlCommandInsert(sb.toString());
        return res;
    }

    @Override
    public long store(final Operand o) {

        final StringBuilder builder = new StringBuilder("");

        // build appropriate insert string
        switch (o.getImplementationType()) {
        case COMP_OPERAND:
            // same as just operand
            buildOperand(builder, o);
            break;
        case GRAPH:
            buildGraph(builder, (Graph) o);
            break;
        case TABLE:
            buildTable(builder, (Table) o);
            break;
        case UNSTRUCTURED:
            buildUnstruct(builder, (Unstructured) o);
            break;
        default:
            // not supported
            break;
        }

        // execute operand insert string
        final long res = this.execSqlCommandInsert(builder.toString());
        return res;
    }

    private void buildUnstruct(final StringBuilder builder, final Unstructured o) {
        buildOperand(builder, o);
    }

    private void buildTable(final StringBuilder builder, final Table o) {
        buildOperand(builder, o);
    }

    private void buildGraph(final StringBuilder builder, final Graph o) {
        buildOperand(builder, o);
    }

    private void buildOperand(final StringBuilder builder, final Operand o) {
        int statusInt = 0;
        if (o.getStatus()) {
            statusInt = 1;
        }
        builder.append(
                "INSERT INTO  operand (status, type, implementationType, customName, initialPath, loadedPath, commandId")
                .append(") VALUES (")
                .append(statusInt)
                .append(", '")
                .append(o.getType())
                .append("', '")
                .append(o.getImplementationType())
                .append("', '")
                .append(o.getCustomName())
                .append("', '")
                .append(o.getInitPath())
                .append("', '")
                .append(o.getLoadedPath())
                .append("', ")
                .append(o.getCommand() == null ? null : o.getCommand()
                        .getCommandSeqNumber()).append(");");
    }

    @Override
    public long store(final Query q) throws AQLException {

        final long resQId = q.getOperMap().get(q.getResultOperand()).getId();

        final StringBuilder sb = new StringBuilder("");
        if (!q.isRepeated()) {
            if (q.getResultOperand() == null) {
                throw new AQLException(
                        "Result operand cannot be null for query "
                                + q.toString());
            }
            sb.append(
                    "INSERT INTO  query (operandIdResult, clientSessionId, queryRequest, queryProgress)")
                    .append("VALUES (").append(resQId).append(",")
                    .append(q.getClientSessionId()).append(", '")
                    .append(q.getQueryRequest()).append("', ")
                    .append(q.getProgress()).append(");");
        }

        final long res = this.execSqlCommandInsert(sb.toString());
        return res;
    }

    @Override
    public long store(final Tool t) {
        final StringBuilder sb = new StringBuilder();
        sb.append(
                "INSERT INTO  tool (name, prefix, path, executionEnvironment, comment)")
                .append("VALUES ('").append(t.getName()).append("', '")
                .append(t.getPrefix()).append("', '").append(t.getPath())
                .append("', '").append(t.getComment()).append("', '")
                .append(t.getExecutionEnvironment()).append("');");

        final long res = this.execSqlCommandInsert(sb.toString());
        return res;
    }

    @Override
    public boolean removeAllQueryExecutionRecords() {

        boolean status = true;

        final List<String> tableNameList = Arrays.asList(new String[] {
                "client_session", "session", "query", "command", "operand",
                "command_operands", "computed_operand", "graph" });

        for (final String tableName : tableNameList) {

            final StringBuilder sqlCommand = new StringBuilder(
                    "truncate table ").append(tableName).append(";");
            final int res = executeSqlCommandUpdate(sqlCommand.toString());
            if (res == Util.FAILED_STATUS) {
                status = false; // removed failed
            }
        }

        return status;
    }

    @Override
    public boolean removeAllLoadRecordsCommandAlgos() {

        boolean status = true;

        final List<String> tableNameList = Arrays.asList(new String[] {
                "command_algorithm", "attrib_key_value_command_algo",
                "input_implement_type_comm_algo" });

        for (final String tableName : tableNameList) {

            final StringBuilder sqlCommand = new StringBuilder(
                    "truncate table ").append(tableName).append(";");
            final int res = executeSqlCommandUpdate(sqlCommand.toString());
            if (res == Util.FAILED_STATUS) {
                status = false; // removed failed
            }
        }

        return status;
    }

    @Override
    public boolean removeAllLoadRecordsTools() {

        boolean status = true;
        final List<String> tableNameList = Arrays
                .asList(new String[] { "tool" });

        for (final String tableName : tableNameList) {

            final StringBuilder sqlCommand = new StringBuilder(
                    "truncate table ").append(tableName).append(";");
            final int res = executeSqlCommandUpdate(sqlCommand.toString());
            if (res == Util.FAILED_STATUS) {
                status = false; // removed failed
            }
        }
        return status;
    }

    @Override
    public boolean removeAllLoadRecordsCommandTypes() {

        boolean status = true;
        final List<String> tableNameList = Arrays
                .asList(new String[] { "command_type" });

        for (final String tableName : tableNameList) {

            final StringBuilder sqlCommand = new StringBuilder(
                    "truncate table ").append(tableName).append(";");
            final int res = executeSqlCommandUpdate(sqlCommand.toString());
            if (res == Util.FAILED_STATUS) {
                status = false; // removed failed
            }
        }
        return status;
    }
}
