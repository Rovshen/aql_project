/**
 * This package contains data access related services, such as load, sore, delete, and update. 
 * This package has implementation class for a specific storage system. MySql is the default storage system for the AQL Server.
 * The services in this package are self contained and perform all storage specific handling within a 
 * service and propagate only non-storage specific errors, data, and messages to the callers.
 * Thus this service is designed to be modular, and have low coupling with other services.
 * Yet this service is extensivly used in all other services that require data access.
 */
package edu.baylor.aql.dataaccess;

