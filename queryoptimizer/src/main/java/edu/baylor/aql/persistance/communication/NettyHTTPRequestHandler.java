package edu.baylor.aql.persistance.communication;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.FullHttpRequest;

/**
 * Handshake processor.
 * TODO add client server handshake.
 * https://github.com/normanmaurer/netty-in-action/blob/master/src/main/java/com
 * /manning/nettyinaction/chapter11/HttpRequestHandler.java
 * 
 * @author Rovshen Nazarov. Baylor University. Sep 12, 2014.All Rights Reserved.
 * 
 */
public final class NettyHTTPRequestHandler extends
        SimpleChannelInboundHandler<FullHttpRequest> {

    @Override
    protected void channelRead0(final ChannelHandlerContext ctx,
            final FullHttpRequest msg) throws Exception {
        // TODO add implementation of the handshake.
    }

}
