package edu.baylor.aql.persistance.xml;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import org.apache.log4j.Logger;
import org.jdom2.Attribute;
//JDOM2 used for XML parsing
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.input.sax.XMLReaders;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import edu.baylor.aql.entities.command.Command;
import edu.baylor.aql.util.Util;

/**
 * @deprecated The use of this class for metadata handling was replaced by the
 *             database based metadata hanlder.
 * 
 *             This service class is used to perform operations on the XML
 *             files, such as reading XML file into memory,
 *             retrieving some values, writing in memory data to XML file.
 * 
 * @author Rovshen Nazarov. All Rights Reserved.
 * 
 */
@Deprecated
public final class XMLService {

    static Logger logger = Logger.getLogger(XMLService.class.getName());

    /**
     * Get node's value. This method retrieves nodes value based on attribute
     * name and value provided by the caller.
     * 
     * This method is used to access two levels below the root selecting each
     * XML tree path based on the provided child
     * name values.
     * 
     * @param root
     *            Tom level in the tree for XML DOM structure.
     * @param childNameLevel1
     *            One level below the treee
     * @param childNameLevel2
     *            Two levels below the tree
     * @param attribute
     *            Attribute name for the 2nd level below the tree *
     * @param attrValue
     *            Attribute value
     * 
     * @return root->parent->child->value Value retrieved from the 2nd level
     *         below the root of the tree based on the
     *         attribute name and value.
     */
    public String getNodeValue(final Element root,
            final String childNameLevel1, final String childNameLevel2,
            final String attribute, final String attrValue) {

        if (root == null || childNameLevel1 == null || childNameLevel2 == null
                || attribute == null || attrValue == null) {
            return "";
        }

        List<Element> childrenList, childrenRoot;
        childrenRoot = root.getChildren(childNameLevel1);
        childrenList = childrenRoot.get(0).getChildren(childNameLevel2);
        /**
         * iterate through all children of the root node and retrieve subtree.
         * */
        for (final Element childNode : childrenList) {
            final String readAttrValue = childNode.getAttributeValue(attribute);
            if (readAttrValue.toLowerCase().equals(attrValue.toLowerCase())) {

                return childNode.getValue();
            }
        }
        // if value not found return empty line
        return "";
    }

    /**
     * Get node's children. This method retrieves level two children from the
     * root node based
     * on the attribute name and value provided by the caller.
     * 
     * This method is used to access two levels below the root selecting each
     * XML tree path
     * based on the provided child name values.
     * 
     * @param root
     *            Tom level in the tree for XML DOM structure.
     * @param childNameLevel1
     *            One level below the treee
     * @param childNameLevel2
     *            Two levels below the tree
     * @param attribute
     *            Attribute name for the 2nd level below the tree *
     * @param attrValue
     *            Attribute value
     * 
     * @return root->parent->child->children Value retrieved from the 2nd level
     *         below the root of the tree based on the
     *         attribute name and value.
     */
    public List<Element> getNodeList(final Element root,
            final String childNameLevel1, final String childNameLevel2,
            final String attribute, final String attrValue) {

        List<Element> childrenList = new ArrayList<Element>();
        if (root == null || childNameLevel1 == null || childNameLevel2 == null
                || attribute == null || attrValue == null) {
            return childrenList;
        }

        List<Element> childrenRoot;
        childrenRoot = root.getChildren(childNameLevel1);
        /**
         * iterate through all children of the root node and retrieve subtree.
         * */
        for (final Element childNode : childrenRoot) {
            final String readAttrValue = childNode.getAttributeValue(attribute);
            if (readAttrValue.toLowerCase().equals(attrValue.toLowerCase())) {

                childrenList = childNode.getChildren(childNameLevel2);
            }
        }
        // if value not found return empty list
        return childrenList;
    }

    /**
     * Add intermediate result id path map to the graphs' metadata.
     * This method stored intermediate result Graph object to the XML
     * Metadata.
     * 
     * @param count
     *            Graph count in the query.
     * @param resultGraphPath
     *            The result path to store intermediate result in
     * @param graphIDs
     *            Stack of the graph ids used to store the new id.
     * @param rootGraphPaths
     *            First level in the tree.
     * @return Graph Id generated during the insert
     */
    public String saveIntermediateResultPath(final int count,
            final String resultGraphPath, final Stack<String> graphIDs,
            final Element rootGraphPaths, final String QueryID) {
        if (resultGraphPath == null || graphIDs == null) {
            logger.error("resultGraphPath or graphIDs NULL.");
            return null;
        }
        final Util util = new Util();
        final Element child = new Element("graph"); // save the command
        // resultGraphPath SET resultGraphPath ID
        final StringBuilder intermGraphId = new StringBuilder(QueryID).append(
                "_intermed_").append(count);
        child.setAttribute(new Attribute("id", intermGraphId.toString()));
        child.setAttribute(new Attribute("queryID", QueryID));
        child.setAttribute(new Attribute("created", util.getCurrDate()));
        child.setText(resultGraphPath); // SET value for resultGraphPath
        // STORE resultGraphPath in the in memory "ID PATH MAP" table
        rootGraphPaths.getChild("inputGraphs").addContent(child);
        // STORE resultGraphPath ID to be used within calling query
        graphIDs.push(intermGraphId.toString());
        return intermGraphId.toString();
    }

    /**
     * Helper function to print content of a single operator,
     * which represents {@link Command} with defined priority
     * and the command String as a value.
     * 
     * @param typeOperator
     */
    public void printOperator(final Element typeOperator) {
        System.out.println("priority "
                + typeOperator.getAttributeValue("priority"));
        System.out.println("data  " + typeOperator.getValue());
    }

    /**
     * Read XML property file into memory. This method loads XML file
     * into the memory using JDOM2 package's XML builder classes.
     * 
     * @param pathname
     *            Path to the XML file to be read
     * @return root element of the read in XML based DOM object, where DOM
     *         object represents hierarchy of operators with
     *         specific type and specific operations.
     */
    public Document readXML(final String pathname) {
        /**
         * DTDVALIDATING boolean value specifies that when the DOM object read
         * its structure should be validated using
         * DTD. DTD must be in the head of the XML file.
         */
        if (pathname == null)
            return null;
        Document docRead = null;
        final SAXBuilder xmlBuilder = new SAXBuilder(XMLReaders.DTDVALIDATING);
        if (!xmlBuilder.isValidating()) {
            logger.error("Skipping DTD Validation.");
        }
        final File fileToRead = new File(pathname);
        try {
            docRead = xmlBuilder.build(fileToRead);
        } catch (final JDOMException jdomE) {
            logger.error(jdomE.getMessage());
        } catch (final IOException e) {
            logger.error(e.getMessage());
        }
        return docRead;
    }

    /**
     * Updates graph path table. Store in memory metadata to
     * the original XML file by rewriting the file content with the
     * new content.
     * 
     * Note that the DTD had to be added as a header as it is not preserved
     * by the {@link SAXBuilder}.
     * 
     * @param graphPathXMLFilePath
     *            The path to the XML file that will be rewritten.
     * @param docRead
     *            The in memory document to write.
     * @throws IOException
     */
    public void updatePathTable(final String graphPathXMLFilePath,
            final Document docRead) throws IOException {
        if (graphPathXMLFilePath == null || docRead == null) {
            logger.error("graphPathXMLFilePath or docRead NULL.");
            return;
        }
        final StringBuilder DTD = new StringBuilder(
                "\t<!ELEMENT graphs (inputGraphs,resultGraphPaths,loadGraphPaths)>\n")
                .append("\t<!ELEMENT inputGraphs (graph+)>\n")
                .append("\t<!ELEMENT resultGraphPaths (resultPath+)>\n")
                .append("\t<!ELEMENT loadGraphPaths (loadPath+)>\n")
                .append("\t<!ELEMENT resultPath (#PCDATA)>\n")
                .append("\t<!ELEMENT loadPath (#PCDATA)>\n")
                .append("\t<!ELEMENT graph (#PCDATA)>\n")
                .append("\t<!ATTLIST resultPath type CDATA #IMPLIED>\n")
                .append("\t<!ATTLIST loadPath type CDATA #IMPLIED>\n")
                .append("\t<!ATTLIST graph id CDATA #IMPLIED>\n")
                .append("\t<!ATTLIST graph queryID CDATA #IMPLIED>\n")
                .append("\t<!ATTLIST graph commandID CDATA #IMPLIED>\n")
                .append("\t<!ATTLIST graph created CDATA #IMPLIED>\n")
                .append("\t<!ATTLIST graph estNodesCount CDATA #IMPLIED>\n")
                .append("\t<!ATTLIST graph estEdgesCount CDATA #IMPLIED>\n")
                .append("\t<!ATTLIST graph graphSize CDATA #IMPLIED>\n")
                // order of a graph is |V| (the number of vertices). A graph's
                // size is |E|,
                .append("\t<!ATTLIST graph graphDensity CDATA #IMPLIED>\n")
                .append("\t<!ATTLIST graph type CDATA #IMPLIED>\n"); // requires
                                                                     // approximation
        // update path map xml
        docRead.getDocType().setInternalSubset(DTD.toString());
        final XMLOutputter xmlOutput = new XMLOutputter();
        // passed fileWriter to write content in specified file
        xmlOutput.setFormat(Format.getPrettyFormat());
        xmlOutput.output(docRead, new FileWriter(graphPathXMLFilePath));
    }
}
