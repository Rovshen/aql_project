package edu.baylor.aql.persistance.communication.blockingio;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import org.apache.log4j.Logger;

/**
 * Deprecated to NIO Server Netty.
 * 
 * @author Rovshen Nazarov. Baylor University. Sep 13, 2014.All Rights Reserved.
 * 
 */
@Deprecated
public final class ServerBootstrap implements Runnable {

    static Logger logger = Logger.getLogger(ServerBootstrap.class.getName());
    private ServerSocket server;
    private final BlockingQueue<Socket> clientQueue;
    private final List<Socket> clientSockets = new ArrayList<>();

    public ServerBootstrap(final int port) {
        try {
            server = new ServerSocket();
        } catch (final IOException e) {
            logger.error("Problems binding the server to the port " + port
                    + e.getMessage());
        }
        clientQueue = new ArrayBlockingQueue<>(10);
    }

    @Override
    public void run() {

        while (!Thread.currentThread().isInterrupted()) {
            Socket socket = null;
            try {
                socket = server.accept();
                clientQueue.put(socket);
                clientSockets.add(socket);
            } catch (final IOException e) {
                logger.error("Problems connecting new client " + e.getMessage());
            } catch (final InterruptedException e) {
            }
            if (socket != null) {
                logger.info("New user connected from "
                        + socket.getRemoteSocketAddress());
            }
        }
    }

    public void shutDownServer() {
        try {
            if (server != null) {
                server.close();
                for (final Socket socket : clientSockets) {
                    socket.close();
                }
            }
        } catch (final IOException e) {
            logger.error("Problems closing server " + e.getMessage());
        }
    }

    public BlockingQueue<Socket> getClientQueue() {
        // TODO Auto-generated method stub
        return null;
    }
}
