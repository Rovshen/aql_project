/**
 * This package contains the communication service that uses a non-blocking or asynchronous communication. 
 * The implementation of the asynchronous communication is performed by 
 * extending Netty framework designed and developed to be extended as asynchronous interprocess communication library. 
 */
package edu.baylor.aql.persistance.communication;