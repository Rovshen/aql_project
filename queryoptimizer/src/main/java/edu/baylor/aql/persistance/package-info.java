/**
 * This package contains persistence related services, where persistence is referenced as accessing and exchanging data between some storage or communication media and this program.
 * <p>
 * This package contains services for XML documents manipulation, and {@link java.net.Socket} based interprocess communication.
 * There are two services for {@link java.net.Socket}s based communication. First one is the standard blocking or synchronous communication, where for each 
 * connection there is an open socket and a dedicated thread to maintain that communication.
 * The second communication service uses a non-blocking or asynchronous communication. The implementation of the asynchronous communication is performed by 
 * extending Netty framework designed and developed to be extended as asynchronous interprocess communication library. 
 */
package edu.baylor.aql.persistance;