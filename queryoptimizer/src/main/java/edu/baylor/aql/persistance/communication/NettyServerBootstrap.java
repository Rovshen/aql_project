package edu.baylor.aql.persistance.communication;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.PooledByteBufAllocator;
import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.util.concurrent.Future;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import org.apache.log4j.Logger;

import edu.baylor.aql.entities.client.ClientSession;

/**
 * This class performs all necessary configurations and settings for the Netty
 * framework. This class also allows to control Netty server, such as
 * termination.
 * 
 * @author Rovshen Nazarov. Baylor University. Jan 14, 2015.All Rights Reserved.
 * 
 */
public final class NettyServerBootstrap {

    static Logger logger = Logger.getLogger(NettyServerBootstrap.class
            .getName());
    private final int port;
    protected static final String lineSeparator = System.lineSeparator();
    private Channel channel;
    private final BlockingQueue<ClientSession> clientSessions;
    // boss accepts new connection
    private final EventLoopGroup bossGroup = new NioEventLoopGroup(1);
    // worker processes new connection
    private final EventLoopGroup workerGroup = new NioEventLoopGroup();

    /**
     * @param port
     */
    public NettyServerBootstrap(final int port) {
        this.port = port;
        clientSessions = new ArrayBlockingQueue<>(10);
    }

    public void run() {
        final ServerBootstrap bootstrap = new ServerBootstrap();
        bootstrap
                .group(bossGroup, workerGroup)
                .channel(NioServerSocketChannel.class)
                .childHandler(createInitializer())
                .option(ChannelOption.SO_BACKLOG, 128)
                .childOption(ChannelOption.SO_KEEPALIVE, true)
                .childOption(ChannelOption.ALLOCATOR,
                        PooledByteBufAllocator.DEFAULT);

        // Bind and start to accept incoming connections.
        bootstrap.bind(port).syncUninterruptibly();
    }

    protected ChannelInitializer<SocketChannel> createInitializer() {
        return new NettyServerInitializer(clientSessions);
    }

    public void terminateServer() {
        // Wait until the server socket is closed and then close the channel.
        if (channel != null) {
            try {
                channel.closeFuture().sync();
            } catch (final InterruptedException e) {
                logger.error("Problem with starting Netty server "
                        + e.getMessage());
                e.printStackTrace();
            }
        }
        shutDownGracefully(bossGroup);
        shutDownGracefully(workerGroup);

    }

    private void shutDownGracefully(final EventLoopGroup group) {
        final Future<?> shutDownFut = group.shutdownGracefully();
        final int waitTime = 500; // .5 sec
        final int maxWaitTime = 60000; // 1 min
        int waitedSoFar = 0;
        while (!shutDownFut.isDone()) {
            if (waitedSoFar == maxWaitTime) {
                final boolean mayInterruptIfRunning = true;
                shutDownFut.cancel(mayInterruptIfRunning);
            }
            try {
                Thread.sleep(waitTime);
                waitedSoFar += waitTime;
            } catch (final InterruptedException e) {
            } // sleep half a second
            logger.info("waiting for socket channel to terminate");
        }
    }

    /**
     * @return the channelGroup
     */
    public BlockingQueue<ClientSession> getClientSessions() {
        return clientSessions;
    }

}
