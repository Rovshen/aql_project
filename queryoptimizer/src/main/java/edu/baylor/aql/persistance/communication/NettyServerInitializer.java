package edu.baylor.aql.persistance.communication;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.handler.codec.Delimiters;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;

import java.util.concurrent.BlockingQueue;

import edu.baylor.aql.entities.client.ClientSession;

/**
 * Netty server initializer defines encoders, decoders, framers, and handlers
 * used by Netty framework when a new message send or received.
 * Note: In this class a size of maximum allowed frame is set to be 8192 for the
 * {@link DelimiterBasedFrameDecoder}. As a delimeter
 * {@link Delimiters#lineDelimiter()} is used.
 * 
 * @author Rovshen Nazarov. Baylor University. Sep 12, 2014.All Rights Reserved.
 * 
 */
public final class NettyServerInitializer extends
        ChannelInitializer<SocketChannel> {

    private final BlockingQueue<ClientSession> clientSessions;

    public <Channel> NettyServerInitializer(
            final BlockingQueue<ClientSession> clientSessions) {
        this.clientSessions = clientSessions;
    }

    @Override
    protected void initChannel(final SocketChannel ch) throws Exception {
        final ChannelPipeline pipline = ch.pipeline();
        final NettyStringServerInBoundHandler serverHandler = new NettyStringServerInBoundHandler(
                clientSessions);

        final int maxFrameLength = 8192;
        // for decoder maxFrameLength, delimiter
        pipline.addLast("framer", new DelimiterBasedFrameDecoder(
                maxFrameLength, Delimiters.lineDelimiter()));
        pipline.addLast("decoder", new StringDecoder());
        pipline.addLast("encoder", new StringEncoder());

        // add handler for business logic
        pipline.addLast("handler", serverHandler);
    }

}
