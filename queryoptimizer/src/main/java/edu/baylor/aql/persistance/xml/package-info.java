/**
 * This package contains persistence related services to operate on XML based documents, with XML related data structures.
 * It allows to retrieve, read contents of, and update XML based data structures. 
 */
package edu.baylor.aql.persistance.xml;