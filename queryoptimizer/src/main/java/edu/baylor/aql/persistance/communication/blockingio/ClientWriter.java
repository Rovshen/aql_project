package edu.baylor.aql.persistance.communication.blockingio;

import java.io.IOException;
import java.io.OutputStream;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import org.apache.log4j.Logger;

/**
 * Write data back to the client. Deprecated to NIO Server Netty.
 * 
 * @author Rovshen Nazarov. Baylor University. Sep 13, 2014.All Rights Reserved.
 * 
 */
@Deprecated
public final class ClientWriter implements Runnable {

    private final OutputStream outStr;
    static Logger logger = Logger.getLogger(ClientWriter.class.getName());
    private final BlockingQueue<String> queue = new ArrayBlockingQueue<>(20);
    static final String delimeter = System.lineSeparator();

    public ClientWriter(final OutputStream outputStream) {
        if (outputStream == null) {
            logger.error("Output stream cannot be null for the client writer.");
        }
        outStr = outputStream;
    }

    public void close() throws IOException {
        outStr.close();
    }

    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                outStr.write(queue.take().getBytes());
                outStr.write(delimeter.getBytes());
                outStr.flush();
            } catch (IOException | InterruptedException e) {
                logger.error("Problems writing to the Output stream.");
                try {
                    outStr.close();
                } catch (final IOException e1) {
                }
            } finally {
                try {
                    outStr.close();
                } catch (final IOException e) {
                }
            }
        }
    }

    public void write(final String message) {
        try {
            queue.put(message);
        } catch (final InterruptedException e) {
        }
    }
}
