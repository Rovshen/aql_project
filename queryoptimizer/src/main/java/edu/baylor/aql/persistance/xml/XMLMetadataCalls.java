package edu.baylor.aql.persistance.xml;

/**
 * @deprecated
 *             This class is just an XML metadata calls holder and at this
 *             moment is not intended to be used.
 * 
 * @author Rovshen Nazarov. Baylor University. Aug 25, 2014.All Rights Reserved.
 * 
 */
@Deprecated
public final class XMLMetadataCalls {

    public void collectionOfCalls() {
        // final String hdfsLoadPath =
        // xmlProcess.gtNodeValue(rootGraphPaths,"loadGraphPaths", "loadPath",
        // "type",
        // "hdfs");
        // final String resultPath =
        // xmlProcess.getNodeValue(rootGraphPaths,"resultGraphPaths",
        // "resultPath", "type", "hdfs");
        // Get graph path form metadata.
        // final String graphPath1 = xmlProcess.getNodeValue(rootGraphPaths,
        // "inputGraphs", "graph", "id", graph1);
        // final String graphPath2 = xmlProcess.getNodeValue(rootGraphPaths,
        // "inputGraphs", "graph", "id", graph2);
        // final String resulGraphPath =
        // xmlProcess.saveIntermediateResultPath(count,
        // resultGraphPath.toString(),graphIDs, rootGraphPaths, QUERY_ID);
        // operators = xmlProcess.getNodeList(root, "Commands", "Command",
        // "type", operator);
        // QueryManager xmlProcess.updatePathTable(idMapPathPropDocPath,
        // idMapPathPropDoc);
    }
}
