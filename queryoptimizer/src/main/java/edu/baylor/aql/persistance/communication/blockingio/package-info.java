/**
 * This package contains the standard blocking or synchronous communication service, where for each 
 * connection there is an open socket and a dedicated thread to maintain that communication.  
 */
package edu.baylor.aql.persistance.communication.blockingio;