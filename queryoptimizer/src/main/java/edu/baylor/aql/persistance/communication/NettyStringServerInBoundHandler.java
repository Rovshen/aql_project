package edu.baylor.aql.persistance.communication;

import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.websocketx.WebSocketServerProtocolHandler;

import java.util.concurrent.BlockingQueue;

import org.apache.log4j.Logger;

import edu.baylor.aql.dataaccess.IMetadata;
import edu.baylor.aql.dataaccess.Metadata;
import edu.baylor.aql.entities.client.ClientSession;
import edu.baylor.aql.util.Util;

/**
 * This is NIO based request handler that uses Netty framework to get and
 * process user requests. This service defines actions to be performed for the
 * cases:
 * <ol>
 * <li>When user connects,
 * <li>When data read from the user,
 * <li>When exception is caught during message exchange,
 * <li>When the user requests connection to be closed,
 * <li>When the user handler is removed.
 * </ol>
 * This server will use Netty see <a
 * href="http://netty.io/">http://netty.io/</a> framework for the network
 * connection. The version we use is netty-4.0.23.Final.tar.bz2 ‐ 15-Aug-2014
 * (Stable, Recommended). Netty is distributed under Apache License, v2.0 see <a
 * href="http://www.apache.org/licenses/LICENSE-2.0">http://www.apache.org
 * /licenses/LICENSE-2.0</a>. Please see the en­closed NOTICE.txt for more
 * information.
 * 
 * Per client read / write handler
 * 
 * @author Rovshen Nazarov. Baylor University. Sep 8, 2014.All Rights Reserved.
 * 
 */
public final class NettyStringServerInBoundHandler extends
        SimpleChannelInboundHandler<String> {

    static Logger logger = Logger
            .getLogger(NettyStringServerInBoundHandler.class.getName());
    private final BlockingQueue<ClientSession> clientSessions;
    private static final int maxNumberOfClients = 10;
    private static final String delimeter = System.lineSeparator();
    IMetadata dao;

    public NettyStringServerInBoundHandler(
            final BlockingQueue<ClientSession> clientSessions) {
        super();
        this.clientSessions = clientSessions;
        dao = new Metadata();
    }

    @Override
    public void handlerAdded(final ChannelHandlerContext ctx) throws Exception {
        // invoked as many times as the client connects
        final String clientRemoteAddress = ctx.channel().remoteAddress()
                .toString();
        logger.info("at handlerAdded. Added this handler to the list. Client connected from "
                + ctx.channel().remoteAddress().toString());
        /**
         * A ChannelHandler instance can be added to more than one
         * ChannelPipeline.
         * It means a single ChannelHandler instance can have more than one
         * ChannelHandlerContext.
         */
        final ClientSession client = new ClientSession(clientRemoteAddress,
                ctx.channel()).setConnected(true);
        client.setSessionId((long) Util.FAILED_STATUS);
        dao.openConnection();
        client.setSessionId(client.insert(dao));
        dao.closeConnection();
        if (!clientSessions.contains(client)) {
            if (clientSessions.size() < maxNumberOfClients) {
                clientSessions.put(client);
            } else {
                final String msg = "Sorry! AQL server can support maximum "
                        + maxNumberOfClients
                        + " simultanius users. Please try to reconnect later.";
                final ChannelFuture future = ctx.writeAndFlush(msg);
                logger.info(msg + " next client session id = "
                        + client.getSessionId());
                /**
                 * ctx.disconnect()
                 * Request to disconnect from the remote peer and
                 * notify the ChannelFuture once the operation completes,
                 * either because the operation was successful or because of an
                 * error.
                 */
                future.addListener(ChannelFutureListener.CLOSE);
                // remove channel handler to reduce overhead
                ctx.channel().pipeline().remove(this); // TODO check this does
                                                       // not affect other
                                                       // clients
            }
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * io.netty.channel.ChannelHandlerAdapter#handlerRemoved(io.netty.channel
     * .ChannelHandlerContext)
     */
    @Override
    public void handlerRemoved(final ChannelHandlerContext ctx)
            throws Exception {
        final String clientRemoteAddress = ctx.channel().remoteAddress()
                .toString();
        ClientSession clientToRemove = null;
        for (final ClientSession client : clientSessions) {
            if (client.getRemoteAddress().equals(clientRemoteAddress)) {
                logger.info("Client with session id = " + client.getSessionId()
                        + " was disconnected");
                clientToRemove = client;
            }
        }
        if (clientToRemove != null) {
            clientToRemove.setConnected(false);

            dao.openConnection();
            clientToRemove.update(dao);
            dao.closeConnection();
            // client parsing is blocking, while waiting for messages
            clientToRemove.getMsgsFromClient().offer("");
            clientSessions.remove(clientToRemove);
        }
    }

    /**
     * Remove processing of a remote client from the context.
     * 
     * @param ctx
     * @param client
     */
    @SuppressWarnings("unused")
    private void closeConnection(final ChannelHandlerContext ctx,
            final ClientSession client) {
        // remove channel handler as soon as possible to reduce overhead
        ctx.channel().pipeline().remove(this);
        logger.info("removed handler for the client " + client.getSessionId());
    }

    /**
     * Read a frame, where frame is defined by delimiter specified on server
     * initialization
     */
    @Override
    protected void channelRead0(final ChannelHandlerContext ctx,
            final String msg) throws Exception {
        final String clientRemoteAddress = ctx.channel().remoteAddress()
                .toString();
        for (final ClientSession client : clientSessions) {
            if (client.getRemoteAddress().equals(clientRemoteAddress)) {
                /**
                 * Save read message in the concurrent queue
                 * Producer consumer pattern.
                 */
                final String msgFromClient = new String(msg);
                client.getMsgsFromClient().put(msgFromClient);
            }
        }
    }

    /**
     * Write back to the client. Write only once done reading from the client.
     * This reduces number of system calls.
     */
    @Override
    public void channelReadComplete(final ChannelHandlerContext ctx) {
    }

    /**
     * Handle protocol/handshake
     */
    @Override
    public void userEventTriggered(final ChannelHandlerContext ctx,
            final Object evt) throws Exception {
        if (evt == WebSocketServerProtocolHandler.ServerHandshakeStateEvent.HANDSHAKE_COMPLETE) {
            ctx.pipeline().remove(NettyHTTPRequestHandler.class);
        } else {
            super.userEventTriggered(ctx, evt);
        }
    }

    @Override
    public void channelActive(final ChannelHandlerContext ctx) {
        logger.info("at channelActive Client connected from "
                + ctx.channel().remoteAddress().toString());
    }

    @Override
    public void exceptionCaught(final ChannelHandlerContext ctx,
            final Throwable cause) {
        // Close the connection when an exception is raised.
        logger.error("Exception occured whiler receiving client message."
                + cause.getMessage());
        // send a response message with an error code before closing the
        // connection.
        ctx.writeAndFlush("Error occured in the communication. Please try to reconnect."
                + delimeter);
        ctx.close();
    }
}
