package edu.baylor.aql.entities.tools;

import java.io.Serializable;
import java.sql.Date;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

import edu.baylor.aql.dataaccess.IMetadata;
import edu.baylor.aql.entities.Element;
import edu.baylor.aql.services.requesthandler.pipeline.Visitor;
import edu.baylor.aql.util.AQLException;

/**
 * Tool such as Hadoop or Graphlab that executes commands or operations.
 * 
 * The tool entity holds specific metadata related to a specific tool, such as
 * Hadoop. Such tool data will be used by
 * command composer service to generate executions using given tool. This tool
 * entity will also be used by plan
 * generator service to select execution plan based on the tool performance.
 * 
 * @author Rovshen Nazarov. Baylor University. Aug 12, 2014.All Rights Reserved.
 * 
 */
public final class Tool extends Element implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = -4936420275218632815L;
    /**
     * Database generated id.
     */
    private Long toolID;
    /**
     * The name of the tool, e.g. hadoop
     */
    private String name;
    /**
     * Tool's prefix part, e.g. hadoop jar ..., java jar ..., etc.
     */
    private String prefix;
    /**
     * SysPropRequirements
     * 
     * Command settings, e.g. # of CPUs to use.
     * This map stores Tool's attribute name and value pair.
     */
    private Map<String, String> systemProperMap;
    /**
     * Absolute path to the tool on a File System.
     */
    private String path;
    /**
     * The date when the tool was added.
     */
    private Date created;

    private String comment;

    /**
     * Execution environment, e.g. Linux, Sun OS, Mac, Cross platform.
     */
    private String executionEnvironment;
    /**
     * To be used to construct SQL statement
     */
    private final StringBuilder sb = new StringBuilder();

    /**
     * Empty constructor.
     */
    public Tool() {
    }

    /** Setters use fluent API design like in String Builder append */

    /**
     * @return the toolID
     */
    public Long getId() {
        return toolID;
    }

    /**
     * @return the comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * @param comment
     *            the comment to set
     * @return
     */
    public Tool setComment(final String comment) {
        this.comment = comment;
        return this;
    }

    /**
     * @param toolId
     *            the toolID to set
     */
    public Tool setId(final Long toolId) {
        toolID = toolId;
        return this;
    }

    /**
     * @return the prefix
     */
    public String getPrefix() {
        return prefix;
    }

    /**
     * @param prefix
     *            the prefix to set
     */
    public Tool setPrefix(final String prefix) {
        this.prefix = prefix;
        return this;
    }

    /**
     * @return the attributesMap
     */
    public Map<String, String> getSysPropRequirements() {
        return systemProperMap;
    }

    /**
     * @param attributesMap
     *            the attributesMap to set
     */
    public Tool setSysPropRequirements(final Map<String, String> attributesMap) {
        systemProperMap = attributesMap;
        return this;
    }

    /**
     * @return the path
     */
    public String getPath() {
        return path;
    }

    /**
     * @param path
     *            the path to set
     */
    public Tool setPath(final String path) {
        this.path = path;
        return this;
    }

    /**
     * @return the created
     */
    public Date getCreated() {
        return created;
    }

    /**
     * @param created
     *            the created to set
     */
    public Tool setCreated(final Date created) {
        this.created = created;
        return this;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     *            the name to set
     */
    public Tool setName(final String name) {
        this.name = name;
        return this;
    }

    /**
     * @return the executionEnvironment
     */
    public String getExecutionEnvironment() {
        return executionEnvironment;
    }

    /**
     * @param executionEnvironment
     *            the executionEnvironment to set
     */
    public Tool setExecutionEnvironment(final String executionEnvironment) {
        this.executionEnvironment = executionEnvironment;
        return this;
    }

    @Override
    public void accept(final Visitor v, final CountDownLatch doneSignal)
            throws InterruptedException, AQLException {
        v.visit(this, doneSignal);

    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime
                * result
                + ((executionEnvironment == null) ? 0 : executionEnvironment
                        .hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((path == null) ? 0 : path.hashCode());
        result = prime * result + ((prefix == null) ? 0 : prefix.hashCode());
        result = prime * result
                + ((systemProperMap == null) ? 0 : systemProperMap.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        final Tool other = (Tool) obj;
        if (executionEnvironment == null) {
            if (other.executionEnvironment != null)
                return false;
        } else if (!executionEnvironment.equals(other.executionEnvironment))
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        if (path == null) {
            if (other.path != null)
                return false;
        } else if (!path.equals(other.path))
            return false;
        if (prefix == null) {
            if (other.prefix != null)
                return false;
        } else if (!prefix.equals(other.prefix))
            return false;
        if (systemProperMap == null) {
            if (other.systemProperMap != null)
                return false;
        } else if (!systemProperMap.equals(other.systemProperMap))
            return false;
        return true;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("Tool [");
        if (toolID != null) {
            builder.append("toolID=").append(toolID).append(", ");
        }
        if (name != null) {
            builder.append("name=").append(name).append(", ");
        }
        if (prefix != null) {
            builder.append("prefix=").append(prefix).append(", ");
        }
        if (systemProperMap != null) {
            builder.append("systemProperMap=").append(systemProperMap)
                    .append(", ");
        }
        if (path != null) {
            builder.append("path=").append(path).append(", ");
        }
        if (created != null) {
            builder.append("created=").append(created).append(", ");
        }
        if (comment != null) {
            builder.append("comment=").append(comment).append(", ");
        }
        if (executionEnvironment != null) {
            builder.append("executionEnvironment=")
                    .append(executionEnvironment).append(", ");
        }
        if (sb != null) {
            builder.append("sb=").append(sb);
        }
        builder.append("]");
        return builder.toString();
    }

    @Override
    public long insert(final IMetadata lookUp) {
        return lookUp.store(this);
    }
}
