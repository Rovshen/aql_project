package edu.baylor.aql.entities.command;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Queue;

import edu.baylor.aql.entities.command.CommandType.OrderingType;
import edu.baylor.aql.entities.operands.Operand;

/**
 * This entity is used to hold list of {@link Command}s and a list of
 * {@link Operand}s related to those {@link Command}s. It also holds a group
 * result {@link Operand}.
 * <p>
 * To be in the group {@link Command}s must be of the same {@link OrderingType}
 * and must be following one another.
 * <p>
 * Group of one command is allowed. Each group associated with one group result
 * {@link Operand}.
 * 
 * @author Rovshen Nazarov. Baylor University. Jan 27, 2015.All Rights Reserved.
 * 
 */
public class OrderingCommandGroup {
    /**
     * List of {@link Operand}s associated with the {@link Command}s in this
     * group.
     */
    private final List<Long> operands;
    /**
     * List of {@link Command}s related to each other by the
     * {@link OrderingType}.
     */
    private final Queue<Command> commands;
    /**
     * The result {@link Operand} for the last command in this group's
     * {@link Command} list.
     * Note that the {@link Command}s in the group are never expected to be
     * shuffled.
     */
    private final Long groupResult;
    /**
     * {@link CommandType} associated with this group
     */
    private final CommandType cType;
    /**
     * Possible orderings for this command group.
     */
    private final Collection<List<Long>> possibleOrderings = new ArrayList<>();

    /**
     * Private parameterized constructor. Called by the builder, thus allowing
     * this object to be immutable, once constructed.
     * 
     * @param operands
     * @param commands
     * @param cType
     * @param groupResult
     */
    private OrderingCommandGroup(final List<Long> operands,
            final Queue<Command> commands, final CommandType cType,
            final Long groupResult) {
        this.operands = operands;
        this.commands = commands;
        this.cType = cType;
        this.groupResult = groupResult;
    }

    /**
     * A helper class that allows making {@link OrderingCommandGroup} an
     * immutable class, and allows building it step by step.
     * 
     * @author Rovshen Nazarov. Baylor University. Jan 29, 2015.All Rights
     *         Reserved.
     * 
     */
    public static class OrderingCommandGroupBuilder {

        /**
         * Nested class variables for building {@link OrderingCommandGroup}.
         */
        private final List<Long> nestedOperands;
        private final Queue<Command> nestedCommands;
        private Long nestedGroupResult;
        private final CommandType nestedCType;

        public OrderingCommandGroupBuilder(final List<Long> nestedOperands,
                final Queue<Command> nestedCommands,
                final CommandType nestedCType) {
            super();
            this.nestedOperands = nestedOperands;
            this.nestedCommands = nestedCommands;
            this.nestedCType = nestedCType;
        }

        public OrderingCommandGroupBuilder groupResult(final Long gResultO) {
            nestedGroupResult = gResultO;
            return this;
        }

        /**
         * @return the nestedOperands
         */
        public List<Long> getNestedOperands() {
            return nestedOperands;
        }

        /**
         * @return the nestedCommands
         */
        public Queue<Command> getNestedCommands() {
            return nestedCommands;
        }

        public OrderingCommandGroup createOrderingCommandGroup() {
            return new OrderingCommandGroup(nestedOperands, nestedCommands,
                    nestedCType, nestedGroupResult);
        }
    }

    /**
     * @return the possibleOrderings
     */
    public Collection<List<Long>> getPossibleOrderings() {
        return possibleOrderings;
    }

    /**
     * @return the cType
     */
    public CommandType getCommType() {
        return cType;
    }

    /**
     * @return the operands
     */
    public List<Long> getOperands() {
        return operands;
    }

    /**
     * @return the commands
     */
    public Queue<Command> getCommands() {
        return commands;
    }

    /**
     * @return the groupResult
     */
    public Long getGroupResult() {
        return groupResult;
    }
}
