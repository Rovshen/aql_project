/**
 * This package contains operands related entities used in AQL.
 * It contains general (abstract) entity {@link edu.baylor.aql.entities.operands.Operand} and 
 * specific implementations of that general entity, which differ by the storage style, and structure. 
 */
package edu.baylor.aql.entities.operands;

