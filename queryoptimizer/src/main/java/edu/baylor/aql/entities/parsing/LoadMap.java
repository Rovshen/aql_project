package edu.baylor.aql.entities.parsing;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

import edu.baylor.aql.dataaccess.IMetadata;
import edu.baylor.aql.entities.Element;
import edu.baylor.aql.services.requesthandler.pipeline.Visitor;
import edu.baylor.aql.util.AQLException;
import edu.baylor.aql.util.Util;

/**
 * This parse entity is used to hold received key value pairs from the client
 * request.
 * 
 * @author Rovshen Nazarov. Baylor University. Jan 14, 2015.All Rights Reserved.
 * 
 */
public final class LoadMap extends Element {

    private final Map<String, String> attValTable;

    public LoadMap() {
        attValTable = new HashMap<>();
    }

    /**
     * PRE: the key - value entry is unique for the key
     * 
     * @param mapEntry
     */
    public void addAttVal(final String[] mapEntry) {
        attValTable.put(mapEntry[0], mapEntry[1]);
    }

    @Override
    public void accept(final Visitor v, final CountDownLatch doneSignal)
            throws InterruptedException, AQLException {
        v.visit(this, doneSignal);

    }

    /**
     * @return the attValTable
     */
    public Map<String, String> getAttValTable() {
        return attValTable;
    }

    @Override
    public long insert(final IMetadata lookUp) {
        // TODO
        return Util.FAILED_STATUS;

    }

}
