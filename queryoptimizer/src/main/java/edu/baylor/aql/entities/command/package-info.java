/**
 * This package contains entities related to the notion of Command. Command in AQL is similar to operator in algebra, such as plus operator. 
 * Command may have {@link edu.baylor.aql.entities.operands.Operand}s and other properties as specified by each command related entity.
 */
package edu.baylor.aql.entities.command;

