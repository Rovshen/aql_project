package edu.baylor.aql.entities.parsing;

import edu.baylor.aql.entities.command.CommandAlgorithm;
import edu.baylor.aql.entities.command.CommandType;
import edu.baylor.aql.entities.operands.Operand;
import edu.baylor.aql.entities.tools.Tool;

/**
 * Type of the load that could be performed, such as loading of a {@link Tool},
 * {@link CommandAlgorithm}, {@link CommandType}, and {@link Operand}.
 * 
 * @author Rovshen Nazarov. Baylor University. Jan 14, 2015.All Rights Reserved.
 * 
 */
public enum LoadType {
    TOOL("tool"), COMMAND_ALGORITHM("command_algorithm"), COMMAND_TYPE(
            "command_type"), OPERAND("operand");
    private String label;

    private LoadType(final String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    /**
     * Return ImplementationType object given the String code. Note that
     * the String code must be a member of the enumeration If the
     * code is not part of predefined enumeration null is returned.
     * 
     * @param code
     *            String code that is defined for this enum type
     * @return type The type based on the String code. if String
     *         code unknown return null.
     */
    public static LoadType getEnumByString(final String code) {
        for (final LoadType type : LoadType.values()) {
            if (type.name().equals(code)) {
                return type;
            }
        }
        return null;
    }

}
