/**
 * This package contains entities used in AQL. 
 * <p>
 * All entities use fluent interface for setting object attributes.
 * A fluent interface as first coined by Eric Evans and Martin Fowler is 
 * an implementation of an object oriented API that aims to provide for more readable code.
 * <p>
 * As one of the authors of a fluent interface described it on his page [1] "the interface
 * is normally implemented by using method cascading (concretely method chaining) to 
 * relay the instruction context of a subsequent call".
 * <p>
 * An example of a use of fluent interface in this project would be:
 * Object obj = new Object();
 * <p><ul>
 * <li>obj.set(val).set(val).set(val).set(val);
 * <ul><p>
 * As the author of the fluent interface says it can be used for different purposes
 * in our case we use it for object setting.
 * <p>
 * Source [1] <a href="http://www.martinfowler.com/bliki/FluentInterface.html">http://www.martinfowler.com/bliki/FluentInterface.html</a>
 */
package edu.baylor.aql.entities;

