package edu.baylor.aql.entities.operands;

import java.util.concurrent.CountDownLatch;

import edu.baylor.aql.services.requesthandler.pipeline.Visitor;
import edu.baylor.aql.util.AQLException;
import edu.baylor.aql.util.Util;

public final class ComputedOperand extends Operand {

    private static final long serialVersionUID = 7805875897231270135L;

    /**
     * Size in Megabytes.
     */
    private Long estSize;
    /**
     * Reusable string builder.
     */
    private final StringBuilder strBuilder = new StringBuilder();
    private final boolean isEstimate;

    public ComputedOperand() {
        isEstimate = true;
        final boolean loadedStat = false;
        super.setId(Util.FAILED_STATUS);
        super.setStatus(loadedStat);
        super.setImplementationType(OperandType.COMP_OPERAND.getLabel());
    }

    /**
     * @return the estSize
     */
    public Long getEstSize() {
        return estSize;
    }

    /**
     * @param estSize
     *            the estSize to set
     */
    public void setEstSize(final Long estSize) {
        this.estSize = estSize;
    }

    /**
     * @return the isEstimate
     */
    public boolean isEstimate() {
        return isEstimate;
    }

    /**
     * Return human readable version of Operand Id.
     */
    @Override
    public String getReadableId() {
        final String estOperandPrefix = "estimated_operand";
        strBuilder.setLength(0); // clear string builder
        strBuilder.append(estOperandPrefix).append(super.getId());
        return strBuilder.toString();
    }

    /**
     * Get an SQL insert statement for an Implementation Operand specific
     * attributes.
     * 
     * @param operandId
     * @return
     */
    public String getSpecificSQLInsert(final Long operandId) {
        final StringBuilder builder = new StringBuilder();
        builder.append("INSERT INTO  graph (operandId, estSize")
                .append(") VALUES (").append(operandId).append(", ")
                .append(estSize).append(");");
        return builder.toString();
    }

    @Override
    public String getOperandName() {
        return Long.toString(getId());
    }

    @Override
    public void accept(final Visitor v, final CountDownLatch doneSignal)
            throws InterruptedException, AQLException {
        v.visit(this, doneSignal);
    }

    @Override
    public Operand clone() {
        // TODO Auto-generated method stub
        final Operand clone = new ComputedOperand()
                .setImplementationType(getImplementationType().getLabel())
                .setCommand(super.getCommand())
                .setId(super.getId())
                .setAliasName(super.getAliasName())
                .setLoadedPath(super.getLoadedPath())
                .setStatus(super.getStatus())
                .setType(
                        (super.getType() != null ? super.getType().getLabel()
                                : null));
        return clone;
    }

}
