/**
 * This package contains client related entities such as {@link edu.baylor.aql.entities.client.ClientSession} 
 * to retain and pass information related to a single client.
 */
package edu.baylor.aql.entities.client;

