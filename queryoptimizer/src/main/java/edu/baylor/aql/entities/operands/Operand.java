package edu.baylor.aql.entities.operands;

import java.io.File;
import java.io.Serializable;
import java.sql.Date;

import edu.baylor.aql.dataaccess.IMetadata;
import edu.baylor.aql.entities.Element;
import edu.baylor.aql.entities.command.Command;
import edu.baylor.aql.entities.command.CommandAlgorithm;
import edu.baylor.aql.util.Util;

/**
 * General operand object for the {@link Command}. This class is expected to be
 * extended by different types of operands.
 * <p>
 * This element needs to implement {@link Comparable} for generating orderings.
 * 
 * @author Rovshen Nazarov. Baylor University. Sep 1, 2014.All Rights Reserved.
 * 
 */
public abstract class Operand extends Element implements Serializable,
        Cloneable, Comparable<Operand> {

    private static final long serialVersionUID = 962264756456833468L;
    private long Id;
    /**
     * Set to true when this operand is ready to be used in a {@link Command}
     * execution, and false if the {@link Operand} is not ready to be used in
     * the execution.
     */
    private boolean status;
    private String initialPath; // initial path
    private String loadedPath; // final path used for graph manipulation
    private Date created;
    private String queryId;
    /**
     * Command this operand belongs to.
     */
    private Command command;
    /**
     * Used to determine the operand name and if it is a folder or a file.
     */
    private File file;
    /**
     * Refer to {@link OperandState}
     */
    private OperandState state;
    /**
     * Implementation type defined by user when {@link Operand} is loaded, e.g.
     * load a graph type.
     * <p>
     * Operations between the same type {@link Operand}s are permitted. If
     * operation is on {@link Operand}s with different types, a conversion is
     * required. {@link Operand} types. The implementation type of
     * {@link Operand}s accepted by the {@link CommandAlgorithm} is defined when
     * the command algorithm is loaded.
     */
    private OperandType implementationType;
    /**
     * Custom name. Default is implementation name.
     */
    private String customName;
    private static final int DEFAULT_PARSE_LINE_NUMBER = -1;
    /**
     * The line number defined by the Command Line Parser. This number
     * represents position of this object in the user request.
     * Default is {@link Util#FAILED_STATUS}.
     * Based on the Command Line parser, this number always starts at 1.
     * NOTE we rely on the fact that the default parse line is -1, e.g. smaller
     * than {@link Command}'s parse line number that is positive.
     */
    private int parseLineNumber = DEFAULT_PARSE_LINE_NUMBER;

    /**
     * Is this command is part of the inner query within an AQL query
     */
    private boolean inSubQuery;
    private static final int DEFAULT_SUB_QUERY_NUMBER = -1;
    /**
     * Based on the Command Line parser, this number always starts at 1.
     * This value represents a sequence number of appearance of the subquery
     * within a query.
     */
    private int subQueryNumber = DEFAULT_SUB_QUERY_NUMBER;
    private String aliasName;

    /**
     * Implementation type of the {@link Operand}, such as graph, table, etc
     * represents a structure represented by this logical object.
     * 
     * @author Rovshen Nazarov. Baylor University. Feb 9, 2015.All Rights
     *         Reserved.
     * 
     */
    public enum OperandType {
        GRAPH("graph"), TABLE("table"), UNSTRUCTURED("unstructured"), COMP_OPERAND(
                "computedOperand");

        private final String label;

        /**
         * Constructor to set the label of this enumeration object.
         * 
         * @param label
         *            The label that we want to set for this
         *            instance of the ImplementationType.
         */
        OperandType(final String label) {
            this.label = label;
        }

        public String getLabel() {
            return label;
        }

        @Override
        public String toString() {
            return label;
        }

        /**
         * Return ImplementationType object given the String code. Note that
         * the String code must be a member of the enumeration If the
         * code is not part of predefined enumeration null is returned.
         * 
         * @param code
         *            String code that is defined for this enum type
         * @return type The type based on the String code. if String
         *         code unknown return null.
         */
        public static OperandType getEnumByString(final String code) {
            for (final OperandType type : OperandType.values()) {
                if (type.getLabel().equals(code)) {
                    return type;
                }
            }
            return null;
        }
    }

    /**
     * Operand type defined by user when AQL query supplied, e.g.
     * graph_1 UNION graph_1 we implement union between two graphs.
     * <p>
     * Only operations between the same type {@link Operand}s are permitted. If
     * operation is on {@link Operand}s with different types, a conversion is
     * required. {@link Operand} types
     */
    /**
     * This enumeration class represents types of operands supported by AQL.
     * The estimated result type is to hold the result estimation for the
     * command result during execution plan generation.
     * 
     * @author Rovshen Nazarov. Baylor University. Sep 1, 2014.All Rights
     *         Reserved.
     * 
     */
    public enum OperandState {
        LOADED("loaded"), RESULT("result"), INTERMEDIATE_RESULT(
                "intermediate result"), TO_DELETE("to delete"), DELETED(
                "deleted"), ESTIMATED_RESULT("estimated result");

        private final String label;

        /**
         * Constructor to set the label of this enumeration object.
         * 
         * @param label
         *            The label that we want to set for this
         *            instance of the OperandType.
         */
        OperandState(final String label) {
            this.label = label;
        }

        public String getLabel() {
            return label;
        }

        @Override
        public String toString() {
            return label;
        }

        /**
         * Return OperandType object given the String code. Note that
         * the String code must be a member of the enumeration If the
         * code is not part of predefined enumeration null is returned.
         * 
         * @param code
         *            String code that is defined for this enum type
         * @return type The type based on the String code. if String
         *         code unknown return null.
         */
        public static OperandState getEnumByString(final String code) {
            for (final OperandState type : OperandState.values()) {
                if (type.label.equals(code)) {
                    return type;
                }
            }
            return null;
        }
    }

    public Operand() {
    }

    /**
     * @return the aliasName
     */
    public final String getAliasName() {
        return aliasName;
    }

    /**
     * @param aliasName
     *            the aliasName to set
     * @return
     */
    public final Operand setAliasName(final String aliasName) {
        this.aliasName = aliasName;
        return this;
    }

    /**
     * @return the inSubQuery
     */
    public boolean isInSubQuery() {
        return inSubQuery;
    }

    /**
     * @param inSubQuery
     *            the inSubQuery to set
     * @return
     */
    private void setInSubQuery(final boolean inSubQuery) {
        this.inSubQuery = inSubQuery;
    }

    /**
     * @return the subQueryNumber
     */
    public int getSubQueryNumber() {
        return subQueryNumber;
    }

    /**
     * @param subQueryNumber
     *            the subQueryNumber to set
     * @return
     */
    public Operand setSubQueryNumber(final int subQueryNumber) {
        this.subQueryNumber = subQueryNumber;
        setInSubQuery(true);
        return this;
    }

    /**
     * @return the parseLineNumber
     */
    public int getParseLineNumber() {
        return parseLineNumber;
    }

    /**
     * @param parseLineNumber
     *            the parseLineNumber to set
     * @return
     */
    public Operand setParseLineNumber(final int parseLineNumber) {
        this.parseLineNumber = parseLineNumber;
        return this;
    }

    /**
     * @return the customName
     */
    public String getCustomName() {
        return customName;
    }

    /**
     * @param customName
     *            the customName to set
     * @return
     */
    public Operand setCustomName(final String customName) {
        this.customName = customName;
        return this;
    }

    /**
     * @return the queryId
     */
    public String getQueryId() {
        return queryId;
    }

    /**
     * @param queryId
     *            the queryId to set
     * @return
     */
    public Operand setQueryId(final String queryId) {
        this.queryId = queryId;
        return this;
    }

    /**
     * @return the Id
     */
    public long getId() {
        return Id;
    }

    /**
     * @param Id
     *            the iD to set
     */
    public Operand setId(final long Id) {
        this.Id = Id;
        return this;
    }

    /**
     * @return the readableID
     */
    public abstract String getReadableId();

    /**
     * Status true means operand is real and not logical and ready to be used in
     * the {@link Command}.
     * 
     * @return the status
     */
    public boolean getStatus() {
        return status;
    }

    /**
     * Status true means operand is real and not logical and ready to be used in
     * the {@link Command}.
     * 
     * @param status
     *            the status to set
     */
    public Operand setStatus(final boolean status) {
        this.status = status;
        return this;
    }

    /**
     * @return the initialPath
     */
    public String getInitPath() {
        return initialPath;
    }

    /**
     * @param initialPath
     *            the initialPath to set
     */
    public Operand setInitialPath(final String initialPath) {
        this.initialPath = initialPath;
        return this;
    }

    /**
     * @return the loadedPath
     */
    public String getLoadedPath() {
        return loadedPath;
    }

    /**
     * @param loadedPath
     *            the loadedPath to set
     */
    public Operand setLoadedPath(final String loadedPath) {
        this.loadedPath = loadedPath;
        return this;
    }

    /**
     * @return the created
     */
    public Date getCreated() {
        return created;
    }

    /**
     * @param created
     *            the created to set
     */
    public Operand setCreated(final Date created) {
        this.created = created;
        return this;
    }

    /**
     * @return the command
     */
    public Command getCommand() {
        return command;
    }

    /**
     * @param command
     *            the command to set
     * @return
     */
    public Operand setCommand(final Command command) {
        this.command = command;
        return this;
    }

    /**
     * Used to determine the operand name and if it is a folder or a file.
     * 
     * @return the file
     */
    public File getFile() {
        return file;
    }

    /**
     * @param file
     *            the file to set
     */
    public Operand setFile(final File file) {
        this.file = file;
        return this;
    }

    /**
     * @return the type
     */
    public OperandState getType() {
        return state;
    }

    /**
     * @param type
     *            the type to set
     */
    public Operand setType(final String type) {
        state = OperandState.getEnumByString(type);
        return this;
    }

    /**
     * @return the implemType
     */
    public OperandType getImplementationType() {
        return implementationType;
    }

    /**
     * @param implemType
     *            the implemType to set
     */
    public Operand setImplementationType(final String implemType) {
        implementationType = OperandType.getEnumByString(implemType);
        customName = implementationType.getLabel();
        return this;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + (int) (Id ^ (Id >>> 32));
        result = prime * result
                + ((aliasName == null) ? 0 : aliasName.hashCode());
        result = prime * result + ((command == null) ? 0 : command.hashCode());
        result = prime
                * result
                + ((implementationType == null) ? 0 : implementationType
                        .hashCode());
        result = prime * result
                + ((loadedPath == null) ? 0 : loadedPath.hashCode());
        result = prime * result + parseLineNumber;
        result = prime * result + ((state == null) ? 0 : state.hashCode());
        result = prime * result + (status ? 1231 : 1237);
        result = prime * result + subQueryNumber;
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        final Operand other = (Operand) obj;
        if (Id != other.Id)
            return false;
        if (aliasName == null) {
            if (other.aliasName != null)
                return false;
        } else if (!aliasName.equals(other.aliasName))
            return false;
        if (command == null) {
            if (other.command != null)
                return false;
        } else if (!command.equals(other.command))
            return false;
        if (implementationType != other.implementationType)
            return false;
        if (loadedPath == null) {
            if (other.loadedPath != null)
                return false;
        } else if (!loadedPath.equals(other.loadedPath))
            return false;
        if (parseLineNumber != other.parseLineNumber)
            return false;
        if (state != other.state)
            return false;
        if (status != other.status)
            return false;
        if (subQueryNumber != other.subQueryNumber)
            return false;
        return true;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("Operand [Id=").append(Id).append(", status=")
                .append(status).append(", parse line number=")
                .append(parseLineNumber).append(", ");
        builder.append("sub query seq number=").append(subQueryNumber)
                .append(", ");
        if (getReadableId() != null) {
            builder.append("readableId=").append(getReadableId()).append(", ");
        }
        if (initialPath != null) {
            builder.append("initialPath=").append(initialPath).append(", ");
        }
        if (state != null) {
            builder.append("type=").append(state).append(", ");
        }
        if (command != null) {
            builder.append("commandId=").append(command.getCommandSeqNumber())
                    .append(", ");
        }
        if (implementationType != null) {
            builder.append("implementationType=").append(implementationType)
                    .append(", ");
        }
        builder.append("alias name=").append(aliasName);

        builder.append("]").append(Util.LINE_SEPARATOR);
        return builder.toString();
    }

    @Override
    public long insert(final IMetadata dao) {
        return dao.store(this);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#clone()
     */
    @Override
    public abstract Operand clone();

    public abstract String getOperandName();

    /*
     * Note: all computed Operands will have -1 for the id, all other operands
     * will have id greater or equal to 0.
     * 
     * We compare Operands based on their id to perform natural ordering.
     * 
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    @Override
    public int compareTo(final Operand obj) {
        if (obj == null)
            throw new NullPointerException(
                    "The Operator to compare to was null.");

        if (obj.getClass().isInstance(this.getClass()))
            throw new ClassCastException(
                    "The provided object is not of the Operand type.");

        final int equal = 0;
        final int less = -1;
        final int greater = 1;
        if (this == obj)
            return equal;
        final Operand other = obj;
        if (Id > other.Id)
            return greater;
        else if (Id < other.Id)
            return less;
        else if (command != null && other.command != null) {
            if (command.getCommandSeqNumber() < other.command
                    .getCommandSeqNumber())
                return less;
            else if (command.getCommandSeqNumber() > other.command
                    .getCommandSeqNumber())
                return greater;
        }
        return equal;

    }
}
