package edu.baylor.aql.entities.query;

import edu.baylor.aql.entities.Element;
import edu.baylor.aql.entities.client.ClientSession;
import edu.baylor.aql.entities.command.CommandAlgorithm;
import edu.baylor.aql.entities.command.CommandType;
import edu.baylor.aql.entities.operands.Operand;
import edu.baylor.aql.entities.tools.Tool;
import edu.baylor.aql.services.requesthandler.pipeline.EPipelineSingleton;

public final class IntermediateLanguageObject {
    /**
     * Type of the parsing process, such as LOAD or QUERY.
     * This type is used to define what {@link EPipelineSingleton} to invoke.
     * 
     * This type for more general processing based on more general object type.
     * 
     * @author Rovshen Nazarov. Baylor University. Nov 15, 2014.All Rights
     *         Reserved.
     * 
     */
    public enum ParseRequestType {
        LOAD("load"), QUERY("query");
        private final String label;

        private ParseRequestType(final String label) {
            this.label = label;
        }

        public String getLabel() {
            return label;
        }
    }

    /**
     * Type of the parsed object, such as {@link Operand}, {@link CommandType},
     * {@link CommandAlgorithm}, {@link Tool}, {@link Query}, and UsingQuery.
     * This type is used to define how to process the
     * {@link IntermediateLanguageObject}.
     * 
     * We need more specific type for specific service to parse the object.
     * 
     * @author Rovshen Nazarov. Baylor University. Nov 15, 2014.All Rights
     *         Reserved.
     * 
     */
    public enum ParseObjType {
        LOAD_OPERAND("operand"), LOAD_COMMANDTYPE("command type"), LOAD_COMMANDALGORITHM(
                "command algorithm"), LOAD_TOOL("tool"), QUERY("query"), USING_QUERY(
                "using query"), COMPOUND_QUERY("compound query");
        private final String label;

        private ParseObjType(final String label) {
            this.label = label;
        }

        public String getLabel() {
            return label;
        }

        /**
         * Return ImplementationType object given the String code. Note that
         * the String code must be a member of the enumeration If the
         * code is not part of predefined enumeration null is returned.
         * 
         * @param code
         *            String code that is defined for this enum type
         * @return type The type based on the String code. if String
         *         code unknown return null.
         */
        public static ParseObjType getEnumByString(final String code) {
            for (final ParseObjType type : ParseObjType.values()) {
                if (type.getLabel().equals(code)) {
                    return type;
                }
            }
            return null;
        }
    }

    private final StringBuilder parseMsg = new StringBuilder("");
    /**
     * Defines parsed object's type, such as BasicQuery, UsingQuery, Operand,
     * Table, Graph, etc.
     * 
     * Based on this type later reqestObj will be down-casted to appropriate
     * type.
     */
    private ParseObjType parseObjType;
    /**
     * Defines parse type such as Query or Load
     */
    private ParseRequestType parseType;
    /**
     * Keep {@link ClientSession} with this object, so that we can access client
     * specific settings and permissions.
     */
    private ClientSession clientSession;
    private Element reqestObj;
    /**
     * 0 ok, non zero error (-1).
     */
    private int parseStateCode;

    /**
     * @return the parseType
     */
    public ParseRequestType getParseType() {
        return parseType;
    }

    /**
     * @param parseType
     *            the parseType to set
     */
    public void setParseType(final ParseRequestType parseType) {
        this.parseType = parseType;
    }

    /**
     * @return the parseStateCode
     */
    public int getParseStateCode() {
        return parseStateCode;
    }

    /**
     * @param parseStateCode
     *            the parseStateCode to set
     */
    public void setParseStateCode(final int parseStateCode) {
        this.parseStateCode = parseStateCode;
    }

    /**
     * @return the parseMsg
     */
    public StringBuilder getParseMsg() {
        return parseMsg;
    }

    /**
     * @return the type
     */
    public ParseObjType getObjType() {
        return parseObjType;
    }

    /**
     * @return the reqestObj
     */
    public Element getReqestObj() {
        return reqestObj;
    }

    /**
     * @param type
     *            the type to set
     */
    public void setType(final ParseObjType type) {
        parseObjType = type;
    }

    /**
     * @param reqestObj
     *            the reqestObj to set
     */
    public void setReqestObj(final Element reqestObj) {
        this.reqestObj = reqestObj;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("IntermediateLanguageObject [parseMsg=")
                .append(parseMsg).append(", parseObjType=")
                .append(parseObjType).append(", parseType=").append(parseType)
                .append(", reqestObj=").append(reqestObj)
                .append(", parseStateCode=").append(parseStateCode).append("]");
        return builder.toString();
    }

    public void setClientSession(final ClientSession clientSession) {
        this.clientSession = clientSession;
    }

    /**
     * @return the clientSession
     */
    public final ClientSession getClientSession() {
        return clientSession;
    }

}
