package edu.baylor.aql.entities;

import java.util.concurrent.CountDownLatch;

import edu.baylor.aql.dataaccess.IMetadata;
import edu.baylor.aql.services.requesthandler.pipeline.Visitor;
import edu.baylor.aql.util.AQLException;

/**
 * This abstract class has similar purpose to that of {@link Object} in java. It
 * defines common properties that are shared by different AQL entities.
 * 
 * @author Rovshen Nazarov. Baylor University. Nov 10, 2014.All Rights Reserved.
 * 
 */
public abstract class Element {
    private String stateMsg = "";
    private boolean state;

    /**
     * @return the state
     */
    public boolean isErrState() {
        return state;
    }

    /**
     * @param state
     *            the state to set
     * @return
     */
    public Element setErrState(final boolean state) {
        this.state = state;
        return this;
    }

    /**
     * @return the stateMsg
     */
    public String getStateMsg() {
        return stateMsg;
    }

    /**
     * @param stateMsg
     *            the stateMsg to set
     */
    public void setStateMsg(final String stateMsg) {
        this.stateMsg = stateMsg;
    }

    /**
     * Accept {@link Visitor} to process this object.
     * 
     * @param v
     * @param doneSignal
     * @throws InterruptedException
     * @throws AQLException
     */
    public abstract void accept(final Visitor v, final CountDownLatch doneSignal)
            throws InterruptedException, AQLException;

    /**
     * Build an SQL insert statement for this object.
     * For building SQL insert statement we use {@link StringBuilder}.
     * 
     * @param lookUp
     * @return none
     * @throws AQLException
     */
    public abstract long insert(IMetadata lookUp) throws AQLException;

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (state ? 1231 : 1237);
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final Element other = (Element) obj;
        if (state != other.state)
            return false;
        return true;
    }

}
