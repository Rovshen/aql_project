/**
 * Parsing related entities. These entities are used for storing and passing parsed data in a structured manner, such as using {@link edu.baylor.aql.entities.parsing.LoadMap}. 
 * They also used to branch parsing based on parsed request type, such as {@link edu.baylor.aql.entities.parsing.LoadType}.
 *
 */
package edu.baylor.aql.entities.parsing;