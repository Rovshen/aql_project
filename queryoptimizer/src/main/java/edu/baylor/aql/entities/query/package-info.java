/**
 * This package contains query related entities used in AQL. Those entities are used to store data related to the AQL query.
 * It also contains the general object used as a placeholder for the AQL request. 
 * {@link edu.baylor.aql.entities.query.IntermediateLanguageObject} (ILO) is used during request parsing and request object generation and processing.
 * Once the ILO is generated and parsed it is passed to the {@link edu.baylor.aql.services.requesthandler.pipeline.EPipelineSingleton}
 * and processed based on the {@link edu.baylor.aql.entities.query.IntermediateLanguageObject.ParseRequestType} of the parse object.
 */
package edu.baylor.aql.entities.query;

