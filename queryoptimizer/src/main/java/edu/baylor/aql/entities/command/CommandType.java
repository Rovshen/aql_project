package edu.baylor.aql.entities.command;

import java.io.Serializable;
import java.sql.Date;
import java.util.concurrent.CountDownLatch;

import edu.baylor.aql.dataaccess.IMetadata;
import edu.baylor.aql.entities.Element;
import edu.baylor.aql.services.requesthandler.pipeline.Visitor;
import edu.baylor.aql.util.AQLException;

/**
 * This object holds command type specific data. Command Type in AQL Framework
 * refers to an operation name that is intended to have one or more distinct
 * implementations of that operation. An example of such is Set union operation.
 * <p>
 * This object besides name holds other operation related properties that are
 * true for all different implementations of this operation.
 * <p>
 * Note: implementation in this context refers to {@link CommandAlgorithm}.
 * 
 * @author Rovshen Nazarov. Baylor University. Sep 17, 2014.All Rights Reserved.
 * 
 */
public final class CommandType extends Element implements Serializable {

    private static final long serialVersionUID = 5289185088516032705L;

    /**
     * This enumeration class represents types of ordering laws supported by
     * AQL.
     * NONE {@link OrderingType} allows {@link CommandType} to have no ordering.
     * 
     * @author Rovshen Nazarov. Baylor University. Sep 1, 2014.All Rights
     *         Reserved.
     * 
     */
    public enum OrderingType {
        ASSOCIATIVE("associative"), COMMUTATIVE("commutative"), ASSOCIATIVE_COMMUTATIVE(
                "associative commutative"), NONE("none");

        private final String label;

        /**
         * Constructor to set the label of this enumeration object.
         * 
         * @param label
         *            The label that we want to set for this
         *            instance of the OperandType.
         */
        OrderingType(final String label) {
            this.label = label;
        }

        public String getLabel() {
            return label;
        }

        @Override
        public String toString() {
            return label;
        }

        /**
         * Return {@link OrderingType} object given the String code. Note that
         * the String code must be a member of the enumeration. If the
         * code is not part of predefined enumeration null is returned.
         * 
         * @param code
         *            String code that is defined for this enum type
         * @return type The type based on the String code. if String
         *         code unknown return {@link OrderingType#NONE}.
         */
        public static OrderingType getEnumByString(final String code) {
            if (code == null || code == "") {
                return OrderingType.NONE;
            }
            for (final OrderingType type : OrderingType.values()) {
                if (type.label.equals(code)) {
                    return type;
                }
            }
            return OrderingType.NONE;
        }
    }

    /**
     * The {@link OrderingType} for this command type.
     */
    private OrderingType orderingType;

    /**
     * To be used to construct SQL statement
     */
    private final StringBuilder sb = new StringBuilder();
    /**
     * Database generated id, automatically generated on the new row insert.
     */
    private Long Id;
    private String typeName;
    private int minnumbOperands;
    private Date created;
    /**
     * Indicate if this command type is for custom command provided by the user.
     */
    boolean usingCommandType;

    public CommandType() {
    }

    /**
     * @return the usingCommandType
     */
    public final boolean isUsingCommandType() {
        return usingCommandType;
    }

    /**
     * @param usingCommandType
     *            the usingCommandType to set
     * @return
     */
    public final CommandType setUsingCommandType(final boolean usingCommandType) {
        this.usingCommandType = usingCommandType;
        return this;
    }

    /**
     * @return the orderingType
     */
    public OrderingType getOrderingType() {
        return orderingType;
    }

    /**
     * @param orderingType
     *            the orderingType to set
     * @return
     */
    public CommandType setOrderingType(final OrderingType orderingType) {
        this.orderingType = orderingType;
        return this;
    }

    /**
     * @param id
     *            the id to set
     * @return
     */
    public CommandType setId(final Long id) {
        Id = id;
        return this;
    }

    /**
     * @param typeName
     *            the typeName to set
     */
    public CommandType setTypeName(final String typeName) {
        this.typeName = typeName;
        return this;
    }

    /**
     * @param numberOfOperands
     *            the numberOfOperands to set
     */
    public CommandType setNumberOfOperands(final int numberOfOperands) {
        minnumbOperands = numberOfOperands;
        return this;
    }

    /**
     * @param created
     *            the created to set
     */
    public CommandType setCreated(final Date created) {
        this.created = created;
        return this;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return Id;
    }

    /**
     * @return the typeName
     */
    public String getTypeName() {
        return typeName;
    }

    /**
     * @return the numberOfOperands
     */
    public int getMinNumbOper() {
        return minnumbOperands;
    }

    /**
     * @return the created
     */
    public Date getCreated() {
        return created;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + minnumbOperands;
        result = prime * result
                + ((orderingType == null) ? 0 : orderingType.hashCode());
        result = prime * result
                + ((typeName == null) ? 0 : typeName.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        final CommandType other = (CommandType) obj;
        if (minnumbOperands != other.minnumbOperands)
            return false;
        if (orderingType != other.orderingType)
            return false;
        if (typeName == null) {
            if (other.typeName != null)
                return false;
        } else if (!typeName.equals(other.typeName))
            return false;
        return true;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("CommandType [Id=").append(Id).append(", typeName=")
                .append(typeName).append(", numberOfOperands=")
                .append(minnumbOperands).append(", is using=")
                .append(usingCommandType).append(", created=").append(created)
                .append("]");
        return builder.toString();
    }

    @Override
    public void accept(final Visitor v, final CountDownLatch doneSignal)
            throws InterruptedException, AQLException {
        v.visit(this, doneSignal);

    }

    @Override
    public long insert(final IMetadata lookUp) {
        return lookUp.store(this);
    }
}
