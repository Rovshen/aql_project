package edu.baylor.aql.entities.operands;

import java.util.concurrent.CountDownLatch;

import edu.baylor.aql.services.requesthandler.pipeline.Visitor;
import edu.baylor.aql.util.AQLException;

public final class Unstructured extends Operand {

    private static final long serialVersionUID = -5492141334550454728L;

    @Override
    public String getReadableId() {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * Get an SQL insert statement for an Implementation Operand specific
     * attributes.
     * 
     * @param operandId
     * @return
     */
    public String getSpecificSQLInsert(final Long operandId) {
        final StringBuilder builder = new StringBuilder();
        return builder.toString();
    }

    @Override
    public String getOperandName() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void accept(final Visitor v, final CountDownLatch doneSignal)
            throws InterruptedException, AQLException {
        v.visit(this, doneSignal);
    }

    @Override
    public Operand clone() {
        // TODO Auto-generated method stub
        final Operand clone = new Unstructured();
        return clone;
    }

}
