package edu.baylor.aql.entities.query;

import java.io.Serializable;

import edu.baylor.aql.entities.command.Command;
import edu.baylor.aql.entities.command.CommandAlgorithm;
import edu.baylor.aql.entities.operands.Operand;

/**
 * This object contains {@link Command} and {@link CommandAlgorithm} and is
 * mainly used to determine the cost of running given {@link Command} with
 * {@link Operand}s using given {@link CommandAlgorithm} An instance of the
 * execution plan for the given command type.
 * One command type is expected to have one or more plan instances
 * that will be used later to select the most efficient execution
 * plan.
 * 
 * @author Rovshen Nazarov. Baylor University. Aug 7, 2014.All Rights Reserved.
 * 
 */
@Deprecated
public final class QueryPlan implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 9055251948767212822L;
    // TODO
    /**
     * Estimated time it should take this plan to process Command.
     */
    private Integer estimatedTimeToRun;
    /**
     * Estimated average main memory (RAM) requirement for this plan.
     */
    private Integer averageMemoryRequired;

    /**
     * AQL {@link Query} and it has cost field.
     */
    private Query query;

    /**
     * @return the query
     */
    public Query getQuery() {
        return query;
    }

    /**
     * @param query
     *            the query to set
     */
    public void setQuery(final Query query) {
        this.query = query;
    }

    /**
     * @return the estimatedTimeToRun
     */
    public Integer getEstimatedTimeToRun() {
        return estimatedTimeToRun;
    }

    /**
     * @param estimatedTimeToRun
     *            the estimatedTimeToRun to set
     */
    public void setEstimatedTimeToRun(final Integer estimatedTimeToRun) {
        this.estimatedTimeToRun = estimatedTimeToRun;
    }

    /**
     * @return the averageMemoryRequired
     */
    public Integer getAverageMemoryRequired() {
        return averageMemoryRequired;
    }

    /**
     * @param averageMemoryRequired
     *            the averageMemoryRequired to set
     */
    public void setAverageMemoryRequired(final Integer averageMemoryRequired) {
        this.averageMemoryRequired = averageMemoryRequired;
    }
}
