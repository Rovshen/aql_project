package edu.baylor.aql.entities.operands;

import java.util.concurrent.CountDownLatch;

import edu.baylor.aql.entities.command.Command;
import edu.baylor.aql.services.requesthandler.pipeline.Visitor;
import edu.baylor.aql.util.AQLException;

/**
 * This entity contains all necessary attributes for the graph object. This
 * object is generated when the graph is loaded
 * or {@link Command} executes successfully or when the graph is already loaded
 * and retrieved by the user or system from
 * the metadata using graph id.
 * 
 * @author Rovshen Nazarov. Baylor University. Aug 7, 2014.All Rights Reserved.
 * 
 */
public final class Graph extends Operand {

    private static final long serialVersionUID = 7695342482485327657L;
    private static final String implementationType = OperandType.GRAPH
            .getLabel();
    private long estNodesCount;
    private long estEdgesCount;
    /**
     * Graph size in Megabytes.
     * or if graph is not a single file than
     * a folder size in MB.
     */
    private long size;
    /**
     * Value between 0 and 1 computed using V and E
     */
    private Double density;

    /**
     * {@link OperandType} is set to graph.
     */
    public Graph() {
        super.setImplementationType(implementationType);
    }

    @Override
    public String getReadableId() {
        return "graph_" + super.getId();
    }

    public Long getEstNodesCount() {
        return estNodesCount;
    }

    public Graph setEstNodesCount(final Long estNodesCount) {
        this.estNodesCount = estNodesCount;
        return this;
    }

    public Long getEstEdgesCount() {
        return estEdgesCount;
    }

    public Graph setEstEdgesCount(final Long estEdgesCount) {
        this.estEdgesCount = estEdgesCount;
        return this;
    }

    public Long getSize() {
        return size;
    }

    public Graph setSize(final Long size) {
        this.size = size;
        return this;
    }

    public Double getDensity() {
        return density;
    }

    public Graph setDensity(final Double density) {
        this.density = density;
        return this;
    }

    /**
     * Get an SQL insert statement for an Implementation Operand specific
     * attributes.
     * 
     * @param operandId
     * @return
     */
    public String getSpecificSQLInsert(final Long operandId) {
        final StringBuilder builder = new StringBuilder();
        builder.append("INSERT INTO  graph (operandId, size, density")
                .append("estNodesCount, estEdgesCount) VALUES (")
                .append(operandId).append(", ").append(size).append(", ")
                .append(density).append(", ").append(estNodesCount)
                .append(", ").append(estEdgesCount).append(");");
        return builder.toString();
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((density == null) ? 0 : density.hashCode());
        result = prime * result
                + (int) (estEdgesCount ^ (estEdgesCount >>> 32));
        result = prime * result
                + (int) (estNodesCount ^ (estNodesCount >>> 32));
        result = prime * result + (int) (size ^ (size >>> 32));
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        final Graph other = (Graph) obj;
        if (density == null) {
            if (other.density != null)
                return false;
        } else if (!density.equals(other.density))
            return false;
        if (estEdgesCount != other.estEdgesCount)
            return false;
        if (estNodesCount != other.estNodesCount)
            return false;
        if (size != other.size)
            return false;
        return true;
    }

    /**
     * This method retrieves the name of the graph file or
     * the folder name where the graph files are if the graph
     * is represented by several files.
     * 
     * @return String representation of the graph name.
     *         If the {
     *         return readableId;
     *         } graph name could not be retrieved return empty String
     */
    public String getGraphName() {
        if (super.getFile() == null) {
            return "";
        }
        String graphName = "";
        if (super.getFile().isFile()) {
            graphName = super.getFile().getName();
        } else if (super.getFile().isDirectory()) {
            graphName = super
                    .getFile()
                    .getAbsolutePath()
                    .substring(
                            super.getFile().getAbsolutePath().lastIndexOf("/"));
        }
        return graphName;
    }

    /**
     * Convert logical Graph Id to human readable graph Id,
     * by prepending a string to the database generated logical Id.
     * 
     * @param graphId
     *            The Id to covert.
     * @return human readable graph Id
     */
    public String convertToReadableID(final Long graphId) {

        final String graphString = "graph";
        final String separator = "_";
        final StringBuilder buildID = new StringBuilder(graphString).append(
                separator).append(graphId);

        return buildID.toString();
    }

    @Override
    public String getOperandName() {
        return getGraphName();
    }

    @Override
    public void accept(final Visitor v, final CountDownLatch doneSignal)
            throws InterruptedException, AQLException {
        v.visit(this, doneSignal);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#clone()
     */
    @Override
    public Operand clone() {
        final Operand clone = new Graph();

        clone.setCreated(super.getCreated())
                .setCustomName(super.getCustomName())
                .setFile(super.getFile())
                .setId(super.getId())
                .setImplementationType(super.getImplementationType().getLabel())
                .setInitialPath(super.getInitPath())
                .setLoadedPath(super.getLoadedPath())
                .setQueryId(super.getQueryId()).setStatus(super.getStatus())
                .setType(super.getType().getLabel())
                .setAliasName(super.getAliasName())
                .setCommand(super.getCommand())
                .setParseLineNumber(super.getParseLineNumber())
                .setSubQueryNumber(super.getSubQueryNumber())
                .setErrState(super.isErrState());
        ((Graph) clone).setDensity(density).setEstEdgesCount(estEdgesCount)
                .setEstNodesCount(estNodesCount).setSize(size);

        /*
         * clone
         * 
         * .setId(super.getId())
         * .setImplementationType(super.getImplementationType().getLabel())
         * .setLoadedPath(super.getLoadedPath())
         * .setStatus(super.getStatus())
         * .setType(super.getType().getLabel())
         * .setCommand(super.getCommand());
         */
        return clone;
    }
}
