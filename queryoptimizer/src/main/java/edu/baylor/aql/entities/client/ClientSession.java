package edu.baylor.aql.entities.client;

import io.netty.channel.Channel;

import java.io.Serializable;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

import edu.baylor.aql.dataaccess.IMetadata;
import edu.baylor.aql.entities.command.Command;
import edu.baylor.aql.entities.operands.Operand;
import edu.baylor.aql.entities.query.Query;

/**
 * The client session represents a connection period from when client connects
 * to the AQL server until client disconnects from the AQL server.
 * <p>
 * The client session also could be called Current session. The client can have
 * more than one sessions running in parallel.
 * <p>
 * This entity holds client's data and settings for the current session. As well
 * as {@link Channel} to get data from client and send reply back to the client.
 * <p>
 * Client session will be flushed periodically to the metadata, so that if the
 * query processing broke client can get the progress of the last query before
 * the crash. CLient session also holds state of the client:
 * <li>If the client is connected
 * <li>Clients IP address.
 * <li> {@link BlockingQueue} of client messages. The reason we use
 * {@link BlockingQueue} is because we use Netty asynchronous server to read
 * client messages. Thus we do not know when the message will arrive ahead of
 * the time.
 * <p>
 * This class may be extended to have additional role specific state data. E.g.
 * Admin session, user session, etc.
 * 
 * @author Rovshen Nazarov. Baylor University. Sep 14, 2014.All Rights Reserved.
 * 
 */
public class ClientSession implements Serializable {

    private static final long serialVersionUID = 4629443989838489129L;
    private final String remoteAddress;
    private Long sessionId;
    private static final int queueMaxSize = 2048;
    private final BlockingQueue<String> msgsFromClient = new ArrayBlockingQueue<>(
            queueMaxSize);
    private volatile boolean isConnected;
    private final Channel channel;
    private Query lastQuery;
    /**
     * client defines if new tools should be used during the client session
     */
    private boolean useExperimentalTools;
    /**
     * If for each client {@link Command} within a client session.
     * 
     * Id starts at 0 and is incremented to positive infinity.
     */
    private final AtomicInteger commandSessionId = new AtomicInteger(0);
    /**
     * We need somehow to distinguish between different alias {@link Operand}s.
     */
    private final AtomicLong aliesSessionId = new AtomicLong(Integer.MAX_VALUE);
    /**
     * Temporary {@link Command}'s result {@link Operand} id within a client
     * session.
     * 
     * This id starts at -2 and keeps decrementing for each new {@link Command}.
     * The reason to keep decrementing this value is to avoid collision with
     * {@link Operand}'s ids that are generated on
     * {@link IMetadata#store(Operand)}.
     * 
     * We start at -2 because -1 is reserved for failed status
     */
    private final AtomicInteger commandResultTmpId = new AtomicInteger(-2);

    public ClientSession(final String remoteAddress, final Channel channel) {
        this.remoteAddress = remoteAddress;
        this.channel = channel;
    }

    /**
     * @return the aliesSessionId
     */
    public final AtomicLong getAliesSessionId() {
        return aliesSessionId;
    }

    /**
     * @param sessionId
     *            the sessionId to set
     */
    public void setSessionId(final Long sessionId) {
        this.sessionId = sessionId;
    }

    /**
     * @return the commandResultTmpId
     */
    public final AtomicInteger getCommandResultOperId() {
        return commandResultTmpId;
    }

    /**
     * @return the commandCount
     */
    public AtomicInteger getCommandSessionId() {
        return commandSessionId;
    }

    /**
     * @return the remoteAddress
     */
    public String getRemoteAddress() {
        return remoteAddress;
    }

    /**
     * @return the sessionId
     */
    public Long getSessionId() {
        return sessionId;
    }

    /**
     * @return theclientSession lastQueryProgress
     */
    public double getLastQueryProgress() {
        if (lastQuery != null) {
            return lastQuery.getProgress();
        }
        return 0.0;
    }

    /**
     * @return the lastQuery
     */
    public synchronized Query getLastQuery() {
        return lastQuery;
    }

    /**
     * @param lastQuery
     *            the lastQuery to set
     */
    public synchronized void setLastQuery(final Query lastQuery) {
        this.lastQuery = lastQuery;
    }

    /**
     * @return the msgFromClient
     */
    public BlockingQueue<String> getMsgsFromClient() {
        return msgsFromClient;
    }

    /**
     * @return the isConnected
     */
    public synchronized boolean isConnected() {
        return isConnected;
    }

    /**
     * @param isConnected
     *            the isConnected to set
     */
    public synchronized ClientSession setConnected(final boolean isConnected) {
        this.isConnected = isConnected;
        return this;
    }

    /**
     * @return the queueMaxSize
     */
    public static int getQueueMaxSize() {
        return queueMaxSize;
    }

    /**
     * @return the channel
     */
    public Channel getChannel() {
        return channel;
    }

    /**
     * @return the useExperimentalTools
     */
    public boolean isUseExperimentalTools() {
        return useExperimentalTools;
    }

    /**
     * @param useExperimentalTools
     *            the useExperimentalTools to set
     */
    public synchronized void setUseExperimentalTools(
            final boolean useExperimentalTools) {
        this.useExperimentalTools = useExperimentalTools;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((remoteAddress == null) ? 0 : remoteAddress.hashCode());
        result = prime * result
                + ((sessionId == null) ? 0 : sessionId.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final ClientSession other = (ClientSession) obj;
        if (remoteAddress == null) {
            if (other.remoteAddress != null)
                return false;
        } else if (!remoteAddress.equals(other.remoteAddress))
            return false;
        if (sessionId == null) {
            if (other.sessionId != null)
                return false;
        } else if (!sessionId.equals(other.sessionId))
            return false;
        return true;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("ClientSession [remoteAddress=").append(remoteAddress)
                .append(", sessionId=").append(sessionId)
                .append(", msgFromClient=").append(msgsFromClient)
                .append(", msgToClient=").append(", lastQuery=")
                .append(lastQuery).append("]");
        return builder.toString();
    }

    public long insert(final IMetadata lookUp) {
        return lookUp.store(this);
    }

    public int update(final IMetadata lookUp) {
        return lookUp.update(this);

    }
}
