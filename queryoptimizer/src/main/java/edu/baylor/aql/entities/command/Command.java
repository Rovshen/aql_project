package edu.baylor.aql.entities.command;

import java.io.Serializable;
import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import edu.baylor.aql.entities.operands.Operand;
import edu.baylor.aql.entities.tools.Tool;
import edu.baylor.aql.util.Util;

/**
 * This is execution related object and depends on
 * user supplied arguments with AQL query.
 * A command may look like g1 Command g2.
 * Query Command may take different number of arguments other than 2.
 * One user transaction may contain one or more Command elements.
 * <p>
 * Result {@link Operand} appears in two places as part of the operands list and
 * as a result of this {@link Command}.
 * 
 * @author Rovshen Nazarov. Baylor University. Aug 9, 2014.All Rights Reserved.
 * 
 */
public final class Command implements Serializable, Cloneable {

    private static final long serialVersionUID = -7105257800258976152L;
    /**
     * The sequence number of the command for the given client session.
     * The number is set to 1 at the beginning of the session and is incremented
     * for each command in each query until the end of the client session.
     */
    private int commandSeqNumber;
    /**
     * {@link Operand} for the command, the regular commands take
     * two operands, but the number of operands could be
     * any number between 1 and up.
     * Based on the Command Line parser, this number always starts at 1.
     */
    private List<Long> operands = null; // stores operands
                                        // of
    /**
     * An attribute name points to the attribute value.
     */
    private final Map<String, Object> attributes = new HashMap<>();
    /**
     * We assume that the command produces a single result.
     */
    private Long result;
    /**
     * This is a final command that is expected to be executable on Linux or
     * any other OS environments by the related to the {@link CommandAlgorithm}
     * {@link Tool}.
     */
    private String command;
    /**
     * The type of the command, such as UNION, DIFF, INTER, etc.
     * Based on this type relevant {@link Tool} are invoked for this command.
     */
    private String commandType;
    /**
     * The Object of the type of the command, such as UNION, DIFF, INTER, etc.
     * Based on this type relevant {@link Tool} are invoked for this command.
     */
    private CommandType commandTypeObj;
    /**
     * The DEFAULT number of operands is stored in metadata and retrieved based
     * on CommandType.
     */
    private int numberOfOperands;
    /**
     * Estimated operands.
     */
    List<Long> estOper;

    /**
     * {@link CommandAlgorithm} associated with the command.
     * The association is optional and can be changed.
     */
    private CommandAlgorithm commandAlgo;
    /**
     * Cost associated with running this command.
     * Cost is an evaluation measure with some range.
     */
    private int cost;

    /**
     * Indicates if this command was stored before.
     */
    private boolean stored;
    /**
     * Creation date.
     */
    Date created;
    /**
     * Command id from the metadata. Must be unique as if command repeats we
     * reuse its results.
     */
    private long id;
    private static final int DEFAULT_PARSE_LINE_NUMBER = -1;
    /**
     * The line number defined by the Command Line Parser. This number
     * represents position of this object in the user request.
     * Default is {@link Util#FAILED_STATUS}.
     * Based on the Command Line parser, this number always starts at 1.
     */
    private int parseLineNumber = DEFAULT_PARSE_LINE_NUMBER;

    /**
     * Is this command is part of the inner query within an AQL query
     */
    private boolean inSubQuery;
    public static final int DEFAULT_SUB_QUERY_NUMBER = -1;
    /**
     * Based on the Command Line parser, this number always starts at 1.
     * This value represents a sequence number of appearance of the subquery
     * within a query.
     * NOTE we rely on the sub query sequence number to be -1 if non defined as
     * it is considered on the query level, where there is no sub queries.
     */
    private int subQueryNumber = DEFAULT_SUB_QUERY_NUMBER;

    /**
     * Constructor
     */
    public Command() {
        commandSeqNumber = Util.FAILED_STATUS; // default sequence number
    }

    /**
     * @return the inSubQuery
     */
    public boolean isInSubQuery() {
        return inSubQuery;
    }

    /**
     * @param inSubQuery
     *            the inSubQuery to set
     * @return
     */
    private void setInSubQuery(final boolean inSubQuery) {
        this.inSubQuery = inSubQuery;
    }

    /**
     * @return the subQueryNumber
     */
    public int getSubQueryNumber() {
        return subQueryNumber;
    }

    /**
     * @param subQueryNumber
     *            the subQueryNumber to set
     * @return
     */
    public Command setSubQueryNumber(final int subQueryNumber) {
        this.subQueryNumber = subQueryNumber;
        setInSubQuery(true);
        return this;
    }

    /**
     * @return the parseLineNumber
     */
    public int getParseLineNumber() {
        return parseLineNumber;
    }

    /**
     * @param parseLineNumber
     *            the parseLineNumber to set
     * @return
     */
    public Command setParseLineNumber(final int parseLineNumber) {
        this.parseLineNumber = parseLineNumber;
        return this;
    }

    /**
     * @return the created
     */
    public Date getCreated() {
        return created;
    }

    /**
     * @param created
     *            the created to set
     * @return
     */
    public Command setCreated(final Date created) {
        this.created = created;
        return this;
    }

    /**
     * @return the stored
     */
    public boolean isStored() {
        return stored;
    }

    /**
     * @param stored
     *            the stored to set
     * @return
     */
    public Command setStored(final boolean stored) {
        this.stored = stored;
        return this;
    }

    /**
     * @return the commandTypeObj
     */
    public CommandType getCommandTypeObj() {
        return commandTypeObj;
    }

    /**
     * @param commandTypeObj
     *            the commandTypeObj to set
     * @return
     */
    public Command setCommandTypeObj(final CommandType commandTypeObj) {
        this.commandTypeObj = commandTypeObj;
        return this;
    }

    /**
     * @return the estOper
     */
    public List<Long> getEstOper() {
        return estOper;
    }

    /**
     * @param estOper
     *            the estOper to set
     */
    public void setEstOper(final List<Long> estOper) {
        this.estOper = estOper;
    }

    /**
     * @return the cost
     */
    public int getCost() {
        return cost;
    }

    /**
     * @param cost
     *            the cost to set
     */
    public void setCost(final int cost) {
        this.cost = cost;
    }

    /**
     * @return the attributes
     */
    public Map<String, Object> getAttributes() {
        return attributes;
    }

    /**
     * @return the commandNumber
     */
    public int getCommandSeqNumber() {
        return commandSeqNumber;
    }

    /**
     * @param commandSeqNumber
     *            the commandNumber to set
     */
    public Command setCommandSeqNumber(final int commandSeqNumber) {
        this.commandSeqNumber = commandSeqNumber;
        return this;
    }

    /**
     * @param operands
     *            the operands to set
     * @return
     */
    public final Command setOperands(final List<Long> operands) {
        this.operands = operands;
        return this;
    }

    /**
     * @return the operands
     */
    public List<Long> getOperands() {
        return operands;
    }

    public Long getResult() {
        return result;
    }

    public Command setResult(final Long result) {
        this.result = result;
        return this;
    }

    public String getCommand() {
        return command;
    }

    public Command setCommand(final String command) {
        this.command = command;
        return this;
    }

    public String getCommandType() {
        return commandType;
    }

    public Command setCommandType(final String commandType) {
        this.commandType = commandType;
        return this;
    }

    /**
     * @return the numberOfOperands
     */
    public int getNumberOfOperands() {
        return numberOfOperands;
    }

    /**
     * @param numberOfOperands
     *            the numberOfOperands to set
     */
    public Command setNumberOfOperands(final int numberOfOperands) {
        this.numberOfOperands = numberOfOperands;
        return this;
    }

    /**
     * @return the commandAlgo
     */
    public CommandAlgorithm getCommandAlgo() {
        return commandAlgo;
    }

    /**
     * @param commandAlgo
     *            the commandAlgo to set
     * @return
     */
    public Command setCommandAlgo(final CommandAlgorithm commandAlgo) {
        this.commandAlgo = commandAlgo;
        return this;
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     * @return
     */
    public Command setId(final long id) {
        this.id = id;
        return this;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((command == null) ? 0 : command.hashCode());
        result = prime * result + commandSeqNumber;
        result = prime * result
                + ((commandType == null) ? 0 : commandType.hashCode());
        result = prime * result + numberOfOperands;
        result = prime * result + (stored ? 1231 : 1237);
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final Command other = (Command) obj;
        if (command == null) {
            if (other.command != null)
                return false;
        } else if (!command.equals(other.command))
            return false;
        if (commandSeqNumber != other.commandSeqNumber)
            return false;
        if (commandType == null) {
            if (other.commandType != null)
                return false;
        } else if (!commandType.equals(other.commandType))
            return false;
        if (numberOfOperands != other.numberOfOperands)
            return false;
        if (stored != other.stored)
            return false;
        return true;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("\nCommand [");
        if (commandSeqNumber != Util.FAILED_STATUS) {
            builder.append("command# ").append(commandSeqNumber).append(", ");
        }

        builder.append("parse line # ").append(parseLineNumber).append(", ");
        builder.append("sub query # ").append(subQueryNumber).append(", ");

        builder.append("id=").append(id).append(", ")
                .append(Util.LINE_SEPARATOR);
        if (operands != null) {
            builder.append("operands=").append(operands).append(", ");
        }

        builder.append("result=").append(result).append(", ");

        if (attributes != null) {
            builder.append("attributes=").append(attributes).append(", ");
        }

        // if (command != null) {
        // builder.append("command=").append(command).append(", ");
        // }
        if (commandType != null) {
            builder.append("commandType=").append(commandType).append(", ");
        }
        builder.append("numberOfOperands=").append(numberOfOperands)
                .append(", ");
        if (estOper != null) {
            builder.append("estOper=").append(estOper).append(", ");
        }
        builder.append("stored=").append(isStored()).append(", ");
        // if (commandAlgo != null) {
        // builder.append("commandAlgo=").append(commandAlgo).append(", ");
        // }
        builder.append("cost=").append(cost).append("]")
                .append(Util.LINE_SEPARATOR);
        return builder.toString();
    }

    public void addAttVal(final String[] attVal) {
        attributes.put(attVal[0], attVal[1]);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#clone()
     */
    @Override
    public Command clone() {
        final Command clone = new Command();
        clone.setOperands(getOperands());
        // ok to share all the objects cloned
        clone.setCommand(command).setCommandAlgo(commandAlgo)
                .setCommandSeqNumber(commandSeqNumber)
                .setCommandType(commandType).setCommandTypeObj(commandTypeObj)
                .setCreated(created).setId(id)
                .setNumberOfOperands(numberOfOperands)
                .setSubQueryNumber(getSubQueryNumber())
                .setResult((result != null) ? result : null)
                .setParseLineNumber(getParseLineNumber());
        return clone;
    }
}
