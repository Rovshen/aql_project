/**
 * This package contains entities related to tools used in AQL. Tools represent a specific framework or technology that could be used to execute {@link edu.baylor.aql.entities.command.CommandAlgorithm}.
 * Example of a tool is Hadoop MapReduce, Graphlab, Hive, etc. The properties their share is that they can perform more than one command that is either user defined or predefined.
 */
package edu.baylor.aql.entities.tools;

