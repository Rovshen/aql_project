package edu.baylor.aql.entities.query;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Stack;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;

import edu.baylor.aql.dataaccess.IMetadata;
import edu.baylor.aql.entities.Element;
import edu.baylor.aql.entities.client.ClientSession;
import edu.baylor.aql.entities.command.Command;
import edu.baylor.aql.entities.operands.Operand;
import edu.baylor.aql.services.optimization.evaluating.EvaluatorHandler;
import edu.baylor.aql.services.requesthandler.pipeline.Visitor;
import edu.baylor.aql.services.requesthandler.pipeline.VisitorH;
import edu.baylor.aql.util.AQLException;

/**
 * Query. Custom data structure that has pointers to previous and next Query.
 * Thus we can call it double ended linked list.
 * 
 * @author Rovshen Nazarov. Baylor University. Sep 29, 2014.All Rights Reserved.
 */
public final class Query extends Element implements Serializable, Cloneable {

    private static final long serialVersionUID = -7489697405300820506L;
    /**
     * Max number of operands per query
     */
    public static final int MAX_NUMBER_OF_COMMANDS = 5000;
    /**
     * Database generated id.
     */
    private long queryId;
    /**
     * Id of the client session associated with this query.
     */
    private Long clientSessionId;
    /**
     * Client session to be used to write query progress to the user.
     */
    private ClientSession clientSession;
    /**
     * How much of the last query was completed
     * 1.0 is completed, 0.5 half of commands are completed.
     * To compute number of computed commands / total commands in the query.
     */
    private double progress;
    /**
     * Query string representing original client request
     */
    private String queryRequest;
    /**
     * List of {@link Command}s that will be executed as part of this query.
     */
    private Queue<Command> commands = null;
    /**
     * List of {@link Operand}'s ids that will be used in commands.
     * 
     * TODO NOTE: the {@link Operand}s objects must be shared because they don
     * not change
     */
    private List<Long> operands = null;
    /**
     * Result operand for this query.
     */
    private Long resultOperand;

    /**
     * Query execution cost computed by {@link EvaluatorHandler}.
     */
    private int estExecCost;
    /**
     * Was query retrieved from the metadata.
     */
    private boolean isRepeated;
    /**
     * Next {@link Query} in the linked list.
     */
    private Query next = null;
    /**
     * Keep a stack of sub query ids, so that we can mark and manipulate
     * {@link Operand}s and {@link Command}s related to specific sub queries.
     * Each {@link Query} needs such a stack.
     * 
     * TODO NOTE: must be shared does not change
     */
    private Stack<Integer> subQuerySeqNumbers = null;
    /**
     * Alias name of this query.
     */
    private String aliasName;
    private ConcurrentHashMap<Long, Operand> operMap = null;
    private Query prev = null;

    public Query() {
    }

    /**
     * @return the operMap
     */
    public final Map<Long, Operand> getOperMap() {
        return operMap;
    }

    /**
     * @param operMap
     *            the operMap to set
     * @return
     */
    public final Query setOperMap(final ConcurrentHashMap<Long, Operand> operMap) {
        this.operMap = operMap;
        return this;
    }

    /**
     * @return the next
     */
    public Query getNext() {
        return next;
    }

    /**
     * Use with caution. This method allows to break linked list.
     * 
     * @param next
     */
    public void setNext(final Query next) {
        this.next = next;
    }

    /**
     * Get the last {@link Element} in the linked list of the {@link Element}s
     * chain.
     * 
     * @param next
     *            the next to set
     * @return
     */
    public Query appendLast(final Query nextIn) {

        if (nextIn == null) {
            return this;
        }
        if (next == null) {
            prev = this;
        }

        // rewind to the final end of the final linked list
        while (prev.next != null) {
            prev = prev.next;
        }
        prev.next = nextIn;
        prev = nextIn; // update prev reference to the last element

        return this;
    }

    /**
     * @return the aliasName
     */
    public final String getAliasName() {
        return aliasName;
    }

    /**
     * @param aliasName
     *            the aliasName to set
     * @return
     */
    public final Query setAliasName(final String aliasName) {
        this.aliasName = aliasName;
        return this;
    }

    /**
     * @param subQuerySeqNumbers
     *            the subQuerySeqNumbers to set
     * @return
     */
    public final Query setSubQuerySeqNumbers(
            final Stack<Integer> subQuerySeqNumbers) {
        this.subQuerySeqNumbers = subQuerySeqNumbers;
        return this;
    }

    /**
     * @return the subQuerySeqNumbers
     */
    public Stack<Integer> getSubQuerySeqNumbers() {
        return subQuerySeqNumbers;
    }

    /**
     * @return the isRepeated
     */
    public boolean isRepeated() {
        return isRepeated;
    }

    /**
     * @param isRepeated
     *            the isRepeated to set
     * @return
     */
    public Query setRepeated(final boolean isRepeated) {
        this.isRepeated = isRepeated;
        return this;
    }

    /**
     * @return the estExecCost
     */
    public int getEstExecCost() {
        return estExecCost;
    }

    /**
     * @param estExecCost
     *            the estExecCost to set
     * @return
     */
    public Query setEstExecCost(final int estExecCost) {
        this.estExecCost = estExecCost;
        return this;
    }

    /**
     * @return the clientSession
     */
    public ClientSession getClientSession() {
        return clientSession;
    }

    /**
     * @param clientSession
     *            the clientSession to set
     * @return
     */
    public Query setClientSession(final ClientSession clientSession) {
        this.clientSession = clientSession;
        if (clientSession != null) {
            setClientSessionId(clientSession.getSessionId());
        }
        return this;
    }

    /**
     * @param operands
     *            the operands to set
     * @return
     */
    public final Query setOperands(final List<Long> operands) {
        this.operands = operands;
        return this;
    }

    /**
     * @return the operands
     */
    public List<Long> getOperands() {
        return operands;
    }

    /**
     * @param commands
     *            the commands to set
     * @return
     */
    public Query setCommands(final Queue<Command> commands) {
        this.commands = commands;
        return this;
    }

    /**
     * Compute Cost associated with running this Query.
     * Cost is an evaluation measure with some range.
     * 
     * AQL uses 1 to 100 range of integers where 100 is the highest cost.
     * 
     * @return the totalCost
     */
    public int getTotalCost() {
        int totalCost = 0;
        for (final Command command : commands) {
            totalCost += command.getCost();
        }
        return totalCost;
    }

    /**
     * @return the queryProgress
     */
    public double getProgress() {
        return progress;
    }

    /**
     * @param queryProgress
     *            the queryProgress to set
     * @return
     */
    public Query setQueryProgress(final double queryProgress) {
        progress = queryProgress;
        return this;
    }

    /**
     * @return the queryId
     */
    public long getQueryId() {
        return queryId;
    }

    /**
     * @param queryId
     *            the queryId to set
     * @return
     */
    public Query setQueryId(final long queryId) {
        this.queryId = queryId;
        return this;
    }

    /**
     * @return the queryRequest
     */
    public String getQueryRequest() {
        return queryRequest;
    }

    /**
     * @param queryRequest
     *            the queryRequest to set
     * @return
     */
    public Query setQueryRequest(final String queryRequest) {
        this.queryRequest = queryRequest;
        return this;
    }

    /**
     * @return the commands
     */
    public Queue<Command> getCommands() {
        return commands;
    }

    /**
     * @return the resultOperand
     */
    public Long getResultOperand() {
        return resultOperand;
    }

    /**
     * @param resultOperand
     *            the resultOperand to set
     * @return
     */
    public Query setResultOperand(final Long resultOperand) {
        this.resultOperand = resultOperand;
        return this;
    }

    /**
     * @return the clientSessionId
     */
    public Long getClientSessionId() {
        return clientSessionId;
    }

    /**
     * @param clientSessionId
     *            the clientSessionId to set
     * @return
     */
    public Query setClientSessionId(final Long clientSessionId) {
        this.clientSessionId = clientSessionId;
        return this;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result
                + ((commands == null) ? 0 : commands.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        final Query other = (Query) obj;
        if (commands == null) {
            if (other.commands != null) {
                return false;
            } else {
                return true;
            }
        }
        if (commands.size() != other.commands.size()) {
            return false;
        }
        for (final Command command : commands) {
            if (!other.commands.contains(command)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Different {@link VisitorH}s may operate concurrently on this object.
     * 
     * @throws InterruptedException
     * @throws AQLException
     */
    @Override
    public void accept(final Visitor v, final CountDownLatch doneSignal)
            throws InterruptedException, AQLException {
        v.visit(this, doneSignal);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("\nQuery [");
        builder.append("queryId=").append(queryId).append(", ");
        if (clientSessionId != null) {
            builder.append("clientSessionId=").append(clientSessionId)
                    .append(", ");
        }
        builder.append("sub query ids=").append(subQuerySeqNumbers)
                .append(", ");
        // if (clientSession != null) {
        // builder.append("clientSession=").append(clientSession).append(", ");
        // }
        builder.append("queryProgress=").append(progress).append(", ");
        // if (queryRequest != null) {
        // builder.append("queryRequest=").append(queryRequest).append(", ");
        // }
        if (commands != null) {
            builder.append("commands=").append(commands).append(", ");
        }
        if (operands != null) {
            builder.append("operands=").append(operands).append(", ");
        }
        // if (operandIds != null) {
        // builder.append("operandIds=").append(operandIds).append(", ");
        // }
        if (resultOperand != null) {
            builder.append("Query resultOperand=").append(resultOperand)
                    .append(", ");
        }
        builder.append("Query estExecCost=").append(estExecCost).append(", ");
        builder.append("Query Alias Name=").append(aliasName).append(", ");
        // if (operandsWaiting != null) {
        // builder.append("operandsWaiting=").append(operandsWaiting)
        // .append(", ");
        // }
        builder.append("Query isRepeated=").append(isRepeated).append("]");
        return builder.toString();
    }

    @Override
    public long insert(final IMetadata lookUp) throws AQLException {
        return lookUp.store(this);
    }

    /**
     * Copy from {@link Query} to {@link Query}.
     */
    @Override
    public Query clone() {
        /**
         * Copy the head query
         */
        final Query qTo = this.cloneFields();

        final List<Query> linkedQueriesCopy = copyLinkedQueries();

        // link cloned queries
        // this affectively copies linked list of queries
        for (final Query q : linkedQueriesCopy) {
            qTo.appendLast(q);
        }

        return qTo;
    }

    /**
     * Copy all the linked queries
     * 
     * @return linked {@link Query}s {@link List}.
     */
    private List<Query> copyLinkedQueries() {

        final List<Query> linkedQueriesCopy = new ArrayList<>();
        Query qNext = next; // operate on the reference of the query

        if (qNext != null) {
            do { // for each linked query
                linkedQueriesCopy.add(qNext.cloneFields());

            } while ((qNext = qNext.getNext()) != null);
        }
        return linkedQueriesCopy;
    }

    /**
     * Clone all fields of this {@link Query} except the linked list of
     * {@link Query}s linked to this object.
     * 
     * @return
     */
    private Query cloneFields() {

        final Query qTo = new Query();
        final Queue<Command> commandsClone = new LinkedList<>();

        for (final Command command : getCommands()) {
            commandsClone.add(command.clone());
        }
        // copy by reference is ok, we will only rearrange the references and
        // never change values
        qTo.setOperands(new ArrayList<>(getOperands()));
        // passing copy is ok, we never expect this to change
        qTo.setSubQuerySeqNumbers(getSubQuerySeqNumbers());
        qTo
        // need to be cloned
        .setCommands(commandsClone)
                // ok to share
                .setClientSession(getClientSession())
                .setClientSessionId(getClientSessionId())
                .setOperMap((ConcurrentHashMap<Long, Operand>) getOperMap())
                .setResultOperand(
                        (getResultOperand() != null) ? getResultOperand()
                                : null).setRepeated(isRepeated())
                .setQueryProgress(getProgress()).setAliasName(getAliasName())
                .setEstExecCost(getEstExecCost()).setQueryId(getQueryId())
                .setQueryRequest(getQueryRequest()).setErrState(isErrState());
        return qTo;
    }
}
