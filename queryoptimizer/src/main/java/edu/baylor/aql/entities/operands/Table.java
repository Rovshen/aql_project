package edu.baylor.aql.entities.operands;

import java.util.concurrent.CountDownLatch;

import edu.baylor.aql.dataaccess.IMetadata;
import edu.baylor.aql.entities.command.Command;
import edu.baylor.aql.services.requesthandler.pipeline.Visitor;
import edu.baylor.aql.util.AQLException;

/**
 * This entity contains all necessary attributes for the Table object. This
 * object is generated when the table is loaded
 * or {@link Command} executes successfully or when the {@link Operand} is
 * already loaded and retrieved by the user or system from
 * the metadata using {@link Operand} id.
 * 
 * @author Rovshen Nazarov. Baylor University. Sep 2, 2014.All Rights Reserved.
 * 
 */
public final class Table extends Operand {

    private static final long serialVersionUID = -5991547004398558017L;
    private static final String implementationType = OperandType.TABLE
            .getLabel();
    private String tableName = "";
    private Long estRowCount;

    public Table() {
        super.setImplementationType(implementationType);
    }

    /**
     * @return the estRowCount
     */
    public Long getEstRowCount() {
        return estRowCount;
    }

    /**
     * @param estRowCount
     *            the estRowCount to set
     */
    public void setEstRowCount(final Long estRowCount) {
        this.estRowCount = estRowCount;
    }

    @Override
    public String getReadableId() {
        return "table_" + super.getId();
    }

    @Override
    public long insert(final IMetadata lookUp) {
        return lookUp.store(this);
    }

    /**
     * @return the tableName
     */
    public String getTableName() {
        return tableName;
    }

    /**
     * @param tableName
     *            the tableName to set
     */
    public void setTableName(final String tableName) {
        this.tableName = tableName;
    }

    @Override
    public String getOperandName() {
        return getTableName();
    }

    @Override
    public void accept(final Visitor v, final CountDownLatch doneSignal)
            throws InterruptedException, AQLException {
        v.visit(this, doneSignal);
    }

    @Override
    public Operand clone() {
        // TODO Auto-generated method stub
        final Operand clone = new Table();
        return clone;
    }

}
