package edu.baylor.aql.entities.command;

import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

import edu.baylor.aql.dataaccess.IMetadata;
import edu.baylor.aql.entities.Element;
import edu.baylor.aql.entities.operands.Operand;
import edu.baylor.aql.entities.operands.Operand.OperandType;
import edu.baylor.aql.entities.tools.Tool;
import edu.baylor.aql.services.requesthandler.pipeline.Visitor;
import edu.baylor.aql.util.AQLException;

/**
 * This entity is used as a data structure to hold data related to a specific
 * algorithm that performs operation related to {@link CommandType}. The
 * specific algorithm
 * defined here is used to execute user query based {@link Command} on a given
 * {@link Tool}.
 * <p>
 * The algorithm implementation has data members that define the following:
 * <li>Fixed or varying number of input {@link Operand}s (where operands could
 * be of the same or of the different type),
 * <li>Fixed type and size result {@link Operand},
 * <li>Varying number of attributes and parameters for the algorithm.
 * <p>
 * This entity may be associated with two input/output data types:
 * <li> {@link Operand}s, which could be seen as values called by reference
 * <li>Parameters, which are of primitive type, such as int, string, double,
 * char, etc.
 * <p>
 * This entity requires association with the {@link Tool} on which it will be
 * executed using a String command returned by {@link #getCommandTemplate()}.
 * The information related to {@link Tool} is used by ExecutionPlanner service
 * during optimal execution plan generation.
 * 
 * @author Rovshen Nazarov. Baylor University. Aug 12, 2014.All Rights Reserved.
 * 
 */
public final class CommandAlgorithm extends Element implements Serializable {

    private static final long serialVersionUID = 9181671589938396238L;

    /**
     * Maximum number of operands allowed for any {@link CommandAlgorithm}. If
     * {@link CommandAlgorithm} has this value, it means it can take on
     * arbitrary number of {@link Operand}s.
     */
    public static final int MAX_OPERANDS = 255;

    /**
     * This enumeration class represents types of tools supported by AQL, such
     * as
     * operator tool that executes {@link CommandAlgorithm} based on user
     * requested {@link Command}.
     * Another type of the Tool is converter. This type is used to convert
     * between different {@link Operand} sub classes, e.g. from graph to table.
     * 
     * @author Rovshen Nazarov. Baylor University. Sep 1, 2014.All Rights
     *         Reserved.
     * 
     */
    public enum FunctionalType {
        OPERATOR("operator"), CONVERTER("converter"), SAMPLER("sampling tool");

        private final String label;

        FunctionalType(final String label) {
            this.label = label;
        }

        public String getLabel() {
            return label;
        }

        @Override
        public String toString() {
            return label;
        }

        /**
         * Return ToolType object given the String code.
         * Note that the String code must be a member of the enumeration
         * If the code is not part of predefined enumeration null
         * is returned.
         * 
         * @param code
         *            String code that is defined for this enum type
         * @return type The type based on the String code.
         *         if String code unknown return null.
         */
        public static FunctionalType getEnumByString(final String code) {
            for (final FunctionalType type : FunctionalType.values()) {
                if (type.name().equals(code)) {
                    return type;
                }
            }
            return null;
        }
    }

    /**
     * To be used to construct SQL statement
     */
    private final StringBuilder sb = new StringBuilder();
    /**
     * Refer to {@link FunctionalType}.
     */
    private FunctionalType functionalType;
    /**
     * Name of the {@link Tool} associated with this object.
     */
    private String toolName;
    /**
     * Database generated id, automatically generated on the new row insert.
     */
    private Long Id;
    /**
     * Type of the command, e.g. UNION, INTER, etc.
     */
    private CommandType commandType;
    /**
     * Name of the command type, used during command algorithm loading.
     */
    private String commandTypeName;
    /**
     * 0 is the default and the lowest priority.
     * Priority is used to define, which algorithm for the given command type
     * should be considered first for execution
     * if the performance of several commands is nearly identical.
     */
    private int priority = 0; // default is the lowest priority
    /**
     * Physical path to the command algorithm to be executed.
     * The command algorithm is executed using command string
     * by the tool associated with a given command algorithm.
     */
    private String path;
    /**
     * Number of {@link Operand}s this command algorithm accepts.
     */
    private Integer numberOfOperands;
    private Date created; // when entry was created
    /**
     * This is a command template that is expected to be executable in Linux or
     * any other OS environments. Program invocation with place holders.
     */
    private String programInvocationTemplate;
    /**
     * Database generated tool id, for the {@link Tool} that will
     * execute this command algorithm.
     */
    private Long toolId; // the tool that this command is executed by
    /**
     * Tool associated with this CommandAlgorithm.
     */
    private Tool tool;

    /**
     * Type of input and result {@link Operand} supported by this algorithm.
     */
    private OperandType resultOperandType;
    /**
     * Types of the input parameters could be the same or different types.
     */
    private final List<OperandType> inputOperandsType = new ArrayList<>();
    /**
     * These map is needed for varying parameter operators.
     * TODO may be redundunt
     * Parameter name points to the parameter value.
     */
    private final Map<String, Object> parameters = new HashMap<>();
    /**
     * An attribute name points to the attribute value.
     */
    private final Map<String, String> attributes = new HashMap<>();
    /**
     * Estimate of the memory (RAM) needed in MB.
     */
    private Integer avgMemoryRequirement;
    /**
     * Performance in MB/second.
     */
    private Integer avgPerformance;
    /**
     * An estimate of CPU clock speed needed in GHz.
     */
    private Integer avgCPURequirement;
    /**
     * Running average time to process a command in Minutes.
     */
    private Double avrRunTime;

    /**
     * Empty constructor.
     */
    public CommandAlgorithm() {

    }

    /** Setters use fluent API design like in String Builder append */

    /**
     * @return the commandTypeName
     */
    public String getCommandTypeName() {
        return commandTypeName;
    }

    /**
     * @return the avgMemoryRequirement
     */
    public Integer getAvgMemoryRequirement() {
        return avgMemoryRequirement;
    }

    /**
     * @param avgMemoryRequirement
     *            the avgMemoryRequirement to set
     * @return
     */
    public CommandAlgorithm setAvgMemoryRequirement(
            final Integer avgMemoryRequirement) {
        this.avgMemoryRequirement = avgMemoryRequirement;
        return this;
    }

    /**
     * @return the avgPerformance
     */
    public Integer getAvgPerformance() {
        return avgPerformance;
    }

    /**
     * @param avgPerformance
     *            the avgPerformance to set
     * @return
     */
    public CommandAlgorithm setAvgPerformance(final Integer avgPerformance) {
        this.avgPerformance = avgPerformance;
        return this;
    }

    /**
     * @return the avgCPURequirement
     */
    public Integer getAvgCPURequirement() {
        return avgCPURequirement;
    }

    /**
     * @param avgCPURequirement
     *            the avgCPURequirement to set
     * @returnCommandAlgorithm
     */
    public CommandAlgorithm setAvgCPURequirement(final Integer avgCPURequirement) {
        this.avgCPURequirement = avgCPURequirement;
        return this;
    }

    /**
     * @return the avrRunTime
     */
    public Double getAvrRunTime() {
        return avrRunTime;
    }

    /**
     * @param avrRunTime
     *            the avrRunTime to set
     * @return
     */
    public CommandAlgorithm setAvrRunTime(final Double avrRunTime) {
        this.avrRunTime = avrRunTime;
        return this;
    }

    /**
     * @return the toolName
     */
    public String getToolName() {
        return toolName;
    }

    /**
     * @param toolName
     *            the toolName to set
     * @return
     */
    public CommandAlgorithm setToolName(final String toolName) {
        this.toolName = toolName;
        return this;
    }

    /**
     * @param commandTypeName
     *            the commandTypeName to set
     */
    public void setCommandTypeName(final String commandTypeName) {
        this.commandTypeName = commandTypeName;
    }

    /**
     * @return the commandType
     */
    public CommandType getCommandType() {
        return commandType;
    }

    /**
     * @param commandType
     *            the commandType to set
     * @return
     */
    public CommandAlgorithm setCommandType(final CommandType commandType) {
        this.commandType = commandType;
        return this;
    }

    /**
     * @return the iD
     */
    public Long getId() {
        return Id;
    }

    /**
     * @param Id
     *            the Id to set
     */
    public CommandAlgorithm setId(final Long Id) {
        this.Id = Id;
        return this;
    }

    /**
     * @return the priority
     */
    public Integer getPriority() {
        return priority;
    }

    /**
     * @param priority
     *            the priority to set
     */
    public CommandAlgorithm setPriority(final Integer priority) {
        this.priority = priority;
        return this;
    }

    /**
     * @return the path
     */
    public String getPath() {
        return path;
    }

    /**
     * @param path
     *            the path to set
     */
    public CommandAlgorithm setPath(final String path) {
        this.path = path;
        return this;
    }

    /**
     * @return the numberOfOperants
     */
    public Integer getNumberOfOperands() {
        return numberOfOperands;
    }

    /**
     * @param numberOfOperands
     */
    public CommandAlgorithm setNumberOfOperands(final Integer numberOfOperands) {
        this.numberOfOperands = numberOfOperands;
        return this;
    }

    /**
     * @return the created
     */
    public Date getCreated() {
        return created;
    }

    /**
     * @param created
     *            the created to set
     */
    public CommandAlgorithm setCreated(final Date created) {
        this.created = created;
        return this;
    }

    /**
     * Build command algorithm command template based on the linked {@link Tool}
     * 's data and {@link CommandAlgorithm}'s data.
     * 
     * @return the command
     * @throws AQLException
     */
    public String getCommandTemplate() {
        return programInvocationTemplate;
    }

    /**
     * @param command
     *            the command to set
     */
    public CommandAlgorithm setCommandTemplate(final String command) {
        programInvocationTemplate = command;
        return this;
    }

    /**
     * @return the serialversionuid
     */
    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    /**
     * Id of the {@link Tool} used to
     * execute this aglorithm
     * 
     * @return the toolId
     */
    public Long getToolId() {
        return toolId;
    }

    /**
     * @param toolId
     *            the toolId to set
     */
    public CommandAlgorithm setToolId(final Long toolId) {
        this.toolId = toolId;
        return this;
    }

    /**
     * @return the operandsType
     */
    public OperandType getOperandsType() {
        return resultOperandType;
    }

    /**
     * @param operandsType
     *            the operandsType to set
     */
    public CommandAlgorithm setOperandsType(final String operandsType) {
        resultOperandType = OperandType.getEnumByString(operandsType);
        return this;
    }

    /**
     * @return the tool
     */
    public Tool getTool() {
        return tool;
    }

    /**
     * @param tool
     *            the tool to set
     */
    public CommandAlgorithm setTool(final Tool tool) {
        this.tool = tool;
        return this;
    }

    /**
     * @return the resultOperandType
     */
    public OperandType getResultOperandType() {
        return resultOperandType;
    }

    /**
     * @param resultOperandType
     *            the resultOperandType to set
     * @return
     */
    public CommandAlgorithm setResultOperandType(final String resultOperandType) {
        this.resultOperandType = OperandType.getEnumByString(resultOperandType);
        return this;
    }

    /**
     * @return the inputOperandsType
     */
    public List<OperandType> getInputOperandsType() {
        return inputOperandsType;
    }

    /**
     * @return the parameters
     */
    public Map<String, Object> getParameters() {
        return parameters;
    }

    /**
     * @return the attributes
     */
    public Map<String, String> getAttributes() {
        return attributes;
    }

    /**
     * @return the functionalType
     */
    public FunctionalType getFunctionalType() {
        return functionalType;
    }

    /**
     * @param functionalType
     *            the functionalType to set
     * @return
     */
    public CommandAlgorithm setFunctionalType(final String functionalType) {
        this.functionalType = FunctionalType.getEnumByString(functionalType);
        return this;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((attributes == null) ? 0 : attributes.hashCode());
        result = prime
                * result
                + ((inputOperandsType == null) ? 0 : inputOperandsType
                        .hashCode());
        result = prime
                * result
                + ((numberOfOperands == null) ? 0 : numberOfOperands.hashCode());
        result = prime * result
                + ((parameters == null) ? 0 : parameters.hashCode());
        result = prime * result + ((path == null) ? 0 : path.hashCode());
        result = prime
                * result
                + ((resultOperandType == null) ? 0 : resultOperandType
                        .hashCode());
        result = prime * result + ((toolId == null) ? 0 : toolId.hashCode());
        result = prime * result
                + ((commandType == null) ? 0 : commandType.hashCode());
        return result;
    }

    /*
     * CommandAlgorithm
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final CommandAlgorithm other = (CommandAlgorithm) obj;
        if (attributes == null) {
            if (other.attributes != null)
                return false;
        } else if (!attributes.equals(other.attributes))
            return false;
        if (inputOperandsType == null) {
            if (other.inputOperandsType != null)
                return false;
        } else if (!inputOperandsType.equals(other.inputOperandsType))
            return false;
        if (numberOfOperands == null) {
            if (other.numberOfOperands != null)
                return false;
        } else if (!numberOfOperands.equals(other.numberOfOperands))
            return false;
        if (parameters == null) {
            if (other.parameters != null)
                return false;
        } else if (!parameters.equals(other.parameters))
            return false;
        if (path == null) {
            if (other.path != null)
                return false;
        } else if (!path.equals(other.path))
            return false;
        if (resultOperandType != other.resultOperandType)
            return false;
        if (toolId == null) {
            if (other.toolId != null)
                return false;
        } else if (!toolId.equals(other.toolId))
            return false;
        if (commandType == null) {
            if (other.commandType != null)
                return false;
        } else if (!commandType.equals(other.commandType))
            return false;
        return true;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("CommandAlgorithm [");
        if (functionalType != null) {
            builder.append("functionalType=").append(functionalType)
                    .append(", ");
        }
        if (toolName != null) {
            builder.append("toolName=").append(toolName).append(", ");
        }
        if (Id != null) {
            builder.append("Id=").append(Id).append(", ");
        }
        if (commandType != null) {
            builder.append("commandType=").append(commandType).append(", ");
        }
        if (commandTypeName != null) {
            builder.append("commandTypeName=").append(commandTypeName)
                    .append(", ");
        }

        builder.append("priority=").append(priority).append(", ");

        if (path != null) {
            builder.append("path=").append(path).append(", ");
        }
        if (numberOfOperands != null) {
            builder.append("numberOfOperands=").append(numberOfOperands)
                    .append(", ");
        }
        if (created != null) {
            builder.append("created=").append(created).append(", ");
        }
        if (programInvocationTemplate != null) {
            builder.append("programInvocationTemplate=")
                    .append(programInvocationTemplate).append(", ");
        }
        if (toolId != null) {
            builder.append("toolId=").append(toolId).append(", ");
        }
        if (tool != null) {
            builder.append("tool=").append(tool).append(", ");
        }

        if (resultOperandType != null) {
            builder.append("resultOperandType=").append(resultOperandType)
                    .append(", ");
        }
        if (inputOperandsType != null) {
            builder.append("inputOperandsType=").append(inputOperandsType)
                    .append(", ");
        }
        if (parameters != null) {
            builder.append("parameters=").append(parameters).append(", ");
        }
        if (attributes != null) {
            builder.append("attributes=").append(attributes).append(", ");
        }
        if (avgMemoryRequirement != null) {
            builder.append("avgMemoryRequirement=")
                    .append(avgMemoryRequirement).append(", ");
        }
        if (avgPerformance != null) {
            builder.append("avgPerformance=").append(avgPerformance)
                    .append(", ");
        }
        if (avgCPURequirement != null) {
            builder.append("avgCPURequirement=").append(avgCPURequirement)
                    .append(", ");
        }
        if (avrRunTime != null) {
            builder.append("avrRunTime=").append(avrRunTime);
        }
        builder.append("]");
        return builder.toString();
    }

    @Override
    public void accept(final Visitor v, final CountDownLatch doneSignal)
            throws InterruptedException, AQLException {
        v.visit(this, doneSignal);

    }

    @Override
    public long insert(final IMetadata dao) throws AQLException {
        return dao.store(this);
    }
}
