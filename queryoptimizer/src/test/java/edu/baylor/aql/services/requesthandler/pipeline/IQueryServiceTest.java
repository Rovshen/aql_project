package edu.baylor.aql.services.requesthandler.pipeline;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.google.common.collect.Iterables;

import edu.baylor.aql.entities.client.ClientSession;
import edu.baylor.aql.entities.command.Command;
import edu.baylor.aql.entities.command.CommandAlgorithm;
import edu.baylor.aql.entities.command.CommandType;
import edu.baylor.aql.entities.command.CommandType.OrderingType;
import edu.baylor.aql.entities.operands.Graph;
import edu.baylor.aql.entities.operands.Operand;
import edu.baylor.aql.entities.operands.Operand.OperandState;
import edu.baylor.aql.entities.operands.Operand.OperandType;
import edu.baylor.aql.entities.query.Query;
import edu.baylor.aql.services.query.IQueryService;
import edu.baylor.aql.services.query.QueryService;
import edu.baylor.aql.util.AQLException;

public class IQueryServiceTest {

    private static Query testQ;
    private static IQueryService qService;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        testQ = new Query();
        qService = new QueryService();
        final List<Operand> operands = new ArrayList<>();
        final int numbOfCommands = 3;
        final int numbOfOperPerCommands = 2;
        final String queryId = "1";
        final long queryIdL = 1;

        final List<Command> commands = new ArrayList<>();

        final CommandAlgorithm ca = new CommandAlgorithm()
                .setResultOperandType(OperandType.GRAPH.getLabel())
                .setNumberOfOperands(numbOfOperPerCommands);

        final CommandType cTypeInter = new CommandType()
                .setTypeName("INTERSECTION")
                .setNumberOfOperands(numbOfOperPerCommands)
                .setOrderingType(OrderingType.ASSOCIATIVE_COMMUTATIVE);
        final CommandType cTypeUnion = new CommandType().setTypeName("UNION")
                .setNumberOfOperands(numbOfOperPerCommands)
                .setOrderingType(OrderingType.ASSOCIATIVE_COMMUTATIVE);
        final CommandType cTypeDiff = new CommandType()
                .setTypeName("DIFFERENCE")
                .setNumberOfOperands(numbOfOperPerCommands)
                .setOrderingType(OrderingType.NONE);

        // create COMMANDS
        for (int i = 0; i < numbOfCommands; i++) {
            commands.add(new Command().setId(i).setCommandSeqNumber(i)
                    .setCommandAlgo(ca));
        }

        // Test that with query ( from A INTER B ) DIFF ( from C UNION D )
        // setting line number is important as we will test command population
        // method, which is based on those numbers.
        commands.get(0).setCommandTypeObj(cTypeInter).setParseLineNumber(3)
                .setSubQueryNumber(1);
        commands.get(1).setCommandTypeObj(cTypeUnion).setParseLineNumber(6)
                .setSubQueryNumber(2);
        commands.get(2).setCommandTypeObj(cTypeDiff).setParseLineNumber(7)
                .setSubQueryNumber(Command.DEFAULT_SUB_QUERY_NUMBER);

        // create OPERANDS
        for (int i = 0; i < numbOfCommands + 1; i++) {
            final boolean loadedStatus = true;
            operands.add(new Graph().setId(1 + i)
                    .setImplementationType(OperandType.GRAPH.getLabel())
                    .setLoadedPath("/test").setQueryId(queryId)
                    .setType(OperandState.LOADED.getLabel())
                    .setStatus(loadedStatus));
        }

        // setting line number is important as we will test command population
        // method, which is based on those numbers.
        operands.get(0).setParseLineNumber(1).setSubQueryNumber(1);
        operands.get(1).setParseLineNumber(2).setSubQueryNumber(1);
        operands.get(2).setParseLineNumber(4).setSubQueryNumber(2);
        operands.get(3).setParseLineNumber(5).setSubQueryNumber(2);

        final List<Long> operandsIds = new ArrayList<>();
        final ConcurrentHashMap<Long, Operand> operMap = new ConcurrentHashMap<>();
        for (final Operand o : operands) {
            operMap.put((long) o.hashCode(), o);
            operandsIds.add((long) o.hashCode());
        }

        final Queue<Command> commandsQ = new LinkedList<>(commands);
        final ClientSession cS = new ClientSession("123", null);
        testQ.setClientSession(cS);
        testQ.setOperMap(operMap);
        testQ.setCommands(commandsQ).setQueryId(queryIdL);
        testQ.setOperands(operandsIds);

    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    /**
     * Test that with query ( from A INTER B ) DIFF ( from C UNION D )
     * we get populated commands as below in exact same order. NOTE: parse line
     * numbers are taken into account in population process.
     * 
     * from (from graph_2 UNION graph_3) DIFF (from graph_4 INTER graph_1);
     * 
     * 1) INTER (A,B) - > R2
     * 2) UNION (C,D) - > R1
     * 3) DIFF (R1, R2) - > R3 (query result)
     * 
     * @throws AQLException
     */
    @Test
    public void testPopulateCommandsWithOperandsSequencial()
            throws AQLException {
        final Query testClone = testQ.clone();

        qService.populateCommandsWithOperandsSequencial(testClone);

        // TODO finish test
        // verify assumptions hold
        final List<List<Long>> operandLists = new ArrayList<>();
        final List<Long> expOperandIsC1 = Arrays.asList(1l, 2l);
        final List<Long> expOperandIsC2 = Arrays.asList(3l, 4l);
        final List<Long> expOperandIsC3 = Arrays.asList(-2l, -3l);
        operandLists.add(expOperandIsC1);
        operandLists.add(expOperandIsC2);
        operandLists.add(expOperandIsC3);

        int indexArr = 0;
        for (final Command c : testClone.getCommands()) {
            final List<Long> expOperandIs = operandLists.get(indexArr);
            indexArr++;
            final List<Long> arrAct = new ArrayList<>();
            for (final Long key : c.getOperands()) {
                arrAct.add(testClone.getOperMap().get(key).getId());
            }

            // guva library call true if the iterables have the same
            // elements in the same order.
            assertTrue(
                    "The actual array is " + Arrays.toString(arrAct.toArray())
                            + " the expected array is "
                            + Arrays.toString(expOperandIs.toArray()),
                    Iterables.elementsEqual(expOperandIs, arrAct));
        }
    }

    /**
     * Test that with query
     * from (from graph_2 UNION graph_3) DIFF (from graph_4 INTER graph_1);
     * 
     * we get populated commands as below in exact same order. NOTE: parse line
     * numbers are taken into account in population process.
     * 
     * 1) UNION (2,3) - > R1
     * 2) INTER (4,1) - > R2
     * 3) DIFF (R1, R2) - > R3 (query result)
     * 
     * @throws AQLException
     */
    @Test
    public void testPopulateCommandsWithOperandsSequencialComplex()
            throws AQLException {

        // from (from graph_2 UNION graph_3) DIFF (from graph_4 INTER graph_1);
        final Query testClone = prepQuery();

        qService.populateCommandsWithOperandsSequencial(testClone);

        // TODO finish test
        // verify assumptions hold
        final List<List<Long>> operandLists = new ArrayList<>();
        final List<Long> expOperandIsC1 = Arrays.asList(2l, 3l);
        final List<Long> expOperandIsC2 = Arrays.asList(4l, 1l);
        final List<Long> expOperandIsC3 = Arrays.asList(-2l, -3l);
        operandLists.add(expOperandIsC1);
        operandLists.add(expOperandIsC2);
        operandLists.add(expOperandIsC3);

        int indexArr = 0;
        for (final Command c : testClone.getCommands()) {
            final List<Long> expOperandIs = operandLists.get(indexArr);
            indexArr++;
            final List<Long> arrAct = new ArrayList<>();
            for (final Long key : c.getOperands()) {
                arrAct.add(testClone.getOperMap().get(key).getId());
            }
            // guva library call true if the iterables have the same
            // elements in the same order.
            assertTrue(
                    "The actual array is " + Arrays.toString(arrAct.toArray())
                            + " the expected array is "
                            + Arrays.toString(expOperandIs.toArray()),
                    Iterables.elementsEqual(expOperandIs, arrAct));
        }
    }

    /**
     * Test that with query
     * from (from graph_2 UNION graph_3) DIFF graph_4;
     * 
     * we get populated commands as below in exact same order. NOTE: parse line
     * numbers are taken into account in population process.
     * 
     * 1) UNION (2,3) - > R1
     * 2) DIFF (R1, 4) - > R2 (query result)
     * 
     * @throws AQLException
     */
    @Test
    public void testPopulateCommandsWithOperandsSequencialSubQueryOperand()
            throws AQLException {

        final Query testClone = prepQuerySubQOper();

        qService.populateCommandsWithOperandsSequencial(testClone);

        // TODO finish test
        // verify assumptions hold
        final List<List<Long>> operandLists = new ArrayList<>();
        final List<Long> expOperandIsC1 = Arrays.asList(2l, 3l);
        final List<Long> expOperandIsC2 = Arrays.asList(-2l, 4l);
        operandLists.add(expOperandIsC1);
        operandLists.add(expOperandIsC2);

        int indexArr = 0;
        for (final Command c : testClone.getCommands()) {
            final List<Long> expOperandIds = operandLists.get(indexArr);
            indexArr++;
            final List<Long> arrAct = new ArrayList<>();
            for (final Long key : c.getOperands()) {
                arrAct.add(testClone.getOperMap().get(key).getId());
            }
            // guva library call true if the iterables have the same
            // elements in the same order.
            assertTrue(
                    "The actual array is " + Arrays.toString(arrAct.toArray())
                            + " the expected array is "
                            + Arrays.toString(expOperandIds.toArray()),
                    Iterables.elementsEqual(expOperandIds, arrAct));
        }
    }

    /**
     * Test that with query
     * from graph_4 DIFF (from graph_2 UNION graph_3);
     * 
     * we get populated commands as below in exact same order. NOTE: parse line
     * numbers are taken into account in population process.
     * 
     * 1) UNION (2,3) - > R1
     * 2) DIFF (4, R1) - > R2 (query result)
     * 
     * @throws AQLException
     */
    @Test
    public void testPopulateCommandsWithOperandsSequencialOperandSubQuery()
            throws AQLException {

        final Query testClone = prepQueryOperSubQ();

        qService.populateCommandsWithOperandsSequencial(testClone);

        // TODO finish test
        // verify assumptions hold
        final List<List<Long>> operandLists = new ArrayList<>();
        final List<Long> expOperandIsC1 = Arrays.asList(2l, 3l);
        final List<Long> expOperandIsC2 = Arrays.asList(4l, -2l);
        operandLists.add(expOperandIsC1);
        operandLists.add(expOperandIsC2);

        int indexArr = 0;
        for (final Command c : testClone.getCommands()) {
            final List<Long> expOperandIds = operandLists.get(indexArr);
            indexArr++;
            final List<Long> arrAct = new ArrayList<>();
            for (final Long key : c.getOperands()) {
                arrAct.add(testClone.getOperMap().get(key).getId());
            }
            // guva library call true if the iterables have the same
            // elements in the same order.
            assertTrue(
                    "The actual array is " + Arrays.toString(arrAct.toArray())
                            + " the expected array is "
                            + Arrays.toString(expOperandIds.toArray()),
                    Iterables.elementsEqual(expOperandIds, arrAct));
        }
    }

    /**
     * Build this query
     * from graph_4 DIFF (from graph_2 UNION graph_3);
     * 
     * 1: OPERAND_ID graph_4
     * 2: OPERAND_ID graph_2
     * 3: OPERAND_ID graph_3
     * 4: 1 UNION
     * 5: 3 DIFFERENCE
     * 
     * @return
     */
    private Query prepQueryOperSubQ() {
        final Query buildQ = new Query();
        qService = new QueryService();
        final List<Operand> operands = new ArrayList<>();
        final int numbOfCommands = 2;
        final int numbOfOperPerCommands = 2;
        final String queryId = "1";
        final long queryIdL = 1l;

        final List<Command> commands = new ArrayList<>();

        final CommandAlgorithm ca = new CommandAlgorithm()
                .setResultOperandType(OperandType.GRAPH.getLabel())
                .setNumberOfOperands(numbOfOperPerCommands);

        final CommandType cTypeUnion = new CommandType().setTypeName("UNION")
                .setNumberOfOperands(numbOfOperPerCommands)
                .setOrderingType(OrderingType.ASSOCIATIVE_COMMUTATIVE);

        final CommandType cTypeDiff = new CommandType()
                .setTypeName("DIFFERENCE")
                .setNumberOfOperands(numbOfOperPerCommands)
                .setOrderingType(OrderingType.NONE);

        // create COMMANDS
        for (int i = 0; i < numbOfCommands; i++) {
            commands.add(new Command().setId(i).setCommandSeqNumber(i)
                    .setCommandAlgo(ca));
        }

        // setting line number is important as we will test command population
        // method, which is based on those numbers.
        commands.get(0).setCommandTypeObj(cTypeUnion).setParseLineNumber(4)
                .setSubQueryNumber(1);

        commands.get(1).setCommandTypeObj(cTypeDiff).setParseLineNumber(5)
                .setSubQueryNumber(Command.DEFAULT_SUB_QUERY_NUMBER);

        // create OPERANDS
        for (int i = 0; i < numbOfCommands + 1; i++) {
            final boolean loadedStatus = true;
            operands.add(new Graph()
                    .setImplementationType(OperandType.GRAPH.getLabel())
                    .setLoadedPath("/test").setQueryId(queryId)
                    .setType(OperandState.LOADED.getLabel())
                    .setStatus(loadedStatus));
        }

        // setting line number is important as we will test command population
        // method, which is based on those numbers.
        operands.get(0).setParseLineNumber(1).setId(4)
                .setSubQueryNumber(Command.DEFAULT_SUB_QUERY_NUMBER);
        operands.get(1).setParseLineNumber(2).setId(2).setSubQueryNumber(1);
        operands.get(2).setParseLineNumber(4).setId(3).setSubQueryNumber(1);

        buildQ.setCommands(new LinkedList<>(commands)).setQueryId(queryIdL);

        final ConcurrentHashMap<Long, Operand> operMap = new ConcurrentHashMap<>();
        final List<Long> operandsIds = new ArrayList<>();
        for (final Operand o : operands) {
            operMap.put((long) o.hashCode(), o);
            operandsIds.add((long) o.hashCode());
        }
        final ClientSession cS = new ClientSession("1234", null);
        buildQ.setClientSession(cS);
        buildQ.setOperands(operandsIds);
        System.out.println(operMap.keySet());
        buildQ.setOperMap(operMap);

        return buildQ;
    }

    /**
     * Build this query
     * from (from graph_2 UNION graph_3) DIFF graph_4;
     * 
     * 1: OPERAND_ID graph_2
     * 2: OPERAND_ID graph_3
     * 3: 1 UNION
     * 4: OPERAND_ID graph_4
     * 5: 3 DIFFERENCE
     * 
     * 
     * @return
     */
    private Query prepQuerySubQOper() {
        final Query buildQ = new Query();
        qService = new QueryService();
        final List<Operand> operands = new ArrayList<>();
        final int numbOfCommands = 2;
        final int numbOfOperPerCommands = 2;
        final String queryId = "1";
        final long queryIdL = 1l;

        final List<Command> commands = new ArrayList<>();

        final CommandAlgorithm ca = new CommandAlgorithm()
                .setResultOperandType(OperandType.GRAPH.getLabel())
                .setNumberOfOperands(numbOfOperPerCommands);

        final CommandType cTypeUnion = new CommandType().setTypeName("UNION")
                .setNumberOfOperands(numbOfOperPerCommands)
                .setOrderingType(OrderingType.ASSOCIATIVE_COMMUTATIVE);

        final CommandType cTypeDiff = new CommandType()
                .setTypeName("DIFFERENCE")
                .setNumberOfOperands(numbOfOperPerCommands)
                .setOrderingType(OrderingType.NONE);

        // create COMMANDS
        for (int i = 0; i < numbOfCommands; i++) {
            commands.add(new Command().setId(i).setCommandSeqNumber(i)
                    .setCommandAlgo(ca));
        }

        // setting line number is important as we will test command population
        // method, which is based on those numbers.
        commands.get(0).setCommandTypeObj(cTypeUnion).setParseLineNumber(3)
                .setSubQueryNumber(1);

        commands.get(1).setCommandTypeObj(cTypeDiff).setParseLineNumber(5)
                .setSubQueryNumber(Command.DEFAULT_SUB_QUERY_NUMBER);

        // create OPERANDS
        for (int i = 0; i < numbOfCommands + 1; i++) {
            final boolean loadedStatus = true;
            operands.add(new Graph()
                    .setImplementationType(OperandType.GRAPH.getLabel())
                    .setLoadedPath("/test").setQueryId(queryId)
                    .setType(OperandState.LOADED.getLabel())
                    .setStatus(loadedStatus));
        }

        // setting line number is important as we will test command population
        // method, which is based on those numbers.
        operands.get(0).setParseLineNumber(1).setId(2).setSubQueryNumber(1);
        operands.get(1).setParseLineNumber(2).setId(3).setSubQueryNumber(1);
        operands.get(2).setParseLineNumber(4).setId(4)
                .setSubQueryNumber(Command.DEFAULT_SUB_QUERY_NUMBER);

        buildQ.setCommands(new LinkedList<>(commands)).setQueryId(queryIdL);

        final ConcurrentHashMap<Long, Operand> operMap = new ConcurrentHashMap<>();
        final List<Long> operandsIds = new ArrayList<>();
        for (final Operand o : operands) {
            operMap.put((long) o.hashCode(), o);
            operandsIds.add((long) o.hashCode());
        }
        final ClientSession cS = new ClientSession("1234", null);
        buildQ.setClientSession(cS);
        buildQ.setOperands(operandsIds);
        System.out.println(operMap.keySet());
        buildQ.setOperMap(operMap);

        return buildQ;
    }

    /**
     * Build this query
     * from (from graph_2 UNION graph_3) DIFF (from graph_4 INTER graph_1);
     * 
     * 1: OPERAND_ID graph_2
     * 2: OPERAND_ID graph_3
     * 3: 1 UNION
     * 4: OPERAND_ID graph_4
     * 5: OPERAND_ID graph_1
     * 6: 2 INTERSECTION
     * 7: 3 DIFFERENCE
     * 
     * 
     * @return
     */
    private Query prepQuery() {
        final Query buildQ = new Query();
        qService = new QueryService();
        final List<Operand> operands = new ArrayList<>();
        final int numbOfCommands = 3;
        final int numbOfOperPerCommands = 2;
        final String queryId = "1";
        final long queryIdL = 1;

        final List<Command> commands = new ArrayList<>();

        final CommandAlgorithm ca = new CommandAlgorithm()
                .setResultOperandType(OperandType.GRAPH.getLabel())
                .setNumberOfOperands(numbOfOperPerCommands);

        final CommandType cTypeUnion = new CommandType().setTypeName("UNION")
                .setNumberOfOperands(numbOfOperPerCommands)
                .setOrderingType(OrderingType.ASSOCIATIVE_COMMUTATIVE);
        final CommandType cTypeInter = new CommandType()
                .setTypeName("INTERSECTION")
                .setNumberOfOperands(numbOfOperPerCommands)
                .setOrderingType(OrderingType.ASSOCIATIVE_COMMUTATIVE);
        final CommandType cTypeDiff = new CommandType()
                .setTypeName("DIFFERENCE")
                .setNumberOfOperands(numbOfOperPerCommands)
                .setOrderingType(OrderingType.NONE);

        // create COMMANDS
        for (int i = 0; i < numbOfCommands; i++) {
            commands.add(new Command().setId(i).setCommandSeqNumber(i)
                    .setCommandAlgo(ca));
        }

        // setting line number is important as we will test command population
        // method, which is based on those numbers.
        commands.get(0).setCommandTypeObj(cTypeUnion).setParseLineNumber(3)
                .setSubQueryNumber(1);
        commands.get(1).setCommandTypeObj(cTypeInter).setParseLineNumber(6)
                .setSubQueryNumber(2);
        commands.get(2).setCommandTypeObj(cTypeDiff).setParseLineNumber(7)
                .setSubQueryNumber(Command.DEFAULT_SUB_QUERY_NUMBER);

        // create OPERANDS
        for (int i = 0; i < numbOfCommands + 1; i++) {
            final boolean loadedStatus = true;
            operands.add(new Graph()
                    .setImplementationType(OperandType.GRAPH.getLabel())
                    .setLoadedPath("/test").setQueryId(queryId)
                    .setType(OperandState.LOADED.getLabel())
                    .setStatus(loadedStatus));
        }

        // setting line number is important as we will test command population
        // method, which is based on those numbers.
        operands.get(0).setParseLineNumber(1).setId(2).setSubQueryNumber(1);
        operands.get(1).setParseLineNumber(2).setId(3).setSubQueryNumber(1);
        operands.get(2).setParseLineNumber(4).setId(4).setSubQueryNumber(2);
        operands.get(3).setParseLineNumber(5).setId(1).setSubQueryNumber(2);

        buildQ.setCommands(new LinkedList<>(commands)).setQueryId(queryIdL);

        final ConcurrentHashMap<Long, Operand> operMap = new ConcurrentHashMap<>();
        final List<Long> operandsIds = new ArrayList<>();
        for (final Operand o : operands) {
            operMap.put((long) o.hashCode(), o);
            operandsIds.add((long) o.hashCode());
        }
        final ClientSession cS = new ClientSession("1234", null);
        buildQ.setClientSession(cS);
        buildQ.setOperands(operandsIds);
        System.out.println(operMap.keySet());
        buildQ.setOperMap(operMap);

        return buildQ;
    }

    @Test
    public void testCheckOperandsLoaded() {
        // TODO
    }

    @Test
    public void testUpdateIntermResults() {
        // TODO
    }

    @Test
    public void testComputeProgress() {
        // TODO
    }

    @Test
    public void testCopyOperand() {
        // TODO
    }

    @Test
    public void testSetQueryResult() {
        // TODO
    }

    @Test
    public void testCopyQuery() {
        // TODO
    }

}
