package edu.baylor.aql.entities.operands;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import edu.baylor.aql.entities.command.Command;
import edu.baylor.aql.entities.operands.Operand.OperandType;
import edu.baylor.aql.entities.operands.Operand.OperandState;

public class OperandTest {

    private static Operand computOper;
    private static Operand graphOper;
    private static Command command;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        computOper = new ComputedOperand();
        graphOper = new Graph();
        command = new Command();
        // init command
        command.setId(1);
        // init two operands
        computOper.setId(-1);
        computOper.setCommand(command);
        computOper.setImplementationType(OperandType.COMP_OPERAND
                .getLabel());
        computOper.setLoadedPath("/test");
        computOper.setStatus(false);
        computOper.setType(OperandState.ESTIMATED_RESULT.getLabel());

        graphOper.setId(1);
        graphOper.setCommand(command);
        graphOper.setImplementationType(OperandType.GRAPH.getLabel());
        graphOper.setLoadedPath("/test");
        graphOper.setStatus(true);
        graphOper.setType(OperandState.LOADED.getLabel());
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        computOper = null;
        graphOper = null;
        command = null;
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testHashCode() {
        assertTrue(computOper.hashCode() != graphOper.hashCode());
    }

    @Test
    public void testEqualsObject() {
        assertTrue(!computOper.equals(graphOper));
        assertTrue(!graphOper.equals(computOper));
    }

    @Test
    public void testClone() {
        final Operand clone2 = graphOper.clone();
        assertTrue(clone2.equals(graphOper));
        assertTrue(graphOper.equals(clone2));
        assertTrue(clone2.hashCode() == graphOper.hashCode());
    }

    @Test
    public void testCompareTo() {
        final int smaller = -1;
        final int bigger = 1;
        final int equal = 0;
        assertEquals(smaller, computOper.compareTo(graphOper));
        assertEquals(bigger, graphOper.compareTo(computOper));

        final Operand clone1 = computOper.clone();

        assertEquals(equal, clone1.compareTo(computOper));
    }

}
