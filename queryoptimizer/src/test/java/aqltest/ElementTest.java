package aqltest;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import edu.baylor.aql.entities.query.Query;

public class ElementTest {

    private Query testEl = null;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }

    /**
     * Execute before each test
     * 
     * @throws Exception
     */
    @Before
    public void setUp() throws Exception {
        testEl = new Query().setClientSessionId((long) 1);
    }

    /**
     * Execute after each test.
     * Clear state data.
     * 
     * @throws Exception
     */
    @After
    public void tearDown() throws Exception {
        testEl = null;
        assertEquals("The element expected to be null", null, testEl);
    }

    @Test
    public void testGet() {

    }

    /**
     * Test next null, next has the 2nd element in the linked list, next.next
     * has 3rd element in the liked list.
     */
    @Test
    public void testAppendLast() {
        final Query next1 = new Query().setClientSessionId((long) 2);
        final Query next2 = new Query().setClientSessionId((long) 3);
        assertEquals("The next element expected to be null", null,
                testEl.getNext());
        testEl.appendLast(next1);
        testEl.appendLast(next2);
        long epxected = 2;
        long actual = testEl.getNext().getClientSessionId();
        assertEquals("The next session id expected is " + epxected
                + " but got " + actual, epxected, actual);

        epxected = 3;
        actual = testEl.getNext().getNext().getClientSessionId();
        assertEquals("The next session id expected is " + epxected
                + " but got " + actual, epxected, actual);
    }

    @Test
    public void testAccept() {

    }

    @Test
    public void testInsert() {

    }

}
