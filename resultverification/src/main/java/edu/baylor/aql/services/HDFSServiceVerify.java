package edu.baylor.aql.services;

import java.io.File;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

import org.apache.log4j.Logger;

import edu.baylor.aql.services.executors.AbstractExecutorVerify;
import edu.baylor.aql.services.executors.BashExecutorVerify;
import edu.baylor.aql.util.UtilVerify;

/**
 * This class performs HDFS system related operations, such as checking if file
 * exists, writing file to HDFS, executing an HDFS command, etc.
 * 
 * @author Rovshen Nazarov. Baylor University. Aug 25, 2014.All Rights Reserved.
 * 
 */
public final class HDFSServiceVerify {

    private static Logger logger = Logger.getLogger(HDFSServiceVerify.class
            .getName());
    /**
     * Thread pool used to execute HDFS related services. The thread pool is
     * initialized externally and passed to this service for use.
     */
    private final ExecutorService execThreadPool;
    private final static String hdfsSpace = "_";

    /**
     * Constructor that takes initialized thread pool to use for HDFS services.
     * 
     * @param execThreadPool Thread pool passed by the creator of this object.
     */
    public HDFSServiceVerify(final ExecutorService execThreadPool) {
        this.execThreadPool = execThreadPool;
    }

    /**
     * Check if file exists on HDFS Hadoop MapReduce related file system This
     * method uses hadoop filesystem listing command using the file path
     * provided.
     * 
     * The HDFS command is executed using {@link BashExecutorVerify} and based on its
     * result the appropriate boolean value returned to the caller.
     * 
     * @param filePathHDFS String the name of the file/folder to check for on
     *        HDFS.
     * 
     * @return status true file exists, false file does not exist on HDFS.
     */
    public boolean checkIfFileExistsHDFS(final String filePathHDFS) {

        if (filePathHDFS == null || execThreadPool == null) {
            logger.error("The file to wait for or its path must be not NULL");
            return false;
        }
        int result = UtilVerify.FAILED_STATUS;
        final StringBuilder commandHDFS = new StringBuilder();
        final AbstractExecutorVerify execCommand;
        commandHDFS.append("hadoop fs -ls ").append(filePathHDFS);
        execCommand = new BashExecutorVerify(commandHDFS.toString());
        final Future<Object> resultFuture = execThreadPool.submit(execCommand);

        try {
            result = (int) resultFuture.get();// blocks here until receives
            // result
        } catch (InterruptedException | ExecutionException e) {
            logger.error("Could not get result of the Callable thread."
                    + e.getMessage());
        }
        if (result != 0) {
            logger.info("File does not exist " + commandHDFS + "\nResult "
                    + result);
            return false;
        }
        return true;
    }

    /**
     * Execute HDFS Command. This method executes HDFS command provided by the
     * caller using {@link BashExecutorVerify} and returns its result to the caller.
     * 
     * @param commandHDFS String HDFS command to execute. This command is
     *        expected to be executed on Linux environment.
     * @return status: 0 ok, -1 fail
     */
    public int executeHDFSCommand(final String commandHDFS) {

        int resultSuccess = UtilVerify.FAILED_STATUS;
        final AbstractExecutorVerify execCommandAfterWait = new BashExecutorVerify(
                commandHDFS.toString());
        final Future<Object> future = execThreadPool
                .submit(execCommandAfterWait);

        try {
            resultSuccess = (int) future.get();// get blocks here until receives
            // the result.
        } catch (InterruptedException | ExecutionException e) {
            logger.error("Could not get result of the Callable thread."
                    + e.getMessage());
        }
        return resultSuccess;
    }

    /**
     * Creates a file on HDFS using Linux like touch command. This method uses
     * {@link HDFSServiceVerify#executeHDFSCommand(String)} method of this
     * service to execute Hadoop touch command that creates a file if it does
     * not exists and is similar to Linux touch command.
     * 
     * @param fileName String the name of the file to create on HDFS.
     */
    public void createFileOnHDFS(final String fileName) {
        final String commandHDFS = "hadoop fs -touchz " + fileName;
        final int resultSuccess = executeHDFSCommand(commandHDFS);
        if (resultSuccess != 0) {
            logger.error("Could not create " + fileName + " file on hdfs.");
        }
    }

    /**
     * Create query folder on HDFS. To create query path hdfs load path is used,
     * which represents the default load path defined in system wide AQL
     * settings and a sub folder for the current client session represented by
     * the query id.
     * 
     * @param commandHDFS {@link StringBuilder} object used to construct HDFS
     *        command.
     * @param hdfsLoadPath This {@link String} object represents the path, where
     *        the directory should be created on HDFS and this path represents
     *        default root path concatenated with query id.
     * @return int result of the executed HDFS command. Default value is the
     *         FAILED_STATUS
     */
    public int createDirOnHDFS(final String hdfsLoadPath) {

        int resultSuccess = UtilVerify.FAILED_STATUS;
        final String commandHDFS = "hadoop fs -mkdir " + hdfsLoadPath;
        resultSuccess = executeHDFSCommand(commandHDFS);

        return resultSuccess;
    }

    /**
     * Copy file or directory to HDFS.
     * 
     * @param commandHDFS {@link StringBuilder} object used to construct HDFS
     *        command.
     * @param hdfsPath This {@link String} object represents the path, where the
     *        directory should be created on HDFS.
     * @return int result of the executed HDFS command. Default value is the
     *         FAILED_STATUS
     */
    public int copyToHDFS(final String localPath, final String hdfsPath) {

        /**
         * HDFS requires underscore instead of space.
         */
        final String initPathHDFSSpace = localPath.replaceAll(" ", hdfsSpace);
        int resultSuccess = UtilVerify.FAILED_STATUS;
        final String commandHDFS = "hadoop fs -copyFromLocal "
                + initPathHDFSSpace + " " + hdfsPath;
        resultSuccess = executeHDFSCommand(commandHDFS);

        return resultSuccess;
    }

    /**
     * Copy file from HDFS. This method copies a file from the HDFS path.
     * 
     * @param hdfsPath This {@link String} object represents the path, where the
     *        the file is on HDFS.
     * @param localPath
     * 
     * @return int result of the executed HDFS command. Default value is the
     *         FAILED_STATUS
     */
    public int copyFromHDFS(final String hdfsPath, final String localPath) {

        /**
         * HDFS requires underscore instead of space.
         */
        final String localPathHDFSSpace = localPath.replaceAll(" ", hdfsSpace);
        int resultSuccess = UtilVerify.FAILED_STATUS;
        final StringBuilder commandHDFS = new StringBuilder(
                "hadoop fs -copyToLocal ").append(hdfsPath).append(" ")
                .append(localPathHDFSSpace);

        logger.info(commandHDFS);
        resultSuccess = executeHDFSCommand(commandHDFS.toString());

        return resultSuccess;
    }

    /**
     * Check file was copied to HDFS. This method verifies if the graph being
     * loaded was copied properly from the original file system to the HDFS. The
     * verification uses graph name to check if the graph was copied, if the
     * graph is a directory we use directory name as the graph name.
     * 
     * @param commandHDFS {@link StringBuilder} object used to construct HDFS
     *        command.
     * @param hdfsPath This {@link String} object represents the path, where
     *        the directory should be created on HDFS and this path represents
     *        default root path concatenated with query id.
     * @return 0 or FAILED_STATUS, which is the value other than 0. 0 represents
     *         success.
     */
    public long verifyFileExistsOnHDFS(final File file,
            final String hdfsPath) {
        // build Operand load path that we will use later.
        final String commandHDFS = hdfsPath
                + UtilVerify.FOLDER_SEPARATOR_LINUX + file.getName();
        final boolean fileExists = checkIfFileExistsHDFS(commandHDFS);

        if (!fileExists) {
            logger.error("Could not load graph to hfds "
                    + commandHDFS.toString());
            return UtilVerify.FAILED_STATUS;
        } else {
            return UtilVerify.SUCCESS_STATUS;
        }
    }
}
