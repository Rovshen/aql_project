package edu.baylor.aql.services.executors;

import java.io.File;
import java.util.concurrent.Callable;

import org.apache.log4j.Logger;
import org.omg.SendingContext.RunTime;

import edu.baylor.aql.util.UtilVerify;

/**
 * This service is responsible for executing bash commands.
 * It captures error and input stream of the command to be executed and
 * evaluated command exit value.
 * 
 * This service uses Callable interface, which is a recent extension to the
 * Runnable interface to support result return from the service thread.
 * 
 * @author Rovshen Nazarov. Baylor University. Aug 25, 2014.All Rights Reserved.
 * 
 */
public final class BashExecutorVerify extends AbstractExecutorVerify {


    private static Logger logger = Logger.getLogger(BashExecutorVerify.class
            .getName());
    /**
     * Single bash command that this bash executor will execute.
     */
    private final String commandToExecute;

    /**
     * Constructor.
     * This constructor creates a BashExecutor object with a given
     * command to execute.
     * 
     * @param commandToExec
     *            {@link Command} to execute by this bash executor.
     */
    public BashExecutorVerify(final String commandToExec) {
        this.commandToExecute = commandToExec;
    }

    /**
     * This method executes given bash command and returns result status of the
     * execution.
     * During execution input and error streams of the executed bash command are
     * captured by {@link StreamProcessorVerify} and printed to the standard output.
     * This method uses {@link RunTime} to process bash command and it uses
     * {@link Process} to get
     * error and input streams.
     * 
     * @param commandToExecute
     *            Command to execute.
     * @return Return status, 0 success, 1 failed.
     */
    @Override
    public int execute() {

        if (commandToExecute == null) {
            return UtilVerify.FAILED_STATUS;
        }
        try {
            final Runtime runT = Runtime.getRuntime();
            logger.info("\nExecuting command " + commandToExecute + "\n");
            final String[] commandElements = commandToExecute
                    .split(UtilVerify.DEFAULT_ELEMENT_SEP);
            final Process processExec = runT.exec(commandElements, null,
                    new File(UtilVerify.WOKRING_DIR));
            final StreamProcessorVerify errStream = new StreamProcessorVerify(
                    processExec.getErrorStream());
            final StreamProcessorVerify inpStream = new StreamProcessorVerify(
                    processExec.getInputStream());
            final Thread errT = new Thread(errStream);
            final Thread inT = new Thread(inpStream);
            errT.start();
            inT.start();
            final int exitVal = processExec.waitFor();
            logger.info("Command exit value: " + exitVal);

            if (exitVal == 0) {
                return UtilVerify.SUCCESS_STATUS;
            }
        } catch (final Throwable e) {
            logger.error("\n\nFailed to execute command " + commandToExecute
                    + "\nError msg: " + e.getMessage());
        }
        return UtilVerify.FAILED_STATUS;

    }

    /**
     * This method is required by the {@link Callable} object.
     * This method runs {@link BashExecutor#execute(String)} and returns the
     * result of executing that method.
     */
    @Override
    public Object call() throws Exception {
        return execute();
    }

}
