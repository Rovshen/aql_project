/**
 * 
 */
package edu.baylor.aql.resultverification;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

/**
 * @author Rovshen Nazarov. Baylor University. Mar 18, 2015.All Rights Reserved.
 * 
 */
public class BootstrapResultVerify {

    static Logger logger = Logger.getLogger(BootstrapResultVerify.class.getName());

    /**
     * 
     */
    public BootstrapResultVerify() {

    }

    /**
     * @param args
     */
    public static void main(final String[] args) {

        final Map<String, List<String>> options = new HashMap<>();

        System.out.println(Arrays.toString(args));

        if (args.length > 0) {
            getOptions(args, options);
        } else {
            System.out
            .println("You can verify files by bytes with options: "
                    + "<original=fileName> (<compareTo=fileName>)+");
        }

        final ResultVerify resVer = new ResultVerify();
        resVer.getSettings().setOptions(options);        
        resVer.verify();
    }

    /**
     * Get and store command line options.
     * 
     * @param args
     * @param options
     */
    private static void getOptions(final String[] args,
            final Map<String, List<String>> options) {

        final String ATT_VAL_SEP = "=";
        // we expect name value pair for AQL Server settings
        for (final String setting : args) {
            final String[] attVal = setting.split(ATT_VAL_SEP);
            if (attVal.length < 2) {
                throw new IllegalArgumentException("Expected: option=value, may be you forgot option= before your file path!");                
            } else {
                final String key = attVal[0];
                final String value = attVal[1];
                if(!options.containsKey(key)){
                    options.put(key, new ArrayList<String>());
                }
                options.get(key).add(value);
            }
        }     
    }

}
