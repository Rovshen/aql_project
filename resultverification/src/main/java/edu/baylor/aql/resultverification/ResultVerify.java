package edu.baylor.aql.resultverification;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import edu.baylor.aql.services.HDFSServiceVerify;
import edu.baylor.aql.util.UtilVerify;

public class ResultVerify {

    private static Logger logger = Logger.getLogger(ResultVerify.class
            .getName());
    private static String localTempFolderPath = "/tmp/aqlVerifyTmp/";

    /**
     * Placeholder class for the settings.
     * 
     * @author Rovshen Nazarov. Baylor University. Mar 18, 2015.All Rights
     *         Reserved.
     * 
     */
    public static class Settings {

        private String originalFileHDFSPath;
        private List<String> compareToFilesHDFSPaths;

        /**
         * @return the originalFileHDFSPath
         */
        public final String getOriginalFileHDFSPath() {
            return originalFileHDFSPath;
        }

        /**
         * @return the compareToFilesHDFSPaths
         */
        public final List<String> getCompareToFilesHDFSPaths() {
            return compareToFilesHDFSPaths;
        }

        /**
         * @param options the options to set
         */
        public final void setOptions(final Map<String, List<String>> options) {

            if (options.size() > 0) {
                // set options
                originalFileHDFSPath = options.get("original").get(0);
                compareToFilesHDFSPaths = options.get("compareTo");

            } else {
                // set defaults
            }
            logger.info("settings original: " + originalFileHDFSPath);
            logger.info("settings compare to files: "
                    + Arrays.toString(compareToFilesHDFSPaths.toArray()));

        }
    }

    /**
     * Verification settings.
     */
    private final Settings settings = new Settings();
    /**
     * Reuse threads from the pool.
     */
    private final ExecutorService execThreadPool;
    /**
     * Number of threads for the executors pool.
     */
    private final int nThreads = 10;
    private final AtomicInteger copiedFileUniqueCount = new AtomicInteger(0);

    public ResultVerify() {
        DOMConfigurator.configure("./config/log4j-config.xml");
        execThreadPool = Executors.newFixedThreadPool(nThreads);
    }

    public Settings getSettings() {
        return settings;
    }

    /**
     * Get the files from HDFS to the local system. Compare the files.
     */
    public void verify() throws IllegalArgumentException {
        logger.info("Verifying files...");

        /**
         * Those steps expected to be done outside of this code in a shell
         * script. hadoop fs -copyToLocal hadoop fs -ls
         * hdfs://localhost:9000/test/load/1/g1, returns 0 on exit, which means
         * file exist, test when file does not exist and exit is 0
         */
        try {
            final File localTmpFolder = createLocalTmpFolder();
            final HDFSServiceVerify hdfsService = new HDFSServiceVerify(
                    execThreadPool);

            if (!hdfsService.checkIfFileExistsHDFS(getSettings()
                    .getOriginalFileHDFSPath())) {
                return; // exit on error
            }
            final File localFileOrig = copyFileToLocalStore(getSettings()
                    .getOriginalFileHDFSPath(), localTmpFolder, hdfsService);

            for (final String compareToFileHDFSPath : getSettings()
                    .getCompareToFilesHDFSPaths()) {

                if (!hdfsService.checkIfFileExistsHDFS(compareToFileHDFSPath)) {
                    return; // exit on error
                }
                final File compareToFile = copyFileToLocalStore(
                        compareToFileHDFSPath, localTmpFolder, hdfsService);

                final boolean equal = compareFiles(localFileOrig, compareToFile);
                displayComparisonMessage(equal, localFileOrig, compareToFile);
                // DELETE the file
                compareToFile.delete(); // empty dir
                compareToFile.getParentFile().delete(); // delete dir

            }
            // DELETE the file
            localFileOrig.delete();
            localFileOrig.getParentFile().delete();

        } catch (final Exception e) {
            logger.error(e.getStackTrace());
        } finally {
            final UtilVerify util = new UtilVerify();
            // STOP the pool
            util.shutdownAndAwaitTermination(execThreadPool);
        }
    }

    /**
     * Copy a file from HDFS system to the local store in the local temporary
     * folder.
     * 
     * @param fileHDFSPath
     * @param localTmpFolder
     * @param hdfsService
     * @return copied file's {@link File} descriptor
     */
    private File copyFileToLocalStore(final String fileHDFSPath,
            final File localTmpFolder, final HDFSServiceVerify hdfsService)
                    throws IllegalArgumentException {

        File localFile = null;
        final StringBuilder localPath = new StringBuilder(
                localTmpFolder.getAbsolutePath());
        localPath.append(UtilVerify.FOLDER_SEPARATOR_LINUX)
        .append(copiedFileUniqueCount.getAndIncrement())
        .append(UtilVerify.FOLDER_SEPARATOR_LINUX);

        final Path path = Paths.get(localPath.toString());
        path.toFile().mkdirs();

        // copy the file from HDFS
        hdfsService.copyFromHDFS(fileHDFSPath, localPath.toString());
        // get the file name, which is the last element
        final String[] parts = fileHDFSPath
                .split(UtilVerify.FOLDER_SEPARATOR_LINUX);
        String fileName = "";
        fileName = parts[parts.length - 1];
        if ("".equals(fileName)) {
            throw new IllegalArgumentException(
                    "Expected a name of the file on HDFS, but was "
                            + fileHDFSPath);
        } else {
            localFile = new File(localPath.toString() + fileName);
            if (localFile.exists()) {
                return localFile;
            } else {
                logger.error("Could not find file "
                        + localFile.getAbsolutePath());
            }
        }
        return localFile;
    }

    private File createLocalTmpFolder() {

        final File localTmpFolder = new File(localTempFolderPath);

        final File parent = localTmpFolder.getParentFile();

        if (parent != null) { // create directory if does not exist
            parent.mkdirs();
        }
        if (!localTmpFolder.exists()) { // create the folder if does not exits
            localTmpFolder.mkdir();
        }
        logger.info("The folder copy to from HDFS is "
                + localTmpFolder.getAbsoluteFile());
        return localTmpFolder;
    }

    /**
     * Display comparison results to the user.
     * 
     * @param equal
     * @param originalFile
     * @param compareToFile
     * @throws IOException
     */
    private void displayComparisonMessage(final boolean equal,
            final File originalFile, final File compareToFile) throws IOException {
        if (!equal) {
            logger.info("\n\n\nThe original file " + originalFile.getCanonicalPath().toString()
                    + " is DIFFERENT than the file we compare to "
                    + compareToFile.getCanonicalPath().toString() + "\n\n");
        } else {
            logger.info("\n\n\nThe original file " + originalFile.getCanonicalPath().toString()
                    + " is the SAME as the file we compare to "
                    + compareToFile.getCanonicalPath().toString() + "\n\n");
        }
    }

    /**
     * Compare two files using {@link FileUtils#contentEquals(File, File)}.
     * @param originalFile
     * @param compareToFile
     * @return
     */
    private boolean compareFiles(final File originalFile,
            final File compareToFile) {
        try {
            return FileUtils.contentEquals(originalFile, compareToFile);
        } catch (final IOException e) {
            logger.info("Problem comparing the files.");
            e.printStackTrace();
            return false;
        }

    }

    /**
     * Check if the {@link File} exists on the local system.
     * 
     * @param file
     * @return
     */
    @SuppressWarnings("unused")
    private boolean checkFileExists(final File file) {
        if (!file.exists() || file.isDirectory()) {
            logger.error("Expected a file with the absolute path: "
                    + file.getAbsolutePath());
            return false;
        }
        return true;
    }
}
